// 2D boundary layer problem
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>
#include <Timer.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Rotation
{
    double k(10.);
    double signed_Ro(1.);

    double spin(const double& t)
    {
        return 1. - std::exp(-k*t*t);
    }
}

struct Time_Format
{
    std::size_t days, hours, minutes, seconds;
    Time_Format(const std::size_t& secs)
    {
        seconds = secs;
        minutes = seconds/60;
        hours = minutes/60;
        days = hours/24;

        seconds %= 60;
        minutes %= 60;
        hours %= 24;
    }
};

namespace Mesh_R
{
    double power(3.);

    double map(const double& s)
    {
        return std::pow(s + 0.1, power) - std::pow(0.1, power);
    }

    double inv_map(const double& x)
    {
        return std::pow(x + std::pow(0.1, power), 1./power) - 0.1;
    }

    double map_ds(const double& s)
    {
        return power*std::pow(s + 0.1, power - 1.);
    }

    double map_ds2(const double& s)
    {
        return power*(power - 1.)*std::pow(s + 0.1, power - 2.);
    }
}

namespace Mesh_Theta
{
    double max_theta(2.*std::atan(1.));

    double map(const double& s)
    {
        return (-(s - 1.1)*(s - 1.1) + 1.1*1.1)*max_theta/1.2;
    }

    double inv_map(const double& x)
    {
        return std::sqrt(-(x*1.2/max_theta - 1.1*1.1)) + 1.1;
    }

    double map_ds(const double& s)
    {
        return -2.*max_theta/1.2*(s - 1.1);
    }

    double map_ds2(const double& s)
    {
        return -2.*max_theta/1.2;
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
    console_out.use_ncurses();

    double max_r(30.), max_theta(2.*std::atan(1.));
    double dt(0.01), max_t(5.), dt2(0.002), change_over_time(2.3);
    char path[100], filename[100];
    std::size_t num_r(201), num_theta(401);
    bool force_output(false);
    Timer estimator("Run time");

    args.option("--change_over", change_over_time);
    args.option("--dt", dt);
    args.option("--dt2", dt2);
    args.flag("-f", force_output);
    args.option("--force_output", force_output);
    args.option("--k", Rotation::k);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--power_r", Mesh_R::power);
//    args.option("--power_theta", Mesh_Theta::power);
    args.option("--Ro", Rotation::signed_Ro);

    console_out.set_title(
            "Test rotating frame BL simulation for spin down with Ro = "
            + str_convert(Rotation::signed_Ro));
    console_out.begin_staging_header();
    console_out.stage_header_line("Performing set-up");
    console_out.end_staging_header();

//    std::sprintf(path, "dat/1602-1609/Ro%.3f_%u_%.1f_%.0f_%u_%.1f_map_new",
//            Rotation::signed_Ro, unsigned(num_r), Mesh_R::power, max_r,
//            unsigned(num_theta), Mesh_Theta::power);
    args.option("--path", path);

    // Check if folder already exists, forcing flag, etc
    {
        struct stat st;
        char command[200];

        if (stat(path, &st) == 0 && S_ISDIR(st.st_mode))
        {
            // Folder exists, check force flag and clear folder or just exit
            if (force_output)
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " set --- cleaning folder\n");
                sprintf(command, "rm %s/*", path);
                system(command);
                sleep(3);
            }
            else
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " not set --- aborting\n");
                sleep(3);
                return 1;
            }
        }
        else
        {
            // Create folder
            sprintf(command, "mkdir %s", path);
            system(command);
        }
    }

    File_Output files(path);
    std::size_t pre_steps(change_over_time/dt + 0.5);
    std::size_t num_steps(pre_steps + ((max_t - change_over_time)/dt2 + 0.5));
    std::size_t num_vars(3);

    Mesh_1D<double> r_mesh, theta_mesh;
    r_mesh.mapped_grid(0., max_r, num_r, num_vars,
            &Mesh_R::map, &Mesh_R::inv_map,
            &Mesh_R::map_ds, &Mesh_R::map_ds2);
    theta_mesh.mapped_grid(0., max_theta, num_theta, num_vars,
            &Mesh_Theta::map, &Mesh_Theta::inv_map,
            &Mesh_Theta::map_ds, &Mesh_Theta::map_ds2);
    Mesh_1D<double> equator_mesh(r_mesh), pole_mesh(r_mesh);
    Mesh_2D<double> mesh(r_mesh, theta_mesh);

    // Leave zero initial conditions due to rotating frame
    // Solvers for pole and equator
    Rotating_Edge_Streamfunction pole_problem(pole_mesh);
    Rotating_Edge_Streamfunction equator_problem(equator_mesh);
    Rotating_Layer_Streamfunction bl_problem(pole_mesh, equator_mesh, mesh);

    pole_problem.set_edge("pole");
    equator_problem.set_edge("equator");

    pole_problem.set_signed_rossby_number(Rotation::signed_Ro);
    equator_problem.set_signed_rossby_number(Rotation::signed_Ro);
    bl_problem.set_signed_rossby_number(Rotation::signed_Ro);

    pole_problem.set_sphere_spin(&Rotation::spin);
    equator_problem.set_sphere_spin(&Rotation::spin);
    bl_problem.set_sphere_spin(&Rotation::spin);

    std::sprintf(filename, "tracker.dat");
    files.open("surface", filename);
    files.open("mesh", "mesh.dat");

    for (std::size_t j(0); j < num_theta; ++j)
    {
        for (std::size_t i(0); i < num_r; ++i)
        {
            files("mesh") << mesh.unmapped_node_y(j) << ' '
                << mesh.unmapped_node_x(i) << std::endl;
        }
        files("mesh") << std::endl;
    }
    files.close("mesh");

    estimator.start();

    try
    {
        double counter(0.5);
        for (std::size_t step(0); step < num_steps; ++step)
        {
            std::size_t total_time_estimate(
                    estimator.seconds()/pole_problem.time()*max_t);
            Time_Format eta(total_time_estimate - estimator.seconds());
            Time_Format curr(estimator.seconds());
            std::string curr_string(
                    str_convert(curr.days) + ":"
                    + str_convert(curr.hours) + ":"
                    + str_convert(curr.minutes) + ":"
                    + str_convert(curr.seconds));
            std::string eta_string(
                    str_convert(eta.days) + ":"
                    + str_convert(eta.hours) + ":"
                    + str_convert(eta.minutes) + ":"
                    + str_convert(eta.seconds));

            console_out.begin_staging_header();
            args.print_recognised_arguments();
            console_out.stage_header_line("Time step: "
                    + str_convert(step + 1) + "\n");
            console_out.stage_header_line("Current time (pre-solve): "
                    + str_convert(pole_problem.time()) + "\n");
            console_out.stage_header_line("Running for "
                    + curr_string + ", ETA: " + eta_string);
            console_out.end_staging_header();

            double dt_actual(step < pre_steps ? dt : dt2);
            pole_problem.step(dt_actual);
            equator_problem.step(dt_actual);
            bl_problem.step(dt_actual);

            // Output every time step
            double inv_2h(0.5/((pole_mesh.node(1) - pole_mesh.node(0))
                        *Mesh_R::map_ds(pole_mesh.node(0))));
            files("surface") << pole_problem.time();
            for (std::size_t i(0); i < num_theta; ++i)
            {
                files("surface") << ' '
                    << -inv_2h*(
                            - 3.*mesh(0, i, fr)
                            + 4.*mesh(1, i, fr)
                            - mesh(2, i, fr)
                            );
            }
            files("surface") << std::endl;

            // Open output files
//            if (!(bl_problem.time() < counter))
            {
//                counter += 0.5;
                std::sprintf(filename, "unsteady_all_%.3f.dat",
                        bl_problem.time());
                files.open("full", filename);

                // Collect and output data
                for (std::size_t j(0); j < num_theta - 1; ++j)
                {
                    double t1(mesh.unmapped_node_y(j));
                    double t2(mesh.unmapped_node_y(j + 1));
                    double dtheta(t2 - t1);
                    double mid_t(0.5*(t1 + t2));
                    double s(std::sin(mid_t));
                    double c(std::cos(mid_t));
                    double c1(std::cos(t1));
                    double c2(std::cos(t2));
                    for (std::size_t k(0); k < num_r; ++k)
                    {
                        files("full") << mid_t << ' ';
                        files("full") << mesh.unmapped_node_x(k) << ' ';
                        files("full") << -0.5*(mesh(k, j, fr)
                                + mesh(k, j + 1, fr)) << ' ';
                        files("full") << 0.5*(mesh(k, j, v)
                                + mesh(k, j + 1, v)) << ' ';
                        files("full") << 0.5*((3.*c1*c1 - 1.)*mesh(k, j, f)
                                + (3.*c2*c2 - 1.)*mesh(k, j + 1, f))
                            + s*c*(mesh(k, j + 1, f)
                                    - mesh(k, j, f))/dtheta << std::endl;

                    }
                    files("full") << std::endl;
                }

                // Clean up
                files.close("full");
            }
        }
    }
    catch (std::exception& e)
    {
        console_out("Something terrible has happened.\n");
        getch();
    }
    getch();
    console_out.stop_ncurses();
    files.close_all();

    return 0;
}
