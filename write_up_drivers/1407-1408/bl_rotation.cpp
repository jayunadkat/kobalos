// 2D boundary layer problem
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Rotation
{
    double k(10.);
    double signed_Ro(-0.5);

    double spin(const double& t)
    {
        return 1. - std::exp(-k*t*t);
    }
}

namespace Mesh_Power2
{
    double map(const double& s)
    {
        return std::pow(s + 0.1, 2.) - 0.01;
    }

    double inv_map(const double& x)
    {
        return std::sqrt(x + 0.01) - 0.1;
    }

    double map_ds(const double& s)
    {
        return 2.*(s + 0.1);
    }

    double map_ds2(const double& s)
    {
        return 2.;
    }
}

namespace Mesh_Power3
{
    double power(2.);

    double map(const double& s)
    {
        return std::pow(s + 0.1, power) - std::pow(0.1, power);
    }

    double inv_map(const double& x)
    {
        return std::pow(x + std::pow(0.1, power), 1./power) - 0.1;
    }

    double map_ds(const double& s)
    {
        return power*std::pow(s + 0.1, power - 1.);
    }

    double map_ds2(const double& s)
    {
        return power*(power - 1.)*std::pow(s + 0.1, power - 2.);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
    console_out.use_ncurses();

    double max_r(30.), max_theta(2.*std::atan(1.));
    double dt(0.01), max_t(5.);
    char path[100];
    std::size_t num_r(201), num_theta(401);
    bool force_output(false);

    args.option("--dt", dt);
    args.flag("-f", force_output);
    args.option("--force_output", force_output);
    args.option("--k", Rotation::k);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--power", Mesh_Power3::power);
    args.option("--Ro", Rotation::signed_Ro);

    console_out.set_title(
            "Test rotating frame BL simulation for sphere with Ro = "
            + str_convert(Rotation::signed_Ro));
    console_out.begin_staging_header();
    console_out.stage_header_line("Performing set-up");
    console_out.end_staging_header();

    std::sprintf(path, "dat/1407-1408/Ro%.3f_%u_%u_%.1f_%.0f",
            Rotation::signed_Ro, unsigned(num_r), unsigned(num_theta),
            Mesh_Power3::power, max_r);
    args.option("--path", path);

    // Check if folder already exists, forcing flag, etc
    {
        struct stat st;
        char command[200];

        if (stat(path, &st) == 0 && S_ISDIR(st.st_mode))
        {
            // Folder exists, check force flag and clear folder or just exit
            if (force_output)
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " set --- cleaning folder\n");
                sprintf(command, "rm %s/*", path);
                system(command);
                sleep(3);
            }
            else
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " not set --- aborting\n");
                sleep(3);
                return 1;
            }
        }
        else
        {
            // Create folder
            sprintf(command, "mkdir %s", path);
            system(command);
        }
    }

    File_Output files(path);
    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(3);

    Mesh_1D<double> r_mesh, theta_mesh;
    r_mesh.mapped_grid(0., max_r, num_r, num_vars,
            &Mesh_Power2::map, &Mesh_Power2::inv_map,
            &Mesh_Power2::map_ds, &Mesh_Power2::map_ds2);
    theta_mesh.mapped_grid(0., max_theta, num_theta, num_vars,
            &Mesh_Power3::map, &Mesh_Power3::inv_map,
            &Mesh_Power3::map_ds, &Mesh_Power3::map_ds2);
    Mesh_1D<double> equator_mesh(r_mesh), pole_mesh(r_mesh);
    Mesh_2D<double> mesh(r_mesh, theta_mesh);

    // Leave zero initial conditions due to rotating frame
    // Solvers for pole and equator
    Rotating_Edge_Streamfunction pole_problem(pole_mesh);
    Rotating_Edge_Streamfunction equator_problem(equator_mesh);
    Rotating_Layer_Streamfunction bl_problem(pole_mesh, equator_mesh, mesh);

    pole_problem.set_edge("pole");
    pole_problem.set_signed_rossby_number(Rotation::signed_Ro);
    pole_problem.set_sphere_spin(&Rotation::spin);
    equator_problem.set_edge("equator");
    equator_problem.set_signed_rossby_number(Rotation::signed_Ro);
    equator_problem.set_sphere_spin(&Rotation::spin);
    bl_problem.set_signed_rossby_number(Rotation::signed_Ro);
    bl_problem.set_sphere_spin(&Rotation::spin);

    files.open("out", "boundary.dat");

    try
    {
        double inv_2h(0.5/((pole_mesh.node(1) - pole_mesh.node(0))
                    *Mesh_Power2::map_ds(pole_mesh.node(0))));
        for (std::size_t step(0); step < num_steps; ++step)
        {
            console_out.begin_staging_header();
            args.print_recognised_arguments();
            console_out.stage_header_line("Time step: "
                    + str_convert(step + 1) + "\n");
            console_out.stage_header_line("Current time (pre-solve): "
                    + str_convert(pole_problem.time()));
            console_out.end_staging_header();

            pole_problem.step(dt);
            equator_problem.step(dt);
            bl_problem.step(dt);

            files("out") << bl_problem.time() << ' '
                << -inv_2h*(
                        - 3.*mesh(0, 0, fr)
                        + 4.*mesh(1, 0, fr)
                        - mesh(2, 0, fr)
                        ) << ' '
                << -inv_2h*(
                        - 3.*mesh(0, num_theta - 1, fr)
                        + 4.*mesh(1, num_theta - 1, fr)
                        - mesh(2, num_theta - 1, fr)
                        ) << std::endl;
        }
    }
    catch (std::exception& e)
    {
        files("out").flush();
    }

    files.close_all();
    
    return 0;
}
