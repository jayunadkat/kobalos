// For Ro \in [3.05, 7.2]

#include <cmath>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <stdexcept>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <File_Output.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

double spin(const double& t)
{
    return 1. - std::exp(-10.*t*t);
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double Ro(0.);
    double max_r(50.), dt(0.025), max_t(100.);
    std::size_t num_r(1001), num_vars(3);
    char path[128], filename[128];
    std::sprintf(path, "final_dat/kobalos/1407-1408");

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--dt", dt);
    args.option("--max_t", max_t);

    std::sprintf(filename, "Ro_search_failure_max_t_%.0f.dat", max_t);
    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");
    File_Output files(path);
    files.open("tracker", filename);

    Mesh_1D<double> mesh_p, mesh_e;
    mesh_p.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_e.uniform_mesh(0., max_r, num_r, num_vars);

    for (Ro = 1.5 /*3.95*//*3.05*/; Ro < 8.01 /*4.05*//*7.21*/; Ro += 0.01)
    {
        mesh_p.clear();
        mesh_e.clear();
        Rotating_Edge_Streamfunction pole_problem(mesh_p);
        Rotating_Edge_Streamfunction equator_problem(mesh_e);

        pole_problem.set_edge("pole");
        pole_problem.set_signed_rossby_number(-Ro);
        pole_problem.set_sphere_spin(&spin);
        equator_problem.set_edge("equator");
        equator_problem.set_signed_rossby_number(-Ro);
        equator_problem.set_sphere_spin(&spin);

//        std::sprintf(filename, "pole_%.2f.dat", Ro);
//        files.open("pole", filename);
//        std::sprintf(filename, "equator_%.2f.dat", Ro);
//        files.open("equator", filename);
        
        std::cout << "Ro = " << pole_problem.signed_rossby_number()
            << std::endl;
        files("tracker") << Ro << ' ';

        try
        {
            do
            {
                pole_problem.step(dt);
//                for (std::size_t i(0); i < num_r; ++i)
//                    files("pole") << pole_problem.time() << ' '
//                        << mesh_p.node(i) << ' '
//                        << mesh_p(i, f) << std::endl;
//                files("pole") << std::endl << std::endl;

                //if (std::abs(mesh_p(num_r - 1, f)) > 200.)
                //    throw std::runtime_error("Streamfunction too large");
            } while (pole_problem.time() < max_t);
        }
        catch (std::exception& e)
        {
        }
        if (pole_problem.time() > max_t)
            files("tracker") << "? ";
        else
            files("tracker") << pole_problem.time() << ' ';
        files("tracker") << std::abs(mesh_p(num_r - 1, f)) << ' ';
        std::cout << "    pole done" << std::endl;

        try
        {
            do
            {
                equator_problem.step(dt);
//                for (std::size_t i(0); i < num_r; ++i)
//                    files("equator") << pole_problem.time() << ' '
//                        << mesh_e.node(i) << ' '
//                        << mesh_e(i, f) << std::endl;
//                files("equator") << std::endl << std::endl;

                //if (std::abs(mesh_e(num_r - 1, f)) > 200.)
                //    throw std::runtime_error("Streamfunction too large");
            } while (equator_problem.time() < max_t);
        }
        catch (std::exception& e)
        {
        }
        if (equator_problem.time() > max_t)
            files("tracker") << "? ";
        else
            files("tracker") << equator_problem.time() << ' ';
        files("tracker") << std::abs(mesh_e(num_r - 1, f)) << std::endl;
        std::cout << "    equator done" << std::endl;

        files("tracker") << std::endl;
//        files.close("pole");
//        files.close("equator");
    }

    return 0;
}

