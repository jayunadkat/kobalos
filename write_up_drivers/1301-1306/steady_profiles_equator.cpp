// Solves steady problem for \(S=1.5\) on \(r\in[0,200]\) with
// \(\Delta r=0.05\). 

#include <cstring>
#include <fstream>
#include <iomanip>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(1.5);
    double max_r(200.);
    std::size_t num_r(4001), num_vars(3);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "dat/1301-1306/");
    std::ofstream data;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--S", S);

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");
    std::sprintf(full_path, path);
    std::sprintf(filename, "steady_profiles_equator.dat");
    std::strcat(full_path, filename);
    data.open(full_path);

    Mesh_1D<double> mesh;
    mesh.uniform_mesh(0., max_r, num_r, num_vars);

    Edge_Streamfunction equator_problem(mesh);
    equator_problem.set_edge("equator");
    equator_problem.spin_up_parameter() = S;

    for (std::size_t i(0); i < num_r; ++i)
        mesh(i, v) = S;

    equator_problem.solve_steady();
    equator_problem.dump_flow_variables(data, 10, 1);

    data.close();

    return 0;
}

