// Solves steady problem at equator for supplied \(S\) range, dumps value of
// \(w_\infty\) against \(S\).

#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double dS(0.01), dS_temp(0.), S_begin(1.), S_end(2.);
    double max_r(100.);
    std::size_t num_r(10001), num_vars(3);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "final_dat/kobalos/1301-1306");
    std::ofstream data;

    args.option("--dS", dS);
    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--S_begin", S_begin);
    args.option("--S_end", S_end);

    std::sprintf(filename, "steady_w_inf_equator_%.0f_%u.dat",
            max_r, unsigned(num_r));
    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");
    std::sprintf(full_path, path);
    std::strcat(full_path, filename);
    data.open(full_path);
    data << "# S    w_inf\n";

    Mesh_1D<double> mesh;
    mesh.uniform_mesh(0., max_r, num_r, num_vars);

    Edge_Streamfunction equator_problem(mesh);
    equator_problem.set_edge("equator");

    for (double S(S_begin),
            end_diff(std::abs(S_end - S_begin) + std::abs(dS)/2.);
            std::abs(S - S_begin) <= end_diff; S += dS_temp)
    {
#ifndef QUIET
        std::cout << "Solving for S = " << S << '\n';
#endif  // QUIET

        equator_problem.spin_up_parameter() = S;

        for (std::size_t i(0); i < num_r; ++i)
        {
            double r(mesh.node(i));
            mesh(i, v) = S + (1. - S)*std::exp(-r);
        }

        equator_problem.solve_steady();

        data << S << ' ';
        data << -mesh(num_r - 1, f) << '\n';
        data.flush();

        if (S < 1.0095)
            dS_temp = 0.001;
        else
            dS_temp = dS;
    }
    data.close();

    return 0;
}

