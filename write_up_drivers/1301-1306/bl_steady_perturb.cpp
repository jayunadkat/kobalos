// Steady BL problem

#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>

#include <boundary_layer/Edge_Streamfunction.h>
#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Bump
{
    double height(0.05), omega(2.);

    double phi(const double& t)
    {
        return t > 0 ? std::exp(-1./(10.*t*t)) : 0.;
    }

    double bump(const double& t)
    {
        return 1. + height*phi(t)*std::sin(omega*t);
    }
}

void flow_3_variable_output(const double& time, const Mesh_2D<double>& mesh,
        std::ofstream& out)
{
    double mid_theta(0.), c(0.), s(0.);

    out << "# Mesh_2D\n";
    out << "# Solution at t = " << time << '\n';
    out << "# theta    r    u    v    w\n# =============================\n";
    for (std::size_t j(0), num_theta(mesh.num_nodes_y()); j < num_theta - 1;
            ++j)
    {
        mid_theta = 0.5*(mesh.node_y(j) + mesh.node_y(j + 1));
        for (std::size_t i(0), num_r(mesh.num_nodes_x()); i < num_r; ++i)
        {
            c = std::cos(mid_theta);
            s = std::sin(mid_theta);
            out << mid_theta << ' ';
            out << mesh.node_x(i) << ' ';
            out << -0.5*(mesh(i, j, fr) + mesh(i, j + 1, fr)) << ' ';
            out << 0.5*(mesh(i, j, v) + mesh(i, j + 1, v)) << ' ';
            out << (3.*c*c - 1.)*0.5*(mesh(i, j, f) + mesh(i, j + 1, f))
                + s*c*(mesh(i, j + 1, f) - mesh(i, j, f))/(
                        mesh.node_y(j + 1) - mesh.node_y(j)) << '\n';
        }
        out << '\n';
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(2.);
    double max_r(20.);
    double max_theta(2.*std::atan(1.));
    double dt(0.05), max_t(20.);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "dat/1301-1306/tmep/");
    std::ofstream data;
    std::size_t num_r(201), num_theta(201), num_vars(3);

    args.option("--bh", Bump::height);
    args.option("--bw", Bump::omega);
    args.option("--dt", dt);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--path", path);
    args.option("--S", S);

    std::size_t num_steps(max_t/dt);

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");

    // Set up output times
    std::vector<double> output_times({2., 4., 6., 8});
    std::vector<std::size_t> output_steps(output_times.size(), 0);
    for (std::size_t i(0), size(output_steps.size()); i < size; ++i)
        output_steps[i] = output_times[i]/dt - 1;

    Mesh_1D<double> mesh_r, mesh_theta;
    mesh_r.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta.uniform_mesh(0., max_theta, num_theta, num_vars);
    Mesh_1D<double> mesh_equator(mesh_r), mesh_pole(mesh_r);

    for (std::size_t i(0); i < num_r; ++i)
    {
        mesh_pole(i, v) = S;
        mesh_equator(i, v) = S;
    }

    // Solvers for pole and equator
    Edge_Streamfunction pole_problem(mesh_pole), equator_problem(mesh_equator);

    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = S;
    pole_problem.set_sphere_rotation(&Bump::bump);
    equator_problem.set_edge("equator");
    equator_problem.spin_up_parameter() = S;
    equator_problem.set_sphere_rotation(&Bump::bump);

#ifndef QUIET
    std::cout << "Solving at the pole\n";
#endif  // QUIET

    pole_problem.solve_steady();

#ifndef QUIET
    std::cout << "Solving at the equator\n";
#endif  // QUIET

    equator_problem.solve_steady();

    // Initial guess will be a linear interpolation from mesh_pole to
    // mesh_equator
    Mesh_2D<double> mesh(mesh_r, mesh_theta);
    for (std::size_t i(0); i < num_r; ++i)
        for (std::size_t j(0); j < num_theta; ++j)
            mesh.set_vars_at_node(i, j, (mesh_pole.get_vars_at_node(i)
                        + double(j)/double(num_theta - 1)*(
                            mesh_equator.get_vars_at_node(i)
                            - mesh_pole.get_vars_at_node(i))));

    Elliptic_Boundary_Layer full_problem(mesh_pole, mesh_equator, mesh);
    full_problem.spin_up_parameter() = S;
    full_problem.set_external_bump(&Bump::bump);

#ifndef QUIET
    std::cout << "Solving steady problem in boundary layer\n";
#endif  // QUIET

    full_problem.solve_steady();

    // Dump
    std::sprintf(filename, "steady_%.1f_%03u_%03u_%.2f.dat",
            max_r, unsigned(num_r), unsigned(num_theta), S);
    std::sprintf(full_path, path);
    std::strcat(full_path, filename);
    data.open(full_path);
    flow_3_variable_output(0., mesh, data);
    data.close();

    std::vector<double> probe(num_theta, 0.);
    for (std::size_t i(0); i < num_theta; ++i)
        probe[i] = -mesh(unsigned(5.*(num_r - 1)/max_r), i, fr);
    
    // Use steady solutions as initial data for time-stepping
    for (std::size_t step(0); step < num_steps; ++step)
    {
        pole_problem.step(dt);
        equator_problem.step(dt);
        full_problem.step(dt);
        //std::cout << '\n';

        //if (std::find(output_steps.begin(), output_steps.end(), step) !=
        //        output_steps.end())
        {
            std::sprintf(filename, "perturb_sol_%.2f.dat", full_problem.time());
            std::sprintf(full_path, path);
            std::strcat(full_path, filename);
            data.open(full_path);

            // Dump difference in u(r = 5, theta; t) from steady profile
            for (std::size_t i(0); i < num_theta; ++i)
            {
                data << mesh.node_y(i) << ' ';
                data << -mesh(unsigned(5.*(num_r - 1)/max_r), i, fr) - probe[i];
                data << std::endl;
            }

            // Clean up
            data.flush();
            data.close();
        }
    }
    
    return 0;
}
