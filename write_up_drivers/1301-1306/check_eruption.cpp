// Solves time-dependent problem at the equator for a supplied \(S\) on
// \(r\in[0,100]\) for \(t\in[0,300]\) with \(\Delta r=\Delta t=0.01\). Dumps
// \(w_\infty\) against \(t\).

#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(0.);
    bool profiles(false);
    std::vector<double> S_list({0., 0.25, 0.5, 0.75, 0.9, 0.99, 0.999});
    double max_r(100.);
    std::size_t num_r(1001), num_vars(3);
    double max_t(300.), dt(0.01);
    std::size_t num_steps(max_t/dt), current_step(0);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "final_dat/kobalos/1301-1306/");
    std::ofstream data, data2;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--profiles", profiles);

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");

    for (std::size_t i(0), size(S_list.size()); i < size; ++i)
    {
        S = S_list[i];
        current_step = 0;
        std::cout << "Solving case S = " << S << '\n';

        std::sprintf(full_path, path);
        std::sprintf(filename, "w_inf_eruption_S%.2f_%u.dat", S,
                unsigned(num_r));
        std::strcat(full_path, filename);
        data.open(full_path);
        data << "# t    w_inf\n";

        Mesh_1D<double> mesh;
        mesh.uniform_mesh(0., max_r, num_r, num_vars);

        Edge_Streamfunction equator_problem(mesh);
        equator_problem.set_edge("equator");
        equator_problem.spin_up_parameter() = S;

        for (std::size_t j(0); j < num_r; ++j)
            mesh(j, v) = S;

        try
        {
            for (; current_step < num_steps; ++current_step)
            {
                if (current_step % 100 == 0)
                    std::cout << "S = " << S
                        << ", t = " << equator_problem.time() << std::endl;
                equator_problem.step(dt);

                if (profiles)
                {
                    for (std::size_t j(0); j < num_r; ++j)
                        data << equator_problem.time() << ' '
                            << mesh.node(j) << ' '
                            << -mesh(j, f) << std::endl;
                    data << std::endl << std::endl;
                }
                else
                    data << equator_problem.time() << ' '
                        << -mesh(num_r - 1, f) << std::endl;
            }
        }
        catch (std::exception& e)
        {
            data << std::endl;
            data << "final step: " << current_step << std::endl;
            std::cout << "Failure time = " << equator_problem.time()
                << std::endl;
        }

        data.close();
    }

    return 0;
}

