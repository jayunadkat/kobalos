// Spin up from rest problem, parabolic marching from equator to pole using
// streamfunction formulation

#include <algorithm>
#include <cmath>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <vector>

#include <Command_Line_Args.h>
#include <boundary_layer/Boundary_Layer.h>
#include <PDE_Bundle.h>

using namespace Kobalos;

enum {f, fr, frr, v, vr};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET
    
    // Data output objects
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "dat/1602-1609/reverse_big");
    std::ofstream data;

    // Spatial/time domain parameters
    const double pi(4.*std::atan(1.));
    double left_r(0.), right_r(30.), left_theta(pi/2.), right_theta(0.);
    double end_time(100.5), dt(0.01);
    std::size_t num_nodes_r(301), num_nodes_theta(101);
    double S(2.);

    // Replace by command line parameters, if appropriate
    args.option("--dt", dt);
    args.option("--end_time", end_time);
    args.option("--num_r", num_nodes_r);
    args.option("--num_theta", num_nodes_theta);
    args.option("--path", path);
    args.option("--right_r", right_r);
    args.option("--S", S);

    std::size_t max_steps(end_time/dt);
    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");

    // Set up output times
    std::vector<double> output_times({5., 10., 20., 30., 40., 50., 60., 70., 80., 90., 100.});
    std::vector<std::size_t> output_steps(output_times.size(), 0);
    for (std::size_t i(0), size(output_steps.size()); i < size; ++i)
        output_steps[i] = output_times[i]/dt - 1;

    // Instantiate equation/boundary condition objects for polar and full
    // boundary layer problems. Set the spin-up parameter
    BoundaryLayer::Pole_Problem pole_eqn;
    BoundaryLayer::Equator_Problem equator_eqn;
    BoundaryLayer::BC_Left bc_left;
    BoundaryLayer::BC_Right bc_right;
    BoundaryLayer::Full_Problem full_bl_eqn;
    BoundaryLayer::S = S;
    BoundaryLayer::unsteady = true;

    // Set up meshes by creating 'templates' for the two coordinate directions
    // and passing these to constructors for the computational meshes. This is
    // not as memory-efficient as setting each up individually but it is more
    // readable
    Mesh_1D<double> mesh_r, mesh_theta;

    mesh_r.uniform_mesh(left_r, right_r, num_nodes_r, equator_eqn.order());
    mesh_theta.uniform_mesh(left_theta, right_theta, num_nodes_theta,
            equator_eqn.order());

    Mesh_1D<double> mesh_pole(mesh_r);
    Mesh_1D<double> mesh_equator(mesh_r);
    Mesh_2D<double> mesh_bl(mesh_r, mesh_theta);
    
    // Set up conditions objects, each one is tied to a particular mesh. Adding
    // conditions takes the form of providing a location and a residual to be
    // applied at that location. In the case of the Condition_2D::add_y method,
    // we provide a reference to an existing mesh from which the solver can
    // directly pull values
    Conditions_1D<double> conditions_pole(mesh_pole);
    Conditions_1D<double> conditions_equator(mesh_equator);
    Conditions_2D<double> conditions_bl(mesh_bl);

    conditions_pole.add(left_r, bc_left);
    conditions_pole.add(right_r, bc_right);
    conditions_equator.add(left_r, bc_left);
    conditions_equator.add(right_r, bc_right);
    conditions_bl.add_x(left_r, bc_left);
    conditions_bl.add_x(right_r, bc_right);
    conditions_bl.add_y(mesh_equator);
    conditions_bl.add_y(mesh_pole);

    // Set up initial data (nothing to do if we have zero conditions)
    for (std::size_t i(0); i < num_nodes_r; ++i)
    {
        mesh_pole(i, v) = BoundaryLayer::S;
        mesh_equator(i, v) = BoundaryLayer::S;
        for (std::size_t j(0); j < num_nodes_theta; ++j)
            mesh_bl(i, j, v) = BoundaryLayer::S;
    }

    // Create PDE solver objects
    PDE_IBVP<PDE_1D_Unsteady, double, double> pole_solver(
            pole_eqn, conditions_pole, mesh_pole);
    PDE_IBVP<PDE_1D_Unsteady, double, double> equator_solver(
            equator_eqn, conditions_equator, mesh_equator);
    PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double> bl_solver(
            full_bl_eqn, conditions_bl, mesh_bl);

    // Set reversed-flow parameters, we must supply the component of velocity
    // in the direction of parabolic coordinate. The 
    // PDE_IBVP::direction_inverted() method sets a flag which indicates that
    // a flow will be considered reversed if the flow velocities at a point are
    // positive.
    //
    // In this case, since the theta coordinate is inverted and our "normal"
    // flow is negative due to the streamfunction formulation, the net result
    // is that the flag must be set to false.
    bl_solver.set_streamwise_component(fr);
    bl_solver.direction_inverted() = false;
    bl_solver.first_order() = false;

    // Step forward in time and dump output
    try
    {
	    for (std::size_t i(0); i < max_steps; ++i)
	    {
		    pole_solver.step(dt);
		    equator_solver.step(dt);
		    bl_solver.step(dt);

            std::cout << "Solved t=" << bl_solver.time() << std::endl;

		    if (std::find(output_steps.begin(), output_steps.end(), i) !=
				    output_steps.end())
		    {
			    // Open output files
			    std::sprintf(filename, "unsteady_u_%u_%.2f.dat",
                        num_nodes_theta, bl_solver.time());
			    std::sprintf(full_path, path);
			    std::strcat(full_path, filename);
			    data.open(full_path);

			    // Collect and output data
			    for (std::size_t j(0); j < num_nodes_theta; ++j)
			    {
				    for (std::size_t k(0); k < num_nodes_r; ++k)
				    {
					    data << mesh_bl.node_y(j) << ' ';
					    data << mesh_bl.node_x(k) << ' ';
					    data << -mesh_bl(k, j, fr) << std::endl;
				    }
				    data << std::endl;
			    }

			    // Clean up
			    data.close();

			    // Open output files
			    std::sprintf(filename, "unsteady_all_%u_%.2f.dat",
                        num_nodes_theta, bl_solver.time());
			    std::sprintf(full_path, path);
			    std::strcat(full_path, filename);
			    data.open(full_path);

			    // Collect and output data
                double dtheta(left_theta/(num_nodes_theta - 1));
			    for (std::size_t j(0); j < num_nodes_theta - 1; ++j)
			    {
                    double t1(mesh_bl.node_y(j));
                    double t2(mesh_bl.node_y(j + 1));
                    double mid_t(0.5*(t1 + t2));
                    double s(std::sin(mid_t));
                    double c(std::cos(mid_t));
                    double c1(std::cos(t1));
                    double c2(std::cos(t2));
				    for (std::size_t k(0); k < num_nodes_r; ++k)
				    {
					    data << 0.5*(mesh_bl.node_y(j) + mesh_bl.node_y(j + 1))
                            << ' ';
					    data << mesh_bl.node_x(k) << ' ';
					    data << -0.5*(mesh_bl(k, j, fr) + mesh_bl(k, j + 1, fr))
                            << ' ';
                        data << 0.5*(mesh_bl(k, j, v) + mesh_bl(k, j + 1, v))
                            << ' ';
                        data << 0.5*((3.*c1*c1 - 1.)*mesh_bl(k, j, f)
                                + (3.*c2*c2 - 1.)*mesh_bl(k, j + 1, f))
                            + s*c*(mesh_bl(k, j, f)
                                    - mesh_bl(k, j + 1, f))/dtheta << std::endl;

				    }
				    data << std::endl;
			    }

			    // Clean up
			    data.close();
		    }
	    }
    }
    catch (std::exception& e)
    {
	    std::cout << "Failure time: " << bl_solver.time() << std::endl;
    }

    return 0;
}

