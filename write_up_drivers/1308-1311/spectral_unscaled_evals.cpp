// 2D boundary layer problem with forcing

#include <cmath>
#include <cstring>
#include <fstream>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction_Unscaled.h>
#include <boundary_layer/Rotating_Fourier_Decomposed_Layer_Unscaled.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Mesh
{
    double p(1.), off(0.1);

    double map(const double& s) {return std::pow(s+off, p);}
    double inv_map(const double& x) {return std::pow(x, 1./p)-off;}
    double map_ds(const double& s) {return p*std::pow(s+off, p-1.);}
    double map_ds2(const double& s) {return p*(p-1.)*std::pow(s+off, p-2.);}
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    // Parameters
    double signed_Ro(-0.5), Ro_iterate(-0.01), Ro, omega_spec(0.);
    double max_r(100.), max_theta(2.*std::atan(1.));
    char path[100], filename[100], full_path[200];
    std::sprintf(path, "./dat/1308-1311/fourier_unscaled3");
    std::size_t num_r(501), num_theta(101);
    std::size_t num_vars(3);
    bool skip_steady(false);

    // Grab command-line options
    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--omega", omega_spec);
    args.option("--path", path);
    args.option("--power", Mesh::p);
    args.option("--rossby", signed_Ro);
    args.option("--skip_steady", skip_steady);

    Ro = std::abs(signed_Ro);

#ifndef QUIET
    // Done with options/flag parsing
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    // File output object
    File_Output files(path);

    // Prepare meshes
    Mesh_1D<double> mesh_r, mesh_theta;
    Mesh_1D<std::complex<double> > mesh_r_complex, mesh_theta_complex;

    mesh_r.mapped_grid(0., max_r, num_r, num_vars,
            Mesh::map, Mesh::inv_map, Mesh::map_ds, Mesh::map_ds2);
    //    mesh_r.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta.uniform_mesh(0., max_theta, num_theta, num_vars);
    mesh_r_complex.mapped_grid(0., max_r, num_r, num_vars,
            Mesh::map, Mesh::inv_map, Mesh::map_ds, Mesh::map_ds2);
    //    mesh_r_complex.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta_complex.uniform_mesh(0., max_theta, num_theta, num_vars);

    Mesh_1D<double> mesh_pole_steady(mesh_r), mesh_equator_steady(mesh_r);
    Mesh_2D<double> mesh_bl_steady(mesh_r, mesh_theta), mesh_bl_steady_flow;
    Mesh_2D<std::complex<double> > mesh_bl(mesh_r_complex, mesh_theta_complex);

    std::sprintf(filename, "steady_Ro%.2f_%.0f_%u_%.2f_%u_flow.dat",
            signed_Ro, max_r, static_cast<unsigned>(num_r), Mesh::p,
            static_cast<unsigned>(num_theta));

    if (!skip_steady)
    {
        // Steady solvers for pole and equator
        Rotating_Edge_Streamfunction pole_steady(mesh_pole_steady);
        Rotating_Edge_Streamfunction equator_steady(mesh_equator_steady);
        Rotating_Layer_Streamfunction_Unscaled bl_steady(mesh_bl_steady);

        // Compute steady solution in BL
        pole_steady.set_edge("pole");
        equator_steady.set_edge("equator");

        for (; Ro_iterate > signed_Ro; Ro_iterate -= 0.01)
        {
            std::cout << "Solving attachment points for Ro = " << Ro_iterate
                << std::endl;
            pole_steady.set_signed_rossby_number(Ro_iterate);
            equator_steady.set_signed_rossby_number(Ro_iterate);

            // Compute steady solutions at pole and equator
            pole_steady.solve_steady();
            equator_steady.solve_steady();
        }

        pole_steady.set_signed_rossby_number(signed_Ro);
        equator_steady.set_signed_rossby_number(signed_Ro);
        bl_steady.set_signed_rossby_number(signed_Ro);

        // Compute steady solutions at pole and equator
        pole_steady.solve_steady();
        equator_steady.solve_steady();

        // Initial guess for BL
        for (std::size_t j(0); j < num_theta; ++j)
        {
            double pos(mesh_theta.unmapped_node(j)/max_theta);
            for (std::size_t i(0); i < num_r; ++i)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    mesh_bl_steady(i, j, k) = mesh_pole_steady(i, k) + pos*(
                            mesh_equator_steady(i, k) - mesh_pole_steady(i, k));
                }
            }
        }

        for (std::size_t j(0); j < num_theta; ++j)
        {
            double theta(mesh_theta.unmapped_node(j));
            double s(std::sin(theta)), c(std::cos(theta));
            for (std::size_t i(0); i < num_r; ++i)
            {
                mesh_bl_steady(i, j, f) *= s*s*c;
                mesh_bl_steady(i, j, fr) *= s*s*c;
                mesh_bl_steady(i, j, v) *= s;
            }
        }

        bl_steady.solve_steady();
        files.open("actual", filename);
        bl_steady.dump_flow_variables(files("actual"), 10);
        files.close("actual");
    }

    std::sprintf(full_path, "%s/%s", path, filename);
    mesh_bl_steady_flow.load_data(full_path);

    // Spectral solvers
    Rotating_Fourier_Decomposed_Layer_Unscaled bl_problem(
            mesh_bl_steady, mesh_bl);

    std::vector<double> omega;
    if (omega_spec > 1e-8)
        omega.push_back(omega_spec);
    else
        omega = {1.5*2., 1.8*2.};

    std::vector<std::vector<double> > transpiration(
            mesh_bl_steady_flow.num_nodes_y(), std::vector<double>(2, 0.));

    std::sprintf(filename, "transpiration_%.2f_%.0f_%.2f.dat", signed_Ro,
            max_r, Mesh::p);
    files.open("transpiration", filename);
    for (std::size_t j(0), num_theta(mesh_bl_steady_flow.num_nodes_y()),
            index(mesh_bl_steady_flow.num_nodes_x() - 1); j < num_theta; ++j)
    {
        transpiration[j][0] = mesh_bl_steady_flow.node_y(j);
        transpiration[j][1] = mesh_bl_steady_flow(index, j, 2);

        files("transpiration") << transpiration[j][0] << ' '
            << transpiration[j][1] << std::endl;
    }
    files("transpiration").close();

    for (std::size_t i(0), num_omega(omega.size()); i < num_omega; ++i)
    {
        // Solution parameters
        bl_problem.omega() = omega[i];
        bl_problem.set_signed_rossby_number(signed_Ro);

        // Compute spectral solutions
        bl_problem.solve();

        // Set up file output
        std::sprintf(filename, "fourier%.2f_%.2f_%.0f_%u_%.2f_%u.dat",
                signed_Ro, omega[i], max_r, static_cast<unsigned>(num_r),
                Mesh::p, static_cast<unsigned>(num_theta));
        files.open("out", filename);
        mesh_bl.dump_real(10, files("out"));
        files.close("out");

        std::sprintf(filename, "fourier%.2f_%.2f_%.0f_%u_%.2f_%u_flow.dat",
                signed_Ro, omega[i], max_r, static_cast<unsigned>(num_r),
                Mesh::p, static_cast<unsigned>(num_theta));
        files.open("out", filename);
        bl_problem.dump_flow_variables(files("out"), 10);
        files.close("out");

        std::sprintf(filename, "steady_rates_%.2f_%.2f_%.0f_%u_%.2f_%u.dat",
                signed_Ro, omega[i], max_r, static_cast<unsigned>(num_r),
                Mesh::p, static_cast<unsigned>(num_theta));
        files.open("steady", filename);

        std::sprintf(filename, "harmonic_rates_%.2f_%.2f_%.0f_%u_%.2f_%u.dat",
                signed_Ro, omega[i], max_r, static_cast<unsigned>(num_r),
                Mesh::p, static_cast<unsigned>(num_theta));
        files.open("harmonic", filename);

        for (std::size_t j(0), num_theta(mesh_bl_steady_flow.num_nodes_y());
                j < num_theta; ++j)
        {
            double c(std::cos(transpiration[j][0])), w(Ro*transpiration[j][1]),
                   r, r2;
            r = 0.25*std::sqrt(2.)*std::sqrt(
                    std::sqrt(w*w*w*w + 64.*c*c) + w*w);
            files("steady") << transpiration[j][0] << ' '
                << (0.5*w - r) << ' ' << (0.5*w + r) << std::endl;
            w *= 0.5;
            r2 = Ro*omega[i] + 2.*c;
            r = std::sqrt(w*w + std::sqrt(w*w*w*w + r2*r2))/std::sqrt(2.);
            files("harmonic") << transpiration[j][0] << ' '
                << w - r << ' ' << w + r << ' ';
            r2 = Ro*omega[i] - 2.*c;
            r = std::sqrt(w*w + std::sqrt(w*w*w*w + r2*r2))/std::sqrt(2.);
            files("harmonic") << w - r << ' ' << w + r << std::endl;
        }

        // Clean up
        files.close_all();
    }
    
    return 0;
}
