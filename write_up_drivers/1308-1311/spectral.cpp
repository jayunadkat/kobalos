// 2D boundary layer problem with forcing

#include <cmath>
#include <cstring>
#include <fstream>

#include <boundary_layer/Edge_Streamfunction.h>
#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <boundary_layer/Fourier_Decomposed_Edge.h>
#include <boundary_layer/Fourier_Decomposed_Layer.h>
#include <Command_Line_Args.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    // Parameters
    double S(2.), omega_mult(1.5);
    double max_r(100.);
    double max_theta(2.*std::atan(1.));
    char path[100], filename[100];
    std::sprintf(path, "dat/1308-1311");
    std::size_t num_r(501), num_theta(201);
    std::size_t num_vars(3);
    bool steady(false);

    // Grab command-line options
    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--omega_mult", omega_mult);
    args.option("--path", path);
    args.option("--S", S);
    args.option("--steady", steady);

#ifndef QUIET
    // Done with options/flag parsing
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET
    
    // File output object
    File_Output files(path);

    // Prepare meshes
    Mesh_1D<double> mesh_r, mesh_theta;
    Mesh_1D<std::complex<double> > mesh_r_complex, mesh_theta_complex;
    mesh_r.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta.uniform_mesh(0., max_theta, num_theta, num_vars);
    mesh_r_complex.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta_complex.uniform_mesh(0., max_theta, num_theta, num_vars);
    Mesh_1D<double> mesh_pole_steady(mesh_r), mesh_equator_steady(mesh_r);
    Mesh_1D<std::complex<double> > mesh_pole(mesh_r_complex),
        mesh_equator(mesh_r_complex);
    Mesh_2D<double> mesh_bl_steady(mesh_r, mesh_theta);
    Mesh_2D<std::complex<double> > mesh_bl(mesh_r_complex,
            mesh_theta_complex);

    // Initial guesses for pole and equator
    for (std::size_t i(0); i < num_r; ++i)
    {
        double r(mesh_pole_steady.node(i));
        mesh_pole_steady(i, v) = S + (1. - S)*std::exp(-r*r);
        mesh_equator_steady(i, v) = S + (1. - S)*std::exp(-r*r);
    }

    // Steady solvers for pole and equator
    Edge_Streamfunction pole_steady(mesh_pole_steady);
    Edge_Streamfunction equator_steady(mesh_equator_steady);
    Elliptic_Boundary_Layer bl_steady(mesh_pole_steady, mesh_equator_steady,
            mesh_bl_steady);

    // Solution parameters
    pole_steady.set_edge("pole");
    pole_steady.spin_up_parameter() = S;
    equator_steady.set_edge("equator");
    equator_steady.spin_up_parameter() = S;
    bl_steady.spin_up_parameter() = S;

    // Compute steady solutions at pole and equator
    pole_steady.solve_steady();
    equator_steady.solve_steady();

    if (steady)
    {
        // Initial guess for BL
        for (std::size_t i(0); i < num_r; ++i)
            for (std::size_t j(0); j < num_theta; ++j)
                mesh_bl_steady.set_vars_at_node(i, j, (
                            mesh_pole_steady.get_vars_at_node(i)
                            + double(j)/double(num_theta - 1)*(
                                mesh_equator_steady.get_vars_at_node(i)
                                - mesh_pole_steady.get_vars_at_node(i))));

        // Compute steady solution in BL
        bl_steady.solve_steady();
        files.open("steady", "steady_S2.00_r100_raw.dat");
        mesh_bl_steady.dump(10, files("steady"));
        files.close_all();
    }
    else
    {
        mesh_bl_steady.load_data("dat/1308-1311/steady_S2.00_r100_raw.dat");

        // Spectral solvers
        Fourier_Decomposed_Edge pole_problem(mesh_pole_steady, mesh_pole);
        Fourier_Decomposed_Edge equator_problem(
                mesh_equator_steady, mesh_equator);
        Fourier_Decomposed_Layer bl_problem(
                mesh_pole, mesh_equator, mesh_bl_steady, mesh_bl);

        // Solution parameters
        pole_problem.set_edge("pole");
        pole_problem.omega() = omega_mult*S;
        equator_problem.set_edge("equator");
        equator_problem.omega() = omega_mult*S;
        bl_problem.omega() = omega_mult*S;

        // Initial guesses for pole, equator and BL
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                mesh_pole(i, k) = mesh_pole_steady(i, k);
                mesh_equator(i, k) = mesh_equator_steady(i, k);
            }
            for (std::size_t j(0); j < num_theta; ++j)
                for (std::size_t k(0); k < num_vars; ++k)
                    mesh_bl(i, j, k) = mesh_bl_steady(i, j, k);
        }

        // Set up file output
        std::sprintf(filename, "fourier%.2f.dat", omega_mult);
        files.open("out", filename);
        std::sprintf(filename, "fourier%.2f.dat.what", omega_mult);
        files.open("out2", filename);
        std::sprintf(filename, "fourier%.2f_pole.dat", omega_mult);
        //files.open("p", filename);
        std::sprintf(filename, "fourier%.2f_equator.dat", omega_mult);
        //files.open("e", filename);

        // Compute spectral solutions
        pole_problem.solve();
        //pole_problem.dump_flow_variables(files("p"), 10);
        equator_problem.solve();
        //equator_problem.dump_flow_variables(files("e"), 10);
        bl_problem.solve();
        bl_problem.dump_flow_variables(files("out"), 10);

        // Output spectral data
        files("out2").precision(20);
        for (std::size_t i(0); i < num_theta; ++i)
        {
            double inv_r(double(num_r - 1)/max_r);
            for (std::size_t j(0); j < num_r - 1; ++j)
            {
                files("out2") << mesh_bl.node_y(i) << ' ' << mesh_bl.node_x(j) << ' '
                    << -0.5*(mesh_bl(j, i, fr) + mesh_bl(j + 1, i, fr)) << ' '
                    << inv_r*(mesh_bl(j, i, fr) - mesh_bl_steady(j + 1, i, fr))
                    << '\n';
            }
            files("out2") << std::endl;
        }

        /*std::size_t r_index(9*(num_r - 1)/10);
          double mid_theta(0.), c(0.), s(0.);
          std::complex<double> temp;
          for (std::size_t j(0); j < num_theta - 1; ++j)
          {
          mid_theta = 0.5*(mesh_bl.node_y(j) + mesh_bl.node_y(j + 1));
          c = std::cos(mid_theta);
          s = std::sin(mid_theta);
          files("out") << mid_theta << ' ';
          temp = -0.5*(mesh_bl(r_index, j, fr) + mesh_bl(r_index, j + 1, fr));
          files("out") << temp.real() << ' ' << temp.imag() << ' ';
          temp = 0.5*(mesh_bl(r_index, j, v) + mesh_bl(r_index, j + 1, v));
          files("out") << temp.real() << ' ' << temp.imag() << ' ';
          temp = (3.*c*c - 1.)*0.5*(mesh_bl(r_index, j, f) 
          + mesh_bl(r_index, j + 1, f)) + s*c*(mesh_bl(r_index, j + 1, f)
          - mesh_bl(r_index, j, f))/(
          mesh_bl.node_y(j + 1)- mesh_bl.node_y(j));
          files("out") << temp.real() << ' ' << temp.imag() << '\n';
          }*/

        // Clean up
        files.close_all();
    }
    
    return 0;
}
