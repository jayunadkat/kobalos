// Solves time-dependent problem for a supplied \(S\) on \(r\in[0,100]\) for 
// \(t\in[0,300]\) with \(\Delta r=\Delta t=0.01\). Dumps \(w_\infty\) against
// \(t\).

#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Rotation
{
    double S(0.), k(10.);

    double sphere_rotation(const double& t)
    {
        return 1. + (S - 1.)*std::exp(-k*t*t);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double max_r(50.);
    std::size_t num_r(1001), num_vars(3);
    double max_t(30.), dt(0.01);
    std::size_t num_steps(max_t/dt);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "final_dat/kobalos/1209-1212");
    std::ofstream data;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--k", Rotation::k);
    args.option("--S", Rotation::S);

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");

    std::cout << "Solving case S = " << Rotation::S << '\n';

    std::sprintf(full_path, path);
    std::sprintf(filename, "w_inf_S%.2f_k%.2f.dat",
            Rotation::S, Rotation::k);
    std::strcat(full_path, filename);
    data.open(full_path);
    data << "# t    w_inf\n";

    Mesh_1D<double> mesh;
    mesh.uniform_mesh(0., max_r, num_r, num_vars);

    Edge_Streamfunction pole_problem(mesh);
    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = Rotation::S;
    pole_problem.set_sphere_rotation(&Rotation::sphere_rotation);

    for (std::size_t i(0); i < num_r; ++i)
        mesh(i, v) = Rotation::S;

    for (std::size_t step(0); step < num_steps; ++step)
    {
        pole_problem.step(dt);
        std::cout << "t = " << pole_problem.time() << std::endl;

        data << pole_problem.time() << ' ';
        data << 2.*mesh(num_r - 1, f) << '\n';
        data.flush();
    }
    data.close();

    return 0;
}

