// Solves time-dependent problem for \(S=1.5\) on \(r\in[0,200]\) with
// \(\Delta r=0.05\). Time domain is \(t\in[0,210]\) with \(\Delta t=0.05\)
// on \(t\in[0,200]\) and \(\Delta t=0.002\) on \(t\in[200,210]\).

#include <cstring>
#include <fstream>
#include <iomanip>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(1.5);
    double max_r(200.);
    std::size_t num_r(4001), num_vars(3);
    double dt1(0.05), dt2(0.002), max_t1(200.), max_t2(7.);
    std::size_t num_steps1(max_t1/dt1), num_steps2(max_t2/dt2);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "final_dat/kobalos/");
    std::ofstream data;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--S", S);

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");
    std::sprintf(full_path, path);
    std::sprintf(filename, "wavepacket_profiles.dat");
    std::strcat(full_path, filename);
    data.open(full_path);

    Mesh_1D<double> mesh;
    mesh.uniform_mesh(0., max_r, num_r, num_vars);

    Edge_Streamfunction pole_problem(mesh);
    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = S;

    for (std::size_t i(0); i < num_r; ++i)
        mesh(i, v) = S;

    for (std::size_t step(0); step < num_steps1; ++step)
        pole_problem.step(dt1);
    pole_problem.dump_flow_variables(data, 10, 1);

    for (std::size_t step(0); step < num_steps2; ++step)
    {
        pole_problem.step(dt2);

        data << "# Solution at t = " << pole_problem.time() << '\n';
        data << "# r    u\n";
        for (std::size_t i(0); i < num_r; ++i)
        {
            data << mesh.node(i) << ' ';
            data << -mesh(i, fr) << '\n';
        }
        data << "\n\n";
    }
    data.close();

    return 0;
}

