// Solves time-dependent problem for \(S=1.5\) on \(r\in[0,400]\) for
// \(t\in[0,250]\), with \(\Delta r=\Delta t=0.05\). Dumps naive peak data for
// disturbance in \(u\), both spatial location against time and temporal maxima
// against time. Dumps scaled data for \(u\) for contour plot.

#include <cmath>
#include <cstring>
#include <fstream>
#include <vector>

#include <boundary_layer/Edge_Streamfunction.h>
#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(1.5);
    double max_r(400.);
    std::size_t num_r(8001), num_vars(3);
    double dt(0.05), max_t(250.);
    char path[100], filename[100], full_path[200];
    std::sprintf(path, "final_dat/kobalos/");
    std::ofstream data_u, data_max, data_peaks;

    args.option("--dt", dt);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--S", S);

    std::size_t num_steps(max_t/dt);
    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");
    std::sprintf(full_path, path);
    std::sprintf(filename, "wavepacket_contours_u_%.2f_%.0f_%.0f.dat",
            S, max_r, max_t);
    std::strcat(full_path, filename);
    data_u.open(full_path);
    std::sprintf(full_path, path);
    std::sprintf(filename, "wavepacket_contours_max_%.2f_%.0f_%.0f.dat",
            S, max_r, max_t);
    std::strcat(full_path, filename);
    data_max.open(full_path);
    std::sprintf(full_path, path);
    std::sprintf(filename, "wavepacket_contours_peaks_%.2f_%.0f_%.0f.dat",
            S, max_r, max_t);
    std::strcat(full_path, filename);
    data_peaks.open(full_path);

    Mesh_1D<double> mesh;
    mesh.uniform_mesh(0., max_r, num_r, num_vars);

    Edge_Streamfunction pole_problem(mesh);
    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = S;

    for (std::size_t i(0); i < num_r; ++i)
        mesh(i, v) = S;

    std::vector<std::vector<double> > peaks;
    std::vector<double> temp(2, 0.);
    double t(0.), sqrt_t(0.), inv_sqrt_t(0.);

    for (std::size_t step(0); step < num_steps; ++step)
    {
        pole_problem.step(dt);

        // Remember u = -fr
        t = pole_problem.time();
        if (t > 40.)
        {
            std::size_t start_node(20.*(num_r - 1.)/max_r);
            std::size_t max_n(start_node);
            double max(std::abs(mesh(max_n, fr))), max_r(mesh.node(max_n));

            for (std::size_t node(max_n + 1); node < num_r - 1; ++node)
            {
                if (std::abs(mesh(node, fr)) > max)
                {
                    max = std::abs(mesh(node, fr));
                    max_n = node;
                    max_r = mesh.node(max_n);
                }
            }

            sqrt_t = std::sqrt(t);
            inv_sqrt_t = 1./sqrt_t;

            temp[0] = t;
            temp[1] = -mesh(max_n, fr);
            peaks.push_back(temp);

            if (t < 100.)
            {
                for (std::size_t node(start_node); node < num_r - 1; node += 2)
                {
                    data_u << t << ' ';
                    data_u << (mesh.node(node) - max_r)*inv_sqrt_t << ' ';
                    data_u << -mesh(node, fr)*sqrt_t << '\n';
                }
                data_u << '\n';
            }
            data_max << t << ' ';
            data_max << max_r << '\n';
        }
    }

    //analyse peaks
    bool climbing(false);
    if (peaks[1][1] > peaks[0][1])
        climbing = true;
    double last(peaks[1][1]);

    for (std::size_t i(2), size(peaks.size()); i < size; ++i)
    {
        if (climbing)
        {
            if (peaks[i][1] < last)
            {
                //now falling, peak found so dump it
                data_peaks << peaks[i - 1][0] << ' ' << last << '\n';
                climbing = false;
            }
        }
        else
        {
            //falling
            if (peaks[i][1] > last)
            {
                //now climbing, flag it as such
                climbing = true;
            }
        }
        last = peaks[i][1];
    }

    data_u.close();
    data_max.close();
    data_peaks.close();

    return 0;
}

