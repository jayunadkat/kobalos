// Solves time-dependent problem for \(S=1.5\) on \(r\in[0,200]\) with
// \(\Delta r=0.05\). Time domain is \(t\in[0,250]\) with \(\Delta t=0.05\)
// on \(t\in[0,200]\) and \(\Delta t=0.05\) on \(t\in[200,250]\).
// Solves problem in scaled travelling wave coordinates, and optionally
// calculates difference between numerical and analytical normalised results.

#include <cstring>
#include <fstream>
#include <iomanip>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <File_Output.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(1.5);
    double max_r(200.);
    std::size_t num_r(4001), num_vars(3);
    double dt1(0.05), dt2(0.05), max_t1(200.), max_t2(50.);
    char path[100];
    std::sprintf(path, "final_dat/kobalos/");
    bool difference_mode(false);

    args.option("--difference", difference_mode);
    args.option("--dt1", dt1);
    args.option("--dt2", dt2);
    args.option("--max_r", max_r);
    args.option("--max_t1", max_t1);
    args.option("--max_t2", max_t2);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--S", S);

    std::size_t num_steps1(max_t1/dt1), num_steps2(max_t2/dt2);
    File_Output files(path);
    files.open("profiles", "wavepacket_comparison.dat");
    files.open("plot_times", "wavepacket_comparison_plot_times.dat");

    Mesh_1D<double> mesh, analytical;
    mesh.uniform_mesh(0., max_r, num_r, num_vars);
    if (difference_mode)
        analytical.load_data("dat/1209-1212/even_odd_modes.dat");

    Edge_Streamfunction pole_problem(mesh);
    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = S;

    for (std::size_t i(0); i < num_r; ++i)
        mesh(i, v) = S;

    for (std::size_t step(0); step < num_steps1; ++step)
        pole_problem.step(dt1);

    // For use in finding temporal maxima. Initial "maximum" is large to avoid
    // premature output.
    std::size_t prev_max_index(0);
    double prev_max(1e8);
    bool rising(false), plot(false);

    for (std::size_t step(0); step < num_steps2; ++step)
    {
        pole_problem.step(dt2);

        // Determine if local max in time and plot
        std::size_t max_index(20.*double(num_r - 1)/double(max_r) + 1);
        double max(-mesh(max_index, fr));

        for (std::size_t i(max_index + 1); i < num_r; ++i)
        {
            if (std::abs(mesh(i, fr)) > std::abs(max))
            {
                max = -mesh(i, fr);
                max_index = i;
            }
        }

        if (rising)
        {
            if (-mesh(max_index, fr) < prev_max)
            {
                plot = true;
                rising = false;
            }
        }
        else
            if (-mesh(max_index, fr) > prev_max)
                rising = true;

        if (plot)
        {
            files.write("plot_times") << pole_problem.time() - dt2 << '\n';

            double scaled_r(0.), profile_height(0.);
            files.write("profiles") << "# r    u\n";
            for (std::size_t i(20.*double(num_r - 1)/double(max_r) + 1);
                    i < num_r; ++i)
            {
                scaled_r = (mesh.node(i) - mesh.node(prev_max_index))/
                    std::sqrt(pole_problem.time());
                profile_height = -mesh(i, fr)/-mesh(prev_max_index, fr);
                if (difference_mode)
                {
                    if (scaled_r > -10. && scaled_r < 10.)
                        profile_height -= analytical.linear_interpolant(
                                scaled_r, 0);
                }
                files.write("profiles") << scaled_r << ' ';
                files.write("profiles") << profile_height << '\n';
            }
            files.write("profiles") << "\n\n";

            plot = false;
        }

        prev_max_index = max_index;
        prev_max = max;
    }
    files.close_all();

    return 0;
}

