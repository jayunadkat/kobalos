#ifndef LINEAR_SYSTEM_BASE_H
#define LINEAR_SYSTEM_BASE_H

namespace Kobalos
{
    class Linear_System_Base
    {
        protected:
        public:
            Linear_System_Base();
            virtual ~Linear_System_Base();

            virtual void solve() = 0;
            virtual std::size_t order() const = 0;
    };
}   // namespace

#endif  // LINEAR_SYSTEM_BASE_H

