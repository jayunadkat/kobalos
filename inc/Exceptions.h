#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <cstddef>
#include <exception>
#include <iostream>
#include <string>

namespace Kobalos
{
    namespace Exceptions
    {
        class Generic : public std::exception
        {
            protected:
                std::string message_;

            public:
                Generic(const std::string& type) throw();
                Generic(const std::string& type,
                        const std::string& info) throw();
                virtual ~Generic() throw();

                virtual const char* what() const throw();
        };

        class Range : public Generic
        {
            public:
                Range(const std::string& container_name,
                        const std::size_t container_size,
                        const std::size_t index);
                Range(const std::string& container_name,
                        const std::size_t container_size1,
                        const std::size_t index1,
                        const std::size_t container_size2,
                        const std::size_t index2);
        };

        class Invalid_Argument : public Generic
        {
            public:
                Invalid_Argument(const std::string& function_name,
                        const std::string& info);
        };

        class Mismatch : public Generic
        {
            public:
                Mismatch(const std::string& function_name,
                        const std::string& container_name);
                Mismatch(const std::string& function_name,
                        const std::string& container_name1,
                        const std::string& container_name2);
        };
    }   // namespace Exceptions
}   // namespace Kobalos

#endif  // EXCEPTION_H 

