#ifndef CONDITION_DATA_H
#define CONDITION_DATA_H

#include <Mesh_1D.h>
#include <Residual.h>

namespace Kobalos
{
    template <template <typename, typename> class condition_type_,
             typename type_, typename xtype_ = double>
    class Condition_Data
    {
        public:
            xtype_ coord_;
            condition_type_<type_, xtype_>* condition_;
            std::size_t node_;

            Condition_Data(const xtype_& location,
                    condition_type_<type_, xtype_>& condition);
            ~Condition_Data();

            static const bool sort_predicate(const Condition_Data& lhs,
                    const Condition_Data& rhs);
    };
}   // namespace

#endif  // CONDITION_DATA_H
