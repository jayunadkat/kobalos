#ifndef SPARSE_LINEAR_SYSTEM_H
#define SPARSE_LINEAR_SYSTEM_H

#include <Sparse_Matrix.h>
#include <Linear_System_Base.h>
#include <Vector.h>

namespace Kobalos
{
    template <typename type_>
    class Sparse_Linear_System : public Linear_System_Base
    {
        protected:
            Sparse_Matrix<type_>& A_;
            Vector<type_>& b_;
            
            void solve_superlu();
            void solve_native();
            void back_substitute();
            
        public:
            Sparse_Linear_System(Sparse_Matrix<type_>& A, Vector<type_>& b);
            ~Sparse_Linear_System();

            void solve();
            std::size_t order() const;
    };
}   // namespace

#endif  // SPARSE_LINEAR_SYSTEM_H

