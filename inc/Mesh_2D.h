#ifndef MESH_2D_H
#define MESH_2D_H

#include <string>

#include <Mesh_1D.h>
#include <Vector.h>

namespace Kobalos
{
    template <typename type_, typename xtype_ = double>
    class Mesh_2D
    {
        protected:
            Mesh_1D<type_, xtype_> mesh_x_, mesh_y_;
            Vector<type_> vars_;
            std::size_t num_vars_;
            static double snap_tolerance_;

        public:
            Mesh_2D();
            Mesh_2D(Mesh_1D<type_, xtype_> mesh_x,
                    Mesh_1D<type_, xtype_> mesh_y);
            ~Mesh_2D();

            const Vector<xtype_>& nodes_x() const;
            const xtype_& node_x(const std::size_t& node_index) const;
            const xtype_ unmapped_node_x(const std::size_t& node_index) const;
            const xtype_ map_ds_x(const xtype_& s) const;
            const xtype_ map_ds2_x(const xtype_& s) const;
            const Vector<xtype_>& nodes_y() const;
            const xtype_& node_y(const std::size_t& node_index) const;
            const xtype_ unmapped_node_y(const std::size_t& node_index) const;
            const xtype_ map_ds_y(const xtype_& s) const;
            const xtype_ map_ds2_y(const xtype_& s) const;

            type_& operator()(const std::size_t& node_x_index,
                    const std::size_t& node_y_index,
                    const std::size_t& var_index);
            const type_& operator()(const std::size_t& node_x_index,
                    const std::size_t& node_y_index,
                    const std::size_t& var_index) const;
            void set_vars_vector(const Vector<type_>& vars_data);
            const Vector<type_>& get_vars_vector() const;
            const Vector<type_> get_vars_at_node(
                    const std::size_t& node_x_index,
                    const std::size_t& node_y_index) const;
            void set_vars_at_node(const std::size_t& node_x_index,
                    const std::size_t& node_y_index,
                    const Vector<type_>& vars_data);


            const std::size_t& num_nodes_x() const;
            const std::size_t& num_nodes_y() const;
            const std::size_t& num_vars() const;

            void dump(std::ostream &out = std::cout,
                    const std::size_t& skip = 1) const;
            void dump(const std::size_t& decimal_places,
                    std::ostream& out = std::cout,
                    const std::size_t& skip = 1) const;
            void dump_real(const std::size_t& decimal_places,
                    std::ostream& out = std::cout) const;
            void dump_paraview(std::ostream &out = std::cout,
                    const std::size_t& skip = 1) const;
            void dump_paraview(const std::size_t& decimal_places,
                    std::ostream &out = std::cout,
                    const std::size_t& skip = 1) const;

            void load_data(const std::string& path);
            void load_data(const char* path);
            void load_data_reversed_axes(const char* path);

            const type_ linear_interpolant(const xtype_& x, const xtype_& y,
                    const std::size_t var) const;
            const type_ derivative_x_at_node(const std::size_t& node_index,
                    const std::size_t& var_index) const;
            const type_ derivative_y_at_node(const std::size_t& node_index,
                    const std::size_t& var_index) const;

            void uniform_mesh(const xtype_& x_begin, const xtype_& x_end,
                    const std::size_t& num_nodes_x, const xtype_& y_begin,
                    const xtype_& y_end, const std::size_t& num_nodes_y,
                    const std::size_t& num_vars);
            const bool query_x(const xtype_& x, std::size_t& index);
            const bool query_y(const xtype_& y, std::size_t& index);

            template <typename t_, typename xt_>
            friend class Conditions_2D;
    };

    template <typename type_, typename xtype_>
    double Mesh_2D<type_, xtype_>::snap_tolerance_ = 1e-8;
}   // namespace

#endif  // MESH_2D_H

