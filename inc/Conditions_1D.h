#ifndef CONDITIONS_1D_H
#define CONDITIONS_1D_H

#include <vector>

#include <Condition_Data.h>
#include <Mesh_1D.h>
#include <Residual.h>

namespace Kobalos
{
    template <typename type_, typename xtype_ = double>
    class Conditions_1D
    {
        protected:
            std::vector<Condition_Data<Residual, type_, xtype_> > conditions_;
            Mesh_1D<type_, xtype_>& mesh_;

        public:
            Conditions_1D(Mesh_1D<type_, xtype_>& mesh);
            ~Conditions_1D();

            void add(const xtype_& location,
                    Residual<type_, xtype_>& condition);
            const std::size_t num_residuals() const;
            const std::size_t num_conditions() const;
            const xtype_& location(const std::size_t& index) const;
            Residual<type_, xtype_>& condition(const std::size_t& index);
            const std::size_t& node(const std::size_t& index) const;
    };
}   // namespace

#endif  // CONDITIONS_1D_H

