#ifndef MATRIX_BASE_H
#define MATRIX_BASE_H

#include <cstddef>

#include <Vector.h>

namespace Kobalos
{
    template <typename type_>
    class Matrix_Base
    {
        protected:
        public:
            Matrix_Base();
            virtual ~Matrix_Base();

            virtual type_& operator()(const std::size_t& row,
                    const std::size_t& column) = 0;
            virtual const type_ operator()(const std::size_t& row,
                    const std::size_t& column) const = 0;
            virtual type_& set(const std::size_t& row,
                    const std::size_t& column) = 0;
            virtual const type_ get(const std::size_t& row,
                    const std::size_t& column) const = 0;

            virtual const std::size_t& num_rows() const = 0;
            virtual const std::size_t& num_columns() const = 0;
            virtual const std::size_t num_elements() const = 0;

            virtual const Vector<type_> operator*(
                    const Vector<type_>& x) const = 0;

            virtual void transpose() = 0;
            virtual const double one_norm() const = 0;
            virtual const double two_norm() const = 0;
            virtual const double inf_norm() const = 0;
    };
}   // namespace

#endif  // MATRIX_BASE_H

