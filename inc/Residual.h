#ifndef RESIDUAL_H
#define RESIDUAL_H

#include <cstddef>

#include <Dense_Matrix.h>
#include <Vector.h>

namespace Kobalos
{
    template <typename type_, typename xtype_ = double>
    class Residual
    {
        protected:
            Dense_Matrix<type_> jacobian_;
            Vector<type_> state_, residual_;
            Vector<xtype_> coords_;
            double t_;
            std::size_t order_, num_vars_;
            static type_ delta_;

            virtual void jacobian(const Vector<type_>& state);

        public:
            Residual(const std::size_t& order);
            Residual(const std::size_t& order, const std::size_t& num_vars);
            virtual ~Residual();

            virtual void update(const Vector<type_>& state);
            virtual void residual_function(const Vector<type_>& state);

            const Dense_Matrix<type_>& jacobian() const;
            const Vector<type_>& residual() const;

            xtype_& x1();
            const xtype_& x1() const;
            xtype_& x2();
            const xtype_& x2() const;
            xtype_& x3();
            const xtype_& x3() const;
            double& t();
            const double& t() const;

            const std::size_t& order() const;
            const std::size_t& num_vars() const;
            static type_& delta();
    };

    template <typename type_, typename xtype_>
    type_ Residual<type_, xtype_>::delta_ = 1e-8; 

    template <typename type_, typename xtype_>
    inline void Residual<type_, xtype_>::update(const Vector<type_>& state)
    {
#ifdef PARANOID
        if (state.size() != num_vars_)
            throw Exceptions::Mismatch("Residual::update", "Vector");
#endif  // PARANOID

        state_ = state;
        residual_function(state_);
        jacobian(state_);
    }

    template <typename type_, typename xtype_>
    inline void Residual<type_, xtype_>::residual_function(const Vector<type_>& state)
    {
        throw Exceptions::Generic("Residual",
                "Residual::residual_function must be overloaded");
    }

    template <typename type_, typename xtype_>
    inline const Dense_Matrix<type_>& Residual<type_, xtype_>::jacobian() const
    {
        return jacobian_;
    }

    template <typename type_, typename xtype_>
    inline const Vector<type_>& Residual<type_, xtype_>::residual() const
    {
        return residual_;
    }

    template <typename type_, typename xtype_>
    inline xtype_& Residual<type_, xtype_>::x1()
    {
        return coords_[0];
    }

    template <typename type_, typename xtype_>
    inline const xtype_& Residual<type_, xtype_>::x1() const
    {
        return coords_[0];
    }

    template <typename type_, typename xtype_>
    inline xtype_& Residual<type_, xtype_>::x2()
    {
        return coords_[1];
    }

    template <typename type_, typename xtype_>
    inline const xtype_& Residual<type_, xtype_>::x2() const
    {
        return coords_[1];
    }

    template <typename type_, typename xtype_>
    inline xtype_& Residual<type_, xtype_>::x3()
    {
        return coords_[2];
    }

    template <typename type_, typename xtype_>
    inline const xtype_& Residual<type_, xtype_>::x3() const
    {
        return coords_[2];
    }

    template <typename type_, typename xtype_>
    inline double& Residual<type_, xtype_>::t()
    {
        return t_;
    }

    template <typename type_, typename xtype_>
    inline const double& Residual<type_, xtype_>::t() const
    {
        return t_;
    }

    template <typename type_, typename xtype_>
    inline const std::size_t& Residual<type_, xtype_>::order() const
    {
        return order_;
    }

    template <typename type_, typename xtype_>
    inline const std::size_t& Residual<type_, xtype_>::num_vars() const
    {
        return num_vars_;
    }

    template <typename type_, typename xtype_>
    inline type_& Residual<type_, xtype_>::delta()
    {
        return delta_;
    }
}   // namespace

#endif  // RESIDUAL_H

