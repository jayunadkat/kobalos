#ifndef ODE_BVP_H
#define ODE_BVP_H

#include <Banded_Linear_System.h>
#include <Banded_Matrix.h>
#include <Conditions_1D.h>
#include <Mesh_1D.h>
#include <Residual.h>
#include <Timer.h>
#include <Vector.h>

namespace Kobalos
{
    template <typename type_, typename xtype_ = double>
    class ODE_BVP
    {
        protected:
            Residual<type_, xtype_>& equation_;
            Conditions_1D<type_, xtype_>& conditions_;
            Banded_Matrix<type_> A_;
            Vector<type_> b_;
            Banded_Linear_System<type_> linear_system_;
            Mesh_1D<type_, xtype_>& mesh_;
            static double tolerance_;
            static std::size_t max_iterations_;

            void assemble_matrix();

        public:
            ODE_BVP(Residual<type_, xtype_>& equation,
                    Conditions_1D<type_, xtype_>& conditions,
                    Mesh_1D<type_, xtype_>& mesh);
            ~ODE_BVP();

            void solve();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;
    };

    template <typename type_, typename xtype_>
    double ODE_BVP<type_, xtype_>::tolerance_ = 1e-8;

    template <typename type_, typename xtype_>
    std::size_t ODE_BVP<type_, xtype_>::max_iterations_ = 50;

    template <typename type_, typename xtype_>
    inline double& ODE_BVP<type_, xtype_>::tolerance()
    {
        return tolerance_;
    }

    template <typename type_, typename xtype_>
    inline const double& ODE_BVP<type_, xtype_>::tolerance() const
    {
        return tolerance_;
    }

    template <typename type_, typename xtype_>
    inline std::size_t& ODE_BVP<type_, xtype_>::max_iterations()
    {
        return max_iterations_;
    }

    template <typename type_, typename xtype_>
    inline const std::size_t& ODE_BVP<type_, xtype_>::max_iterations() const
    {
        return max_iterations_;
    }
}   // namespace

#endif  // ODE_BVP_H
