#ifndef GENERALISED_EIGENVALUE_SYSTEM_COMPLEX_H
#define GENERALISED_EIGENVALUE_SYSTEM_COMPLEX_H

#include <complex>
#include <vector>

#include <Dense_Matrix.h>
#include <Linear_System_Base.h>

namespace Kobalos
{
    class Matrix_Helper
    {
        protected:
            Vector<double> matrix_;
            int n_;

        public:
            Matrix_Helper(const std::size_t& n) : matrix_(2*n*n, 0.), n_(n) {}

            double& operator()(const std::size_t& row,
                    const std::size_t& col,
                    const std::size_t& component)
            {
#ifdef PARANOID
                if (component > 1)
                    throw Exceptions::Invalid_Argument(
                            "Matrix_Helper::operator()",
                            "component < 2 is required");
                if (row >= n_ || col >= n_)
                    throw Exceptions::Range("Matrix_Helper",
                            n_, row, n_, col);
#endif  // PARANOID

                return matrix_[2*(col*n_ + row) + component];
            }

            const double& operator()(const std::size_t& row,
                    const std::size_t& col,
                    const std::size_t& component) const
            {
#ifdef PARANOID
                if (component > 1)
                    throw Exceptions::Invalid_Argument(
                            "Matrix_Helper::operator()",
                            "component < 2 is required");
                if (row >= n_ || col >= n_)
                    throw Exceptions::Range("Matrix_Helper",
                            n_, row, n_, col);
#endif  // PARANOID

                return matrix_[2*(col*n_ + row) + component];
            }

            void fill_zero() { matrix_.fill(0.); }
            // void dump() const
            // {
            //     for (int i(0); i < n_; ++i)
            //     {
            //         for (int j(0); j < n_; ++j)
            //         {
            //             std::cout << "(" << operator()(i, j, 0) << ","
            //                 << operator()(i, j, 1) << ") ";
            //         }
            //         std::cout << std::endl;
            //     }
            // }

            int num_rows() const { return n_; }
            int num_columns() const { return n_; }

            int full_size() const { return 2*n_*n_; }
            int dimension() const { return 2*n_; }

            double* data_pointer() { return &matrix_[0]; }
    };

    class Generalised_Eigenvalue_System_Complex : public Linear_System_Base
    {
        protected:
            static const double tolerance_;

            class Eigen_Stuff
            {
                public:
                    std::complex<double> value;
                    Vector<double> vector_real, vector_imag;

                    Eigen_Stuff(const std::complex<double>& new_value,
                            const Vector<double>& new_vector_real,
                            const Vector<double>& new_vector_imag);
            };
            class Eigen_Stuff_Sort
            {
                public:
                    bool operator()(const Eigen_Stuff& left,
                            const Eigen_Stuff& right) const;
            };

            Matrix_Helper& A_;
            Matrix_Helper& B_;
            Matrix_Helper VR_;

            Vector<double> ALPHA_, BETA_;
            std::vector<Eigen_Stuff> eigen_stuff_;

            void solve_lapack();
            std::complex<double> eigenvalue_(const std::size_t& index) const;
            Vector<double> eigenvector_real_(const std::size_t& index) const;
            Vector<double> eigenvector_imag_(const std::size_t& index) const;

        public:
            Generalised_Eigenvalue_System_Complex(Matrix_Helper& A,
                    Matrix_Helper& B);
            ~Generalised_Eigenvalue_System_Complex();

            void solve();
            std::size_t order() const;
            std::size_t solution_size() const;

            const Vector<double>& alpha() const;
            const Vector<double>& beta() const;
            const std::complex<double>& eigenvalue(const std::size_t& index) const;
            const Matrix_Helper& vr() const;
            const Vector<double>& eigenvector_real(const std::size_t& index) const;
            const Vector<double>& eigenvector_imag(const std::size_t& index) const;
    };
}   // namespace

#endif  // GENERALISED_EIGENVALUE_SYSTEM_H

