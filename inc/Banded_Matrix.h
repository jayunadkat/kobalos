#ifndef BANDED_MATRIX_H
#define BANDED_MATRIX_H

#include <algorithm>
#include <cstddef>
#include <sstream>

#include <Exceptions.h>
#include <Matrix_Base.h>

namespace Kobalos
{
    template <typename type_>
    class Banded_Matrix : public Matrix_Base<type_>
    {
        protected:
            Vector<type_> matrix_;
            std::size_t num_rows_, num_cols_, num_bands_;
        
        public:
            Banded_Matrix();
            Banded_Matrix(const std::size_t& n, const std::size_t& bands,
                    const type_& value);
            Banded_Matrix(const Banded_Matrix<type_>& source);
            Banded_Matrix(type_* const source_pointer,
                    const std::size_t& n, const std::size_t& bands);
            ~Banded_Matrix();
            
            const Banded_Matrix<type_>& operator=(
                    const Banded_Matrix<type_>& source);
            type_& operator()(const std::size_t& row,
                    const std::size_t& column);
            const type_ operator()(const std::size_t& row,
                    const std::size_t& column) const;
            type_& set(const std::size_t& row,
                    const std::size_t& column);
            const type_ get(const std::size_t& row,
                    const std::size_t& column) const;

            const Vector<type_> operator*(const Vector<type_>& x) const;
            void operator*=(const type_& k);

            void assign(type_* const source_pointer, const std::size_t& n,
                    const std::size_t& bands);
            void assign(const std::size_t& n, const std::size_t& bands,
                    const type_& value);
            void resize(const std::size_t& n, const std::size_t& bands);
            void fill(const type_& value);
            void clear();
            void transpose();

            void swap_rows(const std::size_t& row1, const std::size_t& row2);
            const std::size_t column_max_index(const std::size_t& column) const;

            const std::size_t& num_rows() const;
            const std::size_t& num_columns() const;
            const std::size_t num_elements() const;
            const std::size_t& num_bands() const;

            const double one_norm() const;
            const double two_norm() const;
            const double inf_norm() const;

            type_* data_pointer();
            void dump() const;
            void dump(const std::size_t& decimal_places) const;
            void dump_dense() const;
            void dump_dense(const std::size_t& decimal_places) const;
   };

    template <typename type_> 
    inline type_& Banded_Matrix<type_>::operator()(const std::size_t& row,
            const std::size_t& column)
    {
#ifdef PARANOID
        if (row >= num_cols_ || column >= num_cols_)
        {
            throw Exceptions::Range("Banded_Matrix",
                    num_cols_, row, num_cols_, column);
        }
        if (!(column + num_bands_ >= row && column <= row + 2*num_bands_))
        {
            std::stringstream problem;
            problem << "Access outside bands of Banded_Matrix with n = ";
            problem << num_cols_ << " and bands = " << num_bands_ << '\n';
            problem << "Attempted to access element (" << row << ", ";
            problem << column << ")";
            throw Exceptions::Invalid_Argument("Banded_Matrix::operator()",
                    problem.str());
        }
#endif  // PARANOID

        return matrix_[num_bands_*(3*column + 2) + row];
    }

    template <typename type_> 
    inline const type_ Banded_Matrix<type_>::operator()(const std::size_t& row,
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (row >= num_cols_ || column >= num_cols_)
        {
            throw Exceptions::Range("Banded_Matrix",
                    num_cols_, row, num_cols_, column);
        }
        if (!(column + num_bands_ >= row && column <= row + 2*num_bands_))
        {
            std::stringstream problem;
            problem << "Access outside bands of Banded_Matrix with n = ";
            problem << num_cols_ << " and bands = " << num_bands_ << '\n';
            problem << "Attempted to access element (" << row << ", ";
            problem << column << ")";
            throw Exceptions::Invalid_Argument("Banded_Matrix::operator()",
                    problem.str());
        }
#endif  // PARANOID

        return matrix_[num_bands_*(3*column + 2) + row];
    }

    template <typename type_> 
    inline type_& Banded_Matrix<type_>::set(const std::size_t& row,
            const std::size_t& column)
    {
#ifdef PARANOID
        if (row >= num_cols_ || column >= num_cols_)
        {
            throw Exceptions::Range("Banded_Matrix",
                    num_cols_, row, num_cols_, column);
        }
        if (!(column + num_bands_ >= row && column <= row + 2*num_bands_))
        {
            std::stringstream problem;
            problem << "Access outside bands of Banded_Matrix with n = ";
            problem << num_cols_ << " and bands = " << num_bands_ << '\n';
            problem << "Attempted to access element (" << row << ", ";
            problem << column << ")";
            throw Exceptions::Invalid_Argument("Banded_Matrix::set()",
                    problem.str());
        }
#endif  // PARANOID

        return matrix_[num_bands_*(3*column + 2) + row];
    }

    template <typename type_> 
    inline const type_ Banded_Matrix<type_>::get(const std::size_t& row,
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (row >= num_cols_ || column >= num_cols_)
        {
            throw Exceptions::Range("Banded_Matrix",
                    num_cols_, row, num_cols_, column);
        }
#endif  // PARANOID

        if (!(column + num_bands_ >= row && column <= row + 2*num_bands_))
            return 0.;
        else
            return matrix_[num_bands_*(3*column + 2) + row];
    }

    template <typename type_> 
    inline void Banded_Matrix<type_>::swap_rows(const std::size_t& row1,
            const std::size_t& row2)
    {
#ifdef PARANOID
        if (row2 >= num_rows_)
            throw Exceptions::Range("Banded_Matrix, row access", num_rows_,
                    row2);
        if (row1 >= row2)
            throw Exceptions::Invalid_Argument("Banded_Matrix::swap_rows",
                    "row1 < row2 required");
        if (row2 > row1 + num_bands_)
            throw Exceptions::Invalid_Argument("Banded_Matrix::swap_rows",
                    "row2 <= row1 + num_bands() required");
#endif  // PARANOID

        for (std::size_t j(row1); j <= row1 + 2*num_bands_ && j < num_cols_;
                ++j)
        {
            std::swap(matrix_[num_bands_*(3*j + 2) + row1],
                    matrix_[num_bands_*(3*j + 2) + row2]);
        }
    }
            
    template <typename type_> 
    inline const std::size_t Banded_Matrix<type_>::column_max_index(
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (column >= num_cols_)
            throw Exceptions::Range("Banded_Matrix, column access",
                    num_cols_, column);
#endif  // PARANOID

        std::size_t base(column*num_rows_ + 2*num_bands_);
        std::size_t max_index(0);
        double temp = std::abs(matrix_[base]);
        for (std::size_t i = 1; i <= num_bands_; ++i)
            if (std::abs(matrix_[base + i]) > temp)
            {
                max_index = i;
                temp = std::abs(matrix_[base + max_index]);
            }

        return column + max_index;
    }
}   // namespace

#endif  // BANDED_MATRIX_H

