#ifndef MESH_1D_H
#define MESH_1D_H

#include <cstddef>
#include <ostream>

#include <Vector.h>

namespace Kobalos
{
    static const std::size_t Mesh_Left = 0;
    static const std::size_t Mesh_Right = 1;

    template <typename type_, typename xtype_ = double>
    class Mesh_1D
    {
        protected:
            static const double epsilon_;
            typedef xtype_ (*external_func)(const xtype_&);

            Vector<xtype_> nodes_;
            Vector<type_> vars_;
            std::size_t num_nodes_, num_vars_;
            static double snap_tolerance_;

            external_func grid_map_;
            external_func inv_grid_map_;
            external_func map_ds_;
            external_func map_ds2_;

        public:
            Mesh_1D();
            Mesh_1D(const Vector<xtype_>& nodes, const std::size_t& num_vars);
            Mesh_1D(const std::size_t& num_nodes, const std::size_t& num_vars);
            Mesh_1D(const Mesh_1D<type_, xtype_>& source);
            ~Mesh_1D();

            const Vector<xtype_>& nodes() const;
            const xtype_& node(const std::size_t& node_index) const;
            const xtype_ unmapped_node(const std::size_t& node_index) const;
            const xtype_ map(const double& s) const;
            const xtype_ inv_map(const double& x) const;
            const xtype_ map_ds(const xtype_& s) const;
            const xtype_ map_ds2(const xtype_& s) const;

            type_& operator()(const std::size_t& node_index,
                    const std::size_t& var_index);
            const type_& operator()(const std::size_t& node_index,
                    const std::size_t& var_index) const;
            void set_vars_vector(const Vector<type_>& vars_data);
            const Vector<type_>& get_vars_vector() const;
            const Vector<type_> get_vars_at_node(
                    const std::size_t& node_index) const;
            void set_vars_at_node(const std::size_t& node_index,
                    const Vector<type_>& vars_data);

            const std::size_t& num_nodes() const;
            const std::size_t& num_vars() const;

            void dump(std::ostream &out = std::cout,
                    const std::size_t& skip = 1) const;
            void dump(const std::size_t& decimal_places,
                    std::ostream& out = std::cout,
                    const std::size_t& skip = 1) const;

            void load_data(const std::string& path);
            void load_data(const char* path);

            void clear();

            const type_ linear_interpolant(const xtype_& x,
                    const std::size_t var) const;
            const type_ derivative_at_node(const std::size_t& node_index,
                    const std::size_t& var_index) const;

            void uniform_mesh(const xtype_& begin, const xtype_& end, // TODO
                    const std::size_t& num_nodes, const std::size_t& num_vars);
            void power_mesh(const xtype_& begin, const xtype_& end, // TODO
                    const std::size_t& num_nodes, const std::size_t& num_vars,
                    const double& power, const std::size_t& bunching_direction);
            void mapped_grid(const xtype_& begin, const xtype_& end,
                    const std::size_t& num_nodes, const std::size_t& num_vars,
                    external_func grid_map, external_func inv_grid_map,
                    external_func map_ds = NULL,
                    external_func map_ds2 = NULL); // TODO
            const bool query(const xtype_& x, std::size_t& index); // TODO
    };

    template <typename type_, typename xtype_>
    double Mesh_1D<type_, xtype_>::snap_tolerance_ = 1e-8;
}   // namespace

#endif  // MESH_1D_H

