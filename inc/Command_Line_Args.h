#ifndef COMMAND_LINE_ARGS_H
#define COMMAND_LINE_ARGS_H

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

namespace Kobalos
{
    class Command_Line_Args
    {
        protected:
            std::vector<std::string> flags_;
            std::vector<std::string> options_, option_values_;
            std::vector<bool> flags_recognised_, options_recognised_;

        public:
            Command_Line_Args(int argc, char** argv);
            ~Command_Line_Args();

            void flag(const std::string& flag, bool& storage);
            void option(const std::string& option, bool& storage);
            void option(const std::string& option, char* storage);
            void option(const std::string& option, double& storage);
            void option(const std::string& option, int& storage);
            void option(const std::string& option, long& storage);
            void option(const std::string& option, std::size_t& storage);
            void option(const std::string& option, std::string& storage);

            void print_recognised_arguments() const;
    };
}   // namespace

#endif  // COMMAND_LINE_ARGS_H

