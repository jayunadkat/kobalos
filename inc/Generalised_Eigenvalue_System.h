#ifndef GENERALISED_EIGENVALUE_SYSTEM_H
#define GENERALISED_EIGENVALUE_SYSTEM_H

#include <vector>

#include <Dense_Matrix.h>
#include <Linear_System_Base.h>

namespace Kobalos
{
    template <typename type_>
    class Generalised_Eigenvalue_System : public Linear_System_Base
    {
        protected:
            static const double tolerance_;

            class Eigen_Stuff
            {
                public:
                    bool complex_valued;
                    Vector<type_> value, vector_real, vector_imag;

                    Eigen_Stuff(const Vector<type_>& new_value,
                            const Vector<type_>& new_vector);
                    Eigen_Stuff(const Vector<type_>& new_value,
                            const Vector<type_>& new_vector_real,
                            const Vector<type_>& new_vector_imag);
            };
            class Eigen_Stuff_Sort
            {
                public:
                    bool operator()(const Eigen_Stuff& left,
                            const Eigen_Stuff& right) const;
            };

            Dense_Matrix<type_>& A_;
            Dense_Matrix<type_>& B_;
            Dense_Matrix<type_> VR_;
            Vector<type_> ALPHAR_, ALPHAI_, BETA_;
            std::vector<Eigen_Stuff> eigen_stuff_;

            void solve_lapack();
            const Vector<type_> eigenvalue_(const std::size_t& index) const;
            const Vector<type_> eigenvector_(const std::size_t& index) const;

        public:
            Generalised_Eigenvalue_System(
                    Dense_Matrix<type_>& A, Dense_Matrix<type_>& B);
            ~Generalised_Eigenvalue_System();

            void solve();
            std::size_t order() const;
            std::size_t solution_size() const;

            const Vector<type_>& alpha_r() const;
            const Vector<type_>& alpha_i() const;
            const Vector<type_>& beta() const;
            const bool& is_complex(const std::size_t& index) const;
            const Vector<type_>& eigenvalue(const std::size_t& index) const;
            const Dense_Matrix<type_>& vr() const;
            const Vector<type_>& eigenvector_real(
                    const std::size_t& index) const;
            const Vector<type_>& eigenvector_imag(
                    const std::size_t& index) const;
    };
}   // namespace

#endif  // GENERALISED_EIGENVALUE_SYSTEM_H

