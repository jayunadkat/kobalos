#ifndef CONSOLE_OUTPUT_H
#define CONSOLE_OUTPUT_H

#include <iostream>
#include <ncurses.h>
#include <string>
#include <sstream>

namespace Kobalos
{
    class String_Converter
    {
        protected:
            std::stringstream ss_;

        public:
            String_Converter();
            ~String_Converter();

            template <typename type_>
            std::string operator()(const type_& value);
            template <typename type_>
            std::string operator()(const std::string& format_string,
                    const type_& value);
    } extern str_convert;

    template <typename type_>
    std::string String_Converter::operator()(const type_& value)
    {
        ss_.str("");
        ss_ << value;
        return ss_.str();
    }

    template <typename type_>
    std::string String_Converter::operator()(const std::string& format_string,
            const type_& value)
    {
        char temp[format_string.size() + 20];
        std::sprintf(temp, format_string.c_str(), value);
        return std::string(temp);
    }

    class NCurses_Wrapper
    {
        protected:
            class NCurses_Window
            {
                protected:
                    WINDOW* win_;

                public:
                    NCurses_Window();
                    ~NCurses_Window();

                    void resize(int height, int width, int start_y);
                    void set_border(char left, char right,
                        char top, char bottom);
                    void set_title_format();
                    void unset_title_format();
                    void print(const char* output_string);
                    void mvprint(int y, int x, const char* output_string);
                    void refresh_window();
                    void dispose();
            } header_, body_;

        public:
            NCurses_Wrapper();
            ~NCurses_Wrapper();

            void init();
            void clean_up();

            void render_header(const char* title_text, const char* header_text);
            void print_body_line(const char* body_text);
            void refresh_all();
    };

    class Console_Output
    {
        protected:
            bool use_ncurses_wrapper_;
            bool staging_header_;
            std::string title_text_, header_text_;
            NCurses_Wrapper ncurses_wrapper_;

        public:
            Console_Output();
            ~Console_Output();

            void operator()(const char* output_string);
            void operator()(const std::string& output_string);
            void stage_header_line(const char* output_string);
            void stage_header_line(const std::string& output_string);
            void set_title(const std::string& title_string);
            void begin_staging_header();
            void end_staging_header();
            void use_ncurses();
            void stop_ncurses();
            void endl();
    } extern console_out;
}   // namespace

#endif  // CONSOLE_OUTPUT_H

