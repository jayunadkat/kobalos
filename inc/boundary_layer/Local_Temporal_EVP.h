#ifndef LOCAL_TEMPORAL_EVP_H
#define LOCAL_TEMPORAL_EVP_H

// Warning: the discretisation of the problem with three computational
// variables (F, FR, V), assumes a uniform mesh in r

#include <string>
#include <ostream>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>
#include <Generalised_Eigenvalue_System_Complex.h>
#include <Vector.h>

namespace Kobalos
{
    class Local_Temporal_EVP
    {
        protected:
            enum {F, FR, V};

            class Matrix_Column
            {
                private:
                    std::size_t nv_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& k);
            } col;

            double Ro_;
            int sgn_;
            std::size_t theta_index_;
            static double tolerance_;
            static std::size_t max_iterations_;

            Mesh_2D<double>& mesh_;
            double wavenumber_;
            Matrix_Helper A_, B_;
            Generalised_Eigenvalue_System_Complex evp_problem_;

            void assemble_matrix();

        public:
            Local_Temporal_EVP(Mesh_2D<double>& mesh);
            ~Local_Temporal_EVP();

            void set_tolerance(const double& tolerance);
            const double& tolerance() const;
            void set_max_iterations(const std::size_t& max_iterations);
            const std::size_t& max_iterations() const;

            void set_theta_index(const std::size_t& theta_index);
            const std::size_t& theta_index() const;
            void set_wavenumber(const double& wavenumber);
            const double& wavenumber() const;
            void set_signed_rossby_number(const double& signed_rossby_number);
            const double& signed_rossby_number() const;

            void solve();
            const Generalised_Eigenvalue_System_Complex& evp_system() const;
    };

    inline std::size_t Local_Temporal_EVP::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& k)
    {
        return nv_*i + k;
    }

    inline void Local_Temporal_EVP::set_tolerance(
            const double& tolerance)
    {
        tolerance_ = tolerance;
    }

    inline const double& Local_Temporal_EVP::tolerance() const
    {
        return tolerance_;
    }

    inline void Local_Temporal_EVP::set_max_iterations(
            const std::size_t& max_iterations)
    {
        max_iterations_ = max_iterations;
    }

    inline const std::size_t& 
            Local_Temporal_EVP::max_iterations() const
    {
        return max_iterations_;
    }

    inline void Local_Temporal_EVP::set_theta_index(
            const std::size_t& theta_index)
    {
        theta_index_ = theta_index;
    }

    inline const std::size_t& Local_Temporal_EVP::theta_index() const
    {
        return theta_index_;
    }

    inline void Local_Temporal_EVP::set_wavenumber(const double& wavenumber)
    {
        wavenumber_ = wavenumber;
    }

    inline const double& Local_Temporal_EVP::wavenumber() const
    {
        return wavenumber_;
    }

    inline void Local_Temporal_EVP::set_signed_rossby_number(
            const double& signed_rossby_number)
    {
        sgn_ = (0. < signed_rossby_number) - (0. > signed_rossby_number);
        Ro_ = std::abs(signed_rossby_number);
    }

    inline const double& Local_Temporal_EVP::signed_rossby_number() const
    {
        return sgn_*Ro_;
    }
}   // namespace

#endif  // LOCAL_TEMPORAL_EVP_H

