#ifndef ROTATING_EDGE_STREAMFUNCTION_H
#define ROTATING_EDGE_STREAMFUNCTION_H

// Warning: the discretisation of the problem with three computational
// variables (F, FR, V), assumes a uniform mesh in r

#include <string>
#include <ostream>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Banded_Matrix.h>
#include <Banded_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Rotating_Edge_Streamfunction
    {
        protected:
            enum {F, FR, V};

            typedef double (*external_func)(const double&);

            class Matrix_Column
            {
                private:
                    std::size_t nv_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& k);
            } col;

            double c2_, Ro_;
            int sgn_;
            static double tolerance_;
            static std::size_t max_iterations_;

            double t_;

            external_func sphere_spin_;
            external_func sphere_wobble_;

            Banded_Matrix<double> A_;
            Vector<double> b_;
            Banded_Linear_System<double> linear_system_;
            Mesh_1D<double> prev_;
            Mesh_1D<double>& mesh_;

            double sphere_spin(const double& t);
            double sphere_wobble(const double& t);
            void assemble_matrix(const double& dt);

        public:
            Rotating_Edge_Streamfunction(Mesh_1D<double>& mesh);
            ~Rotating_Edge_Streamfunction();

            void set_tolerance(const double& tolerance);
            const double& tolerance() const;
            void set_max_iterations(const std::size_t& max_iterations);
            const std::size_t& max_iterations() const;
            const double& time() const;

            void set_signed_rossby_number(const double& signed_rossby_number);
            double signed_rossby_number() const;
            void set_sphere_spin(external_func spin_function);
            void set_sphere_wobble(external_func wobble_function);
            void set_edge(const std::string& edge);

            void solve_steady();
            void step(const double& dt);
            const Mesh_1D<double>& solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline std::size_t Rotating_Edge_Streamfunction::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& k)
    {
        return nv_*i + k;
    }

    inline void Rotating_Edge_Streamfunction::set_tolerance(
            const double& tolerance)
    {
        tolerance_ = tolerance;
    }

    inline const double& Rotating_Edge_Streamfunction::tolerance() const
    {
        return tolerance_;
    }

    inline void Rotating_Edge_Streamfunction::set_max_iterations(
            const std::size_t& max_iterations)
    {
        max_iterations_ = max_iterations;
    }

    inline const std::size_t& 
            Rotating_Edge_Streamfunction::max_iterations() const
    {
        return max_iterations_;
    }

    inline const double& Rotating_Edge_Streamfunction::time() const
    {
        return t_;
    }

    inline void Rotating_Edge_Streamfunction::set_signed_rossby_number(
            const double& signed_rossby_number)
    {
        sgn_ = (0. < signed_rossby_number) - (0. > signed_rossby_number);
        Ro_ = std::abs(signed_rossby_number);
    }

    inline double Rotating_Edge_Streamfunction::signed_rossby_number() const
    {
        return sgn_*Ro_;
    }
    
    inline void Rotating_Edge_Streamfunction::set_sphere_spin(
            external_func spin_function)
    {
        sphere_spin_ = spin_function;
    }
    
    inline void Rotating_Edge_Streamfunction::set_sphere_wobble(
            external_func wobble_function)
    {
        sphere_wobble_ = wobble_function;
    }

    inline void Rotating_Edge_Streamfunction::set_edge(const std::string& edge)
    {
        if (edge == "pole")
            c2_ = 1.;
        else if (edge == "equator")
            c2_ = 0.;
        else
            throw Exceptions::Invalid_Argument(
                    "Rotating_Edge_Streamfunction::set_edge",
                    "\"pole\" or \"equator\" expected");
    }
}   // namespace

#endif  // EDGE_STREAMFUNCTION_H

