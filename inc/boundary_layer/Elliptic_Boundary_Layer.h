#ifndef ELLIPTIC_BOUNDARY_LAYER_H
#define ELLIPTIC_BOUNDARY_LAYER_H

// Warning: the discretisation of the problem with three computational
// variables (F, FR, V), assumes a uniform mesh in r

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>
#include <Sparse_Matrix.h>
#include <Sparse_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Elliptic_Boundary_Layer
    {
        protected:
            enum {f, fr, frr, v, vr};
            enum {F, FR, V};

            typedef double (*external_func)(const double&);
            typedef double (*external_func2)(const double&, const double&);

            class Matrix_Column
            {
                private:
                    std::size_t nv_, nr_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars,
                            const std::size_t& num_r);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& j, const std::size_t& k);
            };

            double S_;
            static double tolerance_;
            static std::size_t max_iterations_;

            double t_;

            external_func ext_bump_;
            external_func2 ext_bump2_;

            Sparse_Matrix<double> A_;
            Vector<double> b_;
            Sparse_Linear_System<double> linear_system_;
            Mesh_1D<double>& mesh_equator_;
            Mesh_1D<double>& mesh_pole_;
            Mesh_2D<double> prev_;
            Mesh_2D<double>& mesh_;

            double bump(const double& theta, const double& t);
            void assemble_matrix_3(const double& dt = 0.);
            void assemble_matrix_5(const double& dt = 0.);

        public:
            Elliptic_Boundary_Layer(Mesh_1D<double>& mesh_pole,
                    Mesh_1D<double>& mesh_equator, Mesh_2D<double>& mesh_bl);
            ~Elliptic_Boundary_Layer();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;
            double& time();
            const double& time() const;

            double& spin_up_parameter();
            const double& spin_up_parameter() const;
            void set_external_bump(external_func ext_bump);
            void set_external_bump(external_func2 ext_bump);

            void solve_steady();
            void step(const double& dt);
            const Mesh_2D<double>& last_solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline double& Elliptic_Boundary_Layer::tolerance()
    {
        return tolerance_;
    }

    inline const double& Elliptic_Boundary_Layer::tolerance() const
    {
        return tolerance_;
    }

    inline std::size_t& Elliptic_Boundary_Layer::max_iterations()
    {
        return max_iterations_;
    }

    inline const std::size_t& 
            Elliptic_Boundary_Layer::max_iterations() const
    {
        return max_iterations_;
    }

    inline double& Elliptic_Boundary_Layer::time()
    {
        return t_;
    }

    inline const double& Elliptic_Boundary_Layer::time() const
    {
        return t_;
    }

    inline double& Elliptic_Boundary_Layer::spin_up_parameter()
    {
        return S_;
    }

    inline const double& Elliptic_Boundary_Layer::spin_up_parameter() const
    {
        return S_;
    }
    
    inline void Elliptic_Boundary_Layer::set_external_bump(
            external_func ext_bump)
    {
        ext_bump_ = ext_bump;
    }

    inline void Elliptic_Boundary_Layer::set_external_bump(
            external_func2 ext_bump)
    {
        ext_bump2_ = ext_bump;
    }
}   // namespace

#endif  // ELLIPTIC_BOUNDARY_LAYER_H

