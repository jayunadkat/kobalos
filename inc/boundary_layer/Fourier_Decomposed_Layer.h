#ifndef FOURIER_DECOMPOSED_LAYER_H
#define FOURIER_DECOMPOSED_LAYER_H

// Warning: the discretisation of the problem with three computational
// variables (f, fr, v), assumes a uniform mesh in r

#include <complex>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>
#include <Sparse_Matrix.h>
#include <Sparse_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Fourier_Decomposed_Layer
    {
        protected:
            enum {f, fr, v};

            class Matrix_Column
            {
                private:
                    std::size_t nv_, nr_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars,
                            const std::size_t& num_r);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& j, const std::size_t& k);
            };

            double omega_;
            static double tolerance_;
            static std::size_t max_iterations_;

            Sparse_Matrix<std::complex<double> > A_;
            Vector<std::complex<double> > b_;
            Sparse_Linear_System<std::complex<double> > linear_system_;
            Mesh_1D<std::complex<double> >& mesh_equator_;
            Mesh_1D<std::complex<double> >& mesh_pole_;
            Mesh_2D<double>& steady_;
            Mesh_2D<std::complex<double> >& mesh_;

            void assemble_matrix_3(const double& dt = 0.);

        public:
            Fourier_Decomposed_Layer(Mesh_1D<std::complex<double> >& mesh_pole,
                    Mesh_1D<std::complex<double> >& mesh_equator,
                    Mesh_2D<double>& mesh_steady,
                    Mesh_2D<std::complex<double> >& mesh_bl);
            ~Fourier_Decomposed_Layer();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;

            double& omega();
            const double& omega() const;

            void solve();
            const Mesh_2D<std::complex<double> >& solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline double& Fourier_Decomposed_Layer::tolerance()
    {
        return tolerance_;
    }

    inline const double& Fourier_Decomposed_Layer::tolerance() const
    {
        return tolerance_;
    }

    inline std::size_t& Fourier_Decomposed_Layer::max_iterations()
    {
        return max_iterations_;
    }

    inline const std::size_t& 
            Fourier_Decomposed_Layer::max_iterations() const
    {
        return max_iterations_;
    }

    inline double& Fourier_Decomposed_Layer::omega()
    {
        return omega_;
    }

    inline const double& Fourier_Decomposed_Layer::omega() const
    {
        return omega_;
    }
}   // namespace

#endif  // FOURIER_DECOMPOSED_LAYER_H

