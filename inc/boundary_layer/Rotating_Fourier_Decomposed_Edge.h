#ifndef ROTATING_FOURIER_DECOMPOSED_EDGE_H
#define ROTATING_FOURIER_DECOMPOSED_EDGE_H

// Warning: the discretisation of the problem with three computational
// variables (f, fr, v), assumes a uniform mesh in r

#include <complex>
#include <string>
#include <ostream>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Banded_Matrix.h>
#include <Banded_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Rotating_Fourier_Decomposed_Edge
    {
        protected:
            enum {f, fr, v};

            class Matrix_Column
            {
                private:
                    std::size_t nv_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& k);
            };

            double Ro_;
            int sgn_;
            double c2_, omega_;
            static double tolerance_;
            static std::size_t max_iterations_;

            Banded_Matrix<std::complex<double> > A_;
            Vector<std::complex<double> > b_;
            Banded_Linear_System<std::complex<double> > linear_system_;
            Mesh_1D<double>& steady_;
            Mesh_1D<std::complex<double> >& mesh_;

            void assemble_matrix(const double& dt = 0.);

        public:
            Rotating_Fourier_Decomposed_Edge(Mesh_1D<double>& mesh_steady,
                    Mesh_1D<std::complex<double> >& mesh);
            ~Rotating_Fourier_Decomposed_Edge();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;

            void set_signed_rossby_number(const double& signed_rossby_number);
            const double& signed_rossby_number() const;
            double& omega();
            const double& omega() const;
            void set_edge(const std::string& edge);

            void solve();
            const Mesh_1D<std::complex<double> >& solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline double& Rotating_Fourier_Decomposed_Edge::tolerance()
    {
        return tolerance_;
    }

    inline const double& Rotating_Fourier_Decomposed_Edge::tolerance() const
    {
        return tolerance_;
    }

    inline std::size_t& Rotating_Fourier_Decomposed_Edge::max_iterations()
    {
        return max_iterations_;
    }

    inline const std::size_t& 
            Rotating_Fourier_Decomposed_Edge::max_iterations() const
    {
        return max_iterations_;
    }

    inline void Rotating_Fourier_Decomposed_Edge::set_signed_rossby_number(
            const double& signed_rossby_number)
    {
        sgn_ = (0 < signed_rossby_number) - (0 > signed_rossby_number);
        Ro_ = std::abs(signed_rossby_number);
    }

    inline const double&
        Rotating_Fourier_Decomposed_Edge::signed_rossby_number() const
    {
        return sgn_*Ro_;
    }
    
    inline double& Rotating_Fourier_Decomposed_Edge::omega()
    {
        return omega_;
    }

    inline const double& Rotating_Fourier_Decomposed_Edge::omega() const
    {
        return omega_;
    }

    inline void Rotating_Fourier_Decomposed_Edge::set_edge(
            const std::string& edge)
    {
        if (edge == "pole")
            c2_ = 1;
        else if (edge == "equator")
            c2_ = 0;
        else
            throw Exceptions::Invalid_Argument(
                    "Rotating_Fourier_Decomposed_Edge::set_edge",
                    "\"pole\" or \"equator\" expected");
    }
}   // namespace

#endif  // ROTATING_FOURIER_DECOMPOSED_EDGE_H

