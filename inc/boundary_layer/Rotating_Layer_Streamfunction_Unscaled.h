#ifndef ROTATING_LAYER_STREAMFUNCTION_UNSCALED_H
#define ROTATING_LAYER_STREAMFUNCTION_UNSCALED_H

// Warning: the discretisation of the problem with three computational
// variables (F, FR, V), assumes a uniform mesh in r

#include <string>
#include <ostream>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>
#include <Sparse_Matrix.h>
#include <Sparse_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Rotating_Layer_Streamfunction_Unscaled
    {
        protected:
            enum {F, FR, V};

            typedef double (*external_func)(const double&);

            class Matrix_Column
            {
                private:
                    std::size_t nv_, nr_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars,
                            const std::size_t& num_r);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& j, const std::size_t& k);
            } col;

            double Ro_;
            int sgn_;
            static double tolerance_;
            static std::size_t max_iterations_;

            double t_;

            external_func sphere_spin_;
            external_func sphere_wobble_;

            Sparse_Matrix<double> A_;
            Vector<double> b_;
            Sparse_Linear_System<double> linear_system_;
            Mesh_2D<double> prev_;
            Mesh_2D<double>& mesh_;
            Mesh_1D<double> *mesh_pole_, *mesh_equator_;

            double sphere_spin(const double& t);
            double sphere_wobble(const double& t);
            void assemble_matrix(const double& dt);

        public:
            Rotating_Layer_Streamfunction_Unscaled(Mesh_2D<double>& mesh_bl);
            ~Rotating_Layer_Streamfunction_Unscaled();

            void set_tolerance(const double& tolerance);
            const double& tolerance() const;
            void set_max_iterations(const std::size_t& max_iterations);
            const std::size_t& max_iterations() const;
            const double& time() const;

            void set_signed_rossby_number(const double& signed_rossby_number);
            const double& signed_rossby_number() const;
            void set_sphere_spin(external_func spin_function);
            void set_sphere_wobble(external_func wobble_function);
            void set_initial_guess();

            void set_edge_meshes(Mesh_1D<double>& mesh_pole,
                    Mesh_1D<double>& mesh_equator);
            void unset_edge_meshes();

            void solve_steady();
            void step(const double& dt);
            const Mesh_2D<double>& solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline std::size_t
        Rotating_Layer_Streamfunction_Unscaled::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& j, const std::size_t& k)
    {
        return nv_*(nr_*j + i) + k;
    }

    inline void Rotating_Layer_Streamfunction_Unscaled::set_tolerance(
            const double& tolerance)
    {
        tolerance_ = tolerance;
    }

    inline const double&
        Rotating_Layer_Streamfunction_Unscaled::tolerance() const
    {
        return tolerance_;
    }

    inline void Rotating_Layer_Streamfunction_Unscaled::set_max_iterations(
            const std::size_t& max_iterations)
    {
        max_iterations_ = max_iterations;
    }

    inline const std::size_t& 
            Rotating_Layer_Streamfunction_Unscaled::max_iterations() const
    {
        return max_iterations_;
    }

    inline const double& Rotating_Layer_Streamfunction_Unscaled::time() const
    {
        return t_;
    }

    inline void
        Rotating_Layer_Streamfunction_Unscaled::set_signed_rossby_number(
            const double& signed_rossby_number)
    {
        sgn_ = (0 < signed_rossby_number) - (0 > signed_rossby_number);
        Ro_ = std::abs(signed_rossby_number);
    }

    inline const double&
        Rotating_Layer_Streamfunction_Unscaled::signed_rossby_number() const
    {
        return sgn_*Ro_;
    }
    
    inline void Rotating_Layer_Streamfunction_Unscaled::set_sphere_spin(
            external_func spin_function)
    {
        sphere_spin_ = spin_function;
    }
    
    inline void Rotating_Layer_Streamfunction_Unscaled::set_sphere_wobble(
            external_func wobble_function)
    {
        sphere_wobble_ = wobble_function;
    }

    inline void Rotating_Layer_Streamfunction_Unscaled::set_edge_meshes(
            Mesh_1D<double>& mesh_pole, Mesh_1D<double>& mesh_equator)
    {
        mesh_pole_ = &mesh_pole;
        mesh_equator_ = &mesh_equator;
    }

    inline void Rotating_Layer_Streamfunction_Unscaled::unset_edge_meshes()
    {
        mesh_pole_ = NULL;
        mesh_equator_ = NULL;
    }
}   // namespace

#endif  // ELLIPTIC_BOUNDARY_LAYER_H

