#ifndef EDGE_STREAMFUNCTION_H
#define EDGE_STREAMFUNCTION_H

// Warning: the discretisation of the problem with three computational
// variables (F, FR, V), assumes a uniform mesh in r

#include <string>
#include <ostream>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Banded_Matrix.h>
#include <Banded_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Edge_Streamfunction
    {
        protected:
            enum {F, FR, V};

            typedef double (*external_func)(const double&);

            class Matrix_Column
            {
                private:
                    std::size_t nv_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& k);
            };

            double c2_, S_;
            static double tolerance_;
            static std::size_t max_iterations_;

            double t_;

            external_func ext_rot_;

            Banded_Matrix<double> A_;
            Vector<double> b_;
            Banded_Linear_System<double> linear_system_;
            Mesh_1D<double> prev_;
            Mesh_1D<double>& mesh_;

            double sphere_rot(const double& t);
            void assemble_matrix(const double& dt = 0.);

        public:
            Edge_Streamfunction(Mesh_1D<double>& mesh);
            ~Edge_Streamfunction();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;
            double& time();
            const double& time() const;

            double& spin_up_parameter();
            const double& spin_up_parameter() const;
            void set_sphere_rotation(external_func ext_rot);
            void set_edge(const std::string& edge);

            void solve_steady();
            void step(const double& dt);
            const Mesh_1D<double>& last_solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline double& Edge_Streamfunction::tolerance()
    {
        return tolerance_;
    }

    inline const double& Edge_Streamfunction::tolerance() const
    {
        return tolerance_;
    }

    inline std::size_t& Edge_Streamfunction::max_iterations()
    {
        return max_iterations_;
    }

    inline const std::size_t& 
            Edge_Streamfunction::max_iterations() const
    {
        return max_iterations_;
    }

    inline double& Edge_Streamfunction::time()
    {
        return t_;
    }

    inline const double& Edge_Streamfunction::time() const
    {
        return t_;
    }

    inline double& Edge_Streamfunction::spin_up_parameter()
    {
        return S_;
    }

    inline const double& Edge_Streamfunction::spin_up_parameter() const
    {
        return S_;
    }
    
    inline void Edge_Streamfunction::set_sphere_rotation(external_func ext_rot)
    {
        ext_rot_ = ext_rot;
    }

    inline void Edge_Streamfunction::set_edge(const std::string& edge)
    {
        if (edge == "pole")
            c2_ = 1;
        else if (edge == "equator")
            c2_ = 0;
        else
            throw Exceptions::Invalid_Argument("Edge_Streamfunction::set_edge",
                    "\"pole\" or \"equator\" expected");
    }
}   // namespace

#endif  // EDGE_STREAMFUNCTION_H

