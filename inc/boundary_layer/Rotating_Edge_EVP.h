#ifndef ROTATING_EDGE_EVP_H
#define ROTATING_EDGE_EVP_H

// Warning: the discretisation of the problem with three computational
// variables (F, FR, V), assumes a uniform mesh in r

#include <string>
#include <ostream>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Dense_Matrix.h>
#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Generalised_Eigenvalue_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Rotating_Edge_EVP
    {
        protected:
            enum {F, FR, V};

            typedef double (*external_func)(const double&);
            static const size_t Temporal_EVP = 0;
            static const size_t Spatial_EVP = 1;

            class Matrix_Column
            {
                private:
                    std::size_t nv_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& k);
            } col;

            double Ro_, fixed_param_;
            size_t type_;
            int sgn_;
            static double tolerance_;
            static std::size_t max_iterations_;

            Dense_Matrix<double> A_, B_;
            Mesh_1D<double>& mesh_;
            Rotating_Edge_Streamfunction steady_problem_;
            Generalised_Eigenvalue_System<double> evp_problem_;

            void assemble_matrix();

        public:
            Rotating_Edge_EVP(Mesh_1D<double>& mesh);
            ~Rotating_Edge_EVP();

            void set_tolerance(const double& tolerance);
            const double& tolerance() const;
            void set_max_iterations(const std::size_t& max_iterations);
            const std::size_t& max_iterations() const;
            void set_parameter(const double& parameter);
            const double& parameter() const;
            void temporal_evp_problem();
            void spatial_evp_problem();

            void set_signed_rossby_number(const double& signed_rossby_number);
            const double& signed_rossby_number() const;

            void solve();
            const Generalised_Eigenvalue_System<double>& evp_system() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline std::size_t Rotating_Edge_EVP::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& k)
    {
        return nv_*i + k;
    }

    inline void Rotating_Edge_EVP::set_tolerance(
            const double& tolerance)
    {
        tolerance_ = tolerance;
    }

    inline const double& Rotating_Edge_EVP::tolerance() const
    {
        return tolerance_;
    }

    inline void Rotating_Edge_EVP::set_max_iterations(
            const std::size_t& max_iterations)
    {
        max_iterations_ = max_iterations;
    }

    inline const std::size_t& 
            Rotating_Edge_EVP::max_iterations() const
    {
        return max_iterations_;
    }

    inline void Rotating_Edge_EVP::set_parameter(const double& parameter)
    {
        fixed_param_ = parameter;
    }

    inline const double& Rotating_Edge_EVP::parameter() const
    {
        return fixed_param_;
    }
    
    inline void Rotating_Edge_EVP::temporal_evp_problem()
    {
        type_ = Temporal_EVP;
    }

    inline void Rotating_Edge_EVP::spatial_evp_problem()
    {
        type_ = Spatial_EVP;
    }

    inline void Rotating_Edge_EVP::set_signed_rossby_number(
            const double& signed_rossby_number)
    {
        sgn_ = (0. < signed_rossby_number) - (0. > signed_rossby_number);
        Ro_ = std::abs(signed_rossby_number);
        steady_problem_.set_signed_rossby_number(signed_rossby_number);
    }

    inline const double& Rotating_Edge_EVP::signed_rossby_number() const
    {
        return sgn_*Ro_;
    }
}   // namespace

#endif  // EDGE_EVP_H

