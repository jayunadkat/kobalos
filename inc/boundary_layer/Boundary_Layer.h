#ifndef BOUNDARY_LAYER_H
#define BOUNDARY_LAYER_H

#include <cmath>

#include <Equation_1D.h>
#include <Equation_2D.h>
#include <Residual.h>
#include <Vector.h>

namespace BoundaryLayer
{
    using namespace Kobalos;

    enum {f, fr, frr, v, vr};

    typedef double (*external_func)(const double&);

    extern bool unsteady;
    extern double S;
    extern external_func ext_bump;

    double Omega(const double& t);
    void set_external_bump(external_func bump);

    class Pole_Problem : public Equation_1D<double>
    {
        protected:
            void update_jacobian_t();
            void update_jacobian_x();

        public:
            Pole_Problem();
            ~Pole_Problem();

            void residual_function(const Vector<double>& state);
            void matrix_t(const Vector<double>& state);

            void jacobian_t_vec(const Vector<double>& state);
            void jacobian_x_vec(const Vector<double>& state);
    };

    class Equator_Problem : public Equation_1D<double>
    {
        protected:
            void update_jacobian_t();
            void update_jacobian_x();

        public:
            Equator_Problem();
            ~Equator_Problem();

            inline void residual_function(const Vector<double>& state);
            void matrix_t(const Vector<double>& state);

            void jacobian_t_vec(const Vector<double>& state);
            void jacobian_x_vec(const Vector<double>& state);
    };

    class BC_Left : public Residual<double>
    {
        public:
            BC_Left();
            ~BC_Left();

            void residual_function(const Vector<double>& state);
    };

    class BC_Right : public Residual<double>
    {
        public:
            BC_Right();
            ~BC_Right();

            void residual_function(const Vector<double>& state);
    };

    class Full_Problem : public Equation_2D<double>
    {
        protected:
            void update_jacobian_t();
            void update_jacobian_x();
            void update_jacobian_y();

        public:
            Full_Problem();
            ~Full_Problem();

            inline void residual_function(const Vector<double>& state);
            void matrix_t(const Vector<double>& state);
            void matrix_y(const Vector<double>& state);

            inline void update_jacobian_t_vec(const Vector<double>& state);
            inline void update_jacobian_x_vec(const Vector<double>& state);
            inline void update_jacobian_y_vec(const Vector<double>& state);
    };

    inline void Pole_Problem::residual_function(const Vector<double>& state)
    {
        residual_[f] = state[fr];
        residual_[fr] = state[frr];
        residual_[frr] = 2*state[f]*state[frr] - state[fr]*state[fr]
            + state[v]*state[v] - S*S;
        residual_[v] = state[vr];
        residual_[vr] = 2*state[f]*state[vr] - 2*state[v]*state[fr];
    }

    inline void Equator_Problem::residual_function(const Vector<double>& state)
    {
        residual_[f] = state[fr];
        residual_[fr] = state[frr];
        residual_[frr] = -state[f]*state[frr] + state[fr]*state[fr]
            + state[v]*state[v] - S*S;
        residual_[v] = state[vr];
        residual_[vr] = -state[f]*state[vr];
    }

    inline void BC_Left::residual_function(const Vector<double>& state)
    {
        residual_[0] = state[f];
        residual_[1] = state[fr];
        residual_[2] = state[v] - Omega(t());
    }

    inline void BC_Right::residual_function(const Vector<double>& state)
    {
        residual_[0] = state[fr];
        residual_[1] = state[v] - S;
    }

    inline void Full_Problem::residual_function(const Vector<double>& state)
    {
        double c(std::cos(x2())), s(std::sin(x2()));
        double c2(c*c), s2(s*s);

        residual_[f] = state[fr];
        residual_[fr] = state[frr];
        residual_[frr] = (2*c2 - s2)*state[f]*state[frr]
            - (c2 - s2)*state[fr]*state[fr] + state[v]*state[v]
            - S*S;
        residual_[v] = state[vr];
        residual_[vr] = (2*c2 - s2)*state[f]*state[vr]
            - 2*c2*state[fr]*state[v];
    }

    inline void Full_Problem::update_jacobian_t_vec(const Vector<double>& state)
    {
    }

    inline void Full_Problem::update_jacobian_x_vec(const Vector<double>& state)
    {
    }

    inline void Full_Problem::update_jacobian_y_vec(const Vector<double>& state)
    {
        double k(std::sin(x2())*std::cos(x2()));

        jacobian_y_vec_(frr, f) = -k*state[frr];
        jacobian_y_vec_(frr, fr) = k*state[fr];
        jacobian_y_vec_(vr, f) = -k*state[vr];
        jacobian_y_vec_(vr, v) = k*state[fr];
    }
}   // namespace

#endif  // BOUNDARY_LAYER_H

