#ifndef LINEARISED_PERTURBED_EDGE_H
#define LINEARISED_PERTURBED_EDGE_H

// Warning: the discretisation of the problem with three computational
// variables (f, fr, v), assumes a uniform mesh in r

#include <string>
#include <ostream>

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Banded_Matrix.h>
#include <Banded_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Linearised_Perturbed_Edge
    {
        protected:
            enum {f, fr, v};

            typedef double (*external_func)(const double&);

            class Matrix_Column
            {
                private:
                    std::size_t nv_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& k);
            };

            double c2_, S_;
            static double tolerance_;
            static std::size_t max_iterations_;

            double t_;

            external_func ext_rot_;

            Banded_Matrix<double> A_;
            Vector<double> b_;
            Banded_Linear_System<double> linear_system_;
            Mesh_1D<double>& steady_;
            Mesh_1D<double> prev_;
            Mesh_1D<double>& mesh_;

            double sphere_rotation(const double& t);
            void assemble_matrix(const double& dt = 0.);

        public:
            Linearised_Perturbed_Edge(Mesh_1D<double>& mesh_steady,
                    Mesh_1D<double>& mesh);
            ~Linearised_Perturbed_Edge();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;
            double& time();
            const double& time() const;

            double& spin_up_parameter();
            const double& spin_up_parameter() const;
            void set_sphere_rotation(external_func ext_rot);
            void set_edge(const std::string& edge);

            void step(const double& dt);
            const Mesh_1D<double>& last_solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline double& Linearised_Perturbed_Edge::tolerance()
    {
        return tolerance_;
    }

    inline const double& Linearised_Perturbed_Edge::tolerance() const
    {
        return tolerance_;
    }

    inline std::size_t& Linearised_Perturbed_Edge::max_iterations()
    {
        return max_iterations_;
    }

    inline const std::size_t& 
            Linearised_Perturbed_Edge::max_iterations() const
    {
        return max_iterations_;
    }

    inline double& Linearised_Perturbed_Edge::time()
    {
        return t_;
    }

    inline const double& Linearised_Perturbed_Edge::time() const
    {
        return t_;
    }

    inline double& Linearised_Perturbed_Edge::spin_up_parameter()
    {
        return S_;
    }

    inline const double& Linearised_Perturbed_Edge::spin_up_parameter() const
    {
        return S_;
    }
    
    inline void Linearised_Perturbed_Edge::set_sphere_rotation(external_func ext_rot)
    {
        ext_rot_ = ext_rot;
    }

    inline void Linearised_Perturbed_Edge::set_edge(const std::string& edge)
    {
        if (edge == "pole")
            c2_ = 1;
        else if (edge == "equator")
            c2_ = 0;
        else
            throw Exceptions::Invalid_Argument("Linearised_Perturbed_Edge::set_edge",
                    "\"pole\" or \"equator\" expected");
    }
}   // namespace

#endif  // LINEARISED_PERTURBED_EDGE_H

