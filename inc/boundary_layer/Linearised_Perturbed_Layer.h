#ifndef LINEARISED_PERTURBED_LAYER_H
#define LINEARISED_PERTURBED_LAYER_H

// Warning: the discretisation of the problem with three computational
// variables (f, fr, v), assumes a uniform mesh in r

#include <Exceptions.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>
#include <Sparse_Matrix.h>
#include <Sparse_Linear_System.h>
#include <Vector.h>

namespace Kobalos
{
    class Linearised_Perturbed_Layer
    {
        protected:
            enum {f, fr, v};

            typedef double (*external_func)(const double&);

            class Matrix_Column
            {
                private:
                    std::size_t nv_, nr_;

                public:
                    explicit Matrix_Column(const std::size_t& num_vars,
                            const std::size_t& num_r);
                    ~Matrix_Column();

                    std::size_t operator()(const std::size_t& i,
                            const std::size_t& j, const std::size_t& k);
            };

            double S_;
            static double tolerance_;
            static std::size_t max_iterations_;

            double t_;

            external_func ext_rot_;

            Sparse_Matrix<double> A_;
            Vector<double> b_;
            Sparse_Linear_System<double> linear_system_;
            Mesh_1D<double>& mesh_equator_;
            Mesh_1D<double>& mesh_pole_;
            Mesh_2D<double>& steady_;
            Mesh_2D<double> prev_;
            Mesh_2D<double>& mesh_;

            double sphere_rotation(const double& t);
            void assemble_matrix_3(const double& dt = 0.);

        public:
            Linearised_Perturbed_Layer(Mesh_1D<double>& mesh_pole,
                    Mesh_1D<double>& mesh_equator, Mesh_2D<double>& mesh_steady,
                    Mesh_2D<double>& mesh_bl);
            ~Linearised_Perturbed_Layer();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;
            double& time();
            const double& time() const;

            double& spin_up_parameter();
            const double& spin_up_parameter() const;
            void set_sphere_rotation(external_func ext_rot);

            void step(const double& dt);
            const Mesh_2D<double>& last_solution() const;
            void dump_flow_variables(std::ostream& out = std::cout,
                    const std::size_t& decimal_places = 6,
                    const std::size_t& output_skip = 0) const;
    };

    inline double& Linearised_Perturbed_Layer::tolerance()
    {
        return tolerance_;
    }

    inline const double& Linearised_Perturbed_Layer::tolerance() const
    {
        return tolerance_;
    }

    inline std::size_t& Linearised_Perturbed_Layer::max_iterations()
    {
        return max_iterations_;
    }

    inline const std::size_t& 
            Linearised_Perturbed_Layer::max_iterations() const
    {
        return max_iterations_;
    }

    inline double& Linearised_Perturbed_Layer::time()
    {
        return t_;
    }

    inline const double& Linearised_Perturbed_Layer::time() const
    {
        return t_;
    }

    inline double& Linearised_Perturbed_Layer::spin_up_parameter()
    {
        return S_;
    }

    inline const double& Linearised_Perturbed_Layer::spin_up_parameter() const
    {
        return S_;
    }
    
    inline void Linearised_Perturbed_Layer::set_sphere_rotation(
            external_func ext_rot)
    {
        ext_rot_ = ext_rot;
    }
}   // namespace

#endif  // LINEARISED_PERTURBED_LAYER_H

