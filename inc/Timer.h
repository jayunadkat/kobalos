#ifndef TIMER_H
#define TIMER_H

#include <cstddef>
#include <ctime>
#include <string>

namespace Kobalos
{
    class Timer
    {
        private:
            bool stopped_;
            int counter_;
            clock_t start_, delta_t_;
            std::string task_;

        public:
            Timer();
            Timer(const std::string& task);
            ~Timer();

            void start();
            void stop();
            void reset();

            const int count() const;
            const double running_time() const;
            const double average_time() const;
            std::size_t seconds() const;
            void print() const;
    };
}   // namespace Kobalos

#endif  // TIMER_H
