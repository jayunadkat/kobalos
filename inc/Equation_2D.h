#ifndef EQUATION_2D_H
#define EQUATION_2D_H

#include <Dense_Matrix.h>
#include <Equation_1D.h>
#include <Vector.h>

namespace Kobalos
{
    template <typename type_, typename xtype_ = double>
    class Equation_2D : public Equation_1D<type_, xtype_>
    {
        protected:
            Dense_Matrix<type_> matrix_y_;
            Vector<Dense_Matrix<type_> > jacobian_y_;
            Dense_Matrix<type_> jacobian_y_vec_;

            virtual void update_jacobian_y();

        public:
            Equation_2D(const std::size_t& order);
            Equation_2D(const std::size_t& order, const std::size_t& num_vars);
            virtual ~Equation_2D();

            virtual void update(const Vector<type_>& state);
            virtual void update_jacobian_y_vec(const Vector<type_>& vec);

            virtual void matrix_y(const Vector<type_>& state);

            const Dense_Matrix<type_>& matrix_y() const;
            const Dense_Matrix<type_>& jacobian_y_vec() const;
    };
    
    template <typename type_, typename xtype_>
    inline void Equation_2D<type_, xtype_>::update(const Vector<type_>& state)
    {
#ifdef PARANOID
        if (state.size() != this->num_vars_)
            throw Exceptions::Mismatch("Equation_2D::update", "Vector");
#endif  // PARANOID

        Equation_1D<type_, xtype_>::update(state);
        matrix_y(this->state_);
        update_jacobian_y();
    }

    template <typename type_, typename xtype_>
    inline void Equation_2D<type_, xtype_>::update_jacobian_y_vec(
            const Vector<type_>& vec)
    {
#ifdef PARANOID
        if (vec.size() != this->num_vars_)
            throw Exceptions::Mismatch("Equation_2D::update_jacobian_y_vec",
                    "Vector");
#endif  // PARANOID

        jacobian_y_vec_.fill(0.);
        for (std::size_t i(0); i < this->order_; ++i)
            for (std::size_t j(0); j < this->order_; ++j)
                for (std::size_t k(0); k < this->num_vars_; ++k)
                    jacobian_y_vec_(i, j) += jacobian_y_[k](i, j)*vec[k];
    }

    template <typename type_, typename xtype_>
    inline void Equation_2D<type_, xtype_>::matrix_y(const Vector<type_>& state)
    {
    }

    template <typename type_, typename xtype_>
    inline const Dense_Matrix<type_>&
            Equation_2D<type_, xtype_>::matrix_y() const
    {
        return matrix_y_;
    }

    template <typename type_, typename xtype_>
    inline const Dense_Matrix<type_>&
            Equation_2D<type_, xtype_>::jacobian_y_vec() const
    {
        return jacobian_y_vec_;
    }
}   // namespace

#endif  // EQUATION_1D_H
