#ifndef FILE_OUTPUT_H
#define FILE_OUTPUT_H

#include <fstream>
#include <map>
#include <string>
#include <vector>

namespace Kobalos
{
    class File_Output
    {
        protected:
            class File_Compare
            {
                public:
                    File_Compare();
                    ~File_Compare();

                    const bool operator()(const std::string& lhs,
                            const std::string& rhs) const;
            };
            
            typedef std::map<std::string, std::ofstream*,
                    File_Compare> map_data;
            typedef std::pair<std::string, std::ofstream*> file_data;

            map_data files_;
            char path_[200];

        public:
            File_Output(const char* path = "./");
            ~File_Output();

            void open(const std::string& tag, const char* filename);
            std::ofstream& write(const std::string& tag);
            std::ofstream& operator()(const std::string& tag);
            void close(const std::string& tag);
            void close_all();
    };
}   // namespace

#endif  // FILE_OUTPUT_H

