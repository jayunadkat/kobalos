#ifndef CONDITIONS_2D_H
#define CONDITIONS_2D_H

#include <vector>

#include <Condition_Data.h>
#include <Conditions_1D.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

namespace Kobalos
{
    template <typename type_, typename xtype_ = double>
    class Conditions_2D
    {
        protected:
            Mesh_2D<type_, xtype_>& mesh_;
            Conditions_1D<type_, xtype_> conditions_x_;
            std::vector<Condition_Data<Mesh_1D, type_, xtype_> > conditions_y_;
            bool reversed_y_data_;

        public:
            Conditions_2D(Mesh_2D<type_, xtype_>& mesh);
            ~Conditions_2D();

            void add_x(const xtype_& location,
                    Residual<type_, xtype_>& condition);
            void add_y(Mesh_1D<type_, xtype_>& condition);

            const std::size_t num_residuals_x();
            const std::size_t num_conditions_x();
            const std::size_t num_conditions_y();

            const xtype_& location_x(const std::size_t& x_index) const;
            const xtype_& location_y(const std::size_t& y_index) const;
            Residual<type_, xtype_>& condition_x(const std::size_t& x_index);
            Mesh_1D<type_, xtype_>& condition_y(const std::size_t& y_index);
            const std::size_t& node_x(const std::size_t& x_index) const;
            const std::size_t node_y(const std::size_t& y_index) const;

            const bool& reversed_y_data() const;
            void apply_y_start();
            void apply_y_end();
    };
}   // namespace

#endif  // CONDITIONS_2D_H

