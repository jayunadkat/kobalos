#ifndef BANDED_LINEAR_SYSTEM_H
#define BANDED_LINEAR_SYSTEM_H

#include <Banded_Matrix.h>
#include <Linear_System_Base.h>
#include <Vector.h>

namespace Kobalos
{
    template <typename type_>
    class Banded_Linear_System : public Linear_System_Base
    {
        protected:
            Banded_Matrix<type_>& A_;
            Vector<type_>& b_;

#ifdef HAS_INTELMKL
            void solve_intelmkl();
#endif
            void solve_lapack();
            void solve_native();
            void back_substitute();

        public:
            Banded_Linear_System(Banded_Matrix<type_>& A, Vector<type_>& b);
            ~Banded_Linear_System();

            void solve();
            std::size_t order() const;
    };
}   // namespace

#endif  // BANDED_LINEAR_SYSTEM_H

