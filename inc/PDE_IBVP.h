#ifndef PDE_IBVP_H
#define PDE_IBVP_H

#include <Banded_Linear_System.h>
#include <Banded_Matrix.h>
#include <Conditions_1D.h>
#include <Conditions_2D.h>
#include <Equation_1D.h>
#include <Equation_2D.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>
#include <Timer.h>
#include <Vector.h>

namespace Kobalos
{
    static const std::size_t PDE_Base = 0;
    static const std::size_t PDE_1D_Unsteady = 1;
    static const std::size_t PDE_2D_Unsteady_Parabolic = 2;
    static const std::size_t PDE_2D_Steady_Elliptic = 3;
    static const std::size_t PDE_2D_Steady_Parabolic = 4;

    template <std::size_t dim_, typename type_, typename xtype_>
    class PDE_IBVP
    {
        protected:
            static double tolerance_;
            static std::size_t max_iterations_;
            double t_;

        public:
            PDE_IBVP();
            ~PDE_IBVP();

            double& tolerance();
            const double& tolerance() const;
            std::size_t& max_iterations();
            const std::size_t& max_iterations() const;
            double& time();
            const double& time() const;
    };

    template <typename type_, typename xtype_>
    class PDE_IBVP<PDE_1D_Unsteady, type_, xtype_> : public PDE_IBVP<PDE_Base,
          type_, xtype_>
    {
        protected:
            Equation_1D<type_, xtype_>& equation_;
            Conditions_1D<type_, xtype_>& conditions_;
            Banded_Matrix<type_> A_;
            Vector<type_> b_;
            Banded_Linear_System<type_> linear_system_;
            Mesh_1D<type_, xtype_> prev_sol_;
            Mesh_1D<type_, xtype_>& mesh_;

            void assemble_matrix(const double& dt);

        public:
            PDE_IBVP(Equation_1D<type_, xtype_>& equation,
                    Conditions_1D<type_, xtype_>& conditions,
                    Mesh_1D<type_, xtype_>& mesh);
            ~PDE_IBVP();

            void step(const double& dt);
            const Vector<double> change_two_norm() const;
    };

    template <typename type_, typename xtype_>
    class PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_> :
        public PDE_IBVP<PDE_Base, type_, xtype_>
    {
        protected:
            Equation_2D<type_, xtype_>& equation_;
            Conditions_2D<type_, xtype_>& conditions_;
            Banded_Matrix<type_> A_;
            Vector<type_> b_;
            Banded_Linear_System<type_> linear_system_;
            Mesh_2D<type_, xtype_> prev_sol_;
            Mesh_2D<type_, xtype_>& mesh_;
            std::size_t y_, streamwise_component_;
            bool check_reversed_, streamwise_specified_;
            bool direction_inverted_;
            bool first_order_;

            const bool check_sign(const type_& value);
            void assemble_matrix(const double& dt);

        public:
            PDE_IBVP(Equation_2D<type_, xtype_>& equation,
                    Conditions_2D<type_, xtype_>& conditions,
                    Mesh_2D<type_, xtype_>& mesh);
            ~PDE_IBVP();

            void set_streamwise_component(const std::size_t& streamwise_index);
            bool& direction_inverted();
            const bool& direction_inverted() const;
            bool& first_order();

            void step(const double& dt);
    };

    template <typename type_, typename xtype_>
    class PDE_IBVP<PDE_2D_Steady_Elliptic, type_, xtype_> :
        public PDE_IBVP<PDE_Base, type_, xtype_>
    {
        protected:
            Equation_2D<type_, xtype_>& equation_;
            Conditions_2D<type_, xtype_>& conditions_;
            Banded_Matrix<type_> A_;
            Vector<type_> b_;
            Banded_Linear_System<type_> linear_system_;
            Mesh_2D<type_, xtype_>& mesh_;

            void assemble_matrix();

        public:
            PDE_IBVP(Equation_2D<type_, xtype_>& equation,
                    Conditions_2D<type_, xtype_>& conditions,
                    Mesh_2D<type_, xtype_>& mesh);
            ~PDE_IBVP();

            void solve();
    };

    template <std::size_t dim_, typename type_, typename xtype_>
    double PDE_IBVP<dim_, type_, xtype_>::tolerance_ = 1e-8;

    template <std::size_t dim_, typename type_, typename xtype_>
    std::size_t PDE_IBVP<dim_, type_, xtype_>::max_iterations_ = 50;

    template <std::size_t dim_, typename type_, typename xtype_>
    PDE_IBVP<dim_, type_, xtype_>::PDE_IBVP() :
        t_(0.)
    {
    }

    template <std::size_t dim_, typename type_, typename xtype_>
    PDE_IBVP<dim_, type_, xtype_>::~PDE_IBVP()
    {
    }

    template <std::size_t dim_, typename type_, typename xtype_>
    inline double& PDE_IBVP<dim_, type_, xtype_>::tolerance()
    {
        return tolerance_;
    }

    template <std::size_t dim_, typename type_, typename xtype_>
    inline const double& PDE_IBVP<dim_, type_, xtype_>::tolerance() const
    {
        return tolerance_;
    }

    template <std::size_t dim_, typename type_, typename xtype_>
    inline std::size_t& PDE_IBVP<dim_, type_, xtype_>::max_iterations()
    {
        return max_iterations_;
    }

    template <std::size_t dim_, typename type_, typename xtype_>
    inline const std::size_t& 
            PDE_IBVP<dim_, type_, xtype_>::max_iterations() const
    {
        return max_iterations_;
    }

    template <std::size_t dim_, typename type_, typename xtype_>
    inline double& PDE_IBVP<dim_, type_, xtype_>::time()
    {
        return t_;
    }

    template <std::size_t dim_, typename type_, typename xtype_>
    inline const double& PDE_IBVP<dim_, type_, xtype_>::time() const
    {
        return t_;
    }
}   // namespace

#endif  // PDE_IBVP_H
