#ifndef DENSE_MATRIX_H
#define DENSE_MATRIX_H

#include <cstddef>

#include <Exceptions.h>
#include <Matrix_Base.h>

namespace Kobalos
{
    template <typename type_>
    class Dense_Matrix : public Matrix_Base<type_>
    {
        protected:
            Vector<type_> matrix_;
            std::size_t num_rows_, num_cols_;
      
        public:
            Dense_Matrix();
            Dense_Matrix(const std::size_t& n, const type_& value);
            Dense_Matrix(const std::size_t& n, const std::size_t& m,
                    const type_& value);
            Dense_Matrix(const Dense_Matrix<type_>& source);
            Dense_Matrix(type_* const source_pointer,
                    const std::size_t& n, const std::size_t& m);
            ~Dense_Matrix();
            
            const Dense_Matrix<type_>& operator=(
                    const Dense_Matrix<type_>& source);
            type_& operator()(const std::size_t& row,
                    const std::size_t& column);
            const type_ operator()(const std::size_t& row,
                    const std::size_t& column) const;
            type_& set(const std::size_t& row,
                    const std::size_t& column);
            const type_ get(const std::size_t& row,
                    const std::size_t& column) const;

            void set_column(const std::size_t& column,
                    const Vector<type_>& data);

            const Vector<type_> operator*(const Vector<type_>& x) const;
            void operator*=(const type_& k);
            const Dense_Matrix<type_> operator-(
                    const Dense_Matrix<type_>& M) const;
            const Dense_Matrix<type_> operator/(const type_& k) const;

            void assign(type_* const source_pointer, const std::size_t& n,
                    const std::size_t& m);
            void assign(const std::size_t& n, const std::size_t& m,
                    const type_& value);
            void resize(const std::size_t& n, const std::size_t& m);
            void fill(const type_& value);
            void clear();
            void transpose();
            void swap_rows(const std::size_t& row1, const std::size_t& row2);

            const std::size_t& num_rows() const;
            const std::size_t& num_columns() const;
            const std::size_t num_elements() const;

            const double one_norm() const;
            const double two_norm() const;
            const double inf_norm() const;

            type_* data_pointer();
            void dump() const;
            void dump(const std::size_t& decimal_places) const;
    };

    template <typename type_>
    inline type_& Dense_Matrix<type_>::operator()(const std::size_t& row,
            const std::size_t& column)
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
        {
            throw Exceptions::Range("Dense_Matrix",
                    num_rows_, row, num_cols_, column);
        }
#endif  // PARANOID

        return matrix_[column*num_rows_ + row];
    }

    template <typename type_>
    inline const type_ Dense_Matrix<type_>::operator()(const std::size_t& row,
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
        {
            throw Exceptions::Range("Dense_Matrix",
                    num_rows_, row, num_cols_, column);
        }
#endif  // PARANOID

        return matrix_[column*num_rows_ + row];
    }

    template <typename type_>
    inline type_& Dense_Matrix<type_>::set(const std::size_t& row,
            const std::size_t& column)
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
        {
            throw Exceptions::Range("Dense_Matrix",
                    num_rows_, row, num_cols_, column);
        }
#endif  // PARANOID

        return matrix_[column*num_rows_ + row];
    }

    template <typename type_>
    inline const type_ Dense_Matrix<type_>::get(const std::size_t& row,
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
        {
            throw Exceptions::Range("Dense_Matrix",
                    num_rows_, row, num_cols_, column);
        }
#endif  // PARANOID

        return matrix_[column*num_rows_ + row];
    }

    template <typename type_>
    inline void Dense_Matrix<type_>::set_column(const std::size_t& column,
            const Vector<type_>& data)
    {
#ifdef PARANOID
        if (column >= num_cols_)
            throw Exceptions::Range("Dense_Matrix, column access", num_cols_,
                    column);
        if (data.size() != num_rows_)
            throw Exceptions::Mismatch("Dense_Matrix::set_column",
                    "Dense_Matrix", "Vector");
#endif  // PARANOID

        for (std::size_t i = 0; i < num_rows_; ++i)
            matrix_[num_rows_*column + i] = data[i];
    }

}   // namespace

#endif  // DENSE_MATRIX_H

