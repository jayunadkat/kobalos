#ifndef VECTOR_H
#define VECTOR_H

#include <cmath>
#include <cstddef>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <vector>

#include <Exceptions.h>

namespace Kobalos
{
    template <typename type_>
    class Vector
    {
        protected:
            std::vector<type_> storage_;

        public:
            Vector();
            Vector(const std::size_t& size);
            Vector(const std::size_t& size, const type_& value);
            Vector(const Vector<type_>& source);
            Vector(type_* const source_pointer, const std::size_t& size);
            ~Vector();

            type_& operator[](const std::size_t& index);
            const type_& operator[](const std::size_t& index) const;

            void assign(type_* const source_pointer, const std::size_t& size);
            void assign(const std::size_t& size, const type_& value);
            void reserve(const std::size_t& size);
            void resize(const std::size_t& size);
            void fill(const type_& value);
            void clear();

            void push_back(const type_& value);
            void pop_back();
            type_& front();
            const type_& front() const;
            type_& back();
            const type_& back() const;
            void insert(const std::size_t& position, const type_& value);
            void insert(const std::size_t& position, const std::size_t& n,
                    const type_& value);

            const Vector<type_>& operator=(const Vector<type_>& source);
            const Vector<type_> operator+(const Vector<type_>& x) const;
            const Vector<type_> operator+() const;
            Vector<type_>& operator+=(const Vector<type_>& x);
            const Vector<type_> operator-(const Vector<type_>& x) const;
            const Vector<type_> operator-() const;
            Vector<type_>& operator-=(const Vector<type_>& x);
            const Vector<type_> operator*(const type_& k) const;
            Vector<type_>& operator*=(const type_& k);
            const Vector<type_> operator/(const type_& k) const;
            Vector<type_>& operator/=(const type_& k);

            void swap(const std::size_t& i, const std::size_t& j);

            const double one_norm() const;
            const double two_norm() const;
            const double inf_norm() const;

            const std::size_t size() const;
            type_* data_pointer();

            void dump(std::ostream& out = std::cout) const;
            void dump(const std::size_t& decimal_places) const;
    };

    template <typename type_>
    Vector<type_>::Vector() :
        storage_()
    {
    }

    template <typename type_>
    Vector<type_>::Vector(const std::size_t& size) :
        storage_(size)
    {
    }

    template <typename type_>
    Vector<type_>::Vector(const std::size_t& size, const type_& value) :
        storage_(size, value)
    {
    }

    template <typename type_>
    inline Vector<type_>::Vector(const Vector<type_>& source) :
        storage_(source.storage_)
    {
    }

    template <typename type_>
    Vector<type_>::Vector(type_* const source_pointer,
            const std::size_t& size) :
        storage_()
    {
#ifdef PARANOID
        if (size == 0)
        {
            throw Exceptions::Invalid_Argument("Vector::Vector",
                    "size must be non-zero");
        }
        if (source_pointer == NULL)
        {
            throw Exceptions::Invalid_Argument("Vector::Vector",
                    "source_pointer is NULL");
        }
#endif  // PARANOID

        storage_.reserve(size);
        storage_.assign(source_pointer, source_pointer + size);
    }

    template <typename type_>
    Vector<type_>::~Vector()
    {
    }

    template <typename type_>
    inline type_& Vector<type_>::operator[](const std::size_t& index)
    {
#ifdef PARANOID
        std::size_t size = storage_.size();
        if (index >= size)
            throw Exceptions::Range("Vector", size, index);
#endif  // PARANOID

        return storage_[index];
    }

    template <typename type_>
    inline const type_& Vector<type_>::operator[](const std::size_t& index) const
    {
#ifdef PARANOID
        std::size_t size = storage_.size();
        if (index >= size)
            throw Exceptions::Range("Vector", size, index);
#endif  // PARANOID

        return storage_[index];
    }

    template <typename type_>
    void Vector<type_>::assign(type_* const source_pointer,
            const std::size_t& size)
    {
#ifdef PARANOID
        if (size == 0)
        {
            throw Exceptions::Invalid_Argument("Vector::Vector",
                    "size must be non-zero");
        }
        if (source_pointer == NULL)
        {
            throw Exceptions::Invalid_Argument("Vector::Vector",
                    "source_pointer is NULL");
        }
#endif  // PARANOID

        storage_.reserve(size);
        storage_.assign(source_pointer, source_pointer + size);
    }

    template <typename type_>
    inline void Vector<type_>::assign(const std::size_t& size, const type_& value)
    {
        storage_.reserve(size);
        storage_.assign(size, value);
    }

    template <typename type_>
    inline void Vector<type_>::reserve(const std::size_t& size)
    {
        storage_.reserve(size);
    }

    template <typename type_>
    inline void Vector<type_>::resize(const std::size_t& size)
    {
        storage_.resize(size);
    }

    template <typename type_>
    inline void Vector<type_>::fill(const type_& value)
    {
        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            storage_[i] = value;
    }
    
    template <typename type_>
    inline void Vector<type_>::clear()
    {
        storage_.clear();
    }

    template <typename type_>
    inline void Vector<type_>::push_back(const type_& value)
    {
        storage_.push_back(value);
    }

    template <typename type_>
    inline void Vector<type_>::pop_back()
    {
        storage_.pop_back();
    }

    template <typename type_>
    type_& Vector<type_>::front()
    {
        return storage_.front();
    }

    template <typename type_>
    const type_& Vector<type_>::front() const
    {
        return storage_.front();
    }

    template <typename type_>
    type_& Vector<type_>::back()
    {
        return storage_.back();
    }

    template <typename type_>
    const type_& Vector<type_>::back() const
    {
        return storage_.back();
    }

    template <typename type_>
    void Vector<type_>::insert(const std::size_t& position, const type_& value)
    {
#ifdef PARANOID
        std::size_t size = storage_.size();
        if (position > size)
            throw Exceptions::Range("Vector", size, position);
#endif  // PARANOID

        storage_.insert(storage_.begin() + position, value);
    }

    template <typename type_>
    void Vector<type_>::insert(const std::size_t& position,
            const std::size_t& n, const type_& value)
    {
#ifdef PARANOID
        std::size_t size = storage_.size();
        if (position > size)
            throw Exceptions::Range("Vector", size, position);
#endif  // PARANOID

        storage_.insert(storage_.begin() + position, n, value);
    }

    template <typename type_>
    inline const Vector<type_>& Vector<type_>::operator=(const Vector<type_>& source)
    {
        if (&source != this)
        {
            storage_ = source.storage_;
        }
        return *this;
    }

    template <typename type_>
    inline const Vector<type_> Vector<type_>::operator+(const Vector<type_>& x) const
    {
#ifdef PARANOID
        if (storage_.size() != x.size())
        {
            throw Exceptions::Mismatch("Vector::operator+", "Vector");
        }
#endif  // PARANOID

        Vector<type_> temp(*this);
        temp += x;
        return temp;
    }

    template <typename type_>
    inline const Vector<type_> Vector<type_>::operator+() const
    {
        return *this;
    }

    template <typename type_>
    Vector<type_>& Vector<type_>::operator+=(const Vector<type_>& x)
    {
#ifdef PARANOID
        if (storage_.size() != x.size())
        {
            throw Exceptions::Mismatch("Vector::operator+=", "Vector");
        }
#endif  // PARANOID

        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            storage_[i] += x[i];
        return *this;
    }

    template <typename type_>
    inline const Vector<type_> Vector<type_>::operator-(const Vector<type_>& x) const
    {
#ifdef PARANOID
        if (storage_.size() != x.size())
        {
            throw Exceptions::Mismatch("Vector::operator-", "Vector");
        }
#endif  // PARANOID

        Vector<type_> temp(*this);
        temp -= x;
        return temp;
    }

    template <typename type_>
    inline const Vector<type_> Vector<type_>::operator-() const
    {
        Vector<type_> temp(*this);
        temp *= -1.;
        return temp;
    }

    template <typename type_>
    inline Vector<type_>& Vector<type_>::operator-=(const Vector<type_>& x)
    {
#ifdef PARANOID
        if (storage_.size() != x.size())
        {
            throw Exceptions::Mismatch("Vector::operator-=", "Vector");
        }
#endif  // PARANOID

        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            storage_[i] -= x[i];
        return *this;
    }

    template <typename type_>
    inline const Vector<type_> Vector<type_>::operator*(const type_& k) const
    {
        Vector<type_> temp(*this);
        temp *= k;
        return temp;
    }

    template <typename type_>
    inline Vector<type_>& Vector<type_>::operator*=(const type_& k)
    {
        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            storage_[i] *= k;
        return *this;
    }

    template <typename type_>
    inline const Vector<type_> Vector<type_>::operator/(const type_& k) const
    {
        Vector<type_> temp(*this);
        temp /= k;
        return temp;
    }

    template <typename type_>
    inline Vector<type_>& Vector<type_>::operator/=(const type_& k)
    {
#ifdef PARANOID
        if (std::abs(k) == 0)
        {
            throw Exceptions::Invalid_Argument("Vector::operator/=",
                    "std::abs(k) > 0 is required");
        }
#endif  // PARANOID

        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            storage_[i] /= k;
        return *this;
    }

    template <typename type_>
    inline void Vector<type_>::swap(const std::size_t& i, const std::size_t& j)
    {
#ifdef PARANOID
        if (i >= storage_.size() || j >= storage_.size())
            throw Exceptions::Range("Vector", size(), std::max(i, j));
#endif  // PARANOID

        std::swap(storage_[i], storage_[j]);
    }

    template <typename type_>
    const double Vector<type_>::one_norm() const
    {
        double temp(0.);
        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            temp += std::abs(storage_[i]);
        return temp;
    }

    template <typename type_>
    const double Vector<type_>::two_norm() const
    {
        double temp(0.);
        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            temp += std::pow(std::abs(storage_[i]), 2.);
        return std::sqrt(temp);
    }

    template <typename type_>
    const double Vector<type_>::inf_norm() const
    {
        double temp(0.);
        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            if (temp < std::abs(storage_[i]))
                temp = std::abs(storage_[i]);
        return temp;
    }

    template <typename type_>
    inline const std::size_t Vector<type_>::size() const
    {
        return storage_.size();
    }

    template <typename type_>
    inline type_* Vector<type_>::data_pointer()
    {
#ifdef PARANOID
        if (storage_.size() == 0)
            throw Exceptions::Generic("Illegal operation",
                    "Attempt to pass pointer to empty Vector");
#endif  // PARANOID

        return &storage_[0];
    }

    template <typename type_>
    void Vector<type_>::dump(std::ostream& out) const
    {
        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            out << storage_[i] << " ";
        out << '\n';
    }

    template <typename type_>
    void Vector<type_>::dump(const std::size_t& decimal_places) const
    {
        std::size_t width = decimal_places + 8;
        if (decimal_places == 0)
            std::cout.unsetf(std::ios_base::scientific);
        else
            std::cout << std::scientific << std::setprecision((int)width - 8);
        for (std::size_t size = storage_.size(), i = 0; i < size; ++i)
            std::cout << std::setw(width) << storage_[i] << " ";
        std::cout << '\n';
    }

    template <typename type_>
    inline Vector<type_> operator*(const type_& k, const Vector<type_>& vec)
    {
        return vec*k;
    }
}   // namespace

#endif  // VECTOR_H

