#ifndef SPARSE_MATRIX_H
#define SPARSE_MATRIX_H

#include <map>
#include <ostream>

#include <Matrix_Base.h>
#include <Vector.h>

namespace Kobalos
{
    class Sparse_Data_Compare
    {
        protected:
            std::size_t num_cols_;

        public:
            explicit Sparse_Data_Compare(const std::size_t& num_cols);

            const bool operator()(
                    const std::pair<std::size_t, std::size_t>& lhs,
                    const std::pair<std::size_t, std::size_t>& rhs) const;
    };

    template <typename type_>
    class Sparse_Matrix : public Matrix_Base<type_>
    {
        typedef std::pair<std::size_t, std::size_t> size_t_pair;
        typedef std::pair<size_t_pair, type_> element_data;
        typedef std::map<size_t_pair, type_,
                Sparse_Data_Compare> matrix_data;
        typedef typename matrix_data::iterator matrix_iterator;
        typedef typename matrix_data::const_iterator matrix_const_iterator;

        protected:
            matrix_data matrix_;
            std::size_t num_rows_, num_cols_;

        public:
            explicit Sparse_Matrix(const std::size_t& n);
            explicit Sparse_Matrix(const std::size_t& n, const std::size_t& m);
            ~Sparse_Matrix();

            type_& operator()(const std::size_t& row,
                    const std::size_t& column);
            const type_ operator()(const std::size_t& row,
                    const std::size_t& column) const;
            type_& set(const std::size_t& row,
                    const std::size_t& column);
            const type_ get(const std::size_t& row,
                    const std::size_t& column) const;

            const std::size_t& num_rows() const;
            const std::size_t& num_columns() const;
            const std::size_t num_elements() const;

            const Vector<type_> operator*(const Vector<type_>& x) const;

            void assign(const std::size_t& n);
            void transpose();

            void swap_rows(const std::size_t& row1, const std::size_t& row2);
            const std::size_t column_max_index(const std::size_t& column) const;

            const double one_norm() const;
            const double two_norm() const;
            const double inf_norm() const;

            void dump(std::ostream& out = std::cout) const;
            void dump(const std::size_t& decimal_places,
                    std::ostream& out = std::cout) const;
            void dump_sparse(std::ostream& out = std::cout) const;

            void get_superlu_data(Vector<type_>& a, int* asub, int* xa) const;
    };
}   // namespace

#endif  // SPARSE_MATRIX_H

