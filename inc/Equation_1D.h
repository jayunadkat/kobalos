#ifndef EQUATION_1D_H
#define EQUATION_1D_H

#include <Dense_Matrix.h>
#include <Residual.h>
#include <Vector.h>

namespace Kobalos
{
    template <typename type_, typename xtype_ = double>
    class Equation_1D : public Residual<type_, xtype_>
    {
        protected:
            Dense_Matrix<type_> matrix_t_, matrix_x_;
            Vector<Dense_Matrix<type_> > jacobian_t_, jacobian_x_;
            Dense_Matrix<type_> jacobian_t_vec_, jacobian_x_vec_;

            virtual void update_jacobian_t();
            virtual void update_jacobian_x();

        public:
            Equation_1D(const std::size_t& order);
            Equation_1D(const std::size_t& order, const std::size_t& num_vars);
            virtual ~Equation_1D();

            virtual void update(const Vector<type_>& state);
            virtual void update_jacobian_t_vec(const Vector<type_>& vec);
            virtual void update_jacobian_x_vec(const Vector<type_>& vec);

            virtual void matrix_t(const Vector<type_>& state);
            virtual void matrix_x(const Vector<type_>& state);

            const Dense_Matrix<type_>& matrix_t() const;
            const Dense_Matrix<type_>& jacobian_t_vec() const;
            const Dense_Matrix<type_>& matrix_x() const;
            const Dense_Matrix<type_>& jacobian_x_vec() const;
    };
    
    template <typename type_, typename xtype_>
    inline void Equation_1D<type_, xtype_>::update(const Vector<type_>& state)
    {
#ifdef PARANOID
        if (state.size() != this->num_vars_)
            throw Exceptions::Mismatch("Equation_1D::update", "Vector");
#endif  // PARANOID

        Residual<type_, xtype_>::update(state);
        matrix_t(this->state_);
        update_jacobian_t();
        matrix_x(this->state_);
        update_jacobian_x();
    }

    template <typename type_, typename xtype_>
    inline void Equation_1D<type_, xtype_>::update_jacobian_t_vec(
            const Vector<type_>& vec)
    {
#ifdef PARANOID
        if (vec.size() != this->num_vars_)
            throw Exceptions::Mismatch("Equation_1D::update_jacobian_t_vec",
                    "Vector");
#endif  // PARANOID

        jacobian_t_vec_.fill(0.);
        for (std::size_t i(0); i < this->order_; ++i)
            for (std::size_t j(0); j < this->order_; ++j)
                for (std::size_t k(0); k < this->num_vars_; ++k)
                    jacobian_t_vec_(i, j) += jacobian_t_[k](i, j)*vec[k];
    }

    template <typename type_, typename xtype_>
    inline void Equation_1D<type_, xtype_>::update_jacobian_x_vec(
            const Vector<type_>& vec)
    {
#ifdef PARANOID
        if (vec.size() != this->num_vars_)
            throw Exceptions::Mismatch("Equation_1D::update_jacobian_x_vec",
                    "Vector");
#endif  // PARANOID

        jacobian_x_vec_.fill(0.);
        for (std::size_t i(0); i < this->order_; ++i)
            for (std::size_t j(0); j < this->order_; ++j)
                for (std::size_t k(0); k < this->num_vars_; ++k)
                    jacobian_x_vec_(i, j) += jacobian_x_[k](i, j)*vec[k];
    }
    
    template <typename type_, typename xtype_>
    inline void Equation_1D<type_, xtype_>::matrix_t(const Vector<type_>& state)
    {
    }

    template <typename type_, typename xtype_>
    inline void Equation_1D<type_, xtype_>::matrix_x(const Vector<type_>& state)
    {
    }

    template <typename type_, typename xtype_>
    inline const Dense_Matrix<type_>&
            Equation_1D<type_, xtype_>::matrix_t() const
    {
        return matrix_t_;
    }

    template <typename type_, typename xtype_>
    inline const Dense_Matrix<type_>&
            Equation_1D<type_, xtype_>::jacobian_t_vec() const
    {
        return jacobian_t_vec_;
    }

    template <typename type_, typename xtype_>
    inline const Dense_Matrix<type_>&
            Equation_1D<type_, xtype_>::matrix_x() const
    {
        return matrix_x_;
    }

    template <typename type_, typename xtype_>
    inline const Dense_Matrix<type_>&
            Equation_1D<type_, xtype_>::jacobian_x_vec() const
    {
        return jacobian_x_vec_;
    }
}   // namespace

#endif  // EQUATION_1D_H
