#include <cmath>
#include <fstream>

#include <ODE_Bundle.h>

using namespace Kobalos;

enum {f, fp, fpp};

namespace Blasius
{
    class Equation : public Residual<double>
    {
        public:
            Equation() : Residual<double>(3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[f] = state[fp];
                residual_[fp] = state[fpp];
                residual_[fpp] = -state[f]*state[fpp]/2.;
            }
    };

    class BC : public Residual<double>
    {
        public:
            BC() : Residual<double>(3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f];
                residual_[1] = state[fp];
                residual_[2] = state[fpp] - 3.3205866083e-01;
            }
    };

    class BC_Left : public Residual<double>
    {
        public:
            BC_Left() : Residual<double>(2, 3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f];
                residual_[1] = state[fp];
            }
    };

    class BC_Right : public Residual<double>
    {
        public:
            BC_Right() : Residual<double>(1, 3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[fp] - 1;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    double left(0.), right(100.), dx(0.01);
    std::size_t num_nodes((right - left)/dx + 1);
    std::ofstream datafile;
    datafile.open("dat/blasius.dat");

    Blasius::Equation eqn;
    Blasius::BC_Left bc_left;
    Blasius::BC_Right bc_right;
    Blasius::BC bc;

    Mesh_1D<double> mesh;
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());

    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    for (std::size_t i(0), nodes(mesh.num_nodes()); i < nodes; ++i)
    {
        double x(mesh.node(i));
        mesh(i, f) = x*x/(1 + x);
        mesh(i, fp) = (2*x + std::pow(x, 2.))/std::pow(1 + x, 2.);
        mesh(i, fpp) = 2/std::pow(1 + x, 3.);
    }

    ODE_BVP<double> ode_problem(eqn, conditions, mesh);
    ode_problem.solve();
    
    mesh.dump(10, datafile);
    datafile.close();

    return 0;
}

