#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double Omega(double t, double S)
    {
        return 1. - (S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        public:
            double S;

            Equation() :
                Equation_1D<double>(5),
                S(0.)
            {
            }

            void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] - state[u]*state[u] -
                    state[v]*state[v] + S*S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr];
                residual_[w] = state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            double S;

            LeftBC() : Residual<double>(3, 5), S(0.) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t(), S);
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            double S;

            RightBC() : Residual<double>(2, 5), S(0.) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - S;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        return 0;
    }

    //const double pi = 4.*std::atan(1.);
    double S_begin(std::atof(argv[1])), S_end(std::atof(argv[2])), dS(0.01);
    double left(0.), right(50.), x(0.), dt(0.1), dx(0.1);//, sol_tol(1.e-6);
    std::size_t num_nodes((right - left)/dx + 1), max_time_steps(300);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    std::stringstream filename;
    filename << "dat/equator_far_" << S_begin << "-" << S_end << ".dat";

    datafile.open(filename.str().c_str());
    datafile << "# Stopped at time = 3\n";
    datafile << "# S    w_inf\n";
    //double prev_val(0.);

    if (S_begin > S_end)
        dS = -0.01;

    for (double S = S_begin; std::abs(S - S_begin) <= std::abs(S_end - S_begin);
            S += dS)
    {
        std::cout << "Solving for S = " << S << '\n';
        eqn.S = bc_left.S = bc_right.S = S;
        mesh.uniform_mesh(left, right, num_nodes, eqn.order());

        for (std::size_t i(0); i < num_nodes; ++i)
        {
            x = mesh.node(i);
            mesh(i, v) = S;//1. + (S - 1.)*(x - left)/(right - left);
            //mesh(i, vr) = (S - 1.)/(right - left);
        }

        PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(
                eqn, conditions, mesh);
        for (std::size_t time_count(0); time_count < max_time_steps;
                ++time_count)
        {
            pde_problem.step(dt);
        }
        //pde_problem.solve();
        datafile << S << " ";
        datafile << mesh(num_nodes - 1, w) << '\n';
        datafile.flush();
    }
    datafile.close();

    return 0;
}

