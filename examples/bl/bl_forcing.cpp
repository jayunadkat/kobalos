// 2D boundary layer problem with forcing

#include <cmath>
#include <cstring>
#include <fstream>

#include <boundary_layer/Edge_Streamfunction.h>
#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <boundary_layer/Linearised_Perturbed_Edge.h>
#include <boundary_layer/Linearised_Perturbed_Layer.h>
#include <Command_Line_Args.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Rotation
{
    double omega_mult(2.);
    double S(0.);
    double wobble_amp(0.01);

    double wobble(const double& t)
    {
        return 1. + wobble_amp*std::sin(omega_mult*S*t)*(1. - std::exp(-t*t));
        /*if (t < 1.)
            return std::exp(4.*(1. - 1./(1. - std::pow(2.*t - 1, 2.))));
        else
            return 0.;*/
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    double S(2.);
    double max_r(100.);
    double max_theta(2.*std::atan(1.));
    double dt(0.0025), max_t(62.), output_start(60.), output_skip(dt);
    char path[100], filename[100];
    std::sprintf(path, "dat/1308/forcing");
    std::size_t num_r(401), num_theta(101);

    args.option("--dt", dt);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--omega_mult", Rotation::omega_mult);
    args.option("--output_skip", output_skip);
    args.option("--output_start", output_start);
    args.option("--path", path);
    args.option("--wobble", Rotation::wobble_amp);
    args.option("--S", S);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    File_Output files(path);
    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(3);
    std::size_t output_from(output_start/dt - 1);
    std::size_t skip(output_skip/dt);
    Rotation::S = S;

    Mesh_1D<double> mesh_r, mesh_theta;
    mesh_r.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta.uniform_mesh(0., max_theta, num_theta, num_vars);
    Mesh_1D<double> mesh_pole_steady(mesh_r), mesh_equator_steady(mesh_r);
    Mesh_1D<double> mesh_pole(mesh_r), mesh_equator(mesh_r);
    Mesh_2D<double> mesh_bl_steady(mesh_r, mesh_theta),
        mesh_bl(mesh_r, mesh_theta);

    for (std::size_t i(0); i < num_r; ++i)
    {
        double r(mesh_pole_steady.node(i));
        mesh_pole_steady(i, v) = S + (1. - S)*std::exp(-r*r);
        mesh_equator_steady(i, v) = S + (1. - S)*std::exp(-r*r);
    }

    // Solvers for pole and equator
    Edge_Streamfunction pole_steady(mesh_pole_steady);
    Edge_Streamfunction equator_steady(mesh_equator_steady);
    Elliptic_Boundary_Layer bl_steady(mesh_pole_steady, mesh_equator_steady,
            mesh_bl_steady);

    pole_steady.set_edge("pole");
    pole_steady.spin_up_parameter() = S;
    pole_steady.set_sphere_rotation(&Rotation::wobble);
    equator_steady.set_edge("equator");
    equator_steady.spin_up_parameter() = S;
    equator_steady.set_sphere_rotation(&Rotation::wobble);
    bl_steady.spin_up_parameter() = S;
    bl_steady.set_external_bump(&Rotation::wobble);

    pole_steady.solve_steady();
    equator_steady.solve_steady();

    /*for (std::size_t i(0); i < num_r; ++i)
        for (std::size_t j(0); j < num_theta; ++j)
            mesh_bl_steady.set_vars_at_node(i, j, (
                        mesh_pole_steady.get_vars_at_node(i)
                        + double(j)/double(num_theta - 1)*(
                            mesh_equator_steady.get_vars_at_node(i)
                            - mesh_pole_steady.get_vars_at_node(i))));

    bl_steady.solve_steady();

    files.open("steady", "steady_DO_NOT_TOUCH.dat");
    mesh_bl_steady.dump(10, files("steady"));
    files.close("steady");*/
    
    mesh_bl_steady.load_data("dat/1308/forcing/steady_DO_NOT_TOUCH.dat");

    /*Linearised_Perturbed_Edge pole_problem(mesh_pole_steady, mesh_pole);
    Linearised_Perturbed_Edge equator_problem(
            mesh_equator_steady, mesh_equator);
    Linearised_Perturbed_Layer bl_problem(
            mesh_pole, mesh_equator, mesh_bl_steady, mesh_bl);

    pole_problem.set_edge("pole");
    pole_problem.set_sphere_rotation(&Rotation::wobble);
    equator_problem.set_edge("equator");
    equator_problem.set_sphere_rotation(&Rotation::wobble);
    bl_problem.set_sphere_rotation(&Rotation::wobble);*/

    std::sprintf(filename, "%.4f/TESTING", Rotation::omega_mult);
    files.open("test", filename);
    files("test") << "Directory is ready for output of nonlinear code.\n";
    files.close("test");

    try
    {
        for (std::size_t step(0); step < num_steps; ++step)
        {
            pole_steady.step(dt);
            equator_steady.step(dt);
            bl_steady.step(dt);

            if (step >= output_from && (step - output_from)%skip == 0)
            {
                std::sprintf(filename, "%.4f/%.4f", Rotation::omega_mult,
                        bl_steady.time());
                files.open("out", filename);
                bl_steady.dump_flow_variables(files("out"));
                files("out").flush();
                files.close("out");
                /*for (std::size_t i(0); i < num_r; ++i)
                {
                    files("out") << mesh_pole_steady.node(i) << ' ';
                    files("out") << -mesh_pole_steady(i, fr) << ' ';
                    files("out") << mesh_pole_steady(i, v) << ' ';
                    files("out") << 2.*mesh_pole_steady(i, f) << '\n';
                }

                files("out") << "\n\n";
                files("out").flush();*/
            }
        }
    }
    catch (std::exception& e)
    {
    }

    files.close_all();
    
    return 0;
}
