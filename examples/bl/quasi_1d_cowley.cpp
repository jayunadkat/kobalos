#include <iostream>

#include <boundary_layer/Quasi_1D_Cowley.h>
#include <Console_Output.h>
#include <File_Output.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

using namespace Kobalos;

int main(int argc, char** argv)
{
    File_Output files("./dat/15/dumps");
    Mesh_1D<double> wavenumbers;
    Mesh_2D<double> steady_mesh;

    wavenumbers.load_data(
            "./dat/15/dumps/local_wavenumbers_and_phase_speed.dat");
    steady_mesh.load_data(
            "./dat/15/Ro-0.300_201_701_1.0_20_unscaled/Ro-0.300_mesh.dat");

    Quasi_1D_Cowley problem(steady_mesh, wavenumbers);
    std::size_t num_r(steady_mesh.num_nodes_x()),
        num_theta(steady_mesh.num_nodes_y());

    for (std::size_t i(1); i < num_theta - 1; ++i)
    {
        files.open("eval", ("eval_" + str_convert(i) + ".dat").c_str());
        files.open("evec", ("evec_" + str_convert(i) + ".dat").c_str());
        problem.set_theta_index(i);
        problem.solve();

        for (std::size_t j(0), sol_size(problem.evp_system().solution_size());
                j < sol_size; ++j)
        {
            std::complex<double> eval(problem.evp_system().eigenvalue(j));
            files("eval") << j << " "
                << eval.real() << " " << eval.imag() << std::endl;
        }

        const Generalised_Eigenvalue_System_Complex& evp(problem.evp_system());
        for (std::size_t j(0); j < num_r; ++j)
        {
            files("evec") << steady_mesh.node_x(j);
            for (std::size_t k(0), sol_size(evp.solution_size()); k < sol_size;
                    ++k)
            {
                files("evec")
                    << " " << evp.eigenvector_real(k)[3*j + 0]
                    << " " << evp.eigenvector_imag(k)[3*j + 0]
                    << " " << evp.eigenvector_real(k)[3*j + 1]
                    << " " << evp.eigenvector_imag(k)[3*j + 1]
                    << " " << evp.eigenvector_real(k)[3*j + 2]
                    << " " << evp.eigenvector_imag(k)[3*j + 2];
            }
            files("evec") << std::endl;
        }

        files.close("eval");
        files.close("evec");
    }

    return 0;
}

