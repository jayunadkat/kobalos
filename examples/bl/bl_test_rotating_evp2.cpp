#include <cmath>
#include <fstream>

#include <boundary_layer/Rotating_Edge_EVP.h>
#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Mesh
{
    double power(2.);

    double map(const double& s)
    {
        return std::pow(s + 0.1, power) - std::pow(0.1, power);
    }

    double inv_map(const double& x)
    {
        return std::pow(x + std::pow(0.1, power), 1./power) - 0.1;
    }

    double map_ds(const double& s)
    {
        return power*std::pow(s + 0.1, power - 1.);
    }

    double map_ds2(const double& s)
    {
        return power*(power - 1.)*std::pow(s + 0.1, power - 2.);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double begin_Ro(-0.001), end_Ro(-1.5), step_Ro(-0.001);
    double max_r(30.), parameter(0.);
    std::size_t num_r(201), num_vars(3);
    char full_path[150], type_string[16];
    bool spatial_evp(false);

    std::ofstream data;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--param", parameter);
    args.option("--power", Mesh::power);
    args.option("--spatial", spatial_evp);
    args.option("--begin_rossby", begin_Ro);
    args.option("--end_rossby", end_Ro);

    if (spatial_evp)
        std::sprintf(type_string, "_spatial");
    else
        std::sprintf(type_string, "_temporal");

    std::sprintf(full_path, "/tmp/unadkat/evp2/eval_%.3f_%u%s.dat",
            end_Ro, unsigned(num_r), type_string);
    data.open(full_path);

    Mesh_1D<double> mesh;
    mesh.mapped_grid(0., max_r, num_r, num_vars, &Mesh::map, &Mesh::inv_map,
            &Mesh::map_ds, &Mesh::map_ds2);
    for (std::size_t i(0); i < num_r; ++i)
    {
        double r(mesh.unmapped_node(i)), r2(r);
        mesh(i, f) = 0.5*(1. - std::exp(-r*r));
        mesh(i, fr) = r*std::exp(-r*r);
        if (begin_Ro < -1.)
            r2 -= 1.;
        mesh(i, v) = r2*std::exp(-r*r);
    }

    Rotating_Edge_EVP problem(mesh);
    problem.set_parameter(parameter);
    if (spatial_evp)
        problem.spatial_evp_problem();
    else
        problem.temporal_evp_problem();

    for (double rossby(begin_Ro); rossby > end_Ro - 0.5*step_Ro;
            rossby += step_Ro)
    {
        problem.set_signed_rossby_number(rossby);
        problem.solve();

/*        for (std::size_t i(0), size(problem.evp_system().solution_size());
                i < size; ++i)
        {
            if (problem.evp_system().is_complex(i))
            {
                data << "# " << rossby << std::endl
                    << problem.evp_system().eigenvalue(i)[0] << ' '
                    << problem.evp_system().eigenvalue(i)[1] << std::endl
                    << problem.evp_system().eigenvalue(i)[0] << ' '
                    << -problem.evp_system().eigenvalue(i)[1] << std::endl;
            }
            else
            {
                data << problem.evp_system().eigenvalue(i)[0] << ' '
                    << problem.evp_system().eigenvalue(i)[1] << std::endl;
            }
        }
        data << std::endl << std::endl;*/

        std::size_t smallest(0);
        for (std::size_t size(problem.evp_system().solution_size());
                smallest < size; ++smallest)
            if (problem.evp_system().eigenvalue(smallest)[0] > 0.)
                break;
        /*smallest = problem.evp_system().solution_size() - 1;
        for (; smallest < problem.evp_system().solution_size(); --smallest)
            if (std::isfinite(problem.evp_system().eigenvalue(smallest)[0]))
                break;*/
        data
            << rossby << ' '
            << problem.evp_system().eigenvalue(smallest)[0] << ' '
            << problem.evp_system().eigenvalue(smallest)[1] << std::endl;
    }

    data.close();

    return 0;
}

