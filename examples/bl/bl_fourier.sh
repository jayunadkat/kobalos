#! /bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: bl_fourier.sh omega_mult_start omega_mult_end"
    exit
fi

omega=$1
omega_end=$2
while [ $(echo $omega $omega_end | awk '{if ($1 <= $2) {print "0"} else {print "1"} }') == "0" ]; do
    ./bl_fourier.out --omega_mult $omega
    omega=$(echo "$omega + 0.01" | bc)
done
