// Solves steady problem for \(S=1.5\) on \(r\in[0,200]\) with
// \(\Delta r=0.05\). 

#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

const double Ro_to_S(const double& Ro, const bool& spin_down)
{
    double temp(Ro);
    if (spin_down)
        temp *= -1.;
    return 1./(1. + temp);
}

void invert(Mesh_1D<double>& mesh)
{
    double num_nodes(mesh.num_nodes());
    for (std::size_t i(0); i < num_nodes; ++i)
        mesh(i, v) *= -1.;
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double Ro(0.);
    double max_r(200.);
    std::size_t num_r(4001), num_vars(3);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "dat/");
    std::ofstream data;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--path", path);

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");
    std::sprintf(full_path, path);
    std::sprintf(filename, "Ro_search.dat");
    std::strcat(full_path, filename);
    data.open(full_path);

    Mesh_1D<double> mesh_p, mesh_e;
    mesh_p.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_e.uniform_mesh(0., max_r, num_r, num_vars);

    Rotating_Edge_Streamfunction pole_problem(mesh_p);
    Rotating_Edge_Streamfunction equator_problem(mesh_e);
    pole_problem.set_edge("pole");
    equator_problem.set_edge("equator");

    try
    {
        // Initial guess
        {
            double r(0.);
            for (std::size_t i(0); i < num_r; ++i)
            {
                r = mesh_p.node(i);
                mesh_p(i, f) = -0.5*std::exp(-r*r);
                mesh_p(i, fr) = r*std::exp(-r*r);
                mesh_p(i, v) = -std::exp(-r*r);
            }
        }

        std::cout << "Solving for spin-down at the pole...\n";
        for (Ro = 0.001; Ro < 2.; Ro += 0.001)
        {
            pole_problem.set_signed_rossby_number(-Ro);
            pole_problem.solve_steady();
            data << Ro << ' ' << mesh_p(num_r - 1, f) << std::endl;
        }
    }
    catch (std::exception& e)
    {
        std::cout << "Caught at Ro = "
            << pole_problem.signed_rossby_number() << '\n';
    }
    data << std::endl << std::endl;

    try
    {
        // Initial guess
        {
            double r(0.);
            for (std::size_t i(0); i < num_r; ++i)
            {
                r = mesh_p.node(i);
                mesh_e(i, f) = -0.5*std::exp(-r*r);
                mesh_e(i, fr) = r*std::exp(-r*r);
                mesh_e(i, v) = -std::exp(-r*r);
            }
        }

        std::cout << "Solving for spin-down at the equator...\n";
        for (Ro = 0.001; Ro < 3.5; Ro += 0.001)
        {
            equator_problem.set_signed_rossby_number(-Ro);
            equator_problem.solve_steady();
            data << Ro << ' ' << mesh_e(num_r - 1, f) << std::endl;
        }
    }
    catch (std::exception& e)
    {
        std::cout << "Caught at Ro = "
            << equator_problem.signed_rossby_number() << '\n';
    }
    data << std::endl << std::endl;

    try
    {
        // Initial guess
        {
            double r(0.);
            for (std::size_t i(0); i < num_r; ++i)
            {
                r = mesh_p.node(i);
                mesh_p(i, f) = 0.5*std::exp(-r*r);
                mesh_p(i, fr) = -r*std::exp(-r*r);
                mesh_p(i, v) = std::exp(-r*r);
            }
        }

        std::cout << "Solving for spin-up at the pole...\n";
        for (Ro = 0.001; Ro < 10.0005; Ro += 0.001)
        {
            pole_problem.set_signed_rossby_number(Ro);
            pole_problem.solve_steady();
            data << Ro << ' ' << mesh_p(num_r - 1, f) << std::endl;
        }
    }
    catch (std::exception& e)
    {
        std::cout << "Caught at Ro = "
            << pole_problem.signed_rossby_number() << '\n';
    }
    data << std::endl << std::endl;

    try
    {
        std::cout << "Solving for large spin-up at the pole...\n";
        for (Ro = 10.1; Ro < 100.05; Ro += 0.1)
        {
            pole_problem.set_signed_rossby_number(Ro);
            pole_problem.solve_steady();
        }
        
        invert(mesh_p);

        std::cout << "Solving for large spin-down at the pole...\n";
        for (; Ro > 10.05; Ro -= 0.1)
        {
            pole_problem.set_signed_rossby_number(-Ro);
            pole_problem.solve_steady();
        }
        for (; Ro > 0.; Ro -= 0.001)
        {
            pole_problem.set_signed_rossby_number(-Ro);
            pole_problem.solve_steady();
            data << Ro << ' ' << mesh_p(num_r - 1, f) << std::endl;
        }
    }
    catch (std::exception& e)
    {
        std::cout << "Caught at Ro = "
            << pole_problem.signed_rossby_number() << '\n';
    }
    data << std::endl << std::endl;

    data.close();

    return 0;
}

