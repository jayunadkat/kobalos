#include <cmath>
#include <fstream>

#include <boundary_layer/Rotating_Edge_EVP.h>
#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Mesh
{
    double power(2.);

    double map(const double& s)
    {
        return std::pow(s + 0.1, power) - std::pow(0.1, power);
    }

    double inv_map(const double& x)
    {
        return std::pow(x + std::pow(0.1, power), 1./power) - 0.1;
    }

    double map_ds(const double& s)
    {
        return power*std::pow(s + 0.1, power - 1.);
    }

    double map_ds2(const double& s)
    {
        return power*(power - 1.)*std::pow(s + 0.1, power - 2.);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double signed_Ro(-1.2);
    double max_r(30.), parameter(0.);
    std::size_t num_r(201), num_vars(3);
    char full_path[150], type_string[16];
    bool spatial_evp(false);

    std::ofstream data;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--param", parameter);
    args.option("--power", Mesh::power);
    args.option("--spatial", spatial_evp);
    args.option("--rossby", signed_Ro);

    if (spatial_evp)
        std::sprintf(type_string, "_spatial");
    else
        std::sprintf(type_string, "_temporal");

    std::sprintf(full_path, "/tmp/unadkat/evp3/evec_%.3f_%u_%.1f%s.dat",
            signed_Ro, unsigned(num_r), Mesh::power, type_string);
    data.open(full_path);
    data.precision(16);

    Mesh_1D<double> mesh;
    mesh.mapped_grid(0., max_r, num_r, num_vars, &Mesh::map, &Mesh::inv_map,
            &Mesh::map_ds, &Mesh::map_ds2);
    for (std::size_t i(0); i < num_r; ++i)
    {
        double r(mesh.unmapped_node(i)), r2(r);
        mesh(i, f) = 0.5*(1. - std::exp(-r*r));
        mesh(i, fr) = r*std::exp(-r*r);
        if (signed_Ro < -1.)
            r2 -= 1.;
        mesh(i, v) = r2*std::exp(-r*r);
    }

    Rotating_Edge_EVP problem(mesh);
    problem.set_parameter(parameter);
    if (spatial_evp)
        problem.spatial_evp_problem();
    else
        problem.temporal_evp_problem();

    problem.set_signed_rossby_number(signed_Ro);
    problem.solve();

    std::size_t ev_index(0);
    for (std::size_t size(problem.evp_system().solution_size());
            ev_index < size; ++ev_index)
    {
        if (problem.evp_system().eigenvalue(ev_index)[0] > 0.)
            break;
    }

    data << "# e-val: "
        << problem.evp_system().eigenvalue(ev_index)[0] << " +-i"
        << problem.evp_system().eigenvalue(ev_index)[1] << std::endl;
    double dr(0.5/(mesh.node(1) - mesh.node(0)));
    data << mesh.unmapped_node(0) << ' '
        << problem.evp_system().eigenvector_real(ev_index)[3*0 + f] << ' '
        << problem.evp_system().eigenvector_imag(ev_index)[3*0 + f] << ' '
        << problem.evp_system().eigenvector_real(ev_index)[3*0 + fr] << ' '
        << problem.evp_system().eigenvector_imag(ev_index)[3*0 + fr] << ' '
        << dr*(
                - 3.*problem.evp_system().eigenvector_real(
                    ev_index)[3*0 + fr]
                + 4.*problem.evp_system().eigenvector_real(
                    ev_index)[3*1 + fr]
                - 1.*problem.evp_system().eigenvector_real(
                    ev_index)[3*2 + fr]
              )/mesh.map_ds(mesh.node(0)) << ' '
        << dr*(
                - 3.*problem.evp_system().eigenvector_imag(
                    ev_index)[3*0 + fr]
                + 4.*problem.evp_system().eigenvector_imag(
                    ev_index)[3*1 + fr]
                - 1.*problem.evp_system().eigenvector_imag(
                    ev_index)[3*2 + fr]
              )/mesh.map_ds(mesh.node(0)) << std::endl;
    for (std::size_t i(1); i < num_r - 1; ++i)
    {
        data << mesh.unmapped_node(i) << ' '
            << problem.evp_system().eigenvector_real(ev_index)[3*i + f] << ' '
            << problem.evp_system().eigenvector_imag(ev_index)[3*i + f] << ' '
            << problem.evp_system().eigenvector_real(ev_index)[3*i + fr] << ' '
            << problem.evp_system().eigenvector_imag(ev_index)[3*i + fr] << ' '
            << dr*(problem.evp_system().eigenvector_real(
                        ev_index)[3*(i + 1) + fr] - 
                    problem.evp_system().eigenvector_real(
                        ev_index)[3*(i - 1) + fr]) << ' '
            << dr*(problem.evp_system().eigenvector_imag(
                        ev_index)[3*(i + 1) + fr] - 
                    problem.evp_system().eigenvector_imag(
                        ev_index)[3*(i - 1) + fr]) << std::endl;
    }
    data << mesh.unmapped_node(num_r - 1) << ' '
        << problem.evp_system().eigenvector_real(
                ev_index)[3*(num_r - 1) + f] << ' '
        << problem.evp_system().eigenvector_imag(
                ev_index)[3*(num_r - 1) + f] << ' '
        << problem.evp_system().eigenvector_real(
                ev_index)[3*(num_r - 1) + fr] << ' '
        << problem.evp_system().eigenvector_imag(
                ev_index)[3*(num_r - 1) + fr] << ' '
        << dr*(
                + 3.*problem.evp_system().eigenvector_real(
                    ev_index)[3*(num_r - 1) + fr]
                - 4.*problem.evp_system().eigenvector_real(
                    ev_index)[3*(num_r - 2) + fr]
                + 1.*problem.evp_system().eigenvector_real(
                    ev_index)[3*(num_r - 3) + fr]
              )/mesh.map_ds(mesh.node(num_r - 1)) << ' '
        << dr*(
                + 3.*problem.evp_system().eigenvector_imag(
                    ev_index)[3*(num_r - 1) + fr]
                - 4.*problem.evp_system().eigenvector_imag(
                    ev_index)[3*(num_r - 2) + fr]
                + 1.*problem.evp_system().eigenvector_imag(
                    ev_index)[3*(num_r - 3) + fr]
              )/mesh.map_ds(mesh.node(num_r - 1)) << std::endl;

    data.close();

    return 0;
}

