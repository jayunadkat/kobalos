// 2D boundary layer problem with forcing

#include <cmath>
#include <cstring>
#include <fstream>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction_Unscaled.h>
#include <boundary_layer/Rotating_Fourier_Decomposed_Layer_Unscaled.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Mesh
{
    double p(1.), off(0.);

    double map(const double& s) {return std::pow(s+off, p);}
    double inv_map(const double& x) {return std::pow(x, 1./p)-off;}
    double map_ds(const double& s) {return p*std::pow(s+off, p-1.);}
    double map_ds2(const double& s) {return p*(p-1.)*std::pow(s+off, p-2.);}
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    // Parameters
    double signed_Ro(-0.5), Ro_iterate(-0.01), omega(0.);
    bool steady(false), convert(false);
    double max_r(100.), max_theta(2.*std::atan(1.));
    char path[100], filename[100];
    std::sprintf(path, "dat/1308-1311/fourier_unscaled");
    std::size_t num_r(1001), num_theta(101);
    std::size_t num_vars(3);

    // Grab command-line options
    args.option("--dump_flow", convert);
    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--omega", omega);
    args.option("--path", path);
    args.option("--power", Mesh::p);
    args.option("--rossby", signed_Ro);
    args.option("--steady", steady);

#ifndef QUIET
    // Done with options/flag parsing
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET
    
    // File output object
    File_Output files(path);

    // Prepare meshes
    Mesh_1D<double> mesh_r, mesh_theta;
    Mesh_1D<std::complex<double> > mesh_r_complex, mesh_theta_complex;

    mesh_r.mapped_grid(0., max_r, num_r, num_vars,
          Mesh::map, Mesh::inv_map, Mesh::map_ds, Mesh::map_ds2);
//    mesh_r.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta.uniform_mesh(0., max_theta, num_theta, num_vars);
    mesh_r_complex.mapped_grid(0., max_r, num_r, num_vars,
          Mesh::map, Mesh::inv_map, Mesh::map_ds, Mesh::map_ds2);
//    mesh_r_complex.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta_complex.uniform_mesh(0., max_theta, num_theta, num_vars);

    Mesh_1D<double> mesh_pole_steady(mesh_r), mesh_equator_steady(mesh_r);
    Mesh_2D<double> mesh_bl_steady(mesh_r, mesh_theta),
        mesh_bl_steady_inertial(mesh_r, mesh_theta);
    Mesh_2D<std::complex<double> > mesh_bl(mesh_r_complex, mesh_theta_complex);

    // Steady solvers for pole and equator
    Rotating_Edge_Streamfunction pole_steady(mesh_pole_steady);
    Rotating_Edge_Streamfunction equator_steady(mesh_equator_steady);
    Rotating_Layer_Streamfunction_Unscaled bl_steady(mesh_bl_steady);

    // Compute steady solution in BL
    if (steady)
    {
        pole_steady.set_edge("pole");
        equator_steady.set_edge("equator");

        for (; Ro_iterate > signed_Ro; Ro_iterate -= 0.01)
        {
            std::cout << "Solving attachment points for Ro = " << Ro_iterate
                << std::endl;
            pole_steady.set_signed_rossby_number(Ro_iterate);
            equator_steady.set_signed_rossby_number(Ro_iterate);

            // Compute steady solutions at pole and equator
            pole_steady.solve_steady();
            equator_steady.solve_steady();
        }

        pole_steady.set_signed_rossby_number(signed_Ro);
        equator_steady.set_signed_rossby_number(signed_Ro);
        bl_steady.set_signed_rossby_number(signed_Ro);

        // Compute steady solutions at pole and equator
        pole_steady.solve_steady();
        equator_steady.solve_steady();

        // Initial guess for BL
        for (std::size_t j(0); j < num_theta; ++j)
        {
            double pos(mesh_theta.unmapped_node(j)/max_theta);
            for (std::size_t i(0); i < num_r; ++i)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    mesh_bl_steady(i, j, k) = mesh_pole_steady(i, k) + pos*(
                            mesh_equator_steady(i, k) - mesh_pole_steady(i, k));
                }
            }
        }

        for (std::size_t j(0); j < num_theta; ++j)
        {
            double theta(mesh_theta.unmapped_node(j));
            double s(std::sin(theta)), c(std::cos(theta));
            for (std::size_t i(0); i < num_r; ++i)
            {
                mesh_bl_steady(i, j, f) *= s*s*c;
                mesh_bl_steady(i, j, fr) *= s*s*c;
                mesh_bl_steady(i, j, v) *= s;
            }
        }

        bl_steady.solve_steady();

        std::sprintf(filename, "steady_Ro%.2f_%u_%u_raw.dat", signed_Ro,
                static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
        files.open("steady", filename);
        std::sprintf(filename, "steady_Ro%.2f_%u_%u_flow.dat", signed_Ro,
                static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
        files.open("actual", filename);
        mesh_bl_steady.dump(10, files("steady"));
        bl_steady.dump_flow_variables(files("actual"), 10);
        files.close_all();
        return 0;
    }
    else
    {
        std::sprintf(filename, "%s/steady_Ro%.2f_%u_%u_raw.dat", path,
                signed_Ro, static_cast<unsigned>(num_r),
                static_cast<unsigned>(num_theta));
        mesh_bl_steady.load_data(filename);
    }

    // Spectral solvers
    Rotating_Fourier_Decomposed_Layer_Unscaled bl_problem(
            mesh_bl_steady, mesh_bl);

    // Solution parameters
    bl_problem.omega() = omega;
    bl_problem.set_signed_rossby_number(signed_Ro);

    // Compute spectral solutions
    bl_problem.solve();

    // Set up file output
    std::sprintf(filename, "fourier%.2f_%.2f_%u_%u.dat", signed_Ro, omega,
            static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
    files.open("out", filename);
    mesh_bl.dump_real(10, files("out"));

    // More file output
    std::sprintf(filename, "fourier%.2f_%.2f_%u_%u_flow.dat", signed_Ro, omega,
            static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
    files.open("out2", filename);
    bl_problem.dump_flow_variables(files("out2"), 10);

    if (convert)
    {
        std::sprintf(filename, "fourier%.2f_%.2f_%u_%u.decay", signed_Ro, omega,
                static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
        files.open("actual", filename);

        // Compute decay rates and dump
        std::complex<double> temp;
        for (std::size_t j(0); j < num_theta; ++j)
        {
            for (std::size_t index(1); index < num_r - 1; ++index)
            {
                temp = (mesh_bl(index + 1, j, v) - mesh_bl(index - 1, j, v))
                    /(mesh_bl.unmapped_node_x(index + 1)
                            - mesh_bl.unmapped_node_x(index - 1));
                temp /= mesh_bl(index, j, v);
                files("actual") << mesh_bl.unmapped_node_y(j) << ' '
                    << mesh_bl.unmapped_node_x(index) << ' '
                    << temp.real() << ' ' << temp.imag() << std::endl;
            }
            files("actual") << std::endl;
        }
    }

    // Clean up
    files.close_all();
    
    return 0;
}
