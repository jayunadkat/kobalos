#include <cmath>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <string>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

double phi(const double& x)
{
    if (x < 0.)
        return 0.;
    else
        return std::exp(-1./(x*x));
}

double bump(const double& a, const double& b, const double& t)
{
    double left(0.), right(0.);
    if (b < a)
    {
        left = b;
        right = a;
    }
    else
    {
        left = a;
        right = b;
    }
    double magnitude(std::pow(phi(0.5*(right - left)), 2.));
    return phi(t - left)*phi(right - t)/magnitude;
}

namespace Rotation
{
    double signed_Ro(-1.5);
    double wobble_rate(0.);

    double steady_spin(const double& t)
    {
        return 1.;
    }

    double spin(const double& t)
    {
        return 1. + 0.1*bump(0., 0.5, t);
    }

    double wobble(const double& t)
    {
        return 0.1*std::sin(wobble_rate*t);
    }
}

namespace Mesh_Power2
{
    double map(const double& s)
    {
        return std::pow(s + 1, 2.) - 1;
    }

    double inv_map(const double& x)
    {
        return std::sqrt(x + 1) - 1;
    }

    double map_ds(const double& s)
    {
        return 2.*(s + 1);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double signed_Ro(-1.5), rossby(-0.05), rossby_step(-0.01);
    double max_r(50.), dt(0.01), max_t(5.);
    std::size_t num_r(1001), num_vars(3);
    char path[100], filename[50], full_path[150];
    std::string edge("");
    bool uniform(false);

    std::ofstream data;

    args.option("--dt", dt);
    args.option("--edge", edge);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--rossby", signed_Ro);
    args.option("--uniform", uniform);

    if (!((edge == "pole") | (edge == "equator")))
    {
        std::cout << "You must specify --edge <pole|equator>" << std::endl;
        return 1;
    }

    std::sprintf(path, "/tmp/unadkat/steady_%s", edge.c_str());
    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");

    Mesh_1D<double> mesh;
    if (uniform)
        mesh.uniform_mesh(0., max_r, num_r, num_vars);
    else
        mesh.mapped_grid(0., max_r, num_r, num_vars,
                &Mesh_Power2::map, &Mesh_Power2::inv_map, &Mesh_Power2::map_ds);

    Rotating_Edge_Streamfunction problem(mesh);
    problem.set_edge(edge);
    problem.set_sphere_spin(&Rotation::steady_spin);

    try
    {
        do
        {
            problem.set_signed_rossby_number(rossby);
            std::cout << "SOLVING RO = " << rossby << std::endl;
            problem.solve_steady();

            std::sprintf(filename, "Ro%.3f_mesh.dat", rossby);
            std::sprintf(full_path, "%s%s", path, filename);
            data.open(full_path);
            mesh.dump(data);
            data.close();

            std::sprintf(filename, "Ro%.3f.dat", rossby);
            std::sprintf(full_path, "%s%s", path, filename);
            data.open(full_path);
            problem.dump_flow_variables(data);
            data.close();

            rossby += rossby_step;
        } while (rossby > signed_Ro + 0.5*rossby_step);
    }
    catch (std::exception& e)
    {
        if (data.is_open())
        {
            data.flush();
            data.close();
        }
    }

    return 0;
}

