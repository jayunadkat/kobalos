#include <cmath>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <string>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Rotation
{
    double k(10.);

    double spin(const double& t)
    {
        return 1. - std::exp(-k*t*t);
    }
}

namespace Mesh_Power2
{
    double map(const double& s)
    {
        return std::pow(s + 0.1, 2.) - 0.01;
    }

    double inv_map(const double& x)
    {
        return std::sqrt(x + 0.01) - 0.1;
    }

    double map_ds(const double& s)
    {
        return 2.*(s + 0.1);
    }
}

namespace Mesh_Uniform
{
    double map(const double& s)
    {
        return s;
    }

    double map_ds(const double& s)
    {
        return 1.;
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double signed_Ro(-0.5);
    double max_r(50.), dt(0.01), max_t(5.);
    std::size_t num_r(1001), num_vars(3);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "dat/");
    std::string edge(""), uniform_string("");
    bool uniform(false);

    std::ofstream data;

    args.option("--dt", dt);
    args.option("--edge", edge);
    args.option("--k", Rotation::k);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--rossby", signed_Ro);
    args.option("--uniform", uniform);

    if (uniform)
        uniform_string = "unif";
    else
        uniform_string = "map";

    if (!((edge == "pole") | (edge == "equator")))
    {
        std::cout << "You must specify --edge <pole|equator>" << std::endl;
        return 1;
    }

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");
    std::sprintf(full_path, path);
    std::sprintf(filename, "%s_%u.%s", edge.c_str(), unsigned(num_r),
            uniform_string.c_str());
    std::strcat(full_path, filename);
    data.open(full_path);

    Mesh_1D<double> mesh;
    if (uniform)
        mesh.uniform_mesh(0., max_r, num_r, num_vars);
    else
        mesh.mapped_grid(0., max_r, num_r, num_vars,
                &Mesh_Power2::map, &Mesh_Power2::inv_map, &Mesh_Power2::map_ds);

    Rotating_Edge_Streamfunction problem(mesh);
    problem.set_edge(edge);
    problem.set_signed_rossby_number(signed_Ro);
    problem.set_sphere_spin(&Rotation::spin);

    //problem.solve_steady();
    //problem.dump_flow_variables(data);
    //return 0;
    try
    {
        do
        {
            problem.step(dt);
            data
                << problem.time() << ' '
                << -mesh.derivative_at_node(0, fr) << std::endl;
        } while (problem.time() < max_t - 0.5*dt);
    }
    catch (std::exception& e)
    {
        data.flush();
    }

    if (data.is_open())
        data.close();

    return 0;
}

