// Spin up from rest problem, parabolic marching from equator to pole

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <PDE_Bundle.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace BoundaryLayer
{
    double S(0.);

    double Omega(const double& t)
    {
        return 1. + (S - 1)*std::exp(-t*t);
    }

    class Pole_Problem : public Equation_1D<double>
    {
        protected:
            void update_jacobian_t() {}
            void update_jacobian_x() {}

        public:
            Pole_Problem() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u]
                    - state[v]*state[v] + S*S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }

            void jacobian_t_vec(const Vector<double>& state) {}
            void jacobian_x_vec(const Vector<double>& state) {}
    };

    class Equator_Problem : public Equation_1D<double>
    {
        protected:
            void update_jacobian_t() {}
            void update_jacobian_x() {}

        public:
            Equator_Problem() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] - state[u]*state[u]
                    - state[v]*state[v] + S*S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr];
                residual_[w] = state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }

            void jacobian_t_vec(const Vector<double>& state) {}
            void jacobian_x_vec(const Vector<double>& state) {}
    };

    class BC_Left : public Residual<double>
    {
        public:
            BC_Left() : Residual<double>(3, 5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t());
                residual_[2] = state[w];
            }
    };

    class BC_Right : public Residual<double>
    {
        public:
            BC_Right() : Residual<double>(2, 5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - S;
            }
    };

    class Full_Problem : public Equation_2D<double>
    {
        public:
            Full_Problem() : Equation_2D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur]
                    + std::cos(2*x2())*state[u]*state[u] - state[v]*state[v]
                    + S*S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr]
                    + (1 + std::cos(2*x2()))*state[u]*state[v];
                residual_[w] = -(1 + 3*std::cos(2*x2()))*state[u]/2.;
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }

            void matrix_y(const Vector<double>& state)
            {
                matrix_y_(ur, u) = -std::sin(2*x2())*state[u]/2.;
                matrix_y_(vr, v) = -std::sin(2*x2())*state[u]/2.;
                matrix_y_(w, u) = std::sin(2*x2())/2.;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        std::cout << "Usage: bl_basic.out\n";
        return 0;
    }
    
    // Data output objects
    std::stringstream filename;
    std::ofstream data_soln, data_values;

    // Spatial/time domain parameters
    const double pi(4.*std::atan(1.));
    double left_r(0.), right_r(15.), left_theta(pi/2.), right_theta(0.);
    double dt(0.005);
    std::size_t num_nodes_r(201), num_nodes_theta(201), max_steps(30./dt);

    // Instantiate equation/boundary condition objects for polar
    // and full boundary layer problems
    BoundaryLayer::Equator_Problem equator_eqn;
    BoundaryLayer::BC_Left bc_left;
    BoundaryLayer::BC_Right bc_right;
    BoundaryLayer::Full_Problem full_bl_eqn;
    BoundaryLayer::S = 1.5;

    // Set up meshes
    Mesh_1D<double> mesh_r, mesh_theta;

    mesh_r.power_mesh(left_r, right_r, num_nodes_r, equator_eqn.order(), 2.,
            Mesh_Left);
    mesh_theta.uniform_mesh(left_theta, right_theta, num_nodes_theta,
            equator_eqn.order());

    Mesh_1D<double> mesh_equator(mesh_r);
    Mesh_2D<double> mesh_bl(mesh_r, mesh_theta);
    
    // Set up conditions objects
    Conditions_1D<double> conditions_equator(mesh_equator);
    Conditions_2D<double> conditions_bl(mesh_bl);

    conditions_equator.add(left_r, bc_left);
    conditions_equator.add(right_r, bc_right);
    conditions_bl.add_x(left_r, bc_left);
    conditions_bl.add_x(right_r, bc_right);
    conditions_bl.add_y(mesh_equator);

    // Set up initial data (nothing to do if we have zero conditions)
    for (std::size_t i(0); i < num_nodes_r; ++i)
    {
        mesh_equator(i, v) = BoundaryLayer::S;
        for (std::size_t j(0); j < num_nodes_theta; ++j)
            mesh_bl(i, j, v) = BoundaryLayer::S;
    }

    // Create PDE solver objects
    PDE_IBVP<PDE_1D_Unsteady, double, double> equator_solver(
            equator_eqn, conditions_equator, mesh_equator);
    PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double> bl_solver(
            full_bl_eqn, conditions_bl, mesh_bl);

    // Step forward in time and dump output
    for (std::size_t i(0); i < max_steps; ++i)
    {
        equator_solver.step(dt);
        bl_solver.step(dt);

        // Open output files
        filename << "/tmp/dat/big/full_" << bl_solver.time() << ".dat";
        data_soln.open(filename.str().c_str());
        filename.str("");
        filename << "/tmp/dat/big/measure_" << bl_solver.time() << ".dat";
        data_values.open(filename.str().c_str());
        filename.str("");

        // Collect and output data
        mesh_bl.dump(10, data_soln);
        Mesh_1D<double> values;
        values.uniform_mesh(left_theta, right_theta, num_nodes_theta, 2);
        for (std::size_t j(0); j < num_nodes_theta; ++j)
        {
            values(j, 0) = mesh_bl(num_nodes_r - 1, j, w);
            for (std::size_t k(0); k < num_nodes_r; ++k)
                values(j, 1) = std::max(values(j, 1), mesh_bl(k, j, u));
        }
        values.dump(10, data_values);

        // Clean up
        data_soln.close();
        data_values.close();
    }

    return 0;
}

