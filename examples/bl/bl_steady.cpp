// Steady BL problem

#include <cmath>
#include <cstring>
#include <fstream>

#include <boundary_layer/Edge_Streamfunction.h>
#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

using namespace Kobalos;

enum {f, fr, frr, v, vr};
enum {F, FR, V};

namespace Bump
{
    double centre(0.5), height(0.05), width(1.);

    double bump(const double& t)
    {
        if (t <= centre - 0.5*width || t >= centre + 0.5*width)
            return 1.;
        else
            return 1. + height*std::exp(std::pow(0.5*width, -2.))*
                std::exp(-1./(std::pow(0.5*width, 2.)
                            - std::pow(t - centre, 2.)));
    }
}

void flow_3_variable_output(const double& time, const Mesh_2D<double>& mesh,
        std::ofstream& out)
{
    double mid_theta(0.), c(0.), s(0.);

    out << "# Mesh_2D\n";
    out << "# Solution at t = " << time << '\n';
    out << "# theta    r    u    v    w\n# =============================\n";
    for (std::size_t j(0), num_theta(mesh.num_nodes_y()); j < num_theta - 1;
            ++j)
    {
        mid_theta = 0.5*(mesh.node_y(j) + mesh.node_y(j + 1));
        for (std::size_t i(0), num_r(mesh.num_nodes_x()); i < num_r; ++i)
        {
            c = std::cos(mid_theta);
            s = std::sin(mid_theta);
            out << mid_theta << ' ';
            out << mesh.node_x(i) << ' ';
            out << -0.5*(mesh(i, j, FR) + mesh(i, j + 1, FR)) << ' ';
            out << 0.5*(mesh(i, j, V) + mesh(i, j + 1, V)) << ' ';
            out << (3.*c*c - 1.)*0.5*(mesh(i, j, F) + mesh(i, j + 1, F))
                + s*c*(mesh(i, j + 1, F) - mesh(i, j, F))/(
                        mesh.node_y(j + 1) - mesh.node_y(j)) << '\n';
        }
        out << '\n';
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(2);
    double max_r(20.);
    double max_theta(2.*std::atan(1.));
    double dt(0.01), max_t(20.);
    char path[100], filename[50], full_path[150];
    std::sprintf(path, "/tmp/dat/big3/writeup/");
    std::ofstream data;
    std::size_t num_r(101), num_theta(501);
    bool no_output(false);
    std::size_t output_counter(1), output_skip(0);
    bool steady_only(false);

    args.option("--bc", Bump::centre);
    args.option("--bh", Bump::height);
    args.option("--bw", Bump::width);
    args.option("--dt", dt);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--no_output", no_output);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--output_skip", output_skip);
    args.option("--path", path);
    args.option("--S", S);
    args.option("--steady_only", steady_only);

    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(3);

    if (path[std::strlen(path) - 1] != '/')
        std::strcat(path, "/");

    Mesh_1D<double> mesh_r, mesh_theta;
    mesh_r.uniform_mesh(0., max_r, num_r, num_vars);
    mesh_theta.uniform_mesh(0., max_theta, num_theta, num_vars);
    Mesh_1D<double> mesh_equator(mesh_r), mesh_pole(mesh_r);

    for (std::size_t i(0); i < num_r; ++i)
    {
        mesh_pole(i, V) = S;
        mesh_equator(i, V) = S;
    }

    // Solvers for pole and equator
    Edge_Streamfunction pole_problem(mesh_pole), equator_problem(mesh_equator);

    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = S;
    pole_problem.set_sphere_rotation(&Bump::bump);
    equator_problem.set_edge("equator");
    equator_problem.spin_up_parameter() = S;
    equator_problem.set_sphere_rotation(&Bump::bump);

#ifndef QUIET
    std::cout << "\n Solving S = " << S << '\n';
    std::cout << " ===================\n Pole:\n";
#endif  // QUIET

    pole_problem.solve_steady();

#ifndef QUIET
    std::cout << " Equator:\n";
#endif  // QUIET

    equator_problem.solve_steady();

    // Initial guess will be a linear interpolation from mesh_pole to
    // mesh_equator
    Mesh_2D<double> mesh(mesh_r, mesh_theta);
    for (std::size_t i(0); i < num_r; ++i)
        for (std::size_t j(0); j < num_theta; ++j)
            mesh.set_vars_at_node(i, j, (mesh_pole.get_vars_at_node(i)
                        + double(j)/double(num_theta - 1)*(
                            mesh_equator.get_vars_at_node(i)
                            - mesh_pole.get_vars_at_node(i))));

    Elliptic_Boundary_Layer full_problem(mesh_pole, mesh_equator, mesh);
    full_problem.spin_up_parameter() = S;
    full_problem.set_external_bump(&Bump::bump);

    full_problem.solve_steady();

    if (!no_output)
    {
        std::sprintf(filename, "steady_%.1f_%03u_%03u_%.2f.dat",
                max_r, unsigned(num_r), unsigned(num_theta), S);
        std::sprintf(full_path, path);
        std::strcat(full_path, filename);
        data.open(full_path);
        flow_3_variable_output(0., mesh, data);
        data.close();
    }
    
    if (!steady_only)
    {
        // Use steady solutions as initial data for time-stepping
        try
        {
            for (std::size_t step(0); step < num_steps; ++step)
            {
                pole_problem.step(dt);
                equator_problem.step(dt);
                full_problem.step(dt);
                std::cout << '\n';

                if (!no_output && output_counter*(output_skip + 1) <= step + 1)
                {
                    std::sprintf(filename, "sol%03u_%.3f.dat",
                            unsigned(output_counter++),
                            full_problem.time());
                    std::sprintf(full_path, path);
                    std::strcat(full_path, filename);
                    data.open(full_path);
                    flow_3_variable_output(full_problem.time(), mesh, data);
                    data.close();
                }
            }
        }
        catch (std::exception& e)
        {
            std::sprintf(filename, "failure_%.3f.dat",
                    full_problem.time());
            std::sprintf(full_path, path);
            std::strcat(full_path, filename);
            data.open(full_path);
            flow_3_variable_output(full_problem.time(),
                    full_problem.last_solution(), data);
            data.close();
        }
    }
    
    return 0;
}
