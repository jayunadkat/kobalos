// Spin up from rest problem, parabolic marching from equator to pole using
// streamfunction formulation

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>

#include <boundary_layer/Boundary_Layer.h>
#include <Command_Line_Args.h>
#include <PDE_Bundle.h>

enum {f, fr, frr, v, vr};

using namespace Kobalos;

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
    if (argc > 1)
        args.print_recognised_arguments();
    
    // Data output objects
    char filename[50], filename2[50];
    std::ofstream data_soln, data_soln2;

    // Spatial/time domain parameters
    const double pi(4.*std::atan(1.));
    double left_r(0.), right_r(30.), left_theta(pi/2.), right_theta(0.);
    double end_time(20.), dt(0.01), mesh_r_skew(1.2);
    std::size_t num_nodes_r(101), num_nodes_theta(501);
    bool full_solve(true);
    double S(2);

    // Replace by command line parameters, if appropriate
    args.option("--num_r", num_nodes_r);
    args.option("--right_r", right_r);
    args.option("--mesh_r_skew", mesh_r_skew);
    args.option("--num_theta", num_nodes_theta);
    args.option("--end_time", end_time);
    args.option("--dt", dt);
    args.option("--full_solve", full_solve);
    args.option("--S", S);

    std::size_t max_steps(end_time/dt), output_skip(1);

    args.option("--output_skip", output_skip);

    // Instantiate equation/boundary condition objects for polar and full
    // boundary layer problems. Set the spin-up parameter so that S = 1.5
    BoundaryLayer::Pole_Problem pole_eqn;
    BoundaryLayer::Equator_Problem equator_eqn;
    BoundaryLayer::BC_Left bc_left;
    BoundaryLayer::BC_Right bc_right;
    BoundaryLayer::Full_Problem full_bl_eqn;
    BoundaryLayer::S = S;
    BoundaryLayer::unsteady = true;

    // Set up meshes by creating 'templates' for the two coordinate directions
    // and passing these to constructors for the computational meshes. This is
    // not as memory-efficient as setting each up individually but it is more
    // readable
    Mesh_1D<double> mesh_r, mesh_theta;

    mesh_r.power_mesh(left_r, right_r, num_nodes_r, equator_eqn.order(),
            mesh_r_skew, Mesh_Left);
    mesh_theta.uniform_mesh(left_theta, right_theta, num_nodes_theta,
            equator_eqn.order());

    Mesh_1D<double> mesh_pole(mesh_r);
    Mesh_1D<double> mesh_equator(mesh_r);
    Mesh_2D<double> mesh_bl(mesh_r, mesh_theta);
    
    // Set up conditions objects, each one is tied to a particular mesh. Adding
    // conditions takes the form of providing a location and a residual to be
    // applied at that location. In the case of the Condition_2D::add_y method,
    // we provide a reference to an existing mesh from which the solver can
    // directly pull values
    Conditions_1D<double> conditions_pole(mesh_pole);
    Conditions_1D<double> conditions_equator(mesh_equator);
    Conditions_2D<double> conditions_bl(mesh_bl);

    conditions_pole.add(left_r, bc_left);
    conditions_pole.add(right_r, bc_right);
    conditions_equator.add(left_r, bc_left);
    conditions_equator.add(right_r, bc_right);
    conditions_bl.add_x(left_r, bc_left);
    conditions_bl.add_x(right_r, bc_right);
    conditions_bl.add_y(mesh_equator);
    conditions_bl.add_y(mesh_pole);

    // Set up initial data (nothing to do if we have zero conditions)
    for (std::size_t i(0); i < num_nodes_r; ++i)
    {
        mesh_pole(i, v) = BoundaryLayer::S;
        mesh_pole(i, f) = 0.;
        mesh_equator(i, v) = BoundaryLayer::S;
        mesh_equator(i, f) = 0.;
        for (std::size_t j(0); j < num_nodes_theta; ++j)
        {
            mesh_bl(i, j, v) = BoundaryLayer::S;
            mesh_bl(i, j, f) = 0.;
        }
    }

    // Create PDE solver objects
    PDE_IBVP<PDE_1D_Unsteady, double, double> pole_solver(
            pole_eqn, conditions_pole, mesh_pole);
    PDE_IBVP<PDE_1D_Unsteady, double, double> equator_solver(
            equator_eqn, conditions_equator, mesh_equator);
    PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double> bl_solver(
            full_bl_eqn, conditions_bl, mesh_bl);

    // Set reversed-flow parameters, we must supply the component of velocity
    // in the direction of parabolic coordinate. The 
    // PDE_IBVP::direction_inverted() method sets a flag which indicates that
    // a flow will be considered reversed if the flow velocities at a point are
    // positive.
    //
    // In this case, since the theta coordinate is inverted and our "normal"
    // flow is negative due to the streamfunction formulation, the net result
    // is that the flag must be set to false.
    bl_solver.set_streamwise_component(fr);
    bl_solver.direction_inverted() = false;
    bl_solver.first_order() = false;

    // Prepare for output for when full_solve is not specified
    std::vector<std::size_t> output_times;
//    for (std::size_t i(1); i < (end_time + 1)/2.; ++i)
//        output_times.push_back(2*i/dt - 1);

    if (!full_solve && output_times.size() > 0)
    {
        // Open output files
        std::sprintf(filename, "/tmp/dat/shear/shear_%04i_%04i_%.3f.dat",
                int(num_nodes_r), int(num_nodes_theta), dt);
        std::sprintf(filename2, "/tmp/dat/shear/trans_%04i_%04i_%.3f.dat",
                int(num_nodes_r), int(num_nodes_theta), dt);
        data_soln.open(filename);
        data_soln2.open(filename2);

    }

    // Step forward in time and dump output
    for (std::size_t i(0); i < max_steps; ++i)
    {
        pole_solver.step(dt);
        equator_solver.step(dt);
        bl_solver.step(dt);

        if (full_solve && ((i + 1) % output_skip == 0))
        {
            // Open output files
            std::sprintf(filename, "/tmp/unadkat/parabolic_bl/%.3f.dat",
                    bl_solver.time());
            data_soln.open(filename);

            // Collect and output data
            for (std::size_t j(0); j < num_nodes_theta; ++j)
            {
                for (std::size_t k(0); k < num_nodes_r; ++k)
                {
                    data_soln << mesh_bl.node_y(j) << ' ';
                    data_soln << mesh_bl.node_x(k) << ' ';
                    data_soln << -mesh_bl(k, j, fr) << std::endl;
                }
                data_soln << std::endl << std::endl;;
            }

            // Clean up
            data_soln.flush();
            data_soln.close();
        }
        else if (output_times.size() > 0)
        {
            if (std::find(output_times.begin(), output_times.end(), i) !=
                    output_times.end())
            {
                // Dump data
                for (std::size_t j(0); j < num_nodes_theta; ++j)
                {
                    // Shears at wall
                    data_soln << mesh_bl.node_y(j) << ' ';
                    data_soln << mesh_bl(0, j, frr) << ' ';
                    data_soln << mesh_bl(0, j, vr) << '\n';
                }
                double w_mid(0.), f_mid(0.), f_theta_mid(0.);
                double dtheta(0.), theta_mid(0.);
                for (std::size_t j(0); j < num_nodes_theta - 1; ++j)
                {
                    // Transpiration at mid-nodes
                    dtheta = mesh_bl.node_y(j + 1) - mesh_bl.node_y(j);
                    theta_mid = (mesh_bl.node_y(j) + mesh_bl.node_y(j + 1))/2.;
                    f_mid = (mesh_bl(num_nodes_r - 1, j, f) +
                            mesh_bl(num_nodes_r - 1, j + 1, f))/2.;
                    f_theta_mid = (mesh_bl(num_nodes_r - 1, j + 1, f) -
                            mesh_bl(num_nodes_r - 1, j, f))/dtheta;
                    w_mid = (2*std::pow(std::cos(theta_mid), 2.)
                            - std::pow(std::sin(theta_mid), 2.))*f_mid +
                        std::sin(theta_mid)*std::cos(theta_mid)*f_theta_mid;

                    data_soln2 << theta_mid << ' ';
                    data_soln2 << w_mid << '\n';
                }
                data_soln << "\n\n";
                data_soln.flush();
                data_soln2 << "\n\n";
                data_soln2.flush();
            }
        }
    }
   
    if (!full_solve && output_times.size() > 0)
    {
        data_soln.close();
        data_soln2.close();
    }

    return 0;
}

