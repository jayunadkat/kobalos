// 2D boundary layer problem with forcing
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sys/stat.h>

#include <boundary_layer/Edge_Streamfunction.h>
#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <Command_Line_Args.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Rotation
{
    double S(0.);
    double wobble_rate(0.);

    double wobble(const double& t)
    {
        return S + (1. - S + 0.01*std::sin(wobble_rate*S*t))
            *(1. - std::exp(-t*t));
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    double max_r(100.), max_theta(2.*std::atan(1.));
    double dt(0.025), max_t(100.), output_start(0.), output_skip(dt);
    char path[100], filename[100];
    std::size_t num_r(1001), num_theta(100);
    bool force_output(false);

    args.option("--dt", dt);
    args.option("--force_output", force_output);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--output_skip", output_skip);
    args.option("--output_start", output_start);
    args.option("--path", path);
    args.option("--S", Rotation::S);
    args.option("--wobble", Rotation::wobble_rate);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    std::sprintf(path, "/tmp/dat/Reynolds/BL_S%.2f_omega%.2f",
            Rotation::S, Rotation::wobble_rate);

    // Check if folder already exists, forcing flag, etc
    {
        struct stat st;
        char command[200];

        if (stat(path, &st) == 0 && S_ISDIR(st.st_mode))
        {
            // Folder exists, check force flag and clear folder or just exit
            if (force_output)
            {
                std::cout << " Output directory:\n    "
                    << path << "\n already exists and --force_output flag"
                    << " set --- cleaning folder\n";
                sprintf(command, "rm %s/*", path);
                system(command);
            }
            else
            {
                std::cout << " Output directory:\n    "
                    << path << "\n already exists and --force_output flag"
                    << " not set --- aborting\n";
                return 1;
            }
        }
        else
        {
            // Create folder
            sprintf(command, "mkdir %s", path);
            system(command);
        }
    }

    File_Output files(path);
    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(3);
    std::size_t output_from(output_start/dt);
    std::size_t skip(output_skip/dt);

    Mesh_1D<double> r_mesh, theta_mesh;
    r_mesh.uniform_mesh(0., max_r, num_r, num_vars);
    theta_mesh.uniform_mesh(0., max_theta, num_theta, num_vars);
    Mesh_1D<double> equator_mesh(r_mesh), pole_mesh(r_mesh);
    Mesh_2D<double> mesh(r_mesh, theta_mesh);

    for (std::size_t i(0); i < num_r; ++i)
    {
        pole_mesh(i, v) = Rotation::S;
        equator_mesh(i, v) = Rotation::S;
        for (std::size_t j(0); j < num_theta; ++j)
            mesh(i, j, v) = Rotation::S;
    }

    // Solvers for pole and equator
    Edge_Streamfunction pole_problem(pole_mesh);
    Edge_Streamfunction equator_problem(equator_mesh);
    Elliptic_Boundary_Layer bl_problem(pole_mesh, equator_mesh, mesh);

    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = Rotation::S;
    pole_problem.set_sphere_rotation(&Rotation::wobble);
    equator_problem.set_edge("equator");
    equator_problem.spin_up_parameter() = Rotation::S;
    equator_problem.set_sphere_rotation(&Rotation::wobble);
    bl_problem.spin_up_parameter() = Rotation::S;
    bl_problem.set_external_bump(&Rotation::wobble);

    try
    {
        for (std::size_t step(0); step < num_steps; ++step)
        {
            if (step >= output_from && (step - output_from)%skip == 0)
            {
                std::sprintf(filename, "e%u.dat", unsigned(step));
                files.open("e", filename);
                std::sprintf(filename, "p%u.dat", unsigned(step));
                files.open("p", filename);
                std::sprintf(filename, "m%u.dat", unsigned(step));
                files.open("m", filename);

                pole_problem.dump_flow_variables(files("p"));
                equator_problem.dump_flow_variables(files("e"));

                files("m") << "# Mesh_1D\n";
                std::size_t j(num_theta/2 - 1);
                double theta(0.5*(mesh.node_y(j) + mesh.node_y(j + 1)));
                double c(std::cos(theta)), s(std::sin(theta));
                for (std::size_t i(0); i < num_r; ++i)
                {
                    files("m") << mesh.node_x(i) << ' ';
                    files("m") << -0.5*(mesh(i, j, fr)
                            + mesh(i, j + 1, fr)) << ' ';
                    files("m") << 0.5*(mesh(i, j, v)
                            + mesh(i, j + 1, v)) << ' ';
                    files("m") << (3.*c*c - 1.)*0.5*(mesh(i, j, f) +
                            mesh(i, j + 1, f))
                        + s*c*(mesh(i, j + 1, f) - mesh(i, j, f))/(
                                mesh.node_y(j + 1)
                                - mesh.node_y(j)) << '\n';
                }

                files.close_all();
            }

            pole_problem.step(dt);
            equator_problem.step(dt);
            bl_problem.step(dt);
        }
    }
    catch (std::exception& e)
    {
        std::cout << "Something terrible has happened." << std::endl;
    }
    
    return 0;
}
