#include <exception>
#include <iostream>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction_Unscaled.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Mesh_r
{
    double p(1.), off(0.);

    double map(const double& s) {return std::pow(s+off, p);}
    double inv_map(const double& x) {return std::pow(x, 1./p)-off;}
    double map_ds(const double& s) {return p*std::pow(s+off, p-1.);}
    double map_ds2(const double& s) {return p*(p-1.)*std::pow(s+off, p-2.);}
}

namespace Mesh_theta
{
    double p(1.), off(0.);

    double map(const double& s) {return std::pow(s+off, p);}
    double inv_map(const double& x) {return std::pow(x, 1./p)-off;}
    double map_ds(const double& s) {return p*std::pow(s+off, p-1.);}
    double map_ds2(const double& s) {return p*(p-1.)*std::pow(s+off, p-2.);}
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    // Parameters
    double signed_Ro(-0.5);
    double max_r(30.), max_theta(2.*std::atan(1.));
    char path[100];
    std::sprintf(path, "dat/rand/");
    std::size_t num_r(201), num_theta(201);
    std::size_t num_vars(3);

    // Grab command-line options
    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--rossby", signed_Ro);
    args.option("--path", path);

#ifndef QUIET
    // Done with options/flag parsing
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET
    
    // File output object
    File_Output files(path);

    // Prepare meshes
    Mesh_1D<double> mesh_r, mesh_theta;

    mesh_r.mapped_grid(0., max_r, num_r, num_vars,
          Mesh_r::map, Mesh_r::inv_map, Mesh_r::map_ds, Mesh_r::map_ds2);
    mesh_theta.mapped_grid(0., max_theta, num_theta, num_vars,
          Mesh_theta::map, Mesh_theta::inv_map, Mesh_theta::map_ds,
          Mesh_theta::map_ds2);

    Mesh_1D<double> mesh_pole_steady(mesh_r), mesh_equator_steady(mesh_r);
    Mesh_2D<double> mesh_bl_steady(mesh_r, mesh_theta);

    // Steady solvers for pole and equator
    Rotating_Edge_Streamfunction pole_steady(mesh_pole_steady);
    Rotating_Edge_Streamfunction equator_steady(mesh_equator_steady);
    Rotating_Layer_Streamfunction_Unscaled bl_steady(mesh_bl_steady);

    // Compute attachment point steady solutions
    pole_steady.set_edge("pole");
    equator_steady.set_edge("equator");

    for (double Ro_iterate(-0.01); Ro_iterate > signed_Ro; Ro_iterate -= 0.01)
    {
        std::cout << "Solving attachment points for Ro = " << Ro_iterate
            << std::endl;
        pole_steady.set_signed_rossby_number(Ro_iterate);
        equator_steady.set_signed_rossby_number(Ro_iterate);

        // Compute steady solutions at pole and equator
        pole_steady.solve_steady();
        equator_steady.solve_steady();
    }

    pole_steady.set_signed_rossby_number(signed_Ro);
    equator_steady.set_signed_rossby_number(signed_Ro);
    bl_steady.set_signed_rossby_number(signed_Ro);

    // Compute steady solutions at pole and equator
    std::cout << "Solving attachment points for Ro = " << signed_Ro
        << std::endl;
    pole_steady.solve_steady();
    equator_steady.solve_steady();

    // Initial guess for BL
    for (std::size_t j(0); j < num_theta; ++j)
    {
        double pos(mesh_theta.unmapped_node(j)/max_theta);
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                mesh_bl_steady(i, j, k) = mesh_pole_steady(i, k) + pos*(
                        mesh_equator_steady(i, k) - mesh_pole_steady(i, k));
            }
        }
    }

    for (std::size_t j(0); j < num_theta; ++j)
    {
        double theta(mesh_theta.unmapped_node(j));
        double s(std::sin(theta)), c(std::cos(theta));
        for (std::size_t i(0); i < num_r; ++i)
        {
            mesh_bl_steady(i, j, f) *= s*s*c;
            mesh_bl_steady(i, j, fr) *= s*s*c;
            mesh_bl_steady(i, j, v) *= s;
        }
    }

    // Compute steady solution in BL
    std::cout << "Solving 2D boundary layer flow" << std::endl;
    bl_steady.solve_steady();
    std::sprintf(path, "steady_%.2f", signed_Ro);
    files.open("steady", path);
    mesh_bl_steady.dump(10, files("steady"));
    files.close("steady");

    return 0;
}

