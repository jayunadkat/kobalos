#include <exception>
#include <iostream>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction_Unscaled.h>
#include <boundary_layer/Local_Temporal_EVP.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Mesh_r
{
    double p(1.), off(0.);

    double map(const double& s) {return std::pow(s+off, p);}
    double inv_map(const double& x) {return std::pow(x, 1./p)-off;}
    double map_ds(const double& s) {return p*std::pow(s+off, p-1.);}
    double map_ds2(const double& s) {return p*(p-1.)*std::pow(s+off, p-2.);}
}

namespace Mesh_theta
{
    double p(1.), off(0.);

    double map(const double& s) {return std::pow(s+off, p);}
    double inv_map(const double& x) {return std::pow(x, 1./p)-off;}
    double map_ds(const double& s) {return p*std::pow(s+off, p-1.);}
    double map_ds2(const double& s) {return p*(p-1.)*std::pow(s+off, p-2.);}
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    // Parameters
    double signed_Ro(-0.5);
    double max_r(30.), max_theta(2.*std::atan(1.));
    double wavenumber(320.);
    char path[100];
    std::sprintf(path, "dat/local3");
    std::size_t num_r(301), num_theta(201);
    std::size_t num_vars(3), iteration_max(5);

    // Grab command-line options
    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--rossby", signed_Ro);
    args.option("--path", path);

#ifndef QUIET
    // Done with options/flag parsing
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET
    
    // File output object
    File_Output files(path);

    // Prepare meshes
    Mesh_1D<double> mesh_r, mesh_theta;

    mesh_r.mapped_grid(0., max_r, num_r, num_vars,
          Mesh_r::map, Mesh_r::inv_map, Mesh_r::map_ds, Mesh_r::map_ds2);
    mesh_theta.mapped_grid(0., max_theta, num_theta, num_vars,
          Mesh_theta::map, Mesh_theta::inv_map, Mesh_theta::map_ds,
          Mesh_theta::map_ds2);

    Mesh_1D<double> mesh_pole_steady(mesh_r), mesh_equator_steady(mesh_r);
    Mesh_2D<double> mesh_bl_steady(mesh_r, mesh_theta);

    // Steady solvers for pole and equator
    Rotating_Edge_Streamfunction pole_steady(mesh_pole_steady);
    Rotating_Edge_Streamfunction equator_steady(mesh_equator_steady);
    Rotating_Layer_Streamfunction_Unscaled bl_steady(mesh_bl_steady);

    // Compute attachment point steady solutions
    pole_steady.set_edge("pole");
    equator_steady.set_edge("equator");

    for (double Ro_iterate(-0.01); Ro_iterate > signed_Ro; Ro_iterate -= 0.01)
    {
        std::cout << "Solving attachment points for Ro = " << Ro_iterate
            << std::endl;
        pole_steady.set_signed_rossby_number(Ro_iterate);
        equator_steady.set_signed_rossby_number(Ro_iterate);

        // Compute steady solutions at pole and equator
        pole_steady.solve_steady();
        equator_steady.solve_steady();
    }

    pole_steady.set_signed_rossby_number(signed_Ro);
    equator_steady.set_signed_rossby_number(signed_Ro);
    bl_steady.set_signed_rossby_number(signed_Ro);

    // Compute steady solutions at pole and equator
    std::cout << "Solving attachment points for Ro = " << signed_Ro
        << std::endl;
    pole_steady.solve_steady();
    equator_steady.solve_steady();

    // Initial guess for BL
    for (std::size_t j(0); j < num_theta; ++j)
    {
        double pos(mesh_theta.unmapped_node(j)/max_theta);
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                mesh_bl_steady(i, j, k) = mesh_pole_steady(i, k) + pos*(
                        mesh_equator_steady(i, k) - mesh_pole_steady(i, k));
            }
        }
    }

    for (std::size_t j(0); j < num_theta; ++j)
    {
        double theta(mesh_theta.unmapped_node(j));
        double s(std::sin(theta)), c(std::cos(theta));
        for (std::size_t i(0); i < num_r; ++i)
        {
            mesh_bl_steady(i, j, f) *= s*s*c;
            mesh_bl_steady(i, j, fr) *= s*s*c;
            mesh_bl_steady(i, j, v) *= s;
        }
    }

    // Compute steady solution in BL
    bl_steady.solve_steady();
    std::sprintf(path, "steady_%u_%.2f", static_cast<unsigned>(num_theta),
            signed_Ro);
    files.open("steady", path);
    mesh_bl_steady.dump(10, files("steady"));
    files.close("steady");

    for (std::size_t k(0); k < iteration_max; ++k)
    {
        try
        {
            Local_Temporal_EVP evp_problem(mesh_bl_steady);
            evp_problem.set_signed_rossby_number(signed_Ro);
            evp_problem.set_wavenumber(wavenumber);

            std::sprintf(path, "evs_%u_%.2f_%.0f",
                    static_cast<unsigned>(num_theta), signed_Ro, wavenumber);
            files.open("evs", path);
            std::sprintf(path, "primary_efuncs_%u_%.2f_%.0f",
                    static_cast<unsigned>(num_theta), signed_Ro, wavenumber);
            files.open("efuncs", path);

            std::cout << "Now solving (local) temporal EVP for wavenumber "
                << wavenumber << std::endl;

            for (std::size_t i(1); i < num_theta - 1; ++i)
            {
                std::cout << "Now solving for theta_index = " << i << " (of "
                    << num_theta - 2 << ")" << std::endl;

                evp_problem.set_theta_index(i);
                evp_problem.solve();

                std::size_t primary_index(0);
                std::complex<double> eval;

                files("evs") << mesh_bl_steady.unmapped_node_y(i) << " ";
                for (std::size_t j(0), sol_size(
                            evp_problem.evp_system().solution_size());
                        j < sol_size; ++j)
                {
                    eval = evp_problem.evp_system().eigenvalue(j);
                    if (eval.real() < 0)
                    {
                        files("evs") << std::endl;
                        break;
                    }
                    if (primary_index == 0)
                        primary_index = j;
                    files("evs") << eval.real() << " " << eval.imag() << " ";
                }

                eval = evp_problem.evp_system().eigenvalue(primary_index);
                files("efuncs") << "# theta = "
                    << mesh_bl_steady.unmapped_node_y(i)
                    << ", eval = (" << eval.real() << ", " << eval.imag() << ")"
                    << std::endl;
                for (std::size_t j(0); j < num_r; ++j)
                {
                    files("efuncs") << mesh_bl_steady.unmapped_node_x(j) << " ";
                    for (std::size_t l(0); l < num_vars; ++l)
                    {
                        files("efuncs") << 
                            evp_problem.evp_system().eigenvector_real(
                                    primary_index)[j*num_vars + l] << " "
                            << evp_problem.evp_system().eigenvector_imag(
                                    primary_index)[j*num_vars + l] << " ";
                    }
                    files("efuncs") << std::endl;
                }
                files("efuncs") << std::endl << std::endl;
            }
            files.close_all();
        }
        catch (std::exception& e)
        {
            files.close_all();
        }

        wavenumber *= 2.;
    }

    return 0;
}

