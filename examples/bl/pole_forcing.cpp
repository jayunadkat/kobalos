// Polar problem with forcing

#include <cmath>
#include <cstring>
#include <fstream>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <File_Output.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Bump
{
    double S(0.);

    double bump(const double& t)
    {
        return 1. + (S - 1.)*std::exp(-t*t)
            + 0.05*std::sin(2.*S*t)*(1. - std::exp(-t*t));
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(1.5);
    double max_r(200.);
    double dt(0.01), max_t(155.), output_start(150.);
    char path[100];
    std::sprintf(path, "dat/1308");
    std::size_t num_r(2001);
    std::size_t output_skip(4);

    args.option("--dt", dt);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--path", path);
    args.option("--output_skip", output_skip);
    args.option("--output_start", output_start);
    args.option("--S", S);

    File_Output files(path);
    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(3);
    std::size_t output_from(output_start/dt - 1);
    Bump::S = S;

    Mesh_1D<double> mesh_pole;
    mesh_pole.uniform_mesh(0., max_r, num_r, num_vars);

    for (std::size_t i(0); i < num_r; ++i)
        mesh_pole(i, v) = S;

    // Solvers for pole and equator
    Edge_Streamfunction pole_problem(mesh_pole);

    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = S;
    pole_problem.set_sphere_rotation(&Bump::bump);
    files.open("pole", "pole.dat");
    files.write("pole").precision(6);

    for (std::size_t step(0); step < num_steps; ++step)
    {
        pole_problem.step(dt);

        if ((step + 1) % (output_skip + 1) == 0 && step >= output_from)
            //pole_problem.dump_flow_variables(files.write("pole"));
        {
            for (std::size_t i(0); i < num_r; ++i)
            {
                files.write("pole") << mesh_pole.node(i) << ' ';
                files.write("pole") << -mesh_pole(i, fr) << '\n';
            }
            files.write("pole") << "\n\n";
        }
    }
    files.close_all();
    
    return 0;
}
