// Spin up from rest problem, parabolic marching from equator to pole using
// streamfunction formulation

#include <cmath>
#include <cstdlib>
#include <exception>
#include <iostream>

#include <Command_Line_Args.h>
#include <PDE_Bundle.h>

#include "Boundary_Layer.h"

using namespace Kobalos;

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double S(1.2);
    double max_r(30.), mesh_skew(1.2);
    double max_theta(2.*std::atan(1.));
    double dt(0.01), max_t(500.);
    double tol(1e-8);
    std::size_t num_r(400), num_theta(200);

    args.option("--dt", dt);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--S", S);
    args.option("--skew", mesh_skew);
    args.option("--tol", tol);

    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(5);

    Mesh_1D<double> mesh_r, mesh_theta;
    mesh_r.power_mesh(0., max_r, num_r, num_vars, mesh_skew, Mesh_Left);
    mesh_theta.uniform_mesh(max_theta, 0., num_theta, num_vars);
    Mesh_1D<double> mesh_equator(mesh_r), mesh_pole(mesh_r);
    Mesh_2D<double> mesh(mesh_r, mesh_theta);
   
    BoundaryLayer::Equator_Problem eqn_equator;
    BoundaryLayer::Pole_Problem eqn_pole;
    BoundaryLayer::Full_Problem eqn_bl;
    BoundaryLayer::BC_Left bc_left;
    BoundaryLayer::BC_Right bc_right;
    BoundaryLayer::S = S;

    Conditions_1D<double> conditions_equator(mesh_equator),
        conditions_pole(mesh_pole);
    conditions_equator.add(0., bc_left);
    conditions_equator.add(max_r, bc_right);
    conditions_pole.add(0., bc_left);
    conditions_pole.add(max_r, bc_right);
    Conditions_2D<double> conditions_bl(mesh);
    conditions_bl.add_x(0., bc_left);
    conditions_bl.add_x(max_r, bc_right);
    conditions_bl.add_y(mesh_equator);
    conditions_bl.add_y(mesh_pole);

    for (std::size_t i(0); i < num_r; ++i)
    {
        mesh_equator(i, v) = BoundaryLayer::S;
        mesh_pole(i, v) = BoundaryLayer::S;
        for (std::size_t j(0); j < num_theta; ++j)
            mesh(i, j, v) = BoundaryLayer::S;
    }
    
    // Create solver objects and set solution parameters
    // To set the parameters, we must supply the component of velocity
    // in the direction of parabolic coordinate. The 
    // PDE_IBVP::direction_inverted() method sets a flag which indicates that
    // a flow will be considered reversed if the flow velocities at a point are
    // positive.
    //
    // In this case, since the theta coordinate is inverted and our "normal"
    // flow is negative due to the streamfunction formulation, the net result
    // is that the flag must be set to false.
    PDE_IBVP<PDE_1D_Unsteady, double, double> problem_equator(eqn_equator,
            conditions_equator, mesh_equator);
    PDE_IBVP<PDE_1D_Unsteady, double, double> problem_pole(eqn_pole,
            conditions_pole, mesh_pole);
    PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double> problem_bl(eqn_bl,
            conditions_bl, mesh);
    BoundaryLayer::unsteady = true;
    problem_bl.set_streamwise_component(fr);
    problem_bl.direction_inverted() = false;
    problem_bl.first_order() = false;

    // Do the stepping
    try
    {
        for (std::size_t step(0); step < num_steps; ++step)
        {
            problem_equator.step(dt);
            problem_pole.step(dt);
            problem_bl.step(dt);
        }
    } catch (const std::exception& e)
    {
        std::cout << problem_bl.time();
    }

    return 0;
}

