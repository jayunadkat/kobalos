// 2D boundary layer problem
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

double phi(const double& x)
{
    if (x < 0)
        return 0.;
    else
        return std::exp(-1./(x*x));
}

double bump(const double& a, const double& b, const double& t)
{
    double left(0), right(0);
    if (b < a)
    {
        left = b;
        right = a;
    }
    else
    {
        left = a;
        right = b;
    }
    double magnitude(std::pow(phi(0.5*(right - left)), 2.));
    return phi(t - left)*phi(right - t)/magnitude;
}

namespace Rotation
{
    double k(10.);
    double signed_Ro(-1.35);
    double wobble_rate(0.);

    double steady_spin(const double& t)
    {
        return 1.;
    }

    double spin(const double& t)
    {
        return 1. + 0.1*bump(0., 0.5, t);
    }

    double wobble(const double& t)
    {
        return 0.1*std::sin(wobble_rate*t);
    }
}

namespace Mesh_Power2
{
    double map(const double& s)
    {
        return std::pow(s + 0.1, 2.) - 0.01;
    }

    double inv_map(const double& x)
    {
        return std::pow(x + 0.01, 1/2.) - 0.1;
    }

    double map_ds(const double& s)
    {
        return 2.*(s + 0.1);
    }

    double map_ds2(const double& s)
    {
        return 2.;
    }
}

namespace Mesh_Power3
{
    double power(5.);

    double map(const double& s)
    {
        return std::pow(s + 0.1, power) - std::pow(0.1, power);
    }

    double inv_map(const double& x)
    {
        return std::pow(x + std::pow(0.1, power), 1./power) - 0.1;
    }

    double map_ds(const double& s)
    {
        return power*std::pow(s + 0.1, power - 1.);
    }

    double map_ds2(const double& s)
    {
        return power*(power - 1.)*std::pow(s + 0.1, power - 2.);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
    console_out.use_ncurses();

    double max_r(30.), max_theta(2.*std::atan(1.));
    double dt(0.01), max_t(30.), output_start(0.), output_skip(0.);
    char path[100], filename[100];
    std::size_t num_r(301), num_theta(401);
    bool force_output(false), uniform(true);
    double rossby(-0.9);

    args.option("--dt", dt);
    args.flag("-f", force_output);
    args.option("--force_output", force_output);
    args.option("--k", Rotation::k);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--output_skip", output_skip);
    args.option("--output_start", output_start);
    args.option("--power", Mesh_Power3::power);
    args.option("--Ro", Rotation::signed_Ro);
    args.option("--start_Ro", rossby);
    args.option("--wobble", Rotation::wobble_rate);

    args.option("--uniform", uniform);
    char uniform_string[16];
    uniform_string[0] = '\0';
    if (uniform)
        std::sprintf(uniform_string, "_uniform");

    console_out.set_title(
            "Test rotating frame BL simulation for spin down with Ro = "
            + str_convert(Rotation::signed_Ro));
    console_out.begin_staging_header();
    console_out.stage_header_line("Performing set-up");
    console_out.end_staging_header();

    output_skip = dt;
//    std::sprintf(path, "/tmp/dat/Kobalos_test_%u%s", 
//            static_cast<unsigned>(num_r), uniform_string);
    std::sprintf(path, "dat/TEST/WUP%.1f", Mesh_Power3::power);
    args.option("--path", path);

    // Check if folder already exists, forcing flag, etc
    {
        struct stat st;
        char command[200];

        if (stat(path, &st) == 0 && S_ISDIR(st.st_mode))
        {
            // Folder exists, check force flag and clear folder or just exit
            if (force_output)
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " set --- cleaning folder\n");
                sprintf(command, "rm %s/*", path);
                system(command);
                sleep(3);
            }
            else
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " not set --- aborting\n");
                sleep(3);
                return 1;
            }
        }
        else
        {
            // Create folder
            sprintf(command, "mkdir %s", path);
            system(command);
        }
    }

    File_Output files(path);
    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(3);
    std::size_t output_from(output_start/dt);
    std::size_t skip(output_skip/dt);

    std::vector<double> output_Ro({-1.05, -1.1, -1.15, -1.2, -1.25, -1.3});

    Mesh_1D<double> r_mesh, theta_mesh;
    if (uniform)
        r_mesh.uniform_mesh(0., max_r, num_r, num_vars);
    else
        r_mesh.mapped_grid(0., max_r, num_r, num_vars,
                &Mesh_Power2::map, &Mesh_Power2::inv_map,
                &Mesh_Power2::map_ds, &Mesh_Power2::map_ds2);
    //theta_mesh.uniform_mesh(0., max_theta, num_theta, num_vars);
    theta_mesh.mapped_grid(0., max_theta, num_theta, num_vars,
            &Mesh_Power3::map, &Mesh_Power3::inv_map,
            &Mesh_Power3::map_ds, &Mesh_Power3::map_ds2);
    Mesh_1D<double> equator_mesh(r_mesh), pole_mesh(r_mesh);
    Mesh_2D<double> mesh(r_mesh, theta_mesh);

    // Leave zero initial conditions due to rotating frame
    // Solvers for pole and equator
    Rotating_Edge_Streamfunction pole_problem(pole_mesh);
    Rotating_Edge_Streamfunction equator_problem(equator_mesh);
    Rotating_Layer_Streamfunction bl_problem(pole_mesh, equator_mesh, mesh);

    pole_problem.set_edge("pole");
    pole_problem.set_signed_rossby_number(rossby);
    pole_problem.set_sphere_spin(&Rotation::steady_spin);
//    pole_problem.set_sphere_wobble(&Rotation::wobble);
    equator_problem.set_edge("equator");
    equator_problem.set_signed_rossby_number(rossby);
    equator_problem.set_sphere_spin(&Rotation::steady_spin);
//    equator_problem.set_sphere_wobble(&Rotation::wobble);
    bl_problem.set_signed_rossby_number(rossby);
    bl_problem.set_sphere_spin(&Rotation::steady_spin);
//    bl_problem.set_sphere_wobble(&Rotation::wobble);

    for (std::size_t i(0); i < num_r; ++i)
    {
        double r(r_mesh.unmapped_node(i));
        equator_mesh(i, f) = pole_mesh(i, f) = 0.5*(1. - std::exp(-r*r));
        equator_mesh(i, fr) = pole_mesh(i, fr) = r*std::exp(-r*r);
        equator_mesh(i, v) = pole_mesh(i, v) = (r - 1.)*std::exp(-r*r);
    }

    pole_problem.solve_steady();
    equator_problem.solve_steady();

    for (std::size_t j(0); j < num_theta; ++j)
    {
        double pos(theta_mesh.unmapped_node(j)/max_theta);
        for (std::size_t i(0); i < num_r; ++i)
        {
            mesh(i, j, f) = pole_mesh(i, f) + pos*(
                    equator_mesh(i, f) - pole_mesh(i, f));
            mesh(i, j, fr) = pole_mesh(i, fr) + pos*(
                    equator_mesh(i, fr) - pole_mesh(i, fr));
            mesh(i, j, v) = pole_mesh(i, v) + pos*(
                    equator_mesh(i, v) - pole_mesh(i, v));
        }
    }
    bl_problem.solve_steady();

    try
    {
        if (Rotation::signed_Ro >= rossby)
            Rotation::signed_Ro = rossby - 0.001;
        for (; rossby > Rotation::signed_Ro - 0.005; rossby -= 0.02)
        {
            console_out.begin_staging_header();
            args.print_recognised_arguments();
            console_out.stage_header_line("Solving steady problems, Ro = "
                    + str_convert(rossby));
            console_out.end_staging_header();

            pole_problem.set_signed_rossby_number(rossby);
            equator_problem.set_signed_rossby_number(rossby);
            bl_problem.set_signed_rossby_number(rossby);

            pole_problem.solve_steady();
            equator_problem.solve_steady();
            bl_problem.solve_steady();

            for (std::size_t i(0); i < output_Ro.size(); ++i)
            {
                if (std::abs(rossby - output_Ro[i]) < 1e-3)
                {
                    std::sprintf(filename, "Ro%.3f.dat", rossby);
                    files.open("full", filename);
                    bl_problem.dump_flow_variables(files("full"));
                    files.close("full");

                    std::sprintf(filename, "Ro%.3f_mesh.dat", rossby);
                    files.open("mesh", filename);
                    files("mesh").precision(16);
                    mesh.dump(files("mesh"));
                    files.close("mesh");

                    std::sprintf(filename, "Ro%.3f_boundary.dat", rossby);
                    files.open("boundary", filename);
                    files("boundary").precision(16);
                    double dr(0.5/(mesh.node_x(1) - mesh.node_x(0)));
                    for (std::size_t j(0); j < num_theta; ++j)
                    {
                        files("boundary") << mesh.unmapped_node_y(j) << ' '
                            << dr*(
                                    - 3.*mesh(0, j, fr)
                                    + 4.*mesh(1, j, fr)
                                    - 1.*mesh(2, j, fr)
                                  )/mesh.map_ds_x(mesh.node_x(0)) << std::endl;
                    }
                    files.close("boundary");
                }
            }
/*            std::sprintf(filename, "Ro%.3f_diff.dat", rossby);
            files.open("diff", filename);
            files("diff").precision(16);
            for (std::size_t j(0); j < num_theta; ++j)
            {
                for (std::size_t i(0); i < num_r; ++i)
                {
                    files("diff") << mesh.unmapped_node_y(j) << ' '
                        << mesh.unmapped_node_x(i) << ' ';
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        files("diff") << mesh(i, j, k) - pole_mesh(i, k) << ' ';
                    }
                    files("diff") << std::endl;
                }
                files("diff") << std::endl;
            }
            files.close("diff");*/

/*            std::sprintf(filename, "Ro%.3f_pole.dat", rossby);
            files.open("pole", filename);
            pole_mesh.dump(files("pole"));
            files.close("pole");*/

/*            std::sprintf(filename, "Ro%.3f_equator.dat", rossby);
            files.open("equator", filename);
            equator_mesh.dump(files("equator"));
            files.close("equator");*/


/*            getch();
            exit(1);*/
        }
        getch();
        exit(1);

        pole_problem.set_signed_rossby_number(Rotation::signed_Ro);
        equator_problem.set_signed_rossby_number(Rotation::signed_Ro);
        bl_problem.set_signed_rossby_number(Rotation::signed_Ro);

        pole_problem.set_sphere_spin(&Rotation::spin);
        equator_problem.set_sphere_spin(&Rotation::spin);
        bl_problem.set_sphere_spin(&Rotation::spin);

        std::sprintf(filename, "tracker.dat");
        files.open("surface", filename);
        for (std::size_t step(0); step < num_steps; ++step)
        {
            console_out.begin_staging_header();
            args.print_recognised_arguments();
            console_out.stage_header_line("Time step: "
                    + str_convert(step + 1) + "\n");
            console_out.stage_header_line("Current time (pre-solve): "
                    + str_convert(pole_problem.time()));
            console_out.end_staging_header();

            pole_problem.step(dt);
            equator_problem.step(dt);
            bl_problem.step(dt);

            if (step >= output_from && (step - output_from)%skip == 0)
            {
                double inv_2h(0.5/((pole_mesh.node(1) - pole_mesh.node(0))
                            *Mesh_Power2::map_ds(pole_mesh.node(0))));
                files("surface") << pole_problem.time();
                for (std::size_t i(0); i < num_theta; ++i)
                {
                    files("surface") << ' '
                        << -inv_2h*(
                                - 3.*mesh(0, i, fr)
                                + 4.*mesh(1, i, fr)
                                - mesh(2, i, fr)
                                );
                }
                files("surface") << std::endl;
            }
        }
    }
    catch (std::exception& e)
    {
        console_out("Something terrible has happened.\n");
        getch();
    }
    getch();
    console_out.stop_ncurses();
    files.close_all();
    
    return 0;
}
