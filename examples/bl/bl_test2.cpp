// Spin up from rest problem, parabolic marching from equator to pole

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <boundary_layer/Boundary_Layer.h>
#include <File_Output.h>
#include <PDE_Bundle.h>

using namespace Kobalos;

enum {f, fr, frr, v, vr};

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        std::cout << "Usage: bl_test2.out\n";
        return 0;
    }
    
    // Data output objects
    File_Output files("dat/temp");

    // Spatial/time domain parameters
    const double pi(4.*std::atan(1.));
    double left_r(0.), right_r(20.), left_theta(0.), right_theta(pi/2.);
    double dt(0.01);
    std::size_t num_nodes_r(201), num_nodes_theta(201), max_steps(30./dt);
    std::size_t num_vars(5);

    // Set up output times
    std::vector<double> output_times({2., 4., 4.5, 4.6, 4.7, 4.8, 4.9, 5.});
    output_times.clear();
    std::vector<std::size_t> output_steps(output_times.size(), 0);
    for (std::size_t i(0), size(output_steps.size()); i < size; ++i)
        output_steps[i] = output_times[i]/dt - 1;

    // Instantiate equation/boundary condition objects for polar
    // and full boundary layer problems
    BoundaryLayer::Pole_Problem pole_eqn;
    BoundaryLayer::BC_Left bc_left;
    BoundaryLayer::BC_Right bc_right;
    BoundaryLayer::Full_Problem full_bl_eqn;
    BoundaryLayer::S = 0.99;
    BoundaryLayer::unsteady = true;

    // Set up meshes
    Mesh_1D<double> mesh_r, mesh_theta;

    mesh_r.uniform_mesh(left_r, right_r, num_nodes_r, num_vars);
    mesh_theta.uniform_mesh(left_theta, right_theta, num_nodes_theta,
            num_vars);

    Mesh_1D<double> mesh_pole(mesh_r);
    Mesh_2D<double> mesh_bl(mesh_r, mesh_theta);
    
    // Set up conditions objects
    Conditions_1D<double> conditions_pole(mesh_pole);
    Conditions_2D<double> conditions_bl(mesh_bl);

    conditions_pole.add(left_r, bc_left);
    conditions_pole.add(right_r, bc_right);
    conditions_bl.add_x(left_r, bc_left);
    conditions_bl.add_x(right_r, bc_right);
    conditions_bl.add_y(mesh_pole);

    // Set up initial data (nothing to do if we have zero conditions)
    for (std::size_t i(0); i < num_nodes_r; ++i)
    {
        mesh_pole(i, v) = BoundaryLayer::S;
        for (std::size_t j(0); j < num_nodes_theta; ++j)
            mesh_bl(i, j, v) = BoundaryLayer::S;
    }

    // Create PDE solver objects
    PDE_IBVP<PDE_1D_Unsteady, double, double> pole_solver(
            pole_eqn, conditions_pole, mesh_pole);
    PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double> bl_solver(
            full_bl_eqn, conditions_bl, mesh_bl);

    bl_solver.set_streamwise_component(fr);
    bl_solver.direction_inverted() = true;
    bl_solver.first_order() = false;

    files.open("rest", "S0.99.dat");

    // Step forward in time and dump output
    try
    {
    for (std::size_t step(0); step < max_steps; ++step)
    {
        pole_solver.step(dt);
        bl_solver.step(dt);

        if (std::find(output_steps.begin(), output_steps.end(), step) !=
                output_steps.end())
        {
            double s(0.), c(0.), r(0.);
            for (std::size_t j(0); j < num_nodes_theta; ++j)
            {
                s = std::sin(mesh_bl.node_y(j));
                c = std::cos(mesh_bl.node_y(j));
                for (std::size_t i(0); i < num_nodes_r; ++i)
                {
                    r = mesh_bl.node_x(i);
                    files("rest") << (1. + r)*s << ' ';
                    files("rest") << (1. + r)*c << ' ';
                    files("rest") << -mesh_bl(i, j, fr) << '\n';
                }
                files("rest") << '\n';
            }
            files("rest") << '\n';
            files("rest").flush();
        }
    }
    }
    catch (std::exception& e)
    {
    }

    files("rest").close();

    return 0;
}

