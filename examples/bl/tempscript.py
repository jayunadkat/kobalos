#!/usr/bin/env python
"""
usage: tempscript.sh
"""
from os import system
import sys

if len(sys.argv) != 4:
    print " Error: expected 3 arguments (S_start, S_end, dS)"
    sys.exit()

S_current = float(sys.argv[1])
S_end = float(sys.argv[2])
dS = float(sys.argv[3])

while S_current <= S_end:
    system('./bl_steady.out --S ' + str(S_current) + ' --max_r 15 --dr 0.05')
    S_current += dS

