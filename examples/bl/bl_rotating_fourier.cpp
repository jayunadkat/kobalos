// 2D boundary layer problem with forcing

#include <cmath>
#include <cstring>
#include <fstream>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <boundary_layer/Rotating_Layer_Streamfunction.h>
#include <boundary_layer/Rotating_Fourier_Decomposed_Edge.h>
#include <boundary_layer/Rotating_Fourier_Decomposed_Layer.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    // Parameters
    double signed_Ro(-0.5), Ro_iterate(-0.01), omega(1.7);
    bool steady(false), convert(false);
    double max_r(100.), max_theta(2.*std::atan(1.)), power(3.);
    char path[100], filename[100];
    std::sprintf(path, "/tmp/dat/1308");
    std::size_t num_r(101), num_theta(201);
    std::size_t num_vars(3);

    // Grab command-line options
    args.option("--dump_flow", convert);
    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--num_theta", num_theta);
    args.option("--omega", omega);
    args.option("--path", path);
    args.option("--power", power);
    args.option("--rossby", signed_Ro);
    args.option("--steady", steady);

#ifndef QUIET
    // Done with options/flag parsing
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET
    
    // File output object
    File_Output files(path);

    // Prepare meshes
    Mesh_1D<double> mesh_r, mesh_theta;
    Mesh_1D<std::complex<double> > mesh_r_complex, mesh_theta_complex;

    mesh_r.power_mesh(0., max_r, num_r, num_vars, power, Mesh_Left);
    mesh_theta.uniform_mesh(0., max_theta, num_theta, num_vars);
    mesh_r_complex.power_mesh(0., max_r, num_r, num_vars, power, Mesh_Left);
    mesh_theta_complex.uniform_mesh(0., max_theta, num_theta, num_vars);

    Mesh_1D<double> mesh_pole_steady(mesh_r), mesh_equator_steady(mesh_r);
    Mesh_1D<std::complex<double> > mesh_pole(mesh_r_complex),
        mesh_equator(mesh_r_complex);
    Mesh_2D<double> mesh_bl_steady(mesh_r, mesh_theta);
    Mesh_2D<std::complex<double> > mesh_bl(mesh_r_complex,
            mesh_theta_complex);

    /*// Initial guesses for pole and equator
    for (std::size_t i(0); i < num_r; ++i)
    {
        double r(mesh_pole_steady.node(i));
        mesh_pole_steady(i, f) = -0.5*std::exp(-r*r);
        mesh_pole_steady(i, fr) = r*std::exp(-r*r);
        mesh_pole_steady(i, v) = std::exp(-r*r);
        mesh_equator_steady(i, f) = -0.5*std::exp(-r*r);
        mesh_equator_steady(i, fr) = r*std::exp(-r*r);
        mesh_equator_steady(i, v) = std::exp(-r*r);
    }*/

    // Steady solvers for pole and equator
    Rotating_Edge_Streamfunction pole_steady(mesh_pole_steady);
    Rotating_Edge_Streamfunction equator_steady(mesh_equator_steady);
    Rotating_Layer_Streamfunction bl_steady(mesh_pole_steady,
            mesh_equator_steady, mesh_bl_steady);

    // Compute steady solution in BL
    if (steady)
    {
        // Solution parameters
        pole_steady.set_edge("pole");
        equator_steady.set_edge("equator");

        for (; Ro_iterate > signed_Ro; Ro_iterate -= 0.01)
        {
            std::cout << "Solving attachment points for Ro = " << Ro_iterate
                << std::endl;
            pole_steady.set_signed_rossby_number(Ro_iterate);
            equator_steady.set_signed_rossby_number(Ro_iterate);

            // Compute steady solutions at pole and equator
            pole_steady.solve_steady();
            equator_steady.solve_steady();
        }

        pole_steady.set_signed_rossby_number(signed_Ro);
        equator_steady.set_signed_rossby_number(signed_Ro);
        bl_steady.set_signed_rossby_number(signed_Ro);

        // Compute steady solutions at pole and equator
        pole_steady.solve_steady();
        equator_steady.solve_steady();

        // Initial guess for BL
        for (std::size_t i(0); i < num_r; ++i)
            for (std::size_t j(0); j < num_theta; ++j)
                mesh_bl_steady.set_vars_at_node(i, j, (
                            mesh_pole_steady.get_vars_at_node(i)
                            + double(j)/double(num_theta - 1)*(
                                mesh_equator_steady.get_vars_at_node(i)
                                - mesh_pole_steady.get_vars_at_node(i))));

        bl_steady.solve_steady();

        std::sprintf(filename, "steady_Ro%.2f_%u_%u_raw.dat", signed_Ro,
                static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
        files.open("steady", filename);
        std::sprintf(filename, "steady_Ro%.2f_%u_%u_flow.dat", signed_Ro,
                static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
        files.open("actual", filename);
        mesh_bl_steady.dump(10, files("steady"));
        bl_steady.dump_flow_variables(files("actual"), 10);
        files.close_all();
        return 0;
    }
    else
    {
        std::sprintf(filename, "%s/steady_Ro%.2f_%u_%u_raw.dat", path,
                signed_Ro, static_cast<unsigned>(num_r),
                static_cast<unsigned>(num_theta));
        mesh_bl_steady.load_data(filename);

        for (std::size_t i(0); i < num_r; ++i)
        {
            mesh_pole_steady(i, f) = mesh_bl_steady(i, 0, f);
            mesh_pole_steady(i, fr) = mesh_bl_steady(i, 0, fr);
            mesh_pole_steady(i, v) = mesh_bl_steady(i, 0, v);
            mesh_equator_steady(i, f) = mesh_bl_steady(i, num_theta - 1, f);
            mesh_equator_steady(i, fr) = mesh_bl_steady(i, num_theta - 1, fr);
            mesh_equator_steady(i, v) = mesh_bl_steady(i, num_theta - 1, v);
        }
    }

    // Spectral solvers
    Rotating_Fourier_Decomposed_Edge pole_problem(mesh_pole_steady, mesh_pole);
    Rotating_Fourier_Decomposed_Edge equator_problem(
            mesh_equator_steady, mesh_equator);
    Rotating_Fourier_Decomposed_Layer bl_problem(
            mesh_pole, mesh_equator, mesh_bl_steady, mesh_bl);

    // Solution parameters
    pole_problem.set_edge("pole");
    pole_problem.omega() = omega;
    pole_problem.set_signed_rossby_number(signed_Ro);
    equator_problem.set_edge("equator");
    equator_problem.omega() = omega;
    equator_problem.set_signed_rossby_number(signed_Ro);
    bl_problem.omega() = omega;
    bl_problem.set_signed_rossby_number(signed_Ro);

    pole_problem.solve();
    equator_problem.solve();

    // Set up file output
    std::sprintf(filename, "fourier%.2f_%.2f_%u_%u.dat", signed_Ro, omega,
            static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
    files.open("out", filename);
    if (convert)
    {
        std::sprintf(filename, "fourier%.2f_%.2f_%u_%u.flow", signed_Ro, omega,
                static_cast<unsigned>(num_r), static_cast<unsigned>(num_theta));
        files.open("actual", filename);
    }

    // Compute spectral solutions
    bl_problem.solve();
    mesh_bl.dump(10, files("out"));
    if (convert)
        bl_problem.dump_flow_variables(files("actual"), 10);

    // Clean up
    files.close_all();
    
    return 0;
}
