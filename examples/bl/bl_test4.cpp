// 2D boundary layer problem
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sys/stat.h>

#include <boundary_layer/Edge_Streamfunction.h>
#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Rotation
{
    double k(10.);
    double S(2.);
    double wobble_rate(0.);

    double wobble(const double& t)
    {
        return 1. + (S - 1.)*std::exp(-k*t*t);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
    console_out.use_ncurses();
    console_out.set_title("Check effect of spin-up rate k on resulting flow");
    console_out.begin_staging_header();
    console_out.stage_header_line("Performing set-up");
    console_out.end_staging_header();

    double max_r(50.), max_theta(2.*std::atan(1.));
    double dt(0.025), max_t(10.), output_start(0.), output_skip(dt);
    char path[100], filename[100];
    std::size_t num_r(201), num_theta(501);
    bool force_output(false);

    args.option("--dt", dt);
    args.option("--force_output", force_output);
    args.option("--k", Rotation::k);
    args.option("--max_r", max_r);
    args.option("--max_t", max_t);
    args.option("--num_r", num_r);
    args.option("--output_skip", output_skip);
    args.option("--output_start", output_start);
    args.option("--path", path);
    args.option("--S", Rotation::S);
    args.option("--wobble", Rotation::wobble_rate);

    std::sprintf(path, "/tmp/dat/temp_k%.2f", Rotation::k);

    // Check if folder already exists, forcing flag, etc
    {
        struct stat st;
        char command[200];

        if (stat(path, &st) == 0 && S_ISDIR(st.st_mode))
        {
            // Folder exists, check force flag and clear folder or just exit
            if (force_output)
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " set --- cleaning folder\n");
                sprintf(command, "rm %s/*", path);
                system(command);
                sleep(3);
            }
            else
            {
                console_out(" Output directory:\n    " + str_convert(path)
                        + "\n already exists and --force_output flag"
                        + " not set --- aborting\n");
                sleep(3);
                return 1;
            }
        }
        else
        {
            // Create folder
            sprintf(command, "mkdir %s", path);
            system(command);
        }
    }

    File_Output files(path);
    std::size_t num_steps(max_t/dt);
    std::size_t num_vars(3);
    std::size_t output_from(output_start/dt);
    std::size_t skip(output_skip/dt);

    Mesh_1D<double> r_mesh, theta_mesh;
    r_mesh.uniform_mesh(0., max_r, num_r, num_vars);
    theta_mesh.uniform_mesh(0., max_theta, num_theta, num_vars);
    Mesh_1D<double> equator_mesh(r_mesh), pole_mesh(r_mesh);
    Mesh_2D<double> mesh(r_mesh, theta_mesh);

    for (std::size_t i(0); i < num_r; ++i)
    {
        pole_mesh(i, v) = Rotation::S;
        equator_mesh(i, v) = Rotation::S;
        for (std::size_t j(0); j < num_theta; ++j)
            mesh(i, j, v) = Rotation::S;
    }

    // Solvers for pole and equator
    Edge_Streamfunction pole_problem(pole_mesh);
    Edge_Streamfunction equator_problem(equator_mesh);
    Elliptic_Boundary_Layer bl_problem(pole_mesh, equator_mesh, mesh);

    pole_problem.set_edge("pole");
    pole_problem.spin_up_parameter() = Rotation::S;
    pole_problem.set_sphere_rotation(&Rotation::wobble);
    equator_problem.set_edge("equator");
    equator_problem.spin_up_parameter() = Rotation::S;
    equator_problem.set_sphere_rotation(&Rotation::wobble);
    bl_problem.spin_up_parameter() = Rotation::S;
    bl_problem.set_external_bump(&Rotation::wobble);

    try
    {
        for (std::size_t step(0); step < num_steps; ++step)
        {
            console_out.begin_staging_header();
            args.print_recognised_arguments();
            console_out.stage_header_line("Time step: "
                    + str_convert(step + 1) + "\n");
            console_out.stage_header_line("Current time (pre-solve): "
                    + str_convert(pole_problem.time()));
            console_out.end_staging_header();

            if (step >= output_from && (step - output_from)%skip == 0)
            {
                std::sprintf(filename, "full%u.dat", unsigned(step));
                files.open("bl", filename);

                mesh.dump(16, files("bl"));

                files.close_all();
            }

            pole_problem.step(dt);
            equator_problem.step(dt);
            bl_problem.step(dt);
        }
    }
    catch (std::exception& e)
    {
        console_out("Something terrible has happened.\n");
        getch();
    }
    console_out.stop_ncurses();
    
    return 0;
}
