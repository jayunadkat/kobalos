#! /bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: steady_evolution.sh [--equator] [--tol [value]]"
fi

filename="./dat/steady_evolution_pole.dat"
for arg in "$@"; do
    if [ $arg == "--equator" ]; then
        filename="./dat/steady_evolution_equator.dat"
    fi
done

rm -f $filename

S=$(echo "1.00" | bc)
while [ $(echo $S | awk '{if ($1 <= 2.5) {print "0"} else {print "1"} }') == "0" ]; do
    echo -n $S >> $filename
    echo -n " " >> $filename
    ./bl_steady.out $@ --S $S >> $filename
    echo " " >> $filename
    S=$(echo "$S + 0.01" | bc)
done
