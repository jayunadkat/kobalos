#include <cmath>
#include <fstream>

#include <boundary_layer/Rotating_Edge_EVP.h>
#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Command_Line_Args.h>
#include <Mesh_1D.h>

using namespace Kobalos;

enum {f, fr, v};

namespace Mesh
{
    double power(2.);

    double map(const double& s)
    {
        return std::pow(s + 0.1, power) - std::pow(0.1, power);
    }

    double inv_map(const double& x)
    {
        return std::pow(x + std::pow(0.1, power), 1./power) - 0.1;
    }

    double map_ds(const double& s)
    {
        return power*std::pow(s + 0.1, power - 1.);
    }

    double map_ds2(const double& s)
    {
        return power*(power - 1.)*std::pow(s + 0.1, power - 2.);
    }
}

int main(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

#ifndef QUIET
    if (argc > 1)
        args.print_recognised_arguments();
#endif  // QUIET

    double signed_Ro(-1.04);
    double max_r(50.), parameter(0.);
    std::size_t num_r(401), num_vars(3);
    char full_path[150], type_string[16];
    bool spatial_evp(false);

    std::ofstream data, data2, data3;

    args.option("--max_r", max_r);
    args.option("--num_r", num_r);
    args.option("--param", parameter);
    args.option("--power", Mesh::power);
    args.option("--spatial", spatial_evp);
    args.option("--rossby", signed_Ro);

    if (spatial_evp)
        std::sprintf(type_string, "_spatial");
    else
        std::sprintf(type_string, "_temporal");

    std::sprintf(full_path, "/tmp/unadkat/evp/eval_%.3f_%u_%.1f%s.dat",
            signed_Ro, unsigned(num_r), Mesh::power, type_string);
    data.open(full_path);
    std::sprintf(full_path, "/tmp/unadkat/evp/evec_%.3f_%u_%.1f%s.dat",
            signed_Ro, unsigned(num_r), Mesh::power, type_string);
    data2.open(full_path);
    std::sprintf(full_path, "/tmp/unadkat/evp/steady_%.3f_%u_%.1f%s.dat",
            signed_Ro, unsigned(num_r), Mesh::power, type_string);
    data3.open(full_path);

    Mesh_1D<double> mesh;
    mesh.mapped_grid(0., max_r, num_r, num_vars, &Mesh::map, &Mesh::inv_map,
            &Mesh::map_ds, &Mesh::map_ds2);
    for (std::size_t i(0); i < num_r; ++i)
    {
        double r(mesh.unmapped_node(i)), r2(r);
        mesh(i, f) = 0.5*(1. - std::exp(-r*r));
        mesh(i, fr) = r*std::exp(-r*r);
        if (signed_Ro < -1.)
            r2 -= 1.;
        mesh(i, v) = r2*std::exp(-r*r);
    }

    Rotating_Edge_EVP problem(mesh);
    problem.set_parameter(parameter);
    if (spatial_evp)
        problem.spatial_evp_problem();
    else
        problem.temporal_evp_problem();

    problem.set_signed_rossby_number(signed_Ro);
    problem.solve();

    for (std::size_t i(0), size(problem.evp_system().solution_size());
            i < size; ++i)
    {
        if (problem.evp_system().is_complex(i))
        {
            data
                << problem.evp_system().eigenvalue(i)[0] << ' '
                << problem.evp_system().eigenvalue(i)[1] << std::endl;
            data
                << problem.evp_system().eigenvalue(i)[0] << ' '
                << -problem.evp_system().eigenvalue(i)[1] << std::endl;
        }
        else
        {
            data
                << problem.evp_system().eigenvalue(i)[0] << ' '
                << problem.evp_system().eigenvalue(i)[1] << std::endl;
        }
    }
    for (std::size_t i(0); i < num_r; ++i)
    {
        data2 << mesh.unmapped_node(i);
        for (std::size_t j(0), size(problem.evp_system().solution_size());
                j < size; ++j)
        {
            if (problem.evp_system().is_complex(j))
            {
                data2
                    << ' ' << problem.evp_system().eigenvector_real(j)[3*i + f]
                    << ' ' << problem.evp_system().eigenvector_imag(j)[3*i + f];
            }
            else
            {
                data2 << ' '
                    << problem.evp_system().eigenvector_real(j)[3*i + f];
            }
        }
        data2 << std::endl;
    }
    problem.dump_flow_variables(data3);

    data.close();
    data2.close();
    data3.close();

    return 0;
}

