// Solves time-dependent problem for \(S=1.5\) on \(r\in[0,200]\) with
// \(\Delta r=0.05\). Time domain is \(t\in[0,210]\) with \(\Delta t=0.05\)
// on \(t\in[0,200]\) and \(\Delta t=0.002\) on \(t\in[200,210]\). Profile
// data dumped with skip = 2.

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double S(0.);

    double Omega(double t)
    {
        return 1. + (Problem::S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u] -
                    state[v]*state[v] + Problem::S*Problem::S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(3, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t());
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Problem::S;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        std::cout << "Usage: pole_far2\n";
        return 0;
    }

    double left(0.), right(200.), x(0.), dt(0.05), dx(0.05), S(1.5);
    std::size_t num_nodes((right - left)/dx + 1);
    std::size_t max_time_steps(4000);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    datafile.open("/tmp/dat/invest.dat");

    Problem::S = S;
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());
    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        mesh(i, v) = Problem::S;
    }

    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(eqn, conditions, mesh);
    for (std::size_t time_count(0); time_count < max_time_steps;
            ++time_count)
    {
        pde_problem.step(dt);
    }

    for (std::size_t time_count(0); time_count < 5000; ++time_count)
    {
        pde_problem.step(0.002);
        mesh.dump(datafile, 2);
    }

    datafile.close();

    return 0;
}

