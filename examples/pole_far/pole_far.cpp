// Solves steady problem for suppled \(S\) range on \(r\in[0,100]\) with
// \(\Delta r=0.01\). Dumps value of \(w_\infty\) against \(S\).

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double S(0.);

    double Omega(double t)
    {
        return 1. + (Problem::S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u] -
                    state[v]*state[v] + Problem::S*Problem::S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(3, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - 1.;//Omega(t());
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Problem::S;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        return 0;
    }

    //const double pi = 4.*std::atan(1.);
    double S_begin(std::atof(argv[1])), S_end(std::atof(argv[2])), dS(0.01);
    double left(0.), right(100.), x(0.), dt(0.01), dx(0.01);//, sol_tol(1.e-6);
    std::size_t num_nodes((right - left)/dx + 1), max_time_steps(3000);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    std::stringstream filename;
    filename << "dat/pole_far.dat";//_" << S_begin << "-" << S_end << ".dat";

    datafile.open(filename.str().c_str());
    datafile << "# Stopped at time = 3\n";
    datafile << "# S    w_inf\n";
    //double prev_val(0.);

    if (S_begin > S_end)
        dS = -0.01;

    for (double S = S_begin; std::abs(S - S_begin) <= std::abs(S_end - S_begin);
            S += dS)
    {
        std::cout << "Solving for S = " << S << '\n';
        Problem::S = S;
        mesh.uniform_mesh(left, right, num_nodes, eqn.order());
        Conditions_1D<double> conditions(mesh);
        conditions.add(left, bc_left);
        conditions.add(right, bc_right);

        for (std::size_t i(0); i < num_nodes; ++i)
        {
            x = mesh.node(i);
            mesh(i, u) = x*std::exp(-x);
            mesh(i, ur) = (1. - x)*std::exp(-x);
            mesh(i, v) = S + (1. - S)*std::exp(-x);
            mesh(i, vr) = (S - 1.)*std::exp(-x);
            if (i > 0)
                mesh(i, w) = mesh(i - 1, w) - (mesh(i - 1, u) + mesh(i, u))*(mesh.node(i) - mesh.node(i - 1));

            //mesh(i, v) = S;
        }

        ODE_BVP<double> pde_problem(eqn, conditions, mesh);
        /*for (std::size_t time_count(0); time_count < max_time_steps;
                ++time_count)
        {
            pde_problem.step(dt);
        }*/
        pde_problem.solve();
        /*if (S == S_begin)
        {
            mesh.dump(10, datafile);
            datafile << "\n\n";
        }*/
        datafile << S << " ";
        datafile << mesh(num_nodes - 1, w) << '\n';
        datafile.flush();
    }
    datafile.close();

    return 0;
}

