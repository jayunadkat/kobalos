// Solves time-dependent problem for \(S=1.5\) on \(r\in[0,200]\) with
// \(\Delta r=0.05\). Time interval is \(t\in[0,195.6]\) with \(\Delta t=0.05\)
// for \(t\in[0,195.5]\) and \(\Delta t=0.0001\) for \(t\in[195.5,195.6]\),
// dumps all profiles. Commented-out code performs a naive estimation of the
// disturbance width as time increases.

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double S(0.);

    double Omega(double t)
    {
        return 1. + (Problem::S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u] -
                    state[v]*state[v] + Problem::S*Problem::S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(3, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t());
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Problem::S;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        std::cout << "Usage: pole_far2\n";
        return 0;
    }

    double left(0.), right(200.), x(0.), dt(0.05), dx(0.05), S(1.5);
    std::size_t num_nodes((right - left)/dx + 1);
    std::size_t max_time_steps(3910);
    Mesh_1D<double> mesh;
    std::ofstream datafile, datafile2, datafile3;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    std::stringstream filename;
    filename << "dat/temp/dummy.dat";//"pole_far2_info.dat";

    datafile.open(filename.str().c_str());
    datafile2.open("dat/temp/info_v.dat");
    datafile3.open("dat/temp/info_u.dat");
    datafile << std::scientific << std::setprecision(10);
    datafile2 << std::scientific << std::setprecision(10);
    datafile3 << std::scientific << std::setprecision(10);
    datafile << "# S = 1.5\n";

    Problem::S = S;
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        /*mesh(i, u) = x*std::exp(-x);
        mesh(i, ur) = std::exp(-x)*(1 - x);
        mesh(i, v) = S + (1. - S)*std::exp(-x);
        mesh(i, vr) = (S - 1.)*std::exp(-x);
        if (i > 0)
            mesh(i, w) = -(mesh(i - 1, u) + mesh(i, u))*dx;*/
        mesh(i, v) = Problem::S;
    }

    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(
            eqn, conditions, mesh);
    for (std::size_t time_count(0); time_count < max_time_steps;
            ++time_count)
    {
        pde_problem.step(dt);
        //datafile << "# t = " << dt*(double)(time_count + 1) << '\n';
        //mesh.dump(10, datafile);

        //bool interval(false);
        //bool interval2(false);
        //double max(0.), max_r(0.), int_left(0.), int_right(0.), temp(0.);
        //double max2(0.), max_r2(0.), int_left2(0.), int_right2(0.), temp2(0.);

        //for (std::size_t node(unsigned(20/dx)); node < num_nodes; ++node)
        //{
/*
//v
            if (!interval)
            {
                if (std::abs(mesh(node, v) - Problem::S) > 1.e-4)
                {
                    int_left = mesh.node(node);
                    interval = true;
                }
            }
            else
            {
                if (std::abs(mesh(node, v) - Problem::S) < 1.e-4)
                {
                    int_right = mesh.node(node);
                    if (int_right - int_left > temp)
                        temp = int_right - int_left;
                    interval = false;
                }
            }

            if (std::abs(mesh(node, v) - Problem::S) > max)
            {
                max = std::abs(mesh(node, v) - Problem::S);
                max_r = mesh.node(node);
            }
//u
            if (!interval2)
            {
                if (std::abs(mesh(node, u)) > 1.e-4)
                {
                    int_left2 = mesh.node(node);
                    interval2 = true;
                }
            }
            else
            {
                if (std::abs(mesh(node, u)) < 1.e-4)
                {
                    int_right2 = mesh.node(node);
                    if (int_right2 - int_left2 > temp2)
                        temp2 = int_right2 - int_left2;
                    interval2 = false;
                }
            }

            if (std::abs(mesh(node, u)) > max2)
            {
                max2 = std::abs(mesh(node, u));
                max_r2 = mesh.node(node);
            }            */

            //datafile << std::setw(18) << mesh.node(node) << " ";
            //datafile << std::setw(18) << mesh(node, u) << " ";
            //datafile << std::setw(18) << mesh(node, v) << "\n";
            //datafile << std::setw(18) << mesh(node, w) << "\n";
            //datafile << std::setw(18) << mesh(node, u) << " ";
            //datafile << std::setw(18) << mesh(node, v) - Problem::S << " ";
            //datafile << std::setw(18) << mesh(node, w) - mesh(num_nodes - 1, w) << "\n";
        //}
        //datafile << "\n\n";
        //datafile.flush();
/*
        datafile2 << std::setw(18) << dt*(double)(time_count + 1) << " ";
        datafile2 << std::setw(18) << max_r << " ";
        datafile2 << std::setw(18) << max << " ";
        datafile2 << std::setw(18) << temp << "\n";
        datafile2.flush();

        datafile3 << std::setw(18) << dt*(double)(time_count + 1) << " ";
        datafile3 << std::setw(18) << max_r2 << " ";
        datafile3 << std::setw(18) << max2 << " ";
        datafile3 << std::setw(18) << temp2 << "\n";
        datafile3.flush();*/
    }

    for (std::size_t time_count(0); time_count < 1000;
            ++time_count)
    {
        pde_problem.step(0.0001);
        datafile << "# t = " << dt*(double)(max_time_steps) + 0.0001*(double)(time_count + 1) << '\n';

        for (std::size_t node(unsigned(20/dx)); node < num_nodes; ++node)
        {
            datafile << std::setw(18) << mesh.node(node) << " ";
            datafile << std::setw(18) << mesh(node, u) << " ";
            datafile << std::setw(18) << mesh(node, v) - Problem::S << " ";
            datafile << std::setw(18) << mesh(node, w) - mesh(num_nodes - 1, w) << "\n";
        }
        datafile << "\n\n";
        datafile.flush();
    }

    datafile.close();
    datafile2.close();
    datafile3.close();

    return 0;
}

