// Solves time-dependent problem for \(S=1.5\) on \(r\in[0,400]\) for
// \(t\in[0,250]\), with \(\Delta r=\Delta t=0.05\). Dumps naive peak data for
// disturbance in \(u\), both spatial location against time and temporal maxima
// against time. Dumps scaled data for \(u\) for contour plot.

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double S(0.);

    double Omega(double t)
    {
        return 1. + (Problem::S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u] -
                    state[v]*state[v] + Problem::S*Problem::S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(3, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t());
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Problem::S;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        std::cout << "Usage: pole_far3\n";
        return 0;
    }

    double left(0.), right(400.), x(0.), dt(0.05), dx(0.05), S(1.5);
    std::size_t num_nodes((right - left)/dx + 1);
    std::size_t max_time_steps(5000);
    Mesh_1D<double> mesh;
    std::ofstream datafile, datafile2, datafile3;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    datafile.open("/tmp/dat/peaks_analysis.dat");
    datafile2.open("/tmp/dat/info_u.csv");
    datafile3.open("/tmp/dat/wavespeed.dat");

    Problem::S = S;
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());
    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        mesh(i, v) = Problem::S;
    }

    std::vector<std::vector<double> > peaks;
    std::vector<double> temp(2, 0.);
    double t(0.), sqrt_t(0.), inv_sqrt_t(0.);

    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(eqn, conditions, mesh);
    for (std::size_t time_count(0); time_count < max_time_steps;
            ++time_count)
    {
        pde_problem.step(dt);

        if (dt*double(time_count + 1) >= 40.)
        {
            std::size_t max_n(unsigned(20/dx));
            double max(std::abs(mesh(max_n, u))), max_r(mesh.node(max_n));

            for (std::size_t node(max_n + 1); node < num_nodes - 1; ++node)
            {
                if (std::abs(mesh(node, u)) > max)
                {
                    max = std::abs(mesh(node, u));
                    max_n = node;
                    max_r = mesh.node(max_n);
                }
            }

            t = dt*double(time_count + 1);
            sqrt_t = std::sqrt(t);
            inv_sqrt_t = 1./sqrt_t;

            temp[0] = t;
            temp[1] = mesh(max_n, u);
            peaks.push_back(temp);

            for (std::size_t node(unsigned(20/dx)); node < num_nodes - 1;
                    node += 2)
            {
                datafile2 << t << ",";
                datafile2 << (mesh.node(node) - max_r)*inv_sqrt_t << ",0,";
                datafile2 << mesh(node, u)*sqrt_t << "\n";
            }
            datafile2 << "\n";

            datafile3 << t << " ";
            datafile3 << max_r << '\n';
        }
    }

    //analyse peaks
    bool climbing(false);
    if (peaks[1][1] > peaks[0][1])
        climbing = true;
    double last(peaks[1][1]);

    for (std::size_t i(2), size(peaks.size()); i < size; ++i)
    {
        if (climbing)
        {
            if (peaks[i][1] < last)
            {
                //now falling, peak found so dump it
                datafile << peaks[i - 1][0] << " " << last << '\n';
                climbing = false;
            }
        }
        else
        {
            //falling
            if (peaks[i][1] > last)
            {
                //now climbing, flag it as such
                climbing = true;
            }
        }
        last = peaks[i][1];
    }

    datafile.close();
    datafile2.close();
    datafile3.close();

    return 0;
}

