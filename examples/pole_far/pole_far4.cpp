// Solves time-dependent problem for a supplied range of \(S\), on 
// \(r\in[0,400]\) and \(t\in[0,500]\) with \(\Delta r=\Delta t=0.05\).
// Dumps peak data for disturbance to \(v\), both spatial location against time
// and termporal maxima against time.

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double S(0.);

    double Omega(double t)
    {
        return 1. + (Problem::S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u] -
                    state[v]*state[v] + Problem::S*Problem::S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(3, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t());
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Problem::S;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cout << "Usage: pole_far4 S_start S_end\n";
        return 0;
    }

    double left(0.), right(400.), x(0.), dt(0.05), dx(0.05);
    double S_start(std::atof(argv[1])), S_end(std::atof(argv[2]));
    std::size_t num_nodes((right - left)/dx + 1);
    std::size_t max_time_steps(10000);
    Mesh_1D<double> mesh;
    std::ofstream datafile, datafile2;
    std::stringstream filename, filename2;

    std::vector<std::vector<double> > time_sol;
    std::vector<double> temp(2, 0.);
    std::vector<std::size_t> peaks;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    filename << "dat/wavepacket/profiles_";
    filename << S_start << "_" << S_end << ".dat";
    datafile.open(filename.str());
    datafile << std::scientific << std::setprecision(10);
    datafile << " # t | v_max\n";

    filename2 << "dat/wavepacket/peaks_";
    filename2 << S_start << "_" << S_end << ".dat";
    datafile2.open(filename2.str());
    datafile2 << std::scientific << std::setprecision(10);
    datafile2 << " # t | f/S\n";

    for (Problem::S = S_start; Problem::S < S_end + 0.01; Problem::S += 0.05)
    {
        datafile << " # S = " << Problem::S << '\n';
        datafile2 << " # S = " << Problem::S << '\n';
        bool climbing(false);

        time_sol.clear();
        peaks.clear();
        mesh.uniform_mesh(left, right, num_nodes, eqn.order());

        for (std::size_t i(0); i < num_nodes; ++i)
        {
            x = mesh.node(i);
            mesh(i, v) = Problem::S;
        }

        Conditions_1D<double> conditions(mesh);
        conditions.add(left, bc_left);
        conditions.add(right, bc_right);

        PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(
                eqn, conditions, mesh);
        for (std::size_t time_count(0); time_count < max_time_steps;
                ++time_count)
        {
            pde_problem.step(dt);

            if (dt*double(time_count + 1) > 10)
            {
                double max(0.), max_r(0.);
                std::size_t max_n(0);

                for (std::size_t node(unsigned(20/dx)); node < num_nodes - 1;
                        ++node)
                {
                    if (std::abs(mesh(node, v) - Problem::S) > max)
                    {
                        max = std::abs(mesh(node, v) - Problem::S);
                        max_n = node;
                        max_r = mesh.node(max_n);
                    }
                }

                temp[0] = dt*double(time_count + 1);
                temp[1] = mesh(max_n, v);
                time_sol.push_back(temp);

                datafile << temp[0] << " " << temp[1] << '\n';
            }
        }

        datafile << "\n\n";

        // populate vector of peak locations
        for (std::size_t i(1), size(time_sol.size()); i < size; ++i)
        {
            if (time_sol[i][1] > time_sol[i - 1][1])
            {
                // rising
                climbing = true;
            }
            else
            {
                //falling
                if (climbing)
                {
                    climbing = false;
                    peaks.push_back(i - 1);
                }
            }
        }
        
        // time-average frequency calculation & dump
        for (std::size_t j(3), size(peaks.size()); j < size - 3; ++j)
        {
            datafile2 << time_sol[peaks[j]][0] << " ";
            datafile2 << 6./((time_sol[peaks[j + 3]][0]
                    - time_sol[peaks[j - 3]][0])*Problem::S) << '\n';
        }

        datafile2 << "\n\n";
    }

    datafile.close();
    datafile2.close();

    return 0;
}

