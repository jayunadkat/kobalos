// Solves time-dependent problem for \(S=1.5\) on \(r\in[0,100]\) for 
// \(t\in[0,300]\) with \(\Delta r=\Delta t=0.01\). Dumps \(w_\infty\) against
// \(t\).

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double S(0.);

    double Omega(double t)
    {
        return 1. + (Problem::S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u] -
                    state[v]*state[v] + Problem::S*Problem::S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(3, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t());
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 5) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Problem::S;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        return 0;
    }

    double S(std::atof(argv[1]));
    double left(0.), right(100.), dt(0.01), dx(0.01);
    std::size_t num_nodes((right - left)/dx + 1), max_time_steps(30000);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    std::stringstream filename;
    filename << "dat/pole_test" << S << "long.dat";

    datafile.open(filename.str().c_str());
    datafile << "# S = " << S << '\n';
    datafile << "# Stopped at time = " << long(max_time_steps)*dt << '\n';
    datafile << "# t    w_inf\n";
        
    Problem::S = S;
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());
    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        mesh(i, v) = S;
    }

    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(
            eqn, conditions, mesh);
    for (std::size_t time_count(0); time_count < max_time_steps;
            ++time_count)
    {
        pde_problem.step(dt);
        datafile << long(time_count + 1)*dt << " ";
        datafile << mesh(num_nodes - 1, w) << '\n';
        datafile.flush();
    }
    datafile.close();

    return 0;
}

