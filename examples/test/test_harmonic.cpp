#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {f, fp};

namespace Problem
{
    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(2) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[f] = state[fp];
                residual_[fp] = -state[f];
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(1, 2) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(1, 2) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f] - 1.;
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    double left(0.), right(2.*std::atan(1.)), x(0.);
    std::size_t num_nodes(1001);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    datafile.open("test_harmonic.dat");
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());
    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        mesh(i, 0) = x/right;
        mesh(i, 1) = 1./right;
    }

    ODE_BVP<double> ode_problem(eqn, conditions, mesh);
    ode_problem.solve();
    mesh.dump(10, datafile);
    datafile.close();

    return 0;
}

