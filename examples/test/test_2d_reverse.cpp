#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include <PDE_Bundle.h>

using namespace Kobalos;

enum {u, ux};

namespace Problem2D
{
    class Equation : public Equation_2D<double>
    {
        protected:
            double source()
            {
                return -x1()*std::exp(x2() - 5.) +
                    std::pow(x1()*t()*std::exp(x2() - 5.), 2.);
            }

            void update_jacobian_t() {}
            void update_jacobian_x() {}
            void update_jacobian_y() {}

        public:
            Equation() : Equation_2D<double>(2) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[ux];
                residual_[1] = source();
            }

            inline void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ux, u) = -1.;
            }

            inline void matrix_y(const Vector<double>& state)
            {
                matrix_y_(ux, u) = state[u];
            }
            
            void jacobian_t_vec(const Vector<double>& state) {}
            void jacobian_x_vec(const Vector<double>& state) {}

            void jacobian_y_vec(const Vector<double>& state)
            {
                jacobian_y_vec_(ux, u) = state[u];
            }
    };

    class BCLeft : public Residual<double>
    {
        public:
            BCLeft() : Residual<double>(1, 2) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
            }
    };

    class BCRight : public Residual<double>
    {
        public:
            BCRight() : Residual<double>(1, 2) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u] - t()*std::exp(x2() - 5.);
            }
    };
}   // namespace

int nonlinear_2d(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    std::ofstream datafile, d2;
    datafile.open("/tmp/dat/test.dat");
    d2.open("/tmp/dat/test2.dat");

    std::vector<double> k_real({0.5, 0.25, 0.2, 0.125, 0.1, 0.05, 0.025, 0.02,
            0.0125, 0.01}), k({0.05});

    for (std::size_t l(0), k_size(k.size()); l < k_size; ++l)
    {
        double left_x(0.), right_x(1.), dx(k[l]);
        double left_y(5.), right_y(0.), dy(k[l]);
        double dt(k[l]), x(0.), y(0.), t(0.);
        std::size_t max_steps(10/dt);
        std::size_t num_nodes_x((right_x - left_x)/dx + 1);
        std::size_t num_nodes_y((left_y - right_y)/dy + 1);

        Mesh_1D<double> begin_y;
        Mesh_2D<double> mesh;
        begin_y.uniform_mesh(left_x, right_x, num_nodes_x, 2);
        mesh.uniform_mesh(left_x, right_x, num_nodes_x,
                left_y, right_y, num_nodes_y, 2);

        Problem2D::Equation eqn;
        Problem2D::BCLeft bcleft;
        Problem2D::BCRight bcright;

        Conditions_2D<double> conditions(mesh);
        conditions.add_x(left_x, bcleft);
        conditions.add_x(right_x, bcright);
        conditions.add_y(begin_y);

        PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double> pde_problem(
                eqn, conditions, mesh);
        for (std::size_t i(0); i < max_steps; ++i)
        {
            for (std::size_t j(0); j < num_nodes_x; ++j)
            {
                x = begin_y.node(j);
                t = pde_problem.time() + dt;
                begin_y(j, u) = x*t;
                begin_y(j, ux) = t;
            }
            //begin_y.dump(10, d2);
            //d2.flush();

            pde_problem.step(dt);

            //mesh.dump(10, datafile);
            //datafile.flush();
        }

        for (std::size_t i(0); i < num_nodes_x; ++i)
        {
            for (std::size_t j(0); j < num_nodes_y; ++j)
            {
                x = mesh.node_x(i);
                y = mesh.node_y(j);
                datafile << x << ' ' << y << ' ' << mesh(i, j, u) << ' ';
                datafile << x*10*std::exp(y - 5.) << '\n';
            }
            datafile << '\n';
        }
        /*
        x = mesh.node_x(0.7/dx);
        y = mesh.node_y(0.3/dy);
        d2 << dt << ' ';
        d2 << std::abs(mesh(0.7/dx, 0.3/dy, u) - x*5*std::exp(-y)) << '\n';
        d2.flush();*/
    }

    datafile.close();
    d2.close();

    return 0;
}

int main(int argc, char** argv)
{
    return nonlinear_2d(argc, argv);
}
