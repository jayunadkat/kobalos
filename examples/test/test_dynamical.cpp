#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include <Conditions_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {u, ux, v, vx};

namespace Problem
{
    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(4) {}

            void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ux];
                residual_[ux] = state[u];
                residual_[v] = state[vx];
                residual_[vx] = state[ux];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ux, v) = -1.;
                matrix_t_(vx, u) = 1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(2, 4) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u] - std::sin(2*t());
                residual_[1] = state[v] - std::cos(2*t());
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 4) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u] + std::sin(2*t());
                residual_[1] = state[v] + std::cos(2*t());
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    const double pi = 4.*std::atan(1.);
    double left(0.), right(pi), x(0.), dt(0.005), dx(0.005);
    std::size_t num_nodes((right - left)/dx + 1), max_time_steps(301);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    datafile.open("test.dat");
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());

    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);
    
    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        mesh(i, u) = std::sin(x);
        mesh(i, ux) = std::cos(x);
        mesh(i, v) = std::cos(x);
        mesh(i, vx) = -std::sin(x);
    }

    mesh.dump(10, datafile);
    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(
            eqn, conditions, mesh);
    for (std::size_t time_count(0); time_count < max_time_steps; ++time_count)
    {
        pde_problem.step(dt);
        datafile << "Time = " << (time_count + 1)*dt << '\n';
        mesh.dump(10, datafile);
    }
    datafile.close();

    return 0;
}

