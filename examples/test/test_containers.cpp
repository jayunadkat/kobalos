#include <cstdlib>
#include <iostream>
#include <vector>

#include <Banded_Matrix.h>
#include <Dense_Matrix.h>
#include <Exceptions.h>
#include <Vector.h>
#include <Timer.h>

using namespace Kobalos;

template <typename type_>
class Matrix
{
    protected:
        std::vector<std::vector<type_> > storage_;
        std::size_t N_;

    public:
        Matrix(const std::size_t& N, const type_& value);
        ~Matrix();

        type_& operator()(const std::size_t& row, const std::size_t& column);
        const type_& operator()(const std::size_t& row,
                const std::size_t& column) const;
};

template <typename type_>
Matrix<type_>::Matrix(const std::size_t& N, const type_& value) :
    storage_(N, std::vector<type_>(N, value)),
    N_(N)
{
}

template <typename type_>
Matrix<type_>::~Matrix()
{
}

template <typename type_>
type_& Matrix<type_>::operator()(const std::size_t& row,
        const std::size_t& column)
{
    return storage_[row][column];
}

template <typename type_>
const type_& Matrix<type_>::operator()(const std::size_t& row,
        const std::size_t& column) const
{
    return storage_[row][column];
}

void fill_random(Dense_Matrix<double>& A)
{
    std::size_t n(A.num_rows()), m(A.num_columns()), i(0), j(0);
    double inv = 1./(double)RAND_MAX;
    for (; i < n; ++i)
        for (j = 0; j < m; ++j)
            A(i, j) = rand()*inv;
}

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    Timer timer1("Dense_Matrix"), timer2("native array"), timer3("Matrix");
    double* B;
    double perc_slowdown;
    std::size_t N = 5000;

    Dense_Matrix<double> A(N, 0.);
    fill_random(A);
    Matrix<double> C(N, 0.);
    B = new double[N*N];
    for (std::size_t i = 0; i < N; ++i)
        for (std::size_t j = 0; j < N; ++j)
            C(i, j) = B[i*N + j] = A(i, j);

    timer1.start();
    for (std::size_t counter = 0; counter < 20; ++counter)
    {
        for (std::size_t j = 0; j < N; ++j)
            for (std::size_t i = 0; i < N; ++i)
                A.set(i, j) *= 2.;
    }
    timer1.stop();

    timer2.start();
    for (std::size_t counter = 0; counter < 20; ++counter)
    {
        for (std::size_t j = 0; j < N; ++j)
            for (std::size_t i = 0; i < N; ++i)
                B[j*N + i] *= 2.;
    }
    timer2.stop();

    timer3.start();
    for (std::size_t counter = 0; counter < 20; ++counter)
    {
        for (std::size_t i = 0; i < N; ++i)
            for (std::size_t j = 0; j < N; ++j)
                C(j, i) *= 2.;
    }
    timer3.stop();

    timer1.print();
    timer2.print();
    timer3.print();
    perc_slowdown = 100.*(timer1.running_time() - timer2.running_time());
    perc_slowdown /= timer2.running_time();
    std::cout << "Percentage slowdown: " << perc_slowdown << "%\n\n";

    return 0;
}

