#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include <PDE_Bundle.h>

using namespace Kobalos;

enum {u, ux};

namespace Problem2D
{
    class Equation : public Equation_2D<double>
    {
        protected:
            double source()
            {
                return -2*std::exp(-x2()*t()) +
                    (1 - x1()*x1())*(x2() + t())*std::exp(-x2()*t());
            }

        public:
            Equation() : Equation_2D<double>(2) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[ux];
                residual_[1] = source();
            }

            inline void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ux, u) = -1.;
            }

            inline void matrix_y(const Vector<double>& state)
            {
                matrix_y_(ux, u) = -1.;
            }

            void update_jacobian_t_vec() {}
            void update_jacobian_x_vec() {}
            void update_jacobian_y_vec() {}
    };

    class BCBoth : public Residual<double>
    {
        public:
            BCBoth() : Residual<double>(1, 2) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
            }
    };
}   // namespace

int linear_2d(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    std::ofstream datafile;
    datafile.open("/tmp/dat/test.dat");

    double left_x(-1.), right_x(1.), dx(0.05);
    double left_y(0.), right_y(10.), dy(0.05);
    double dt(0.01), x(0.);
    std::size_t max_steps(500);
    std::size_t num_nodes_x((right_x - left_x)/dx + 1);
    std::size_t num_nodes_y((right_y - left_y)/dy + 1);

    Mesh_1D<double> begin_y;
    Mesh_2D<double> mesh;
    begin_y.uniform_mesh(left_x, right_x, num_nodes_x, 2);
    mesh.uniform_mesh(left_x, right_x, num_nodes_x,
            left_y, right_y, num_nodes_y, 2);

    Problem2D::Equation eqn;
    Problem2D::BCBoth bcboth;

    Conditions_2D<double> conditions(mesh);
    conditions.add_x(left_x, bcboth);
    conditions.add_x(right_x, bcboth);
    conditions.add_y(begin_y);
    
    for (std::size_t i(0); i < num_nodes_x; ++i)
    {
        x = mesh.node_x(i);
        for (std::size_t j(0); j < num_nodes_y; ++j)
        {
            mesh(i, j, u) = 1. - x*x;
            mesh(i, j, ux) = -2.*x;
        }
    }

    for (std::size_t j(0); j < num_nodes_x; ++j)
    {
        x = begin_y.node(j);
        begin_y(j, u) = 1. - x*x;
        begin_y(j, ux) = -2.*x;
    }

    PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double> pde_problem(
            eqn, conditions, mesh);
    for (std::size_t i(0); i < max_steps; ++i)
    {
        pde_problem.step(dt);

        mesh.dump(10, datafile);
        datafile.flush();
        
        //for (std::size_t i(0); i < num_nodes_x; ++i)
        //    datafile << mesh.node_x(i) << " " << mesh(i, 0, 0) << '\n';
    }

    datafile.close();

    return 0;
}

int main(int argc, char** argv)
{
    return linear_2d(argc, argv);
}
