#include <cmath>
#include <complex>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

#include <Banded_Linear_System.h>
#include <Banded_Matrix.h>
#include <Conditions_1D.h>
#include <Dense_Matrix.h>
#include <Exceptions.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <Residual.h>
#include <Vector.h>
#include <Timer.h>

using namespace Kobalos;

enum {f, fp, fpp};

namespace Problem
{
    class Blasius_Eqn : public Residual<double>
    {
        public:
            Blasius_Eqn() : Residual<double>(3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[f] = state[fp];
                residual_[fp] = state[fpp];
                residual_[fpp] = -state[f]*state[fpp]/2.;
            }
    };

    class Blasius_LeftBC : public Residual<double>
    {
        public:
            Blasius_LeftBC() : Residual<double>(2, 3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f];
                residual_[1] = state[fp];
            }
    };

    class Blasius_RightBC : public Residual<double>
    {
        public:
            Blasius_RightBC() : Residual<double>(1, 3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[fp] - 1.;
            }
    };
}

int main(int argc, char** argv)
{
    double left(0.), right(20.), x(0.);
    std::size_t num_nodes(200001);
    std::ofstream datafile;
    datafile.open("test_blasius_new.dat");

    Problem::Blasius_Eqn equation;
    Problem::Blasius_LeftBC left_bc;
    Problem::Blasius_RightBC right_bc;
    Mesh_1D<double> mesh;
    Conditions_1D<double> conditions(mesh);
    conditions.add(left, left_bc);
    conditions.add(right, right_bc);

    mesh.uniform_mesh(left, right, num_nodes, equation.num_vars());
    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        /*mesh(i, 0) = x*x/(1 + x);
          mesh(i, 1) = x*(2 + x)/((1 + x)*(1 + x));
          mesh(i, 2) = 2/((1 + x)*(1 + x)*(1 + x));*/
        mesh(i, f) = x*(1. - std::exp(-x));
        mesh(i, fp) = (1. - std::exp(-x)) + x*std::exp(-x);
        mesh(i, fpp) = 2.*std::exp(-x) - x*std::exp(-x);
    }

    ODE_BVP<double> blasius_problem(equation, conditions, mesh);
    blasius_problem.solve();

    mesh.dump(10, datafile);
    datafile.close();

    return 0;
}

