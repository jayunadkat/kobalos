#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {f, fp, fpp};

namespace Problem
{
    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(3) {}

            void residual_function(const Vector<double>& state)
            {
                residual_[f] = state[fp];
                residual_[fp] = state[fpp];
                residual_[fpp] = -2.*state[fp];
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(fpp, f) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(2, 3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f] - std::sin(t());
                residual_[1] = state[fp] - std::cos(t());
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(1, 3) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f] + std::sin(t());
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    double left(0.), right(4.*std::atan(1.)), x(0.), dt(0.005), dx(0.005);
    std::size_t num_nodes((right - left)/dx + 1), max_time_steps(301);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    datafile.open("test_weird.dat");
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());

    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        mesh(i, f) = std::sin(x);
        mesh(i, fp) = std::cos(x);
        mesh(i, fpp) = -std::sin(x);
    }

    mesh.dump(10, datafile);
    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(
            eqn, conditions, mesh);
    for (std::size_t time_count(0); time_count < max_time_steps; ++time_count)
    {
        pde_problem.step(dt);
        for (std::size_t j(0); j < num_nodes; ++j)
        {
            datafile << mesh.node(j) << " ";
            datafile << mesh(j, 0) << '\n';
        }
        datafile << "\n\n";
    }
    datafile.close();

    return 0;
}

