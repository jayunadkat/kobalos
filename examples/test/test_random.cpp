#include <complex>
#include <iostream>
#include <vector>

#include <Dense_Matrix.h>
#include <Exceptions.h>
#include <Vector.h>
#include <Timer.h>

using namespace Kobalos;

double max_rand = (double)RAND_MAX;

double get_rand(double max)
{
    return (double)std::rand()*max/max_rand;
}

void randomise_matrix(Dense_Matrix<double>& A, double max_component)
{
    for (std::size_t n = A.num_rows(), i = 0; i < n; ++i)
        for (std::size_t m = A.num_columns(), j = 0; j < m; ++j)
            A(i, j) = get_rand(max_component);
}

void randomise_matrix(Dense_Matrix<std::complex<double> >& A,
        double max_component)
{
    for (std::size_t n = A.num_rows(), i = 0; i < n; ++i)
        for (std::size_t m = A.num_columns(), j = 0; j < m; ++j)
        {
            A(i, j) = std::complex<double>(get_rand(max_component),
                    get_rand(max_component));
        }
}

int main()
{
    Dense_Matrix<double> testing(4, 5, 0);
    randomise_matrix(testing, 1.);
    testing.dump();
    std::cout << '\n';
    testing.transpose();
    testing.dump();
    return 0;
}
