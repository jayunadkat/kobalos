#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include <Conditions_1D.h>
#include <Equation_1D.h>
#include <Mesh_1D.h>
#include <ODE_BVP.h>
#include <PDE_IBVP.h>
#include <Residual.h>
#include <Vector.h>

using namespace Kobalos;

enum {f, fp};

namespace Problem
{
    class Equation : public Equation_1D<double>
    {
        public:
            Equation() : Equation_1D<double>(2) {}

            void residual_function(const Vector<double>& state)
            {
                residual_[f] = state[fp];
                residual_[fp] = -std::sin(x1());
            }

            void matrix_t(const Vector<double>& state)
            {
                matrix_t_(fp, f) = -1.;
            }
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(1, 2) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f] - std::exp(-t());
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(1, 2) {}
            void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[f] + std::exp(-t());
            }
    };
}   // namespace

int main(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    const double pi = 4.*std::atan(1.);
    double left(0.), right(pi), x(0.), dt(0.005), dx(0.005);
    std::size_t num_nodes((right - left)/dx + 1), max_time_steps(10000);
    Mesh_1D<double> mesh;
    std::ofstream datafile;
    
    Problem::Equation eqn;
    Problem::LeftBC bc_left;
    Problem::RightBC bc_right;

    datafile.open("test.dat");
    mesh.uniform_mesh(left, right, num_nodes, eqn.order());
    
    Conditions_1D<double> conditions(mesh);
    conditions.add(left, bc_left);
    conditions.add(right, bc_right);

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        x = mesh.node(i);
        mesh(i, f) = std::sin(x) + std::cos(x);
        mesh(i, fp) = std::cos(x) - std::sin(x);
    }

    mesh.dump(10, datafile);
    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(
            eqn, conditions, mesh);
    for (std::size_t time_count(0); time_count < max_time_steps; ++time_count)
    {
        pde_problem.step(dt);
        datafile << "Time = " << (time_count + 1)*dt << '\n';
        mesh.dump(10, datafile);
    }
    datafile.close();

    return 0;
}

