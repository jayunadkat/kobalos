#include <cmath>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <Command_Line_Args.h>
#include <File_Output.h>
#include <Generalised_Eigenvalue_System.h>
#include <Generalised_Eigenvalue_System_Complex.h>
#include <ODE_Bundle.h>
#include <PDE_Bundle.h>
#include <Sparse_Linear_System.h>
#include <Sparse_Matrix.h>
#include <Vector.h>

#include <slu_ddefs.h>

using namespace Kobalos;

enum {u, ur, v, vr, w};

namespace Problem
{
    double S(0.);

    double Omega(double t)
    {
        return 1. + (Problem::S - 1.)*std::exp(-t*t);
    }

    class Equation : public Equation_1D<double>
    {
        protected:
            void update_jacobian_t() {}
            void update_jacobian_x() {}
            
        public:
            Equation() : Equation_1D<double>(5) {}

            inline void residual_function(const Vector<double>& state)
            {
                residual_[u] = state[ur];
                residual_[ur] = state[w]*state[ur] + state[u]*state[u] -
                    state[v]*state[v] + Problem::S*Problem::S;
                residual_[v] = state[vr];
                residual_[vr] = state[w]*state[vr] + 2*state[u]*state[v];
                residual_[w] = -2*state[u];
            }

            inline void matrix_t(const Vector<double>& state)
            {
                matrix_t_(ur, u) = -1.;
                matrix_t_(vr, v) = -1.;
            }

            void update_jacobian_t_vec() {}
            void update_jacobian_x_vec() {}
    };

    class LeftBC : public Residual<double>
    {
        public:
            LeftBC() : Residual<double>(3, 5) {}
            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Omega(t());
                residual_[2] = state[w];
            }
    };

    class RightBC : public Residual<double>
    {
        public:
            RightBC() : Residual<double>(2, 5) {}
            inline void residual_function(const Vector<double>& state)
            {
                residual_[0] = state[u];
                residual_[1] = state[v] - Problem::S;
            }
    };
}   // namespace

int pole_far(int argc, char** argv)
{
    if (argc != 1)
    {
        return 0;
    }

    double left(0.), right(100.), dx(0.01), dt(0.01);
    std::size_t max_steps(1000), num_nodes((right - left)/dx + 1);

    Mesh_1D<double> mesh;
    mesh.uniform_mesh(left, right, num_nodes, 5);

    Problem::Equation eqn;
    Problem::LeftBC left_bc;
    Problem::RightBC right_bc;
    Problem::S = 1.5;

    Conditions_1D<double> conditions(mesh);
    conditions.add(0., left_bc);
    conditions.add(100., right_bc);

    for (std::size_t i(0), nodes(mesh.num_nodes()); i < nodes; ++i)
    {
        mesh(i, v) = Problem::S;
    }

    PDE_IBVP<PDE_1D_Unsteady, double, double> pde_problem(eqn, conditions,
            mesh);
    for (std::size_t i(0); i < max_steps; ++i)
        pde_problem.step(dt);

    return 0;
}

int test_conditions_2d(int argc, char** argv)
{
    Mesh_2D<double> mesh;
    Mesh_1D<double> mesh2;

    mesh.uniform_mesh(0., 10., 101, 0, 5, 501, 0);
    mesh2.uniform_mesh(0., 10., 101, 0);
    Conditions_2D<double> conditions(mesh);

    conditions.add_y(mesh2);
    conditions.add_y(mesh2);

    std::cout << conditions.num_conditions_y() << ", ";
    std::cout << conditions.location_y(1) << '\n';
    std::cout << "reversed y? " << conditions.reversed_y_data() << '\n';
    return 0;
}

int test_residual_1d(int argc, char** argv)
{
    Residual<double> testing(1);
    Mesh_1D<double> mesh;
    mesh.uniform_mesh(1., 2., 100, 1);
    Conditions_1D<double> conditions(mesh);
    conditions.add(1., testing);

    return 0;
}

int test_equation_2d(int argc, char** argv)
{
    Equation_2D<double> testing(5);
    std::cout << "Instantiated Equation_2D<double> object.\n";
    return 0;
}

int test_power_mesh(int argc, char** argv)
{
    std::ofstream file;
    file.open("test.dat");
    double pi(4.*std::atan(1.));
    Mesh_1D<double> mesh1, mesh2;
    mesh1.uniform_mesh(0., 1., 3, 0);
    mesh2.power_mesh(0., pi/2., 51, 0, 2., Mesh_Right);
    Mesh_2D<double> mesh(mesh1, mesh2);
    mesh.dump(file);
    file.close();
    return 0;
}

int test_utility(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
    double test(5);
    args.option("--haha", test);
    bool someflag(false);
    args.flag("-f", someflag);

    std::cout << "test = " << test << '\n';
    std::cout << "someflag = " << someflag << "\n\n";

    args.print_recognised_arguments();

    return 0;
}

int test_mesh_interpolation(int argc, char** argv)
{
    std::ofstream data;

    Mesh_1D<double> shit_mesh, better_mesh;
    shit_mesh.uniform_mesh(0., 10., 5, 1);
    better_mesh.uniform_mesh(0., 10., 11, 1);
    Mesh_2D<double> shit_grid(shit_mesh, shit_mesh);
    Mesh_2D<double> better_grid(better_mesh, better_mesh);

    double x(0.), y(0.);
    for (std::size_t i(0), num_x(shit_grid.num_nodes_x()); i < num_x; ++i)
    {
        x = shit_grid.node_x(i);
        for (std::size_t j(0), num_y(shit_grid.num_nodes_y()); j < num_y; ++j)
        {
            y = shit_grid.node_y(j);
            shit_grid(i, j, 0) = std::sin(x + y);
        }
    }

    data.open("./test_shit_mesh.dat");
    shit_grid.dump(data);
    data.close();

    for (std::size_t i(0), num_x(better_grid.num_nodes_x()); i < num_x; ++i)
    {
        x = better_grid.node_x(i);
        for (std::size_t j(0), num_y(better_grid.num_nodes_y()); j < num_y; ++j)
        {
            y = better_grid.node_y(j);
            better_grid(i, j, 0) = shit_grid.linear_interpolant(x, y, 0);
        }
    }

    data.open("./test_refined_mesh.dat");
    better_grid.dump(data);
    data.close();

    return 0;
}

int test_complex_banded_solver(int argc, char** argv)
{
    Banded_Matrix<std::complex<double> > A(10, 1, 0.);
    Vector<std::complex<double> > b(10, 0.);
    Banded_Linear_System<std::complex<double> > solver(A, b);

    for (std::size_t i(0); i < 10; ++i)
    {
        A(i, i) = 1.;
        b[i] = double(2*i + 1);
    }
    A(0, 1) = std::complex<double>(1., 1.);
    solver.solve();

    b.dump();

    return 0;
}

int test_sparse_matrix(int argc, char** argv)
{
    Sparse_Matrix<double> A(6);
    Vector<double> b(6, 0.);
    for (std::size_t i(0); i < 6; ++i)
    {
        A(i, i) = 1.;
        b[i] = 2*i + 1;
    }
    A(0, 5) = 2.;
    A(1, 4) = -5.;
    A(4, 0) = 3.;
    A(5, 4) = 1.;

    Sparse_Linear_System<double> system(A, b);
    system.solve();
    b.dump();

    return 0;
}

int test_superlu_example(int argc, char** argv)
{
    Sparse_Matrix<double> A(5);
    Vector<double> b(5, 0.);
    A(0, 0) = 1;
    A(0, 1) = -5;
    A(0, 2) = -2;
    A(0, 3) = -4;
    A(0, 4) = -1;
    A(1, 0) = -1;
    A(1, 1) = -2;
    A(1, 2) = -2;
    A(1, 3) = 3;
    A(1, 4) = 3;
    A(2, 0) = -2;
    A(2, 1) = -3;
    A(2, 2) = -4;
    A(2, 3) = -4;
    A(2, 4) = -5;
    A(3, 0) = 0;
    A(3, 1) = 5;
    A(3, 2) = 4;
    A(3, 3) = -3;
    A(3, 4) = 3;
    A(4, 0) = 0;
    A(4, 1) = -5;
    A(4, 2) = -3;
    A(4, 3) = 3;
    A(4, 4) = 5;
    b[0] = 5;
    b[1] = -2;
    b[2] = -4;
    b[3] = 1;
    b[4] = 5;

    Vector<double> a(25);
    int asub[25];
    int xa[6];

    A.get_superlu_data(a, asub, xa);

    std::cout << "a: ";
    for (std::size_t i(0); i < 25; ++i)
        std::cout << a[i] << " ";
    std::cout << "\nasub: ";
    for (std::size_t i(0); i < 25; ++i)
        std::cout << asub[i] << " ";
    std::cout << "\nxa: ";
    for (std::size_t i(0); i < 6; ++i)
        std::cout << xa[i] << " ";
    std::cout << '\n';
    
    // LU solution:
    // 
    //  1    -5   -2       -4       -1
    //  0     5    4       -3        3
    //  0    -1    1        0        8
    // -1  -1.4  1.6     -5.2     -6.6
    // -2  -2.6  2.4  3.80769  6.73077
    //
    // Linear solve:
    //
    // 11.64  6.48  -10.64  -1.64  2.08
    //
    
    Sparse_Linear_System<double> system(A, b);
    system.solve();

    return 0;
}

int test_file_input(int argc, char** argv)
{
    Mesh_1D<double> uniform_1d;
    uniform_1d.uniform_mesh(0., 10., 101, 1);
    Mesh_2D<double> mesh(uniform_1d, uniform_1d);

    double x(0.), y(0.);
    for (std::size_t i(0), size_x(mesh.num_nodes_x()); i < size_x; ++i)
    {
        x = mesh.node_x(i);
        for (std::size_t j(0), size_y(mesh.num_nodes_y()); j < size_y; ++j)
        {
            y = mesh.node_y(j);
            mesh(i, j, 0) = std::sin(x + y);
        }
    }

    std::cout << "mesh stats before save:\n";
    std::cout << "num_nodes_x() = " << mesh.num_nodes_x() << '\n';
    std::cout << "num_nodes_y() = " << mesh.num_nodes_y() << '\n';
    std::cout << "num_vars() = " << mesh.num_vars() << "\n\n";

    const char* filepath = "test_data_io.dat";
    const char* filepath2 = "test_data_io_2.dat";

    std::ofstream ofile;
    ofile.open(filepath);
    mesh.dump(ofile);
    ofile.close();

    mesh = Mesh_2D<double>();
    mesh.load_data(filepath);

    std::cout << "mesh stats after load:\n";
    std::cout << "num_nodes_x() = " << mesh.num_nodes_x() << '\n';
    std::cout << "num_nodes_y() = " << mesh.num_nodes_y() << '\n';
    std::cout << "num_vars() = " << mesh.num_vars() << '\n';

    ofile.open(filepath2);
    mesh.dump(ofile);
    ofile.close();

    return 0;
}

int test_file_output(int argc, char** argv)
{
    File_Output files("./dat/");
    files.open("Nice", "testing1.dat");
    files.open("and", "testing2.dat");
    files.open("useful", "testing3.dat");
    files.open("class", "testing4.dat");

    files.write("Nice") << "the word is 'Nice'\n";
    files.write("and") << "the word is 'and'\n";
    files.write("useful") << "it's about to fail!\n";

    files.close("class");

    try
    {
        files.write("class") << "should break\n";
    }
    catch (std::exception& e)
    {
    }

    return 0;
}

int test_sparse_matrix_complex(int argc, char** argv)
{
    std::size_t test_size(10);
    Sparse_Matrix<std::complex<double> > A(test_size);
    Vector<std::complex<double> > b(test_size);
    Sparse_Linear_System<std::complex<double> > system(A, b);
    
    for (std::size_t i(0); i < test_size; ++i)
        A(i, i) = b[i] = 1.;
    A(0, 0) += std::complex<double>(0., 1.);

    A.dump();
    std::cout << '\n';
    b.dump();
    std::cout << '\n';

    system.solve();
    b.dump();

    return 0;
}

namespace TestMap
{
    double power(4.);
    double map(const double& s)
    {
        return std::pow(s + 0.1, power) - std::pow(0.1, power);
    }
    double inv_map(const double& x)
    {
        return std::pow(x + std::pow(0.1, power), 1./power) - 0.1;
    }
    double map_ds(const double& s)
    {
        return power*std::pow(s + 0.1, power - 1.);
    }
    double map_ds2(const double& s)
    {
        return power*(power - 1.)*std::pow(s + 0.1, power - 2.);
    }
}

int test_mapping(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);
    args.option("--power", TestMap::power);

    Mesh_1D<double> test_mesh;
    double left(0.), right(10.), num_nodes(101);
    test_mesh.mapped_grid(left, right, num_nodes, 0,
            &TestMap::map, &TestMap::inv_map);

    double begin(test_mesh.inv_map(left)), end(test_mesh.inv_map(right));
    double step((end - begin)*0.05);

    for (double s(begin); s < end + 0.5*step; s += step)
    {
        std::cout
            << "x = " << test_mesh.map(s)
            << ", s = " << s << std::endl
            << "    approx_ds = " << test_mesh.map_ds(s)
            << ", real_ds = " << TestMap::map_ds(s)
            << ", error = " << test_mesh.map_ds(s) - TestMap::map_ds(s)
            << std::endl
            << "    approx_ds2 = " << test_mesh.map_ds2(s)
            << ", real_ds2 = " << TestMap::map_ds2(s)
            << ", error = " << test_mesh.map_ds2(s) - TestMap::map_ds2(s)
            << std::endl;
    }

    return 0;
}

int test_generalised_eigenvalue_real(int argc, char** argv)
{
    std::size_t n(101);
    double pi(4.*std::atan(1.)), inv_h(double(n - 1)/pi);
    Dense_Matrix<double> A(n, 0.), B(n, 0.);
    Generalised_Eigenvalue_System<double> system(A, B);

    // Set up A, B
    for (std::size_t i(1); i < n - 1; ++i)
    {
        A(i, i - 1) = inv_h*inv_h;
        A(i, i) = -2.*inv_h*inv_h;
        A(i, i + 1) = inv_h*inv_h;
        B(i, i) = 1;
    }
    A(0, 0) = A(n - 1, n - 1) = inv_h*inv_h;

    system.solve();

    std::cout << "Eigenvalues:\n------------\n";
    for (std::size_t i(0); i < n; ++i)
    {
        Vector<double> eig(system.eigenvalue(i));
        std::cout << std::sqrt(-eig[0]) << " + " << eig[1] << "i\n";
    }

    std::ofstream out("./dat/test.dat");
    out.precision(16);
    for (std::size_t i(0); i < n; i++)
    {
        out << double(i)/inv_h;
        for (std::size_t j(0); j < n; ++j)
        {
            out << ' ' << system.eigenvector_real(j)[i];
        }
        out << std::endl;
    }
    out.close();

    return 0;
}

int test_generalised_eigenvalue_complex(int argc, char** argv)
{
    std::size_t n(101);
    double pi(4.*std::atan(1.)), inv_h(double(n - 1)/pi);
    Matrix_Helper A(n), B(n);
    Generalised_Eigenvalue_System_Complex system(A, B);

    // Set up A, B
    for (std::size_t i(1); i < n - 1; ++i)
    {
        A(i, i - 1, 0) = inv_h*inv_h;
        A(i, i, 0) = -2.*inv_h*inv_h;
        A(i, i + 1, 0) = inv_h*inv_h;
        B(i, i, 0) = 1;
    }
    A(0, 0, 0) = A(n - 1, n - 1, 0) = inv_h*inv_h;

    system.solve();

    std::cout << "Eigenvalues:\n------------\n";
    for (std::size_t i(0); i < system.solution_size(); ++i)
    {
        std::complex<double> eig(system.eigenvalue(i));
        std::cout << eig.real() << " + " << eig.imag() << "i\n";
    }
    /*for (std::size_t i(0); i < n; ++i)
    {
        std::complex<double> eig(system.eigenvalue(i));
        std::cout << eig.real() << " + " << eig.imag() << "i\n";
    }

    std::ofstream out("./dat/test.dat");
    out.precision(16);
    for (std::size_t i(0); i < n; i++)
    {
        out << double(i)/inv_h;
        for (std::size_t j(0); j < n; ++j)
        {
            out << ' ' << system.eigenvector_real(j)[i];
            out << ' ' << system.eigenvector_imag(j)[i];
        }
        out << std::endl;
    }
    out.close();*/

    return 0;
}

int main(int argc, char** argv)
{
    //return test_residual_1d(argc, argv);
    //return pole_far(argc, argv);
    //return test_equation_2d(argc, argv);
    //return test_conditions_2d(argc, argv);
    //return test_power_mesh(argc, argv);
    //return test_utility(argc, argv);
    //return test_mesh_interpolation(argc, argv);
    //return test_complex_banded_solver(argc, argv);
    //return test_sparse_matrix(argc, argv);
    //return test_superlu_example(argc, argv);
    //return test_file_input(argc, argv);
    //return test_file_output(argc, argv);
    //return test_sparse_matrix_complex(argc, argv);
    //return test_mapping(argc, argv);
    return test_generalised_eigenvalue_complex(argc, argv);
}

