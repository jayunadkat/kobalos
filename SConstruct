import os.path
import glob

debug = ARGUMENTS.get('debug', 0)
examples = ARGUMENTS.get('examples', 0)
home = ARGUMENTS.get('home', 0)
lapack = ARGUMENTS.get('lapack', 0)
library = ARGUMENTS.get('library', 0)
mainfile = ARGUMENTS.get('target', 'test')
optimise = ARGUMENTS.get('optimise', 1)
paranoid = ARGUMENTS.get('paranoid', 0)
profile = ARGUMENTS.get('profile', 0)
quiet = ARGUMENTS.get('quiet', 0)
superlu = ARGUMENTS.get('superlu', 0)
time = ARGUMENTS.get('time', 0)
intel_mkl = ARGUMENTS.get('intelmkl', 1)
parallel = ARGUMENTS.get('parallel', 1)

# Use colours
# <ESC>[{attr};{fg};{bg}m
red = "\033[1;31m"
yellow = "\033[1;33m"
green = "\033[1;32m"
blue = "\033[1;34m"
off = "\033[0m"

# default output format for messaging
def message( col, text ):
    print col + " * " + text + off

comp_switches = '-Wall -std=c++14 '
preproc = ''
libs = 'kobalos ncurses '
link_switches = ''
intel_link = ''
inc_dirs = './inc '
lib_dirs = './lib '
all_src = glob.glob('src/*.cpp') + glob.glob('src/boundary_layer/*.cpp')
main_src = [mainfile + '.cpp']
binary = mainfile.split('/')[-1]

if int(optimise):
    comp_switches += '-O3 '
if int(debug):
    comp_switches += '-g '
    preproc += '-DDEBUG '
if int(paranoid):
    preproc += '-DPARANOID '
if int(lapack)*int(superlu):
    preproc += '-DLAPACK -DSUPERLU '
    libs += 'lapack superlu blas '
elif int(lapack):
    preproc += '-DLAPACK '
    libs += 'lapack blas '
elif int(superlu):
    preproc += '-DSUPERLU '
    libs += 'superlu blas '
if int(quiet):
    preproc += '-DQUIET '
if int(time):
    preproc += '-DTIME '
if int(profile):
    comp_switches += '-pg '
    link_switches += '-pg '
if int(intel_mkl):
    preproc += '-DINTELMKL -DHAS_INTELMKL -DMKL_ILP64 '
    comp_switches += '-m64 '
    if int(parallel):
        libs += 'gomp '
    libs += 'pthread m dl '
    intel_link += '-Wl,--start-group,-static -lmkl_intel_ilp64 '
    if int(parallel):
        intel_link += '-lmkl_gnu_thread '
    else:
        intel_link += '-lmkl_sequential '
    intel_link += '-lmkl_core -Wl,--end-group,-Bdynamic '
    if int(home):
        lib_dirs += '/home/jay/intel/mkl/lib/intel64'
    else:
        lib_dirs += '/home/staff/jayu/intel/mkl/lib/intel64 '

inc_dir_list = inc_dirs.split()
lib_dir_list = lib_dirs.split()

env = Environment(ENV = os.environ, FORTRAN = 'gfortran', CXX = 'g++', CPPPATH = inc_dir_list, CCFLAGS = comp_switches + preproc, LINKFLAGS = link_switches , LIBS = libs.split(), INTEL_LIBS=intel_link, LIBPATH = lib_dir_list, LINKCOM = '$LINK -o $TARGET $LINKFLAGS $__RPATH $SOURCES $_LIBDIRFLAGS $INTEL_LIBS $_LIBFLAGS')

if int(library):
    message(red, 'Building static library')
    env.StaticLibrary('lib/kobalos', all_src)
else:
    message(red, 'Building target')
    env.Program(target = binary + '.out', source = main_src)

