# Kobalos: A library of object-oriented routines for the numerical solution of differential equations via finite differencing.
***
Currently supports:

* Systems of first-order ODEs with boundary data and an initial guess.
* Systems of first-order PDEs with time and one spatial coordinate with given boundary and initial data, where time steps are performed using the Crank-Nicolson scheme.
* Systems of first-order PDEs with time and two spatial coordinates with given boundary and initial data, where time steps are performed using the Crank-Nicolson scheme. The system of equations must be parabolic.
* Support for reversed flow in the parabolic coordinate when solving a problem with two spatial coordinates and time. This uses a zig-zag scheme as described by Cebeci (1978).

To be implemented:

* Steady/unsteady elliptic systems of first-order PDEs.

