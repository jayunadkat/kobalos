#include <cmath>
#include <complex>

#include <Command_Line_Args.h>
#include <Console_Output.h>
#include <File_Output.h>
#include <Mesh_1D.h>
#include <Mesh_2D.h>

using namespace Kobalos;

int DOIT()
{
    Mesh_1D<double> data;
    data.load_data("./dat/1602-1609/Ro-4.000_201_3.0_30_401_1.0_map/tracker.dat");
    std::ofstream NEW("./dat/1602-1609/Ro-4.000_201_3.0_30_401_1.0_map/new.dat");

    std::size_t num_theta(data.num_vars());
    double theta_unit(2.*std::atan(1.)/(num_theta - 1));
    for (std::size_t i(0); i < data.num_nodes(); ++i)
    {
        for (std::size_t j(0); j < num_theta; ++j)
        {
            NEW << data.node(i) << ' ' << j*theta_unit << ' '
                << data(i, j) << std::endl;
        }
        NEW << std::endl;
    }

    NEW.close();

    return 0;
}

namespace Cowley
{
    int calculate_local_wavenumber_and_phase_speed(int argc, char** argv)
    {
        File_Output files("./dat/15/dumps");
        files.open("test", "local_wavenumbers_and_phase_speed.dat");
        std::complex<double> I(0., 1.), K(0.),
            sigma(0.9201628760900, 19.49061442080);
        double dtheta(0.);

        Mesh_2D<std::complex<double> > mesh;
        mesh.load_data_reversed_axes(
                "./dat/15/Q_local_EVP_4_fine.dat");
        dtheta = mesh.node_x(1) - mesh.node_x(0);

        files("test") << "# theta Re(K) Im(K) Re(c(K; sigma)) Im(c(K; sigma))"
            << std::endl;
        files("test") << "0 0 0 0 0" << std::endl;
        for (std::size_t i(1), num_theta(mesh.num_nodes_x()),
                index(mesh.num_nodes_y() - 1); i < num_theta - 1; ++i)
        {
            files("test") << mesh.node_x(i) << " ";
            K = -I*((mesh(i + 1, index, 0) - mesh(i - 1, index, 0))/
                    (2.*dtheta*mesh(i, index, 0)));
            files("test") << K.real() << " " << K.imag() << " ";
            K = sigma/(-I*K);
            files("test") << K.real() << " " << K.imag() << std::endl;
        }

        files("test") << 2.*std::atan(1.) << " 0 0 0 0" << std::endl;
        return 0;
    }

    int find_critical_point_data(int argc, char** argv)
    {
        enum {f, fr, v};

        File_Output files("./dat/15/dumps");
        files.open("vars", "vars_at_critical_point.dat");
        files("vars") << "# theta r_max u_max max_index" << std::endl;

        Mesh_2D<double> mesh;
        mesh.load_data(
                "./dat/15/Ro-0.500_201_701_1.0_20_unscaled/Ro-0.300_mesh.dat");

        std::size_t num_r(mesh.num_nodes_x()), num_theta(mesh.num_nodes_y());
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            std::size_t max_index(0);
            double max(0.), temp(0.), inv_s(1./std::sin(mesh.node_y(j)));
            for (std::size_t i(0); i < num_r; ++i)
            {
                if (max < (temp = mesh(i, j, fr)*inv_s))
                {
                    max_index = i;
                    max = temp;
                }
            }
            //if (max_index > 0 && max_index < num_r - 1)
            //    deriv = (mesh(max_index + 1, j, 0) - 2.*mesh(max_index, j, 0)
            //            + mesh(max_index - 1, j, 0))/std::pow(
            //            mesh.node_x(max_index + 1) - mesh.node_x(max_index), 2.);

            files("vars") << mesh.node_y(j) << ' '
                << mesh.node_x(max_index) << ' '
                << -max << ' '
                //<< (max_index > 0 && max_index < num_r - 1 ? deriv : '?') << ' '
                << max_index << std::endl;
        }

        return 0;
    }

    int find_critical_point_data2(int argc, char** argv)
    {
        enum {f, fr, v};

        File_Output files("./dat/local3");
        files.open("vars", "vars_at_critical_point.dat");
        files("vars") << "# theta r_max u_max max_index ..." << std::endl;

        Mesh_2D<double> mesh;
        mesh.load_data(
                "./dat/local3/steady_201_-0.50");

        std::size_t num_r(mesh.num_nodes_x()), num_theta(mesh.num_nodes_y());
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            files("vars") << mesh.node_y(j) << ' ';
            files.open("outer", ("outer_sol_" + str_convert(j) + ".dat").c_str());
            files("outer") << "# r outer_sol" << std::endl;

            double inv_s(1./std::sin(mesh.node_y(j))), stationary(0.),
                   inv_r(1./(mesh.node_x(1) - mesh.node_x(0)));
            std::vector<double> f(3, 0.);
            std::vector<double> x(3, 0.);
            bool now_rising(false), was_rising(false);

            // Base step for comparing triads of values
            for (std::size_t i(0); i < 2; ++i)
            {
                x[i + 1] = mesh.node_x(i);
                f[i + 1] = -mesh(i, j, fr)*inv_s;
            }
            was_rising = (f[1] <= f[2]);

            // Iterative step. Some algebra gives the maximal expression
            for (std::size_t i(2); i < num_r; ++i)
            {
                f[0] = f[1]; f[1] = f[2]; f[2] = -mesh(i, j, fr)*inv_s;
                x[0] = x[1]; x[1] = x[2]; x[2] = mesh.node_x(i);
                now_rising = (f[1] <= f[2]);
                if (was_rising != now_rising)
                {
                    // found stationary point
                    double x20(x[2] - x[0]), x21(x[2] - x[1]), x10(x[1] - x[0]);
                    double c(x[1]*x[2]*f[0]/(x10*x20) - x[0]*x[2]*f[1]/(x10*x21)
                            + x[0]*x[1]*f[2]/(x20*x21));
                    double b(- (x[1] + x[2])*f[0]/(x10*x20)
                            + (x[0] + x[2])*f[1]/(x10*x21)
                            - (x[0] + x[1])*f[2]/(x20*x21));
                    double a(f[0]/(x10*x20) - f[1]/(x10*x21) + f[2]/(x20*x21));
                    stationary = c - b*b/(4*a);
                    files("vars") << -b/(2*a) << ' ' << stationary << ' '
                        << 2*a << ' ';

                    for (std::size_t k(0); k < num_r; ++k)
                    {
                        files("outer") << mesh.node_x(k) << ' '
                            << -mesh(k, j, fr)*inv_s - stationary << ' '
                            << (k > 0 ? str_convert(-inv_s*inv_r*(mesh(k, j, v)
                                        - mesh(k - 1, j, v))) : "?")
                            << std::endl;

                    }
                    files("outer") << std::endl << std::endl;
                }
                was_rising = now_rising;
            }
            files("vars") << std::endl;
            files.close("outer");
        }

        return 0;
    }
}   // namespace Cowley

int oddjob7(int argc, char** argv)
{
    std::ofstream data("/home/unadkat/PhD/random/rich/playing/Ro-0.300_201_701_1.0_20_unscaled/post.dat");
    Mesh_2D<double> mesh;
    mesh.load_data("/home/unadkat/PhD/random/rich/playing/Ro-0.300_201_701_1.0_20_unscaled/Ro-0.300.dat");

    std::size_t num_r(mesh.num_nodes_x()), num_theta(mesh.num_nodes_y());
    for (std::size_t j(0); j < num_theta; ++j)
    {
        std::size_t max_index(0);
        double max(0.), deriv(0.);
        for (std::size_t i(0); i < num_r; ++i)
        {
            if (max > mesh(i, j, 0))
            {
                max_index = i;
                max = mesh(i, j, 0);
            }
        }
        if (max_index > 0 && max_index < num_r - 1)
            deriv = (mesh(max_index + 1, j, 0) - 2.*mesh(max_index, j, 0)
                    + mesh(max_index - 1, j, 0))/std::pow(
                    mesh.node_x(max_index + 1) - mesh.node_x(max_index), 2.);

        data << mesh.node_y(j) << ' '
            << mesh.node_x(max_index) << ' '
            << max << ' '
            << (max_index > 0 && max_index < num_r - 1 ? deriv : '?') << ' '
            << max_index << std::endl;
    }

    data.close();

    return 0;
}

int oddjob6(int argc, char** argv)
{
    // Also implemented in write_up_drivers/1308-1311/spectral_unscaled_evals
    Command_Line_Args args(argc, argv);

    double signed_Ro(-0.5), omega(1.40);

    args.option("--omega", omega);
    args.option("--rossby", signed_Ro);

    double Ro(std::abs(signed_Ro));

    Mesh_2D<double> mesh, mesh2;
    mesh.load_data(str_convert(
                "/tmp/dat/1308_unscaled/steady_Ro%.2f_201_201_flow.dat",
                signed_Ro));
    mesh2.load_data(str_convert(
                "/tmp/dat/1308_unscaled/fourier%.2f", signed_Ro) + str_convert(
                    "_%.2f_201_201.dat", omega));

    std::ofstream data_steady("/tmp/dat/1308_unscaled/steady_rates.dat");
    std::ofstream data_fourier("/tmp/dat/1308_unscaled/fourier_rates.dat");

    std::vector<std::vector<double> > transpiration(mesh.num_nodes_y(),
            std::vector<double>(2, 0.));

    for (std::size_t j(0), num_theta(mesh.num_nodes_y()),
            index(mesh.num_nodes_x() - 1); j < num_theta; ++j)
    {
        transpiration[j][0] = mesh.node_y(j);
        transpiration[j][1] = mesh(index, j, 2);
    }

    for (std::size_t j(0), num_theta(mesh.num_nodes_y()); j < num_theta; ++j)
    {
        double c(std::cos(transpiration[j][0])), w(Ro*transpiration[j][1]),
               r, r2;
        r = 0.25*std::sqrt(2.)*std::sqrt(
                std::sqrt(w*w*w*w + 64.*c*c) + w*w);
        data_steady << transpiration[j][0] << ' '
            << (0.5*w - r) << ' ' << (0.5*w + r) << std::endl;
        w *= 0.5;
        r2 = Ro*omega + 2.*c;
        r = std::sqrt(w*w + std::sqrt(w*w*w*w + r2*r2))/std::sqrt(2.);
        data_fourier << transpiration[j][0] << ' '
            << w - r << ' ' << w + r << ' ';
        r2 = Ro*omega - 2.*c;
        r = std::sqrt(w*w + std::sqrt(w*w*w*w + r2*r2))/std::sqrt(2.);
        data_fourier << w - r << ' ' << w + r << std::endl;
    }

    data_steady.close();
    data_fourier.close();

    return 0;
}

int oddjob5(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    Mesh_2D<double> mesh;
    std::size_t var(0);
    std::size_t index(500), max_index(0);
    int direction(0), new_direction(0);
    bool threshold(false), threshold_new(false);
    double max(0.), epsilon(1e-1), diff(0.);

    args.option("--index", index);
    args.option("--level1", epsilon);

    mesh.load_data(str_convert("/tmp/dat/big3/writeup/sol%03u_", index)
            + str_convert("%.3f.dat", 0.01*index));
    std::ofstream out(str_convert("/tmp/dat/big3/max%03u", index));

    for (std::size_t j(0), num_y(mesh.num_nodes_y()); j < num_y; ++j)
    {
        direction = new_direction = 0;
        max_index = 0;
        max = 0.;
        // To avoid compiler warnings only
        max = max_index;
        max_index = max;
        // REMOVE!
        threshold = threshold_new = false;

        for (std::size_t i(0), num_x(mesh.num_nodes_x() - 1); i < num_x; ++i)
        {
            diff = mesh(i + 1, j, var) - mesh(i, j, var);

            if (new_direction == 0)
            {
                new_direction = 2*(diff > 0) - 1;
                threshold_new = (std::abs(diff) < epsilon);
            }
            else
            {
                direction = new_direction;
                threshold = threshold_new;
                new_direction = 2*(diff > 0) - 1;
                threshold_new = (std::abs(diff) < epsilon);

                if ((direction*new_direction == -1) && threshold
                        && threshold_new)
                {
                    out << mesh.node_y(j) << ' '
                        << mesh.node_x(i) << ' '
                        << mesh(i, j, var) << std::endl;
                    break;
                }
            }
        }
    }
    out.close();

    args.option("--level2", epsilon);

    out.open(str_convert("/tmp/dat/big3/oscillate%03u", index));

    for (std::size_t i(0), num_x(mesh.num_nodes_x()); i < num_x; ++i)
    {
        direction = new_direction = 0;
        max_index = 0;
        max = 0.;
        threshold = threshold_new = false;

        for (std::size_t j(0), num_y(mesh.num_nodes_y() - 1); j < num_y; ++j)
        {
            diff = mesh(i, j + 1, var) - mesh(i, j, var);

            if (new_direction == 0)
            {
                new_direction = 2*(diff > 0) - 1;
                threshold_new = (std::abs(diff) < epsilon);
            }
            else
            {
                direction = new_direction;
                threshold = threshold_new;
                new_direction = 2*(diff > 0) - 1;
                threshold_new = (std::abs(diff) < epsilon);

                if ((direction*new_direction == -1) && !threshold
                        && !threshold_new)
                {
                    out << mesh.node_y(j) << ' '
                        << mesh.node_x(i) << ' '
                        << mesh(i, j, var) << std::endl;
                    break;
                }
            }
        }
    }
    out.close();

    return 0;
}

int oddjob4(int argc, char** argv)
{
    Command_Line_Args args(argc, argv);

    double power(2.);
    args.option("--power", power);

    File_Output files("/tmp/unadkat");
    files.open("evp", str_convert("evp3/evec_401_normalised.dat",
                power).c_str());
    files.open("full", str_convert(
                "Ro-1.250_201_401_\%.1f_30_unscaled/mesh_normalised.dat",
                power).c_str());

    Mesh_1D<double> evec_mesh;
    Mesh_2D<double> full_mesh;

    evec_mesh.load_data("/tmp/unadkat/evp3/evec_-1.250_401_spatial.dat");
    full_mesh.load_data(str_convert(
            "/tmp/unadkat/Ro-1.250_201_401_\%.1f_30_unscaled/Ro-1.250_diff.dat",
            power));

    double max(0.), scale(0.);
    std::size_t num_nodes(evec_mesh.num_nodes());
    Vector<double> evec(num_nodes, 0.);

    for (std::size_t i(0); i < num_nodes; ++i)
    {
        if ((evec[i] = std::sqrt(
                std::pow(evec_mesh(i, 0), 2.) + std::pow(evec_mesh(i, 1), 2.)))
                > max)
        {
            max = evec[i];
        }
    }
    for (std::size_t i(0); i < num_nodes; ++i)
    {
        files("evp") << evec_mesh.node(i) << ' ' << evec[i]/max << std::endl;
    }

    num_nodes = full_mesh.num_nodes_x();
    for (std::size_t j(0), num_theta(full_mesh.num_nodes_y()); j < num_theta;
            ++j)
    {
        double theta(full_mesh.node_y(j));
        max = 0.;
        scale = std::sin(theta)*std::sin(theta)*std::cos(theta);

        for (std::size_t i(0); i < num_nodes; ++i)
        {
            if (std::abs(evec[i] = full_mesh(i, j, 0)/scale) > max)
            {
                max = std::abs(evec[i]);
            }
        }
        for (std::size_t i(0); i < num_nodes; ++i)
        {
            files("full") << theta << ' '
                << full_mesh.node_x(i) << ' '
                << evec[i]/max << std::endl;
        }
        files("full") << std::endl;
    }

    files.close_all();
        
    return 0;
}

double bl_thickness_integrand3(const double& S, const double& v)
{
    if (S > 1)
        return 1. - v/S;
    else
        return v/S - 1.;
    //return v;
}

double bl_thickness_integrand2(const double& S, const double& v)
{
    if (S > 1)
        return -v;
    else
        return v;
}

int oddjob3(int argc, char** argv)
{
    File_Output files("./");
    files.open("p", "bl_thickness_pole.dat");
    files.open("e", "bl_thickness_equator.dat");
    files.open("m", "bl_thickness_middle.dat");
    Mesh_1D<double> mesh_p, mesh_e, mesh_m;
    char filename[120];
    char directory[100];
    std::vector<double> Re_numbers({8000});
    std::vector<int> mesh_size({100, 150, 200});
    double dt(0.025);
    double S(2.00);
    std::size_t i_inc(0);
    double wobble(0.00);
    bool do_bl(true);
    bool do_oomph(true);

    if (S > 1)
        i_inc = 1;
    else
        i_inc = (std::size_t)(1./S + 0.5);

    if (do_bl)
    {
        std::sprintf(directory, "/tmp/dat/Reynolds/BL_S%.2f_omega%.2f/",
                S, wobble);
        std::cout << "Calculating BL thicknesses for boundary layer solution\n";

        files("p") << "# BL\n";
        files("e") << "# BL\n";
        files("m") << "# BL\n";
        std::size_t i(1);
        try
        {
            for (; i < 751; i += i_inc)
            {
                // Load data for pole and equator at given Re and time index
                std::sprintf(filename, "%sp%u.dat", directory, unsigned(i));
                mesh_p.load_data(filename);
                std::sprintf(filename, "%se%u.dat", directory, unsigned(i));
                mesh_e.load_data(filename);
                std::sprintf(filename, "%sm%u.dat", directory, unsigned(i));
                mesh_m.load_data(filename);

                // Calculate BL thickness measures  at pole and equator (using
                // volumetric flow rate using v as velocity measure.
                std::size_t nnodes_p(mesh_p.num_nodes());
                std::size_t nnodes_e(mesh_e.num_nodes());
                std::size_t nnodes_m(mesh_m.num_nodes());
                double temp(0.), time(0.025*double(i));
                std::size_t index(1);

                temp += 0.5*(mesh_p.node(1) - mesh_p.node(0))
                    *bl_thickness_integrand3(S, mesh_p(0, index))
                    + 0.5*(mesh_p.node(nnodes_p - 1)
                            - mesh_p.node(nnodes_p - 2))
                    *bl_thickness_integrand3(S, mesh_p(nnodes_p - 1, index));
                for (std::size_t i(0); i < nnodes_p - 2; ++i)
                    temp +=0.5*(mesh_p.node(i + 2) - mesh_p.node(i))
                        *bl_thickness_integrand3(S, mesh_p(i + 1, index));
                files("p") << time << ' ' << temp << '\n';

                temp = 0.;
                temp += 0.5*(mesh_e.node(1) - mesh_e.node(0))
                    *bl_thickness_integrand3(S, mesh_e(0, index))
                    + 0.5*(mesh_e.node(nnodes_e - 1)
                            - mesh_e.node(nnodes_e - 2))
                    *bl_thickness_integrand3(S, mesh_e(nnodes_e - 1, index));
                for (std::size_t i(0); i < nnodes_e - 2; ++i)
                    temp +=0.5*(mesh_e.node(i + 2) - mesh_e.node(i))
                        *bl_thickness_integrand3(S, mesh_e(i + 1, index));
                files("e") << time << ' ' << temp << '\n';

                temp = 0.;
                temp += 0.5*(mesh_m.node(1) - mesh_m.node(0))
                    *bl_thickness_integrand3(S, mesh_m(0, index))
                    + 0.5*(mesh_m.node(nnodes_m - 1)
                            - mesh_m.node(nnodes_m - 2))
                    *bl_thickness_integrand3(S, mesh_m(nnodes_m - 1, index));
                for (std::size_t i(0); i < nnodes_m - 2; ++i)
                    temp +=0.5*(mesh_m.node(i + 2) - mesh_m.node(i))
                        *bl_thickness_integrand3(S, mesh_m(i + 1, index));
                files("m") << time << ' ' << temp << '\n';
            }
        }
        catch (std::exception& e)
        {
            files("p").flush();
            files("e").flush();
            files("m").flush();

            std::cout << "Failed at time step " << i << '\n';
        }

        files("p") << "\n\n";
        files("e") << "\n\n";
        files("m") << "\n\n";
    }

    if (do_oomph)
    {
        for (std::size_t re_index(0); re_index < Re_numbers.size(); ++re_index)
        {
            for (std::size_t mesh_index(0); mesh_index < mesh_size.size();
                    ++mesh_index)
            {
                std::sprintf(directory,
                        "/tmp/dat/Reynolds/"
                        "S%.2f_omega%.2f_%.0f_%i_60_0.025_rotating/",
                        S, wobble, Re_numbers[re_index], mesh_size[mesh_index]);
                std::cout << "Calculating BL thicknesses for Re = "
                    << Re_numbers[re_index] << ", S = "
                    << S << ", wobble = " << wobble << ", num_r = "
                    << mesh_size[mesh_index] << "\n";
                std::cout << "Looking in directory: " << directory << '\n';

                files("p") << "# Re = " << Re_numbers[re_index] << ", S = "
                    << S << ", wobble = " << wobble << ", num_r = "
                    << mesh_size[mesh_index] << '\n';
                files("e") << "# Re = " << Re_numbers[re_index] << ", S = "
                    << S << ", wobble = " << wobble << ", num_r = "
                    << mesh_size[mesh_index] << '\n';
                files("m") << "# Re = " << Re_numbers[re_index] << ", S = "
                    << S << ", wobble = " << wobble << ", num_r = "
                    << mesh_size[mesh_index] << '\n';
                std::size_t i(1);
                try
                {
                    for (std::size_t size(801); i < size; i += i_inc)
                    {
                        // Load data for pole and equator at given Re and
                        // time index
                        std::sprintf(filename, "%sp%u.dat", directory,
                                unsigned(S*i));
                        mesh_p.load_data(filename);
                        std::sprintf(filename, "%se%u.dat", directory,
                                unsigned(S*i));
                        mesh_e.load_data(filename);
                        std::sprintf(filename, "%sm%u.dat", directory,
                                unsigned(S*i));
                        mesh_m.load_data(filename);

                        // Calculate BL thickness measures  at pole and equator
                        // (using volumetric flow rate with relative velocity
                        // measure 1 - v so that the free-stream is non-zero.
                        std::size_t nnodes_p(mesh_p.num_nodes());
                        std::size_t nnodes_e(mesh_e.num_nodes());
                        std::size_t nnodes_m(mesh_m.num_nodes());
                        double temp(0.), time(dt*double(i));

                        temp += 0.5*(mesh_p.node(1) - mesh_p.node(0))
                            *bl_thickness_integrand2(S, mesh_p(0, 5))
                            + 0.5*(mesh_p.node(nnodes_p - 1)
                                    - mesh_p.node(nnodes_p - 2))
                            *bl_thickness_integrand2(S,
                                    mesh_p(nnodes_p - 1, 5));
                        for (std::size_t i(0); i < nnodes_p - 2; ++i)
                            temp +=0.5*(mesh_p.node(i + 2) - mesh_p.node(i))
                                *bl_thickness_integrand2(S, mesh_p(i + 1, 5));
                        files("p") << time << ' ' << temp << '\n';

                        temp = 0.;
                        temp += 0.5*(mesh_e.node(1) - mesh_e.node(0))
                            *bl_thickness_integrand2(S, mesh_e(0, 3))
                            + 0.5*(mesh_e.node(nnodes_e - 1)
                                    - mesh_e.node(nnodes_e - 2))
                            *bl_thickness_integrand2(S,
                                    mesh_e(nnodes_e - 1, 3));
                        for (std::size_t i(0); i < nnodes_e - 2; ++i)
                            temp +=0.5*(mesh_e.node(i + 2) - mesh_e.node(i))
                                *bl_thickness_integrand2(S, mesh_e(i + 1, 3));
                        files("e") << time << ' ' << temp << '\n';

                        temp = 0.;
                        temp += 0.5*(mesh_m.node(1) - mesh_m.node(0))
                            *bl_thickness_integrand2(S, 
                                    std::sqrt(2.)*mesh_m(0, 3))
                            + 0.5*(mesh_m.node(nnodes_m - 1)
                                    - mesh_m.node(nnodes_m - 2))
                            *bl_thickness_integrand2(S, 
                                    std::sqrt(2.)*mesh_m(nnodes_m - 1, 3));
                        for (std::size_t i(0); i < nnodes_m - 2; ++i)
                            temp +=0.5*(mesh_m.node(i + 2) - mesh_m.node(i))
                                *bl_thickness_integrand2(S, 
                                        std::sqrt(2.)*mesh_m(i + 1, 3));
                        files("m") << time << ' ' << temp << '\n';
                    }
                }
                catch (std::exception& e)
                {
                    files("p").flush();
                    files("e").flush();
                    files("m").flush();

                    std::cout << "Failed at time step " << S*i << '\n';
                }

                files("p") << "\n\n";
                files("e") << "\n\n";
                files("m") << "\n\n";
            }
        }
    }

    files.close_all();
    return 0;
}

double bl_thickness_integrand(const double& time, const double& v)
{
    //double free_stream(1. - std::exp(-time*time));
    //return v/free_stream;
    return v;
}

int oddjob2(int argc, char** argv)
{
    File_Output files("./");
    files.open("p", "bl_thickness_pole.dat");
    files.open("e", "bl_thickness_equator.dat");
    files.open("m", "bl_thickness_middle.dat");
    Mesh_1D<double> mesh_p, mesh_e, mesh_m;
    char filename[120];
    char directory[100];
    std::vector<double> Re_numbers({8000, 16000});
    std::size_t base_mesh(100);
    double base_dt(0.025);
    std::vector<double> mult({0.5, 0.625, 1., 1.25, 2.5});
    bool do_bl(true);
    bool do_oomph(true);

    if (do_bl)
    {
        std::sprintf(directory, "/tmp/dat/Reynolds/BL_S0.00_omega0.00");
        std::cout << "Calculating BL thicknesses for boundary layer solution\n";

        files("p") << "BL\n";
        files("e") << "BL\n";
        files("e") << "BL\n";
        for (std::size_t i(1); i < 221; ++i)
        {
            // Load data for pole and equator at given Re and time index
            std::sprintf(filename, "%sp%u.dat", directory, unsigned(i));
            mesh_p.load_data(filename);
            std::sprintf(filename, "%se%u.dat", directory, unsigned(i));
            mesh_e.load_data(filename);
            std::sprintf(filename, "%sm%u.dat", directory, unsigned(i));
            mesh_m.load_data(filename);

            // Calculate BL thickness measures  at pole and equator (using
            // volumetric flow rate with relative velocity measure 1 - v so
            // that the free-stream is non-zero.
            std::size_t nnodes_p(mesh_p.num_nodes());
            std::size_t nnodes_e(mesh_e.num_nodes());
            std::size_t nnodes_m(mesh_m.num_nodes());
            double temp(0.), time(0.025*double(i));

            temp += 0.5*(mesh_p.node(1) - mesh_p.node(0))
                *bl_thickness_integrand(time, mesh_p(0, 1))
                + 0.5*(mesh_p.node(nnodes_p - 1) - mesh_p.node(nnodes_p - 2))
                *bl_thickness_integrand(time, mesh_p(nnodes_p - 1, 1));
            for (std::size_t i(0); i < nnodes_p - 2; ++i)
                temp +=0.5*(mesh_p.node(i + 2) - mesh_p.node(i))
                    *bl_thickness_integrand(time, mesh_p(i + 1, 1));
            files("p") << time << ' ' << temp << '\n';

            temp = 0.;
            temp += 0.5*(mesh_e.node(1) - mesh_e.node(0))
                *bl_thickness_integrand(time, mesh_e(0, 1))
                + 0.5*(mesh_e.node(nnodes_e - 1) - mesh_e.node(nnodes_e - 2))
                *bl_thickness_integrand(time, mesh_e(nnodes_e - 1, 1));
            for (std::size_t i(0); i < nnodes_e - 2; ++i)
                temp +=0.5*(mesh_e.node(i + 2) - mesh_e.node(i))
                    *bl_thickness_integrand(time, mesh_e(i + 1, 1));
            files("e") << time << ' ' << temp << '\n';

            temp = 0.;
            temp += 0.5*(mesh_m.node(1) - mesh_m.node(0))
                *bl_thickness_integrand(time, mesh_m(0, 1))
                + 0.5*(mesh_m.node(nnodes_m - 1) - mesh_m.node(nnodes_m - 2))
                *bl_thickness_integrand(time, mesh_m(nnodes_m - 1, 1));
            for (std::size_t i(0); i < nnodes_m - 2; ++i)
                temp +=0.5*(mesh_m.node(i + 2) - mesh_m.node(i))
                    *bl_thickness_integrand(time, mesh_m(i + 1, 1));
            files("m") << time << ' ' << temp << '\n';
        }

        files("p") << "\n\n";
        files("e") << "\n\n";
        files("m") << "\n\n";
    }

    if (do_oomph)
    {
        for (std::size_t re_index(0); re_index < Re_numbers.size(); ++re_index)
        {
            for (std::size_t mesh_index(0); mesh_index < mult.size();
                    ++mesh_index)
            {
                double dt(base_dt/mult[mesh_index]);
                /*if (Re_numbers[re_index] > 20000)
                {
                    std::sprintf(directory,
                            "/tmp/dat/Reynolds_Old/"
                            "DATA_S0_omega0.00_%.0f_1-%.0f_%u_60_adapt/",
                            Re_numbers[re_index], R_max[r_max_index],
                            num_elements_r);
                    std::cout << "Calculating BL thicknesses for Re = "
                        << Re_numbers[re_index] << ", R_max = "
                        << R_max[r_max_index] << ", adapt\n";
                }
                else*/
                {
                    std::sprintf(directory,
                            "/tmp/dat/Reynolds/"
                            "S0.00_omega0.00_%.0f_%u_60_%.3f_inertial/",
                            Re_numbers[re_index],
                            unsigned(double(base_mesh)*mult[mesh_index] + 0.5),
                            dt);
                    std::cout << "Calculating BL thicknesses for Re = "
                        << Re_numbers[re_index] << ", mult = "
                        << mult[mesh_index] << "\n";
                    std::cout << "Looking in directory: " << directory << '\n';
                }

                files("p") << "Re = " << Re_numbers[re_index] << ", mult = "
                    << mult[mesh_index] << '\n';
                files("e") << "Re = " << Re_numbers[re_index] << ", mult = "
                    << mult[mesh_index] << '\n';
                files("m") << "Re = " << Re_numbers[re_index] << ", mult = "
                    << mult[mesh_index] << '\n';
                std::size_t i(1);
                try
                {
                    for (std::size_t size(6./dt + 0.5); i < size; ++i)
                    {
                        // Load data for pole and equator at given Re and
                        // time index
                        std::sprintf(filename, "%sp%u.dat", directory,
                                unsigned(i));
                        mesh_p.load_data(filename);
                        std::sprintf(filename, "%se%u.dat", directory,
                                unsigned(i));
                        mesh_e.load_data(filename);
                        std::sprintf(filename, "%sm%u.dat", directory,
                                unsigned(i));
                        mesh_m.load_data(filename);

                        // Calculate BL thickness measures  at pole and equator
                        // (using volumetric flow rate with relative velocity
                        // measure 1 - v so that the free-stream is non-zero.
                        std::size_t nnodes_p(mesh_p.num_nodes());
                        std::size_t nnodes_e(mesh_e.num_nodes());
                        std::size_t nnodes_m(mesh_m.num_nodes());
                        double temp(0.), time(dt*double(i));

                        temp += 0.5*(mesh_p.node(1) - mesh_p.node(0))
                            *bl_thickness_integrand(time, mesh_p(0, 5))
                            + 0.5*(mesh_p.node(nnodes_p - 1)
                                    - mesh_p.node(nnodes_p - 2))
                            *bl_thickness_integrand(time,
                                    mesh_p(nnodes_p - 1, 5));
                        for (std::size_t i(0); i < nnodes_p - 2; ++i)
                            temp +=0.5*(mesh_p.node(i + 2) - mesh_p.node(i))
                                *bl_thickness_integrand(time, mesh_p(i + 1, 5));
                        files("p") << time << ' ' << temp << '\n';

                        temp = 0.;
                        temp += 0.5*(mesh_e.node(1) - mesh_e.node(0))
                            *bl_thickness_integrand(time, mesh_e(0, 3))
                            + 0.5*(mesh_e.node(nnodes_e - 1)
                                    - mesh_e.node(nnodes_e - 2))
                            *bl_thickness_integrand(time,
                                    mesh_e(nnodes_e - 1, 3));
                        for (std::size_t i(0); i < nnodes_e - 2; ++i)
                            temp +=0.5*(mesh_e.node(i + 2) - mesh_e.node(i))
                                *bl_thickness_integrand(time, mesh_e(i + 1, 3));
                        files("e") << time << ' ' << temp << '\n';

                        temp = 0.;
                        temp += 0.5*(mesh_m.node(1) - mesh_m.node(0))
                            *bl_thickness_integrand(time, 
                                    std::sqrt(2.)*mesh_m(0, 3))
                            + 0.5*(mesh_m.node(nnodes_m - 1)
                                    - mesh_m.node(nnodes_m - 2))
                            *bl_thickness_integrand(time, 
                                    std::sqrt(2.)*mesh_m(nnodes_m - 1, 3));
                        for (std::size_t i(0); i < nnodes_m - 2; ++i)
                            temp +=0.5*(mesh_m.node(i + 2) - mesh_m.node(i))
                                *bl_thickness_integrand(time, 
                                        std::sqrt(2.)*mesh_m(i + 1, 3));
                        files("m") << time << ' ' << temp << '\n';
                    }
                }
                catch (std::exception& e)
                {
                    files("p").flush();
                    files("e").flush();
                    files("m").flush();

                    std::cout << "Failed at time step " << i << '\n';
                }

                files("p") << "\n\n";
                files("e") << "\n\n";
                files("m") << "\n\n";
            }
        }
    }

    files.close_all();
    return 0;
}

int oddjob(int argc, char** argv)
{
    File_Output files("./dat/1308");
    Mesh_2D<double> mesh;
    mesh.load_data("./dat/1308/steady_S2.00_r100_flow.dat");
    files.open("out", "steady_transpirations.dat");
    files("out").precision(10);
    for (std::size_t i(0), num_theta(mesh.num_nodes_y()),
            num_r(mesh.num_nodes_x()), w(2); i < num_theta; ++i)
    {
        files("out") << mesh.node_y(i) << ' ' << mesh(num_r - 1, i, w) << '\n';
    }
    files("out").flush();
    files.close_all();
    /*
    Command_Line_Args args(argc, argv);
    double omega_mult(1.5);
    args.option("--omega_mult", omega_mult);
    args.print_recognised_arguments();
    char filename[100];

    Mesh_2D<double> mesh;
    Mesh_1D<double> decay;
    std::sprintf(filename, "./dat/1308/fourier%.2f.dat", omega_mult);
    mesh.load_data(filename);
    std::sprintf(filename, "./dat/1308/decay_rates_perturb.dat");
    decay.load_data(filename);

    File_Output files("./dat/1308");
    std::sprintf(filename, "fourier%.2f_decay.dat", omega_mult);
    files.open("out", filename);
    files("out").precision(10);

    for (std::size_t j(0), num_theta(mesh.num_nodes_y()),
            num_vars(mesh.num_vars()); j < num_theta; ++j)
    {
        for (std::size_t i(0), num_r(mesh.num_nodes_x());
                i < num_r; ++i)
        {
            files("out") << mesh.node_y(j) << ' ' << mesh.node_x(i) << ' ';
            for (std::size_t k(0); k < num_vars; ++k)
                files("out") << mesh(i, j, k) << ' ';
            files("out") << decay(j, 0) << ' ' << decay(j, 1) << ' ';
            files("out") << decay(j, 2) << ' ' << decay(j, 3) << ' ';
            files("out") << decay(j, 4) << ' ' << decay(j, 5) << ' ';
            files("out") << decay(j, 6) << ' ' << decay(j, 7) << '\n';
        }
        files("out") << '\n';
        files("out").flush();
    }

    files.close_all();
    */
    return 0;
}

int main(int argc, char** argv)
{
    return DOIT();
}

