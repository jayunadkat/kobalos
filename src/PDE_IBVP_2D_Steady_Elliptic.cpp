#include <complex>

#include <Console_Output.h>
#include <Exceptions.h>
#include <PDE_IBVP.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    void PDE_IBVP<PDE_2D_Steady_Elliptic, type_, xtype_>::assemble_matrix()
    {
        /*
        std::size_t row(0), condition_index(0),
            num_conditions(conditions_.num_residuals_x());
        std::size_t eqn_order(equation_.order());
        std::size_t num_nodes_x(mesh_.num_nodes_x());
        std::size_t num_nodes_y(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        xtype_ inv_x(0.), inv_y(0.), mid_x(0.), mid_y(0.);
        Vector<type_> mid_state(num_vars, 0.);

        A_.assign(num_nodes_x*num_nodes_y*num_vars,
                (num_nodes_x + 2)*num_vars - 1, 0.);
        b_.assign(num_nodes_x*num_nodes_y*num_vars, 0.);
        
        for (std::size_t j(0); j < num_nodes_y - 1; ++j)
        {
            for (std::size_t i(0); i < num_nodes_x - 1; ++i)
            {
                while (condition_index < num_conditions &&
                        conditions_.node_x(condition_index) == i)
                {
                    conditions_.condition_x(condition_index).x2() =
                        mesh_.node_y(j);
                    conditions_.condition_x(condition_index).update(
                            mesh_.get_vars_at_node(i));
                    for (std::size_t k(0);
                            k < conditions_.condition(condition_index).order(); ++k)
                    {
                        for (std::size_t l(0); l < num_vars; ++l)
                        {
                            A_(row, num_vars*i + l) =
                                conditions_.condition(
                                        condition_index).jacobian()(k, l);
                        }
                        b_[row] = -conditions_.condition(
                                condition_index).residual()[k];
                        ++row;
                    }
                    ++condition_index;
                }

                inv = 1./(mesh_.node(i + 1) - mesh_.node(i));
                for (std::size_t k(0); k < num_vars; ++k)
                    mid_state[k] = (mesh_(i, j, k) + mesh_(i + 1, j, k))/2.;
                mid_x = (mesh_.node(i) + mesh_.node(i + 1))/2.;
                equation_.x1() = mid_x;
                equation_.update(mid_state);
                for (std::size_t j(0); j < eqn_order; ++j)
                {
                    A_(row, num_vars*i + j) = -inv;
                    A_(row, num_vars*(i + 1) + j) = inv;
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) -=
                            equation_.jacobian()(j, k)/2.;
                        A_(row, num_vars*(i + 1) + k) -=
                            equation_.jacobian()(j, k)/2.;
                    }
                    b_[row] = equation_.residual()[j] - 
                        (mesh_(i + 1, j) - mesh_(i, j))*inv;
                    ++row;
                }
            }
        }

        while (condition_index < num_conditions &&
                conditions_.node(condition_index) == num_nodes - 1)
        {
            conditions_.condition(condition_index).update(
                    mesh_.get_vars_at_node(num_nodes - 1));
            for (std::size_t j(0);
                    j < conditions_.condition(condition_index).order(); ++j)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    A_(row, num_vars*(num_nodes - 1) + k) =
                        conditions_.condition(
                                condition_index).jacobian()(j, k);
                }
                b_[row] = -conditions_.condition(
                        condition_index).residual()[j];
                ++row;
            }
            ++condition_index;
        }

#ifdef PARANOID
        if (row != num_nodes*num_vars)
            throw Exceptions::Generic("PDE_IBVP::assemble_matrix",
                    "row != num_nodes*num_vars");
#endif  // PARANOID
        */
    }

    template <typename type_, typename xtype_>
    PDE_IBVP<PDE_2D_Steady_Elliptic, type_, xtype_>::PDE_IBVP(
            Equation_2D<type_, xtype_>& equation,
            Conditions_2D<type_, xtype_>& conditions,
            Mesh_2D<type_, xtype_>& mesh) :
        PDE_IBVP<PDE_Base, type_, xtype_>(),
        equation_(equation),
        conditions_(conditions),
        A_(mesh.num_nodes_x()*mesh.num_nodes_y()*mesh.num_vars(),
                (mesh.num_nodes_x() + 2)*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes_x()*mesh.num_nodes_y()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        mesh_(mesh)
    {
#ifdef PARANOID
/*        if (equation.order() != conditions.num_conditions_x())
            throw Exceptions::Invalid_Argument("PDE_BVP::PDE_IBVP",
                    "Residual objects form an over/underconstrained system");*/
        if (conditions.num_conditions_y() != 2)
            throw Exceptions::Invalid_Argument("PDE_BVP::PDE_IBVP",
                    "Residual objects form an over/underconstrained system");
        if (equation.num_vars() != conditions.condition_x(0).num_vars())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Equation and conditions objects have differing degrees" 
                    " of freedom");
        if (equation.num_vars() != mesh.num_vars())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Mesh object has too few degrees of freedom for equation");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    PDE_IBVP<PDE_2D_Steady_Elliptic, type_, xtype_>::~PDE_IBVP()
    {
    }

    template <typename type_, typename xtype_>
    void PDE_IBVP<PDE_2D_Steady_Elliptic, type_, xtype_>::solve()
    {
        std::size_t counter(0);
        std::size_t num_nodes_x(mesh_.num_nodes_x());
        std::size_t num_nodes_y(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);

        do
        {
            assemble_matrix();
            max_residual = b_.inf_norm();
            linear_system_.solve();

            for (std::size_t i(0); i < num_nodes_x; ++i)
                for (std::size_t j(0); j < num_nodes_y; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) += b_[num_vars*(num_nodes_x*j + i)
                            + k];
            
#ifndef QUIET
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual: "
                    + str_convert(max_residual) + "\n");
#endif  // QUIET

            ++counter;
        }
        while (max_residual > this->tolerance_ &&
                counter < this->max_iterations_);

        if (counter >= this->max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    template class PDE_IBVP<PDE_2D_Steady_Elliptic, double, double>;
    template class PDE_IBVP<PDE_2D_Steady_Elliptic, std::complex<double>,
             double>;
    template class PDE_IBVP<PDE_2D_Steady_Elliptic, std::complex<double>,
             std::complex<double> >;
}   // namespace
