#include <cmath>
#include <complex>
#include <fstream>
#include <iomanip>
#include <sstream>

#include <Exceptions.h>
#include <Mesh_2D.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    Mesh_2D<type_, xtype_>::Mesh_2D() :
        mesh_x_(),
        mesh_y_(),
        vars_(),
        num_vars_(0)
    {
    }

    template <typename type_, typename xtype_>
    Mesh_2D<type_, xtype_>::Mesh_2D(Mesh_1D<type_, xtype_> mesh_x,
            Mesh_1D<type_, xtype_> mesh_y) :
        mesh_x_(mesh_x),
        mesh_y_(mesh_y),
        vars_(mesh_x.num_nodes()*mesh_y.num_nodes()*mesh_x.num_vars(), 0.),
        num_vars_(mesh_x.num_vars())
    {
#ifdef PARANOID
        if (mesh_x.num_vars() != mesh_y.num_vars())
            throw Exceptions::Invalid_Argument("Mesh_2D::Mesh_2D",
                    "Incompatible Mesh_1D objects, num_vars must be the same"
                    " for each mesh");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    Mesh_2D<type_, xtype_>::~Mesh_2D()
    {
    }

    template <typename type_, typename xtype_>
    const Vector<xtype_>& Mesh_2D<type_, xtype_>::nodes_x() const
    {
        return mesh_x_.nodes();
    }

    template <typename type_, typename xtype_>
    const xtype_& Mesh_2D<type_, xtype_>::node_x(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= mesh_x_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_x_)", mesh_x_.num_nodes(),
                    node_index);
#endif  // PARANOID

        return mesh_x_.node(node_index);
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_2D<type_, xtype_>::unmapped_node_x(
            const std::size_t& node_index) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::unmapped_node_x with xtype_ != double"
                );
    }

    template <>
    const double Mesh_2D<double>::unmapped_node_x(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= mesh_x_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_x_)", mesh_x_.num_nodes(),
                    node_index);
#endif  // PARANOID

        return mesh_x_.unmapped_node(node_index);
    }

    template <>
    const double Mesh_2D<std::complex<double>, double>::unmapped_node_x(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= mesh_x_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_x_)", mesh_x_.num_nodes(),
                    node_index);
#endif  // PARANOID

        return mesh_x_.unmapped_node(node_index);
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_2D<type_, xtype_>::map_ds_x(
            const xtype_& s) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::map_ds_x with xtype_ != double");
    }

    template <>
    const double Mesh_2D<double>::map_ds_x(const double& s) const
    {
        return mesh_x_.map_ds(s);
    }

    template <>
    const double Mesh_2D<std::complex<double>, double>::map_ds_x(
            const double& s) const
    {
        return mesh_x_.map_ds(s);
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_2D<type_, xtype_>::map_ds2_x(
            const xtype_& s) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::map_ds2_x with xtype_ != double");
    }

    template <>
    const double Mesh_2D<double>::map_ds2_x(const double& s) const
    {
        return mesh_x_.map_ds2(s);
    }

    template <>
    const double Mesh_2D<std::complex<double>, double>::map_ds2_x(
            const double& s) const
    {
        return mesh_x_.map_ds2(s);
    }

    template <typename type_, typename xtype_>
    const Vector<xtype_>& Mesh_2D<type_, xtype_>::nodes_y() const
    {
        return mesh_y_.nodes();
    }

    template <typename type_, typename xtype_>
    const xtype_& Mesh_2D<type_, xtype_>::node_y(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= mesh_y_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_y_)", mesh_y_.num_nodes(),
                    node_index);
#endif  // PARANOID

        return mesh_y_.node(node_index);
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_2D<type_, xtype_>::unmapped_node_y(
            const std::size_t& node_index) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::unmapped_node_y with xtype_ != double"
                );
    }

    template <>
    const double Mesh_2D<double>::unmapped_node_y(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= mesh_y_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_y_)", mesh_y_.num_nodes(),
                    node_index);
#endif  // PARANOID

        return mesh_y_.unmapped_node(node_index);
    }

    template <>
    const double Mesh_2D<std::complex<double>, double>::unmapped_node_y(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= mesh_y_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_y_)", mesh_y_.num_nodes(),
                    node_index);
#endif  // PARANOID

        return mesh_y_.unmapped_node(node_index);
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_2D<type_, xtype_>::map_ds_y(
            const xtype_& s) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::map_ds_y with xtype_ != double");
    }

    template <>
    const double Mesh_2D<double>::map_ds_y(const double& s) const
    {
        return mesh_y_.map_ds(s);
    }

    template <>
    const double Mesh_2D<std::complex<double>, double>::map_ds_y(
            const double& s) const
    {
        return mesh_y_.map_ds(s);
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_2D<type_, xtype_>::map_ds2_y(
            const xtype_& s) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::map_ds2_y with xtype_ != double");
    }

    template <>
    const double Mesh_2D<double>::map_ds2_y(const double& s) const
    {
        return mesh_y_.map_ds2(s);
    }

    template <>
    const double Mesh_2D<std::complex<double>, double>::map_ds2_y(
            const double& s) const
    {
        return mesh_y_.map_ds2(s);
    }

    template <typename type_, typename xtype_>
    type_& Mesh_2D<type_, xtype_>::operator()(const std::size_t& node_x_index,
            const std::size_t& node_y_index,
            const std::size_t& var_index)
    {
#ifdef PARANOID
        if (node_x_index >= mesh_x_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_x_)", mesh_x_.num_nodes(),
                    node_x_index);
        if (node_y_index >= mesh_y_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_y_)", mesh_y_.num_nodes(),
                    node_y_index);
        if (var_index >= num_vars_)
            throw Exceptions::Range("Mesh_2D (vars_)", num_vars_, var_index);
#endif  // PARANOID

        return vars_[num_vars_*(node_y_index*mesh_x_.num_nodes() +
                node_x_index) + var_index];
    }

    template <typename type_, typename xtype_>
    const type_& Mesh_2D<type_, xtype_>::operator()(
            const std::size_t& node_x_index,
            const std::size_t& node_y_index,
            const std::size_t& var_index) const
    {
#ifdef PARANOID
        if (node_x_index >= mesh_x_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_x_)", mesh_x_.num_nodes(),
                    node_x_index);
        if (node_y_index >= mesh_y_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_y_)", mesh_y_.num_nodes(),
                    node_y_index);
        if (var_index >= num_vars_)
            throw Exceptions::Range("Mesh_2D (vars_)", num_vars_, var_index);
#endif  // PARANOID

        return vars_[num_vars_*(node_y_index*mesh_x_.num_nodes() +
                node_x_index) + var_index];
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::set_vars_vector(const Vector<type_>& vars_data)
    {
#ifdef PARANOID
        if (vars_data.size() != 
                mesh_x_.num_nodes()*mesh_y_.num_nodes()*num_vars_)
            throw Exceptions::Mismatch("Mesh_2D::set_vars_vector", "Vector");
#endif  // PARANOID

        vars_ = vars_data;
    }

    template <typename type_, typename xtype_>
    const Vector<type_>& Mesh_2D<type_, xtype_>::get_vars_vector() const
    {
        return vars_;
    }

    template <typename type_, typename xtype_>
    const Vector<type_> Mesh_2D<type_, xtype_>::get_vars_at_node(
            const std::size_t& node_x_index,
            const std::size_t& node_y_index) const
    {
#ifdef PARANOID
        if (node_x_index >= mesh_x_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_x_)", mesh_x_.num_nodes(),
                    node_x_index);
        if (node_y_index >= mesh_y_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_y_)", mesh_y_.num_nodes(),
                    node_y_index);
#endif  // PARANOID

        Vector<type_> temp;
        for (std::size_t i = 0; i < num_vars_; ++i)
            temp.push_back(vars_[num_vars_*(mesh_x_.num_nodes()*node_y_index +
                        node_x_index) + i]);
        return temp;
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::set_vars_at_node(
            const std::size_t& node_x_index,
            const std::size_t& node_y_index,
            const Vector<type_>& vars_data)
    {
#ifdef PARANOID
        if (node_x_index >= mesh_x_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_x_)", mesh_x_.num_nodes(),
                    node_x_index);
        if (node_y_index >= mesh_y_.num_nodes())
            throw Exceptions::Range("Mesh_2D (mesh_y_)", mesh_y_.num_nodes(),
                    node_y_index);
        if (vars_data.size() != num_vars_)
            throw Exceptions::Mismatch("Mesh_2D::set_vars_at_node", "Vector");
#endif  // PARANOID

        for (std::size_t i = 0; i < num_vars_; ++i)
            vars_[num_vars_*(mesh_x_.num_nodes()*node_y_index +
                    node_x_index) + i] = vars_data[i];
    }

    template <typename type_, typename xtype_>
    const std::size_t& Mesh_2D<type_, xtype_>::num_nodes_x() const
    {
        return mesh_x_.num_nodes();
    }

    template <typename type_, typename xtype_>
    const std::size_t& Mesh_2D<type_, xtype_>::num_nodes_y() const
    {
        return mesh_y_.num_nodes();
    }

    template <typename type_, typename xtype_>
    const std::size_t& Mesh_2D<type_, xtype_>::num_vars() const
    {
        return num_vars_;
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::dump(std::ostream& out,
            const std::size_t& skip) const
    {
        out << "# Mesh_2D\n";
        out << "# Node (y, x) ";
        out << "# Data\n";
        for (std::size_t i(0), num_y(mesh_y_.num_nodes()); i < num_y; i += skip)
        {
            for (std::size_t j(0), num_x(mesh_x_.num_nodes()); j < num_x;
                    j += skip)
            {
                out << mesh_y_.node(i) << ' ' << mesh_x_.node(j);
                for (std::size_t k = 0; k < num_vars_; ++k)
                    out << ' ' << vars_[num_vars_*(mesh_x_.num_nodes()*i + j) +
                        k];
                out << '\n';
            }
            out << '\n';
        }
        out << "# Number of nodes in x: " << mesh_x_.num_nodes() << '\n';
        out << "# Number of nodes in y: " << mesh_y_.num_nodes() << '\n';
        out << "# Skip size: " << skip << '\n';
        out << "# Number of variables: " << num_vars_ << "\n\n\n";
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::dump(const std::size_t& decimal_places,
            std::ostream& out, const std::size_t& skip) const
    {
        std::size_t width = decimal_places + 8;
        std::size_t old_precision(out.precision());
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision(decimal_places);
        out.setf(std::ios_base::showpos);

        out.setf(std::ios::left);
        out << "# Mesh_2D\n";
        out << std::setw(2*width + 1) << "# Node (y, x)" << ' ';
        out << std::setw(width) << "# Data" << '\n';
        out.unsetf(std::ios::left);
        for (std::size_t i(0), num_y(mesh_y_.num_nodes()); i < num_y; i += skip)
        {
            for (std::size_t j(0), num_x(mesh_x_.num_nodes()); j < num_x;
                    j += skip)
            {
                out << std::setw(width) << mesh_y_.node(i) << ' ';
                out << std::setw(width) << mesh_x_.node(j);
                for (std::size_t k = 0; k < num_vars_; ++k)
                    out << ' ' << std::setw(width) << vars_[num_vars_*
                        (mesh_x_.num_nodes()*i + j) + k];
                out << '\n';
            }
            out << '\n';
        }
        out << "# Number of nodes in x: " << mesh_x_.num_nodes() << '\n';
        out << "# Number of nodes in y: " << mesh_y_.num_nodes() << '\n';
        out << "# Skip size: " << skip << '\n';
        out << "# Number of variables: " << num_vars_ << "\n\n\n";
        out.precision(old_precision);
    }

    template <>
    void Mesh_2D<std::complex<double> >::dump_real(
            const std::size_t& decimal_places, std::ostream& out) const
    {
        std::size_t width = decimal_places + 8;
        std::size_t old_precision(out.precision());
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision(decimal_places);
        out.setf(std::ios_base::showpos);

        out.setf(std::ios::left);
        out << "# Mesh_2D\n";
        out << std::setw(2*width + 1) << "# Node (y, x)" << ' ';
        out << std::setw(width) << "# Data" << '\n';
        out.unsetf(std::ios::left);
        for (std::size_t i(0), num_y(mesh_y_.num_nodes()); i < num_y; ++i)
        {
            for (std::size_t j(0), num_x(mesh_x_.num_nodes()); j < num_x; ++j)
            {
                out << std::setw(width) << mesh_y_.node(i) << ' ';
                out << std::setw(width) << mesh_x_.node(j);
                for (std::size_t k = 0; k < num_vars_; ++k)
                    out << ' ' << std::setw(width) << vars_[num_vars_*
                        (mesh_x_.num_nodes()*i + j) + k].real() << ' '
                        << vars_[num_vars_*(mesh_x_.num_nodes()*i + j)
                                + k].imag();
                out << '\n';
            }
            out << '\n';
        }
        out << "# Number of nodes in x: " << mesh_x_.num_nodes() << '\n';
        out << "# Number of nodes in y: " << mesh_y_.num_nodes() << '\n';
        out << "# Number of variables: " << num_vars_ << "\n\n\n";
        out.precision(old_precision);
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::dump_paraview(std::ostream& out,
            const std::size_t& skip) const
    {
        for (std::size_t i(0), num_y(mesh_y_.num_nodes()); i < num_y; i += skip)
        {
            for (std::size_t j(0), num_x(mesh_x_.num_nodes()); j < num_x;
                    j += skip)
            {
                out << mesh_x_.node(j) << ',' << mesh_y_.node(i) << ",0,";
                for (std::size_t k = 0; k < num_vars_; ++k)
                    out << ',' << vars_[num_vars_*(mesh_x_.num_nodes()*i + j) +
                        k];
                out << '\n';
            }
        }
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::dump_paraview(
            const std::size_t& decimal_places,
            std::ostream& out, const std::size_t& skip) const
    {
        std::size_t width = decimal_places + 8;
        std::size_t old_precision(out.precision());
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision(decimal_places);
        out.setf(std::ios_base::showpos);

        for (std::size_t i(0), num_y(mesh_y_.num_nodes()); i < num_y; i += skip)
        {
            for (std::size_t j(0), num_x(mesh_x_.num_nodes()); j < num_x;
                    j += skip)
            {
                out << std::setw(width) << mesh_x_.node(j) << ',';
                out << std::setw(width) << mesh_y_.node(i) << ",0,";
                for (std::size_t k = 0; k < num_vars_; ++k)
                    out << ',' << std::setw(width) << vars_[num_vars_*
                        (mesh_x_.num_nodes()*i + j) + k];
                out << '\n';
            }
        }
        out.precision(old_precision);
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::load_data(const std::string& path)
    {
        load_data(path.c_str());
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::load_data(const char* path)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::load_data with xtype_ != double "
                "or type_ != double");
    }

    template <>
    void Mesh_2D<double>::load_data(const char* path)
    {
        std::ifstream data;
        std::string line_data;

        data.open(path);
        if (data.is_open())
        {
            /*if (!std::getline(data, line_data))
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Unspecified error during reading of data file");
            }
            if (line_data != "# Mesh_2D")
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Data must be in Mesh_2D output format with "
                        "\"# Mesh_2D\" at head of file");
            }*/

            bool new_block(true), first_block(true), possible_block(false);
            bool num_vars_found(false);
            double temp(0.);
            Vector<double> nodes_x, nodes_y;

            num_vars_ = 0;
            vars_.clear();

            while (std::getline(data, line_data))
            {
                if (line_data[0] == '#')
                    continue;
                if (line_data == "")
                {
                    first_block = false;
                    new_block = true;
                    if (possible_block)
                        break;
                    else
                        possible_block = true;
                    continue;
                }
                possible_block = false;
                std::istringstream iss(line_data);
                if (!(iss >> temp))
                    throw Exceptions::Generic("Data IO",
                            "Error during reading of data file, probably "
                            "missing data");
                if (new_block)
                {
                    nodes_y.push_back(temp);
                    new_block = false;
                }
                if (!(iss >> temp))
                    throw Exceptions::Generic("Data IO",
                            "Error during reading of data file, probably "
                            "missing data");
                if (first_block)
                    nodes_x.push_back(temp);
                while (iss >> temp)
                {
                    vars_.push_back(temp);
                    if (!num_vars_found)
                        ++num_vars_;
                }
                num_vars_found = true;
            }
            data.close();

            mesh_x_ = Mesh_1D<double>(nodes_x, num_vars_);
            mesh_y_ = Mesh_1D<double>(nodes_y, num_vars_);
        }
        else
            throw Exceptions::Generic("Data IO", "Unable to open data file");
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::load_data_reversed_axes(const char* path)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::load_data_reversed_axes with "
                "xtype_ != double or type_ = double");
    }

    template <>
    void Mesh_2D<double>::load_data_reversed_axes(const char* path)
    {
        std::ifstream data;
        std::string line_data;

        data.open(path);
        if (data.is_open())
        {
            /*if (!std::getline(data, line_data))
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Unspecified error during reading of data file");
            }
            if (line_data != "# Mesh_2D")
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Data must be in Mesh_2D output format with "
                        "\"# Mesh_2D\" at head of file");
            }*/

            bool new_block(true), first_block(true), possible_block(false);
            bool num_vars_found(false);
            double temp(0.);
            Vector<double> nodes_x, nodes_y;

            num_vars_ = 0;
            vars_.clear();

            while (std::getline(data, line_data))
            {
                if (line_data[0] == '#')
                    continue;
                if (line_data == "")
                {
                    first_block = false;
                    new_block = true;
                    if (possible_block)
                        break;
                    else
                        possible_block = true;
                    continue;
                }
                possible_block = false;
                std::istringstream iss(line_data);
                if (!(iss >> temp))
                    throw Exceptions::Generic("Data IO",
                            "Error during reading of data file, probably "
                            "missing data");
                if (first_block)
                    nodes_x.push_back(temp);
                if (!(iss >> temp))
                    throw Exceptions::Generic("Data IO",
                            "Error during reading of data file, probably "
                            "missing data");
                if (new_block)
                {
                    nodes_y.push_back(temp);
                    new_block = false;
                }
                while (iss >> temp)
                {
                    vars_.push_back(temp);
                    if (!num_vars_found)
                        ++num_vars_;
                }
                num_vars_found = true;
            }
            data.close();

            mesh_x_ = Mesh_1D<double>(nodes_x, num_vars_);
            mesh_y_ = Mesh_1D<double>(nodes_y, num_vars_);
        }
        else
            throw Exceptions::Generic("Data IO", "Unable to open data file");
    }

    template <>
    void Mesh_2D<std::complex<double> >::load_data_reversed_axes(
            const char* path)
    {
        std::ifstream data;
        std::string line_data;

        data.open(path);
        if (data.is_open())
        {
            /*if (!std::getline(data, line_data))
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Unspecified error during reading of data file");
            }
            if (line_data != "# Mesh_2D")
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Data must be in Mesh_2D output format with "
                        "\"# Mesh_2D\" at head of file");
            }*/

            bool new_block(true), first_block(true), possible_block(false);
            bool num_vars_found(false);
            double temp(0.), temp2(0.);
            Vector<double> nodes_x, nodes_y;

            num_vars_ = 0;
            vars_.clear();

            while (std::getline(data, line_data))
            {
                if (line_data[0] == '#')
                    continue;
                if (line_data == "")
                {
                    first_block = false;
                    new_block = true;
                    if (possible_block)
                        break;
                    else
                        possible_block = true;
                    continue;
                }
                possible_block = false;
                std::istringstream iss(line_data);
                if (!(iss >> temp))
                    throw Exceptions::Generic("Data IO",
                            "Error during reading of data file, probably "
                            "missing data");
                if (first_block)
                    nodes_x.push_back(temp);
                if (!(iss >> temp))
                    throw Exceptions::Generic("Data IO",
                            "Error during reading of data file, probably "
                            "missing data");
                if (new_block)
                {
                    nodes_y.push_back(temp);
                    new_block = false;
                }
                while (iss >> temp)
                {
                    iss >> temp2;
                    vars_.push_back(std::complex<double>(temp, temp2));
                    if (!num_vars_found)
                        ++num_vars_;
                }
                num_vars_found = true;
            }
            data.close();

            mesh_x_ = Mesh_1D<std::complex<double> >(nodes_x, num_vars_);
            mesh_y_ = Mesh_1D<std::complex<double> >(nodes_y, num_vars_);
        }
        else
            throw Exceptions::Generic("Data IO", "Unable to open data file");
    }

    template <typename type_, typename xtype_>
    const type_ Mesh_2D<type_, xtype_>::linear_interpolant(const xtype_& x,
            const xtype_& y, const std::size_t var) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::linear_interpolant with type_, xtype_ "
                "!= double");
    }

    template <>
    const double Mesh_2D<double>::linear_interpolant(const double& x,
            const double& y, const std::size_t var) const
    {
        double x2(mesh_x_.inv_map(x)), y2(mesh_y_.inv_map(y));
        std::size_t num_nodes_x(mesh_x_.num_nodes());
        std::size_t num_nodes_y(mesh_y_.num_nodes());
        double x_left(mesh_x_.node(0)), y_left(mesh_y_.node(0));
        double x_right(mesh_x_.node(num_nodes_x - 1));
        double y_right(mesh_y_.node(num_nodes_y - 1));
        bool x_reversed(x_right < x_left), y_reversed(y_right < y_left);

#ifdef PARANOID
        if ((!x_reversed && (x2 < x_left || x2 > x_right)) ||
                (x_reversed && (x2 < x_right || x2 > x_left)))
            throw Exceptions::Invalid_Argument("Mesh_2D::linear_interpolant",
                    "x must be within the domain");
        if ((!y_reversed && (y2 < y_left || y2 > y_right)) ||
                (y_reversed && (y2 < y_right || y2 > y_left)))
            throw Exceptions::Invalid_Argument("Mesh_2D::linear_interpolant",
                    "y must be within the domain");
        if (var >= num_vars_)
            throw Exceptions::Invalid_Argument("Mesh_2D::linear_interpolant",
                    "var < num_vars_ is required");
#endif  // PARANOID

        std::size_t node_x(0), node_y(0);
        double diff_x(0.), diff_y(0.), x_step(0.), y_step(0.);
        int mx(x_reversed*-2 + 1), my(y_reversed*-2 + 1);

        for (std::size_t i(1); i < num_nodes_x - 1; ++i)
            if (mx*x2 >= mx*mesh_x_.node(i))
                node_x = i;
        for (std::size_t i(1); i < num_nodes_y - 1; ++i)
            if (my*y2 >= my*mesh_y_.node(i))
                node_y = i;

        diff_x = x2 - mesh_x_.node(node_x);
        diff_y = y2 - mesh_y_.node(node_y);
        x_step = mesh_x_.node(node_x + 1) - mesh_x_.node(node_x);
        y_step = mesh_y_.node(node_y + 1) - mesh_y_.node(node_y);

        if (x2 == mesh_x_.node(num_nodes_x - 1) &&
                y2 == mesh_y_.node(num_nodes_y - 1))
            return operator()(num_nodes_x - 1, num_nodes_y - 1, var);
        else if (x2 == mesh_x_.node(num_nodes_x - 1))
            return operator()(num_nodes_x - 1, node_y, var)
                + diff_y/y_step*(
                        operator()(num_nodes_x - 1, node_y + 1, var)
                        - operator()(num_nodes_x - 1, node_y, var));
        else if (y2 == mesh_y_.node(num_nodes_y - 1))
            return operator()(node_x, num_nodes_y - 1, var)
                + diff_x/x_step*(
                        operator()(node_x + 1, num_nodes_y - 1, var)
                        - operator()(node_x, num_nodes_y - 1, var));

        return (operator()(node_x, node_y, var)*(y_step - diff_y)*(
                x_step - diff_x) + operator()(node_x + 1, node_y, var)*(
                    y_step - diff_y)*diff_x + operator()(
                        node_x, node_y + 1, var)*diff_y*(x_step - diff_x)
                    + operator()(node_x + 1, node_y + 1, var)*diff_x*diff_y)/
            (x_step*y_step);
    }

    template <typename type_, typename xtype_>
    const type_ Mesh_2D<type_, xtype_>::derivative_x_at_node(
            const std::size_t& node_index, const std::size_t& var_index) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::derivative_x_at_node with type_, "
                "xtype_ != double");
    }

    template <>
    const double Mesh_2D<double>::derivative_x_at_node(
            const std::size_t& node_index, const std::size_t& var_index) const
    {
        return mesh_x_.derivative_at_node(node_index, var_index);
    }

    template <typename type_, typename xtype_>
    const type_ Mesh_2D<type_, xtype_>::derivative_y_at_node(
            const std::size_t& node_index, const std::size_t& var_index) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::derivative_y_at_node with type_, "
                "xtype_ != double");
    }

    template <>
    const double Mesh_2D<double>::derivative_y_at_node(
            const std::size_t& node_index, const std::size_t& var_index) const
    {
        return mesh_x_.derivative_at_node(node_index, var_index);
    }

    template <typename type_, typename xtype_>
    void Mesh_2D<type_, xtype_>::uniform_mesh(const xtype_& x_begin,
            const xtype_& x_end, const std::size_t& num_nodes_x,
            const xtype_& y_begin, const xtype_& y_end,
            const std::size_t& num_nodes_y, const std::size_t& num_vars)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::uniform_mesh with xtype_ != double");
    }

    template <>
    void Mesh_2D<double>::uniform_mesh(const double& x_begin,
            const double& x_end, const std::size_t& num_nodes_x,
            const double& y_begin, const double& y_end,
            const std::size_t& num_nodes_y, const std::size_t& num_vars)
    {
#ifdef PARANOID
        if (num_nodes_x <= 1)
            throw Exceptions::Invalid_Argument("Mesh_2D::uniform_mesh",
                    "num_nodes_x >= 2 is required");
        if (num_nodes_y <= 1)
            throw Exceptions::Invalid_Argument("Mesh_2D::uniform_mesh",
                    "num_nodes_y >= 2 is required");
#endif  // PARANOID

        mesh_x_.uniform_mesh(x_begin, x_end, num_nodes_x, num_vars);
        mesh_y_.uniform_mesh(y_begin, y_end, num_nodes_y, num_vars);
        num_vars_ = num_vars;
        vars_.assign(num_nodes_x*num_nodes_y*num_vars_, 0.);    
    }

    template <>
    void Mesh_2D<std::complex<double>, double>::uniform_mesh(
            const double& x_begin, const double& x_end,
            const std::size_t& num_nodes_x, const double& y_begin,
            const double& y_end, const std::size_t& num_nodes_y,
            const std::size_t& num_vars)
    {
#ifdef PARANOID
        if (num_nodes_x <= 1)
            throw Exceptions::Invalid_Argument("Mesh_2D::uniform_mesh",
                    "num_nodes_x >= 2 is required");
        if (num_nodes_y <= 1)
            throw Exceptions::Invalid_Argument("Mesh_2D::uniform_mesh",
                    "num_nodes_y >= 2 is required");
#endif  // PARANOID

        mesh_x_.uniform_mesh(x_begin, x_end, num_nodes_x, num_vars);
        mesh_y_.uniform_mesh(y_begin, y_end, num_nodes_y, num_vars);
        num_vars_ = num_vars;
        vars_.assign(num_nodes_x*num_nodes_y*num_vars_, 0.);    
    }

    template <typename type_, typename xtype_>
    const bool Mesh_2D<type_, xtype_>::query_x(const xtype_& x,
            std::size_t& index)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::query with xtype_ != double");
    }

    template <>
    const bool Mesh_2D<double>::query_x(const double& x,
            std::size_t& index)
    {
#ifdef PARANOID
        if (x < std::min(mesh_x_.node(0),
                    mesh_x_.node(mesh_x_.num_nodes() - 1)) - snap_tolerance_ ||
                x > std::max(mesh_x_.node(0),
                    mesh_x_.node(mesh_x_.num_nodes() - 1)) + snap_tolerance_)
            throw Exceptions::Invalid_Argument("Mesh_2D::query_x",
                    "x must be within the domain");
#endif  // PARANOID
        
        return mesh_x_.query(x, index);
    }

    template <>
    const bool Mesh_2D<std::complex<double>, double>::query_x(const double& x,
            std::size_t& index)
    {
#ifdef PARANOID
        if (x < std::min(mesh_x_.node(0),
                    mesh_x_.node(mesh_x_.num_nodes() - 1)) - snap_tolerance_ ||
                x > std::max(mesh_x_.node(0),
                    mesh_x_.node(mesh_x_.num_nodes() - 1)) + snap_tolerance_)
            throw Exceptions::Invalid_Argument("Mesh_2D::query_x",
                    "x must be within the domain");
#endif  // PARANOID
        
        return mesh_x_.query(x, index);
    }

    template <typename type_, typename xtype_>
    const bool Mesh_2D<type_, xtype_>::query_y(const xtype_& y,
            std::size_t& index)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_2D::query_y with xtype_ != double");
    }

    template <>
    const bool Mesh_2D<double>::query_y(const double& y,
            std::size_t& index)
    {
#ifdef PARANOID
        if (y < std::min(mesh_y_.node(0),
                    mesh_y_.node(mesh_y_.num_nodes() - 1)) - snap_tolerance_ ||
                y > std::max(mesh_y_.node(0),
                    mesh_y_.node(mesh_y_.num_nodes() - 1)) + snap_tolerance_)
            throw Exceptions::Invalid_Argument("Mesh_2D::query_y",
                    "y must be within the domain");
#endif  // PARANOID
        
        return mesh_y_.query(y, index);
    }

    template <>
    const bool Mesh_2D<std::complex<double>, double>::query_y(const double& y,
            std::size_t& index)
    {
#ifdef PARANOID
        if (y < std::min(mesh_y_.node(0),
                    mesh_y_.node(mesh_y_.num_nodes() - 1)) - snap_tolerance_ ||
                y > std::max(mesh_y_.node(0),
                    mesh_y_.node(mesh_y_.num_nodes() - 1)) + snap_tolerance_)
            throw Exceptions::Invalid_Argument("Mesh_2D::query_y",
                    "y must be within the domain");
#endif  // PARANOID
        
        return mesh_y_.query(y, index);
    }

    template class Mesh_2D<double>;
    template class Mesh_2D<std::complex<double>, double>;
    template class Mesh_2D<std::complex<double>, std::complex<double> >;
}   // namespace

