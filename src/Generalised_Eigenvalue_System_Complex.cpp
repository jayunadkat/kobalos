#include <algorithm>
#include <cmath>
#include <complex>
#include <sstream>

#include <Console_Output.h>
#include <Generalised_Eigenvalue_System_Complex.h>
#include <Exceptions.h>
#include <Fortran_LAPACK.h>
#include <Timer.h>

namespace Kobalos
{
    const double Generalised_Eigenvalue_System_Complex::tolerance_ = 1e-10;

    Generalised_Eigenvalue_System_Complex::Eigen_Stuff::Eigen_Stuff(
            const std::complex<double>& new_value,
            const Vector<double>& new_vector_real,
            const Vector<double>& new_vector_imag) :
        value(new_value),
        vector_real(new_vector_real),
        vector_imag(new_vector_imag)
    {
    }

    bool Generalised_Eigenvalue_System_Complex::Eigen_Stuff_Sort::operator()(
            const Eigen_Stuff& left, const Eigen_Stuff& right) const
    {
        return left.value.real() > right.value.real();
    }

    Generalised_Eigenvalue_System_Complex::Generalised_Eigenvalue_System_Complex(
            Matrix_Helper& A, Matrix_Helper& B) :
        A_(A),
        B_(B),
        VR_(A.num_rows()),
        ALPHA_(A.dimension(), 0.),
        BETA_(A.dimension(), 0.),
        eigen_stuff_()
    {
        if (A_.num_rows() != B_.num_rows())
            throw Exceptions::Mismatch(
                    "Generalised_Eigenvalue_System_Complex::"
                    "Generalised_Eigenvalue_System_Complex()",
                    "Vector", "Vector");
    }

    Generalised_Eigenvalue_System_Complex::~Generalised_Eigenvalue_System_Complex()
    {
    }

    void Generalised_Eigenvalue_System_Complex::solve()
    {
        solve_lapack();
    }

    std::size_t Generalised_Eigenvalue_System_Complex::order() const
    {
        return A_.num_rows();
    }

    std::size_t Generalised_Eigenvalue_System_Complex::solution_size() const
    {
        return eigen_stuff_.size();
    }

    void Generalised_Eigenvalue_System_Complex::solve_lapack()
    {
#ifndef LAPACK
        throw Exceptions::Generic("Generalised_Eigenvalue_System_Complex::"
                "solve_lapack()",
                "LAPACK support must be enabled by compiling with -DLAPACK");
#else
        char JOBVL = 'N';
        char JOBVR = 'V';
        int N = A_.num_rows();
        double* A = A_.data_pointer();
        int LDA = N;
        double* B = B_.data_pointer();
        int LDB = N;
        double* ALPHA = ALPHA_.data_pointer();
        double* BETA = BETA_.data_pointer();
        double* VL = NULL;
        int LDVL = 1;
        double* VR = VR_.data_pointer();
        int LDVR = N;
        int LWORK = 1;
        Vector<double> work(LWORK, 0.);
        Vector<double> rwork(8*N, 0.);
        double* WORK = work.data_pointer();
        double* RWORK = rwork.data_pointer();
        int INFO = 0;

#ifdef TIME
        Timer t_solve("Calling LAPACK's zggev");
        t_solve.start();
#endif  // TIME

        LAPACK_ZGGEV(&JOBVL, &JOBVR, N, A, LDA, B, LDB, ALPHA, BETA,
                VL, LDVL, VR, LDVR, WORK, -1, RWORK, INFO);

        LWORK = static_cast<int>(WORK[0]);
        work.resize(2*LWORK);
        WORK = work.data_pointer();

        LAPACK_ZGGEV(&JOBVL, &JOBVR, N, A, LDA, B, LDB, ALPHA, BETA,
                VL, LDVL, VR, LDVR, WORK, LWORK, RWORK, INFO);

#ifdef TIME
        t_solve.stop();
        t_solve.print();
#endif  // TIME

        if (INFO != 0)
        {
            std::stringstream problem;
            problem << "LAPACK_ZGGEV returned INFO = " << INFO;
            if (INFO < 0 || INFO > N)
                throw Exceptions::Generic(
                        "Generalised_Eigenvalue_System_Complex::solve_lapack()",
                        problem.str());
            else
                console_out(
                        str_convert("Eigenvectors could not be calculated, "
                            "INFO = \%u\n", INFO));
        }

        eigen_stuff_.clear();
        for (int i(0); i < N; ++i)
        {
            Eigen_Stuff temp_e_stuff(eigenvalue_(i), eigenvector_real_(i),
                    eigenvector_imag_(i));
            if (!std::isnan(temp_e_stuff.value.real()))
                eigen_stuff_.push_back(temp_e_stuff);
        }
        std::sort(eigen_stuff_.begin(), eigen_stuff_.end(), Eigen_Stuff_Sort());
#endif  // LAPACK
    }
    
    std::complex<double> Generalised_Eigenvalue_System_Complex::eigenvalue_(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= static_cast<std::size_t>(A_.num_rows()))
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        double Ar(ALPHA_[2*index]);
        double Ai(ALPHA_[2*index + 1]);
        double Br(BETA_[2*index]);
        double Bi(BETA_[2*index + 1]);
        double bottom(Br*Br + Bi*Bi);

        return std::complex<double>(
                (Ar*Br + Ai*Bi)/bottom, (Ai*Br - Ar*Bi)/bottom);
    }

    Vector<double> Generalised_Eigenvalue_System_Complex::eigenvector_real_(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= static_cast<std::size_t>(A_.num_rows()))
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        Vector<double> eigenvector(A_.num_rows(), 0.);
        for (int i(0); i < A_.num_rows(); ++i)
            eigenvector[i] = VR_(i, index, 0);

        return eigenvector;
    }

    Vector<double> Generalised_Eigenvalue_System_Complex::eigenvector_imag_(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= static_cast<std::size_t>(A_.num_rows()))
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        Vector<double> eigenvector(A_.num_rows(), 0.);
        for (int i(0); i < A_.num_rows(); ++i)
            eigenvector[i] = VR_(i, index, 1);

        return eigenvector;
    }

    const Vector<double>& Generalised_Eigenvalue_System_Complex::alpha() const
    {
        return ALPHA_;
    }

    const Vector<double>& Generalised_Eigenvalue_System_Complex::beta() const
    {
        return BETA_;
    }

    const std::complex<double>& Generalised_Eigenvalue_System_Complex::eigenvalue(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= eigen_stuff_.size())
            throw Exceptions::Range("Vector", eigen_stuff_.size(), index);
#endif  // PARANOID

        return eigen_stuff_[index].value;
    }

    const Matrix_Helper& Generalised_Eigenvalue_System_Complex::vr() const
    {
        return VR_;
    }

    const Vector<double>&
        Generalised_Eigenvalue_System_Complex::eigenvector_real(
                const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= eigen_stuff.size())
            throw Exceptions::Range("Vector", eigen_stuff_.size(), index);
#endif  // PARANOID

        return eigen_stuff_[index].vector_real;
    }

    const Vector<double>&
        Generalised_Eigenvalue_System_Complex::eigenvector_imag(
                const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= eigen_stuff_.size())
            throw Exceptions::Range("Vector", eigen_stuff_.size(), index);
#endif  // PARANOID

        return eigen_stuff_[index].vector_imag;
    }
}   // namespace

