#include <algorithm>
#include <cmath>
#include <complex>
#include <sstream>

#include <Console_Output.h>
#include <Generalised_Eigenvalue_System.h>
#include <Exceptions.h>
#include <Fortran_LAPACK.h>
#include <Timer.h>

namespace Kobalos
{
    template <typename type_>
    const double Generalised_Eigenvalue_System<type_>::tolerance_ = 1e-10;

    template <typename type_>
    Generalised_Eigenvalue_System<type_>::Eigen_Stuff::Eigen_Stuff(
            const Vector<type_>& new_value, const Vector<type_>& new_vector) :
        complex_valued(false),
        value(new_value),
        vector_real(new_vector),
        vector_imag(Vector<double>(vector_real.size(), 0.))
    {
    }

    template <typename type_>
    Generalised_Eigenvalue_System<type_>::Eigen_Stuff::Eigen_Stuff(
            const Vector<type_>& new_value,
            const Vector<type_>& new_vector_real,
            const Vector<type_>& new_vector_imag) :
        complex_valued(true),
        value(new_value),
        vector_real(new_vector_real),
        vector_imag(new_vector_imag)
    {
    }

    template <typename type_>
    bool Generalised_Eigenvalue_System<type_>::Eigen_Stuff_Sort::operator()(
            const Eigen_Stuff& left, const Eigen_Stuff& right) const
    {
        if (left.value[0] < right.value[0])
            return true;
        else
            return false;
    }

    template <typename type_>
    Generalised_Eigenvalue_System<type_>::Generalised_Eigenvalue_System(
            Dense_Matrix<type_>& A, Dense_Matrix<type_>& B) :
        A_(A),
        B_(B),
        VR_(A.num_rows(), 0.),
        ALPHAR_(A.num_rows(), 0.),
        ALPHAI_(A.num_rows(), 0.),
        BETA_(A.num_rows(), 0.)
    {
        if ((A_.num_columns() != B_.num_columns()) ||
                (A_.num_rows() != B_.num_rows()) ||
                (A_.num_rows() != A_.num_columns()))
            throw Exceptions::Mismatch(
                    "Generalised_Eigenvalue_System::"
                    "Generalised_Eigenvalue_System()",
                    "Dense_Matrix", "Dense_Matrix");
    }

    template <typename type_>
    Generalised_Eigenvalue_System<type_>::~Generalised_Eigenvalue_System()
    {
    }

    template <typename type_>
    void Generalised_Eigenvalue_System<type_>::solve()
    {
        solve_lapack();
    }

    template <typename type_>
    std::size_t Generalised_Eigenvalue_System<type_>::order() const
    {
        return A_.num_columns();
    }

    template <typename type_>
    std::size_t Generalised_Eigenvalue_System<type_>::solution_size() const
    {
        return eigen_stuff_.size();
    }

    template <typename type_>
    void Generalised_Eigenvalue_System<type_>::solve_lapack()
    {
        throw Exceptions::Generic("Generalised_Eigenvalue_System::"
                "solve_lapack()",
                "Not implemented for the selected data type");
    }

    template <>
    void Generalised_Eigenvalue_System<double>::solve_lapack()
    {
#ifndef LAPACK
        throw Exceptions::Generic("Generalised_Eigenvalue_System::"
                "solve_lapack()",
                "LAPACK support must be enabled by compiling with -DLAPACK");
#else
        char JOBVL = 'N';
        char JOBVR = 'V';
        int N = A_.num_columns();
        double* A = A_.data_pointer();
        int LDA = N;
        double* B = B_.data_pointer();
        int LDB = N;
        double* ALPHAR = ALPHAR_.data_pointer();
        double* ALPHAI = ALPHAI_.data_pointer();
        double* BETA = BETA_.data_pointer();
        double* VL = NULL;
        int LDVL = 1;
        double* VR = VR_.data_pointer();
        int LDVR = N;
        Vector<double> work(8*N, 0.);
        double* WORK = work.data_pointer();
        int LWORK = 8*N;
        int INFO = 0;

#ifdef TIME
        Timer t_solve("Calling LAPACK's dggev");
        t_solve.start();
#endif  // TIME

        LAPACK_DGGEV(&JOBVL, &JOBVR, N, A, LDA, B, LDB, ALPHAR, ALPHAI, BETA,
                VL, LDVL, VR, LDVR, WORK, LWORK, INFO);

#ifdef TIME
        t_solve.stop();
        t_solve.print();
#endif  // TIME

        if (INFO != 0)
        {
            std::stringstream problem;
            problem << "LAPACK_DGGEV returned INFO = " << INFO;
            if (INFO < 0 || INFO > N)
                throw Exceptions::Generic(
                        "Generalised_Eigenvalue_System::solve_lapack()",
                        problem.str());
            else
                console_out(
                        str_convert("Eigenvectors could not be calculated, "
                            "INFO = \%u\n", INFO));
        }

        eigen_stuff_.clear();
        for (int i(0); i < N; ++i)
        {
            Vector<double> eval_temp(eigenvalue_(i));
            Vector<double> evec_temp(eigenvector_(i));
            if (std::abs(eval_temp[1]) > tolerance_)
            {
                Vector<double> evec_temp2(eigenvector_(i + 1));
                eigen_stuff_.push_back(
                        Eigen_Stuff(eval_temp, evec_temp, evec_temp2));
                ++i;
            }
            else
                eigen_stuff_.push_back(Eigen_Stuff(eval_temp, evec_temp));
        }
        std::sort(eigen_stuff_.begin(), eigen_stuff_.end(), Eigen_Stuff_Sort());
#endif  // LAPACK
    }
    
    template <typename type_>
    const Vector<type_> Generalised_Eigenvalue_System<type_>::eigenvalue_(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= A_.num_rows())
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        Vector<type_> eigenvalue(2, 0.);
        eigenvalue[0] = ALPHAR_[index]/BETA_[index];
        eigenvalue[1] = ALPHAI_[index]/BETA_[index];

        return eigenvalue;
    }

    template <typename type_>
    const Vector<type_> Generalised_Eigenvalue_System<type_>::eigenvector_(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= A_.num_rows())
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        Vector<type_> eigenvector(A_.num_rows(), 0.);
        for (std::size_t i(0), size(A_.num_rows()); i < size; ++i)
            eigenvector[i] = VR_(i, index);

        return eigenvector;
    }

    template <typename type_>
    const Vector<type_>& Generalised_Eigenvalue_System<type_>::alpha_r() const
    {
        return ALPHAR_;
    }

    template <typename type_>
    const Vector<type_>& Generalised_Eigenvalue_System<type_>::alpha_i() const
    {
        return ALPHAI_;
    }

    template <typename type_>
    const Vector<type_>& Generalised_Eigenvalue_System<type_>::beta() const
    {
        return BETA_;
    }

    template <typename type_>
    const bool& Generalised_Eigenvalue_System<type_>::is_complex(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= eigen_stuff_.size())
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        return eigen_stuff_[index].complex_valued;
    }

    template <typename type_>
    const Vector<type_>& Generalised_Eigenvalue_System<type_>::eigenvalue(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= eigen_stuff_.size())
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        return eigen_stuff_[index].value;
    }

    template <typename type_>
    const Dense_Matrix<type_>& Generalised_Eigenvalue_System<type_>::vr() const
    {
        return VR_;
    }

    template <typename type_>
    const Vector<type_>& Generalised_Eigenvalue_System<type_>::eigenvector_real(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= eigen_stuff_.size())
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        return eigen_stuff_[index].vector_real;
    }

    template <typename type_>
    const Vector<type_>& Generalised_Eigenvalue_System<type_>::eigenvector_imag(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= eigen_stuff_.size())
            throw Exceptions::Range("Vector", A_.num_rows(), index);
#endif  // PARANOID

        return eigen_stuff_[index].vector_imag;
    }

    template class Generalised_Eigenvalue_System<double>;
}   // namespace

