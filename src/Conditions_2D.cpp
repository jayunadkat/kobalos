#include <complex>

#include <Conditions_2D.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    Conditions_2D<type_, xtype_>::Conditions_2D(Mesh_2D<type_, xtype_>& mesh) :
        mesh_(mesh),
        conditions_x_(mesh_.mesh_x_),
        conditions_y_(),
        reversed_y_data_(false)
    {
    }

    template <typename type_, typename xtype_>
    Conditions_2D<type_, xtype_>::~Conditions_2D()
    {
    }

    template <typename type_, typename xtype_>
    void Conditions_2D<type_, xtype_>::add_x(const xtype_& location,
            Residual<type_, xtype_>& condition)
    {
#ifdef PARANOID
        if (condition.num_vars() != mesh_.num_vars())
            throw Exceptions::Invalid_Argument("Conditions_2D::add_x",
                    "Residual object incompatible with the number"
                    " of variables stored in the mesh object");
#endif  // PARANOID

        conditions_x_.add(location, condition);
    }

    template <typename type_, typename xtype_>
    void Conditions_2D<type_, xtype_>::add_y(Mesh_1D<type_, xtype_>& condition)
    {
#ifdef PARANOID
        if (condition.num_vars() != mesh_.num_vars())
            throw Exceptions::Invalid_Argument("Conditions_2D::add_y",
                    "Mesh_1D condition object incompatible with the number"
                    " of variables stored in the mesh object");
        if (condition.num_nodes() != mesh_.num_nodes_x())
            throw Exceptions::Invalid_Argument("Conditions_2D::add_y",
                    "Mesh_1D condition object incompatible with the number"
                    " of nodes stored in the mesh object");
        for (std::size_t i(0), size(condition.num_nodes()), test(0); i < size;
                ++i)
            if (condition.query(mesh_.node_x(i), test))
                throw Exceptions::Invalid_Argument("Conditions_2D::add_y",
                        "Mesh_1D condition object incompatible with the"
                        " nodal positions stored in the mesh object");
        if (conditions_y_.size() >= 2)
            throw Exceptions::Generic("Conditioning",
                    "Data only permitted for at most two locations in"
                    " parabolic coordinate");
#endif  // PARANOID

        switch (conditions_y_.size())
        {
            case 0:
                conditions_y_.push_back(
                        Condition_Data<Mesh_1D, type_, xtype_>(
                            mesh_.node_y(0), condition));
                break;
            case 1:
                conditions_y_.push_back(
                        Condition_Data<Mesh_1D, type_, xtype_>(
                            mesh_.node_y(mesh_.num_nodes_y() - 1), condition));
                reversed_y_data_ = true;
        }
    }

    template <typename type_, typename xtype_>
    const std::size_t Conditions_2D<type_, xtype_>::num_residuals_x()
    {
        return conditions_x_.num_residuals();
    }

    template <typename type_, typename xtype_>
    const std::size_t Conditions_2D<type_, xtype_>::num_conditions_x()
    {
        return conditions_x_.num_conditions();
    }

    template <typename type_, typename xtype_>
    const std::size_t Conditions_2D<type_, xtype_>::num_conditions_y()
    {
        return conditions_y_.size();
    }

    template <typename type_, typename xtype_>
    const xtype_& Conditions_2D<type_, xtype_>::location_x(
            const std::size_t& x_index) const
    {
#ifdef PARANOID
        if (x_index >= conditions_x_.num_residuals())
            throw Exceptions::Range("Conditions_2D (conditions_x_)",
                    conditions_x_.num_residuals(), x_index);
#endif  // PARANOID

        return conditions_x_.location(x_index);
    }

    template <typename type_, typename xtype_>
    const xtype_& Conditions_2D<type_, xtype_>::location_y(
            const std::size_t& y_index) const
    {
#ifdef PARANOID
        if (y_index >= conditions_y_.size())
            throw Exceptions::Range("Conditions_2D (conditions_y_)",
                    conditions_y_.size(), y_index);
#endif  // PARANOID

        return conditions_y_[y_index].coord_;
    }

    template <typename type_, typename xtype_>
    Residual<type_, xtype_>& Conditions_2D<type_, xtype_>::condition_x(
            const std::size_t& x_index)
    {
#ifdef PARANOID
        if (x_index >= conditions_x_.num_residuals())
            throw Exceptions::Range("Conditions_2D (conditions_x_)",
                    conditions_x_.num_residuals(), x_index);
#endif  // PARANOID

        return conditions_x_.condition(x_index);
    }

    template <typename type_, typename xtype_>
    Mesh_1D<type_, xtype_>& Conditions_2D<type_, xtype_>::condition_y(
            const std::size_t& y_index)
    {
#ifdef PARANOID
        if (y_index >= conditions_y_.size())
            throw Exceptions::Range("Conditions_2D (conditions_y_)",
                    conditions_y_.size(), y_index);
#endif  // PARANOID

        return *(conditions_y_[y_index].condition_);
    }

    template <typename type_, typename xtype_>
    const std::size_t& Conditions_2D<type_, xtype_>::node_x(
            const std::size_t& x_index) const
    {
#ifdef PARANOID
        if (x_index >= conditions_x_.num_residuals())
            throw Exceptions::Range("Conditions_2D (conditions_x_)",
                    conditions_x_.num_residuals(), x_index);
#endif  // PARANOID

        return conditions_x_.node(x_index);
    }

    template <typename type_, typename xtype_>
    const std::size_t Conditions_2D<type_, xtype_>::node_y(
            const std::size_t& y_index) const
    {
#ifdef PARANOID
        if (y_index >= conditions_y_.size())
            throw Exceptions::Range("Conditions_2D (conditions_y_)",
                    conditions_y_.size(), y_index);
#endif  // PARANOID

        if (y_index == 0)
            return 0;
        else
            return mesh_.num_nodes_y() - 1;
    }

    template <typename type_, typename xtype_>
    const bool& Conditions_2D<type_, xtype_>::reversed_y_data() const
    {
        return reversed_y_data_;
    }

    template <typename type_, typename xtype_>
    void Conditions_2D<type_, xtype_>::apply_y_start()
    {
#ifdef PARANOID
        if (conditions_y_.size() == 0)
            throw Exceptions::Range("Conditions_2D (conditions_y_)",
                    conditions_y_.size(), 0);
#endif  // PARANOID

        for (std::size_t i(0), size(mesh_.num_nodes_x()); i < size; ++i)
            for (std::size_t k(0), num_vars(mesh_.num_vars()); k < num_vars;
                    ++k)
                mesh_(i, 0, k) = (*(conditions_y_[0].condition_))(i, k);
    }

    template <typename type_, typename xtype_>
    void Conditions_2D<type_, xtype_>::apply_y_end()
    {
        if (reversed_y_data_)
        {
            std::size_t num_nodes_y(mesh_.num_nodes_y());
            for (std::size_t i(0), size(mesh_.num_nodes_x()); i < size; ++i)
                for (std::size_t k(0), num_vars(mesh_.num_vars()); k < num_vars;
                        ++k)
                    mesh_(i, num_nodes_y - 1, k) =
                        (*(conditions_y_[1].condition_))(i, k);
        }
    }

    template class Conditions_2D<double>;
    template class Conditions_2D<std::complex<double>, double>;
    template class Conditions_2D<std::complex<double>, std::complex<double> >;
}   // namespace

