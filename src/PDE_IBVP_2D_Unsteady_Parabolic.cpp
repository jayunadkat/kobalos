#include <complex>

#include <Console_Output.h>
#include <Exceptions.h>
#include <PDE_IBVP.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    const bool PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::check_sign(
            const type_& value)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use PDE_IBVP::check_sign with type_ != double");
    }

    template <>
    const bool PDE_IBVP<PDE_2D_Unsteady_Parabolic, double,
          std::complex<double> >::check_sign(const double& value)
    {
        return (direction_inverted_) ? value > 0. : value < 0.;
    }

    template <>
    const bool PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double>::check_sign(
            const double& value)
    {
        return (direction_inverted_) ? value > 0. : value < 0.;
    }

    template <typename type_, typename xtype_>
    void PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::assemble_matrix(
            const double& dt)
    {
        std::size_t row(0), condition_index(0),
            num_conditions(conditions_.num_residuals_x());
        std::size_t eqn_order(equation_.order());
        std::size_t num_nodes(mesh_.num_nodes_x());
        std::size_t num_vars(mesh_.num_vars());
        xtype_ inv_x(0.), inv_y(0.), inv_y2(0.), theta(0.);
        double inv_t(1./dt);
        Vector<type_> mid_state(num_vars, 0.), diff_state_t(num_vars, 0.),
            diff_state_x(num_vars, 0.), diff_state_y(num_vars, 0.),
            Q(num_vars, 0.), R(num_vars, 0.), diff_state_y2(num_vars, 0.);

        A_.assign(num_nodes*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_nodes*num_vars, 0.);

        for (std::size_t i(0); i < num_nodes - 1; ++i)
        {
            while (condition_index < num_conditions &&
                    conditions_.node_x(condition_index) == i)
            {
                conditions_.condition_x(condition_index).t() =
                    this->t_ + dt;
                conditions_.condition_x(condition_index).x1() =
                    mesh_.node_x(i);
                conditions_.condition_x(condition_index).x2() =
                    mesh_.node_y(y_ + 1);
                conditions_.condition_x(condition_index).update(
                        mesh_.get_vars_at_node(i, y_ + 1));
                for (std::size_t j(0); j < conditions_.condition_x(
                            condition_index).order(); ++j)
                {
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) =
                            conditions_.condition_x(
                                    condition_index).jacobian()(j, k);
                    }
                    b_[row] = -conditions_.condition_x(
                            condition_index).residual()[j];
                    ++row;
                }
                ++condition_index;
            }

            if (check_reversed_ && first_order_)
            {
                inv_x = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
                inv_y = 1./(mesh_.node_y(y_ + 2) - mesh_.node_y(y_));
                for (std::size_t j(0); j < num_vars; ++j)
                {
                    mid_state[j] = (
                            + mesh_(i, y_ + 1, j)
                            + mesh_(i + 1, y_ + 1, j))/2.;
                    diff_state_t[j] = (
                            + mesh_(i, y_ + 1, j)
                            + mesh_(i + 1, y_ + 1, j) 
                            - prev_sol_(i, y_ + 1, j) 
                            - prev_sol_(i + 1, y_ + 1, j));
                    diff_state_x[j] = (
                            + mesh_(i + 1, y_ + 1, j)
                            + prev_sol_(i + 1, y_ + 1, j)
                            - mesh_(i, y_ + 1, j)
                            - prev_sol_(i, y_ + 1, j));
                    diff_state_y[j] = (
                            + prev_sol_(i, y_ + 2, j) 
                            + prev_sol_(i + 1, y_ + 2, j)
                            - prev_sol_(i, y_, j) 
                            - prev_sol_(i + 1, y_, j));
                }

                equation_.t() = this->t_ + dt/2.;
                equation_.x1() = (mesh_.node_x(i) + mesh_.node_x(i + 1))/2.;
                equation_.x2() = mesh_.node_y(y_ + 1);
                equation_.update(mid_state);
                equation_.update_jacobian_t_vec(diff_state_t*inv_t);
                equation_.update_jacobian_x_vec(diff_state_x*inv_x);
                equation_.update_jacobian_y_vec(diff_state_y*inv_y);
                for (std::size_t j(0); j < eqn_order; ++j)
                {
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) +=
                            + equation_.matrix_t()(j, k)*inv_t
                            + equation_.jacobian_t_vec()(j, k)/2.
                            - equation_.matrix_x()(j, k)*inv_x
                            + equation_.jacobian_x_vec()(j, k)/2.
                            + equation_.jacobian_y_vec()(j, k)/2.
                            - equation_.jacobian()(j, k);
                        A_(row, num_vars*(i + 1) + k) +=
                            + equation_.matrix_t()(j, k)*inv_t
                            + equation_.jacobian_t_vec()(j, k)/2.
                            + equation_.matrix_x()(j, k)*inv_x
                            + equation_.jacobian_x_vec()(j, k)/2.
                            + equation_.jacobian_y_vec()(j, k)/2.
                            - equation_.jacobian()(j, k);
                    }
                    b_[row] = 2.*equation_.residual()[j];
                    for (std::size_t k(0); k < eqn_order; ++k)
                        b_[row] -=
                            + equation_.matrix_t()(j, k)*
                            diff_state_t[k]*inv_t
                            + equation_.matrix_x()(j, k)*
                            diff_state_x[k]*inv_x
                            + equation_.matrix_y()(j, k)*
                            diff_state_y[k]*inv_y;
                    ++row;
                }
            }
            else if (check_reversed_ && check_sign(
                        prev_sol_(i, y_ + 2, streamwise_component_)))
            {
                inv_x = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
                inv_y = 1./(mesh_.node_y(y_ + 1) - mesh_.node_y(y_));
                inv_y2 = 1./(mesh_.node_y(y_ + 2) - mesh_.node_y(y_ + 1));
                theta = (mesh_.node_y(y_ + 2) - mesh_.node_y(y_ + 1))/
                    (mesh_.node_y(y_ + 2) - mesh_.node_y(y_));
                for (std::size_t j(0); j < num_vars; ++j)
                {
                    mid_state[j] = (
                            + mesh_(i, y_ + 1, j)
                            + mesh_(i + 1, y_ + 1, j)
                            + prev_sol_(i, y_ + 1, j) 
                            + prev_sol_(i + 1, y_ + 1, j))/4.;
                    Q[j] = (
                            + mesh_(i, y_, j)
                            + mesh_(i + 1, y_, j)
                            + mesh_(i, y_ + 1, j)
                            + mesh_(i + 1, y_ + 1, j))/4.;
                    R[j] = (
                            + prev_sol_(i, y_ + 1, j)
                            + prev_sol_(i + 1, y_ + 1, j)
                            + prev_sol_(i, y_ + 2, j)
                            + prev_sol_(i + 1, y_ + 2, j))/4.;
                    diff_state_t[j] = (
                            + mesh_(i, y_ + 1, j)
                            + mesh_(i + 1, y_ + 1, j) 
                            - prev_sol_(i, y_ + 1, j) 
                            - prev_sol_(i + 1, y_ + 1, j));
                    diff_state_x[j] = (
                            + mesh_(i + 1, y_ + 1, j)
                            + prev_sol_(i + 1, y_ + 1, j)
                            - mesh_(i, y_ + 1, j)
                            - prev_sol_(i, y_ + 1, j));
                    diff_state_y[j] = (
                            + mesh_(i, y_ + 1, j) 
                            + mesh_(i + 1, y_ + 1, j)
                            - mesh_(i, y_, j) 
                            - mesh_(i + 1, y_, j));
                    diff_state_y2[j] = (
                            + prev_sol_(i, y_ + 2, j)
                            + prev_sol_(i + 1, y_ + 2, j)
                            - prev_sol_(i, y_ + 1, j)
                            - prev_sol_(i + 1, y_ + 1, j));
                }

                equation_.t() = this->t_ + dt/2.;
                equation_.x1() = (mesh_.node_x(i) + mesh_.node_x(i + 1))/2.;
                equation_.x2() = mesh_.node_y(y_ + 1);
                equation_.update(mid_state);
                equation_.update_jacobian_t_vec(diff_state_t*inv_t);
                equation_.update_jacobian_x_vec(diff_state_x*inv_x);
                equation_.update_jacobian_y_vec(diff_state_y*inv_y);
                for (std::size_t j(0); j < eqn_order; ++j)
                {
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) +=
                            + equation_.matrix_t()(j, k)*inv_t
                            + equation_.jacobian_t_vec()(j, k)/4.
                            - equation_.matrix_x()(j, k)*inv_x
                            + equation_.jacobian_x_vec()(j, k)/4.
                            + theta*equation_.matrix_y()(j, k)*inv_y
                            + theta*equation_.jacobian_y_vec()(j, k)/4.
                            - equation_.jacobian()(j, k)/2.;
                        A_(row, num_vars*(i + 1) + k) +=
                            + equation_.matrix_t()(j, k)*inv_t
                            + equation_.jacobian_t_vec()(j, k)/4.
                            + equation_.matrix_x()(j, k)*inv_x
                            + equation_.jacobian_x_vec()(j, k)/4.
                            + theta*equation_.matrix_y()(j, k)*inv_y
                            + theta*equation_.jacobian_y_vec()(j, k)/4.
                            - equation_.jacobian()(j, k)/2.;
                    }
                    b_[row] = 2.*equation_.residual()[j];
                    for (std::size_t k(0); k < eqn_order; ++k)
                        b_[row] -=
                            + equation_.matrix_t()(j, k)*diff_state_t[k]*inv_t
                            + equation_.matrix_x()(j, k)*diff_state_x[k]*inv_x
                            + theta*equation_.matrix_y()(j, k)*
                            diff_state_y[k]*inv_y
                            + (1. - theta)*equation_.matrix_y()(j, k)*
                            diff_state_y2[k]*inv_y2;
                    ++row;
                }

                row -= eqn_order;
                equation_.update_jacobian_y_vec(diff_state_y2*inv_y2);
                for (std::size_t j(0); j < eqn_order; ++j)
                {
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) +=
                            + (1. - theta)*equation_.jacobian_y_vec()(j, k)/4.;
                        A_(row, num_vars*(i + 1) + k) +=
                            + (1. - theta)*equation_.jacobian_y_vec()(j, k)/4.;
                    }
                    ++row;
                }
            }
            else
            {
                inv_x = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
                inv_y = 1./(mesh_.node_y(y_ + 1) - mesh_.node_y(y_));
                for (std::size_t j(0); j < num_vars; ++j)
                {
                    mid_state[j] = (
                            + mesh_(i, y_, j)
                            + mesh_(i + 1, y_, j)
                            + mesh_(i, y_ + 1, j)
                            + mesh_(i + 1, y_ + 1, j) 
                            + prev_sol_(i, y_, j) 
                            + prev_sol_(i + 1, y_, j) 
                            + prev_sol_(i, y_ + 1, j) 
                            + prev_sol_(i + 1, y_ + 1, j))/8.;
                    diff_state_t[j] = (
                            + mesh_(i, y_, j) 
                            + mesh_(i + 1, y_, j) 
                            + mesh_(i, y_ + 1, j)
                            + mesh_(i + 1, y_ + 1, j) 
                            - prev_sol_(i, y_, j) 
                            - prev_sol_(i + 1, y_, j) 
                            - prev_sol_(i, y_ + 1, j) 
                            - prev_sol_(i + 1, y_ + 1, j));
                    diff_state_x[j] = (
                            + mesh_(i + 1, y_, j) 
                            + mesh_(i + 1, y_ + 1, j)
                            + prev_sol_(i + 1, y_, j) 
                            + prev_sol_(i + 1, y_ + 1, j)
                            - mesh_(i, y_, j) 
                            - mesh_(i, y_ + 1, j)
                            - prev_sol_(i, y_, j) 
                            - prev_sol_(i, y_ + 1, j));
                    diff_state_y[j] = (
                            + mesh_(i, y_ + 1, j) 
                            + mesh_(i + 1, y_ + 1, j)
                            + prev_sol_(i, y_ + 1, j) 
                            + prev_sol_(i + 1, y_ + 1, j)
                            - mesh_(i, y_, j) 
                            - mesh_(i + 1, y_, j)
                            - prev_sol_(i, y_, j) 
                            - prev_sol_(i + 1, y_, j));
                }

                equation_.t() = this->t_ + dt/2.;
                equation_.x1() = (mesh_.node_x(i) + mesh_.node_x(i + 1))/2.;
                equation_.x2() = (mesh_.node_y(y_) +
                        mesh_.node_y(y_ + 1))/2.;
                equation_.update(mid_state);
                equation_.update_jacobian_t_vec(diff_state_t*inv_t);
                equation_.update_jacobian_x_vec(diff_state_x*inv_x);
                equation_.update_jacobian_y_vec(diff_state_y*inv_y);
                for (std::size_t j(0); j < eqn_order; ++j)
                {
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) +=
                            + equation_.matrix_t()(j, k)*inv_t
                            + equation_.jacobian_t_vec()(j, k)/8.
                            - equation_.matrix_x()(j, k)*inv_x
                            + equation_.jacobian_x_vec()(j, k)/8.
                            + equation_.matrix_y()(j, k)*inv_y
                            + equation_.jacobian_y_vec()(j, k)/8.
                            - equation_.jacobian()(j, k)/2.;
                        A_(row, num_vars*(i + 1) + k) +=
                            + equation_.matrix_t()(j, k)*inv_t
                            + equation_.jacobian_t_vec()(j, k)/8.
                            + equation_.matrix_x()(j, k)*inv_x
                            + equation_.jacobian_x_vec()(j, k)/8.
                            + equation_.matrix_y()(j, k)*inv_y
                            + equation_.jacobian_y_vec()(j, k)/8.
                            - equation_.jacobian()(j, k)/2.;
                    }
                    b_[row] = 4.*equation_.residual()[j];
                    for (std::size_t k(0); k < eqn_order; ++k)
                        b_[row] -=
                            + equation_.matrix_t()(j, k)*diff_state_t[k]*inv_t
                            + equation_.matrix_x()(j, k)*diff_state_x[k]*inv_x
                            + equation_.matrix_y()(j, k)*diff_state_y[k]*inv_y;
                    ++row;
                }
            }
        }

        while (condition_index < num_conditions &&
                conditions_.node_x(condition_index) == num_nodes - 1)
        {
            conditions_.condition_x(condition_index).t() = this->t_ + dt;
            conditions_.condition_x(condition_index).x1() =
                mesh_.node_x(num_nodes - 1);
            conditions_.condition_x(condition_index).x2() =
                mesh_.node_y(y_ + 1);
            conditions_.condition_x(condition_index).update(
                    mesh_.get_vars_at_node(num_nodes - 1, y_ + 1));
            for (std::size_t j(0);
                    j < conditions_.condition_x(condition_index).order(); ++j)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    A_(row, num_vars*(num_nodes - 1) + k) =
                        conditions_.condition_x(
                                condition_index).jacobian()(j, k);
                }
                b_[row] = -conditions_.condition_x(
                        condition_index).residual()[j];
                ++row;
            }
            ++condition_index;
        }

#ifdef PARANOID
        if (row != num_nodes*num_vars)
            throw Exceptions::Generic("PDE_IBVP::assemble_matrix",
                    "row != num_nodes*num_vars");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::PDE_IBVP(
            Equation_2D<type_, xtype_>& equation,
            Conditions_2D<type_, xtype_>& conditions,
            Mesh_2D<type_, xtype_>& mesh) :
        PDE_IBVP<PDE_Base, type_, xtype_>(),
        equation_(equation),
        conditions_(conditions),
        A_(mesh.num_nodes_x()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes_x()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        prev_sol_(mesh),
        mesh_(mesh),
        y_(0),
        streamwise_component_(0),
        check_reversed_(conditions.reversed_y_data()),
        streamwise_specified_(false),
        direction_inverted_(false),
        first_order_(false)
    {
#ifdef PARANOID
        if (equation.order() != conditions.num_conditions_x())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Residual objects form an over/underconstrained system");
        if (equation.num_vars() != conditions.condition_x(0).num_vars())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Equation and conditions objects have differing degrees"
                    " of freedom");
        if (equation.num_vars() != mesh.num_vars())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Mesh object has too few degrees of freedom for equation");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::~PDE_IBVP()
    {
    }

    template <typename type_, typename xtype_>
    void PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::
        set_streamwise_component(const std::size_t& streamwise_index)
    {
#ifdef PARANOID
        if (streamwise_index >= equation_.order())
            throw Exceptions::Invalid_Argument("PDE_IBVP::"
                    "streamwise_component", "Index supplied for streamwise"
                    " component is out of bounds");
#endif  // PARANOID

        streamwise_component_ = streamwise_index;
        streamwise_specified_ = true;
    }

    template <typename type_, typename xtype_>
    bool& PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::
        direction_inverted()
    {
        return direction_inverted_;
    }

    template <typename type_, typename xtype_>
    const bool& PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::
        direction_inverted() const
    {
        return direction_inverted_;
    }

    template <typename type_, typename xtype_>
    bool& PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::first_order()
    {
        return first_order_;
    }

    template <typename type_, typename xtype_>
    void PDE_IBVP<PDE_2D_Unsteady_Parabolic, type_, xtype_>::step(
            const double& dt)
    {
#ifdef PARANOID
        if (dt == 0)
            throw Exceptions::Invalid_Argument("PDE_IBVP::step",
                    "dt != 0 is required");
#endif  // PARANOID

        if (check_reversed_ && !streamwise_specified_)
            throw Exceptions::Generic("Solution", "Data is specified at the"
                    " end of the y domain but no streamwise component has"
                    " been indicated");

        std::size_t num_nodes_x(mesh_.num_nodes_x());
        std::size_t num_nodes_y(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        prev_sol_.set_vars_vector(mesh_.get_vars_vector());
        conditions_.apply_y_start();
        if (check_reversed_)
        {
            conditions_.apply_y_end();
            --num_nodes_y;
        }

#ifndef QUIET
        console_out("Solving 2D problem\n");
        console_out(" Time " + str_convert(this->t_ + dt) + "\n");
#endif  // QUIET

        for (y_ = 0; y_ < num_nodes_y - 1; ++y_)
        {
            std::size_t counter(0);
            double max_residual(0.);
            do
            {
                assemble_matrix(dt);
                max_residual = b_.inf_norm();

#if defined DEBUG && not defined QUIET
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: "
                    + str_convert(max_residual) + "\n");
#endif  // DEBUG && !QUIET

                if (max_residual < this->tolerance_)
                    break;
                linear_system_.solve();

                for (std::size_t i(0); i < num_nodes_x; ++i)
                    for (std::size_t j(0); j < num_vars; ++j)
                        mesh_(i, y_ + 1, j) += b_[num_vars*i + j];
                ++counter;
            }
            while (max_residual > this->tolerance_ &&
                    counter < this->max_iterations_);
            
            if (counter >= this->max_iterations_)
                throw Exceptions::Generic("Solution",
                        "Solution algorithm failed to converge");
        }

        this->t_ += dt;
    }

    template class PDE_IBVP<PDE_2D_Unsteady_Parabolic, double, double>;
    template class PDE_IBVP<PDE_2D_Unsteady_Parabolic, std::complex<double>,
             double>;
    template class PDE_IBVP<PDE_2D_Unsteady_Parabolic, std::complex<double>,
             std::complex<double> >;
}   // namespace
