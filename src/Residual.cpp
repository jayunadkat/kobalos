#include <complex>

#include <Exceptions.h>
#include <Residual.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    Residual<type_, xtype_>::Residual(const std::size_t& order) :
        jacobian_(order, 0.),
        state_(order, 0.),
        residual_(order, 0.),
        coords_(3, 0.),
        t_(0.),
        order_(order),
        num_vars_(order)
    {
    }

    template <typename type_, typename xtype_>
    Residual<type_, xtype_>::Residual(const std::size_t& order,
            const std::size_t& num_vars) :
        jacobian_(order, num_vars, 0.),
        state_(num_vars, 0.),
        residual_(order, 0.),
        coords_(3, 0.),
        t_(0.),
        order_(order),
        num_vars_(num_vars)
    {
    }

    template <typename type_, typename xtype_>
    Residual<type_, xtype_>::~Residual()
    {
    }

    template <typename type_, typename xtype_>
    void Residual<type_, xtype_>::jacobian(const Vector<type_>& state)
    {
        Vector<type_> temp(state_), residual_old(residual_);
        for (std::size_t i = 0; i < num_vars_; ++i)
        {
            temp[i] += delta_;
            residual_function(temp);
            temp[i] -= delta_;
            jacobian_.set_column(i, (residual_ - residual_old)/delta_);
        }
        residual_ = residual_old;
    }

    template class Residual<double>;
    template class Residual<std::complex<double> >;
    template class Residual<std::complex<double>, std::complex<double> >;
}   // namespace

