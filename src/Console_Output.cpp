#include <cstring>

#include <Console_Output.h>

namespace Kobalos
{
    String_Converter::String_Converter() : ss_()
    {
    }

    String_Converter::~String_Converter()
    {
    }

    NCurses_Wrapper::NCurses_Window::NCurses_Window() : win_(NULL)
    {
    }

    NCurses_Wrapper::NCurses_Window::~NCurses_Window()
    {
        if (win_)
            delwin(win_);
    }

    void NCurses_Wrapper::NCurses_Window::resize(int height, int width,
            int start_y)
    {
        if (win_)
            delwin(win_);
        win_ = newwin(height, width, start_y, 0);
    }

    void NCurses_Wrapper::NCurses_Window::set_border(char left, char right,
            char top, char bottom)
    {
        if (win_)
            wborder(win_, left, right, top, bottom,
                    top, top, bottom, bottom);
    }

    void NCurses_Wrapper::NCurses_Window::set_title_format()
    {
        if (win_)
            wattron(win_, A_BOLD | A_UNDERLINE);
    }

    void NCurses_Wrapper::NCurses_Window::unset_title_format()
    {
        if (win_)
            wattroff(win_, A_BOLD | A_UNDERLINE);
    }

    void NCurses_Wrapper::NCurses_Window::print(const char* output_string)
    {
        if (win_)
            wprintw(win_, output_string);
    }

    void NCurses_Wrapper::NCurses_Window::mvprint(int y, int x,
            const char* output_string)
    {
        if (win_)
            mvwprintw(win_, y, x, output_string);
    }

    void NCurses_Wrapper::NCurses_Window::refresh_window()
    {
        if (win_)
            wrefresh(win_);
    }

    void NCurses_Wrapper::NCurses_Window::dispose()
    {
        if (win_)
            delwin(win_);
        wrefresh(stdscr);
    }

    NCurses_Wrapper::NCurses_Wrapper() : header_(), body_()
    {
    }

    NCurses_Wrapper::~NCurses_Wrapper()
    {
        clean_up();
    }

    void NCurses_Wrapper::init()
    {
        initscr();
        cbreak();
        noecho();
        refresh_all();
    }

    void NCurses_Wrapper::clean_up()
    {
        header_.dispose();
        body_.dispose();
        endwin();
    }

    void NCurses_Wrapper::render_header(
            const char* title_text, const char* header_text)
    {
        int size_y, size_x, header_height = 0, counter = 0;
        getmaxyx(stdscr, size_y, size_x);

        header_height += std::strlen(title_text)/size_x + 1;
        for (int i(0), size(std::strlen(header_text)); i < size; ++i)
        {
            if (header_text[i] == '\n')
            {
                ++header_height;
                counter = 0;
            }
            else
                ++counter;
            if (counter > size_x)
            {
                ++header_height;
                counter = 0;
            }
        }
        if (header_text[std::strlen(header_text) - 1] != '\n')
            ++header_height;
        ++header_height;

        header_.resize(header_height, size_x, 0);
        header_.set_border(' ', ' ', ' ', '=');
        body_.resize(size_y - header_height - 1, size_x, header_height);
        body_.set_border(' ', ' ', ' ', ' ');
        
        header_.set_title_format();
        header_.print(title_text);
        header_.print("\n");
        header_.unset_title_format();
        header_.print(header_text);

        refresh_all();
    }

    void NCurses_Wrapper::print_body_line(const char* body_text)
    {
        body_.print(body_text);
        body_.refresh_window();
    }

    void NCurses_Wrapper::refresh_all()
    {
        wrefresh(stdscr);
        header_.refresh_window();
        body_.refresh_window();
    }

    Console_Output::Console_Output() :
        use_ncurses_wrapper_(false),
        staging_header_(false),
        title_text_("Kobalos"),
        header_text_(""),
        ncurses_wrapper_()
    {
    }

    Console_Output::~Console_Output()
    {
        if (use_ncurses_wrapper_)
            ncurses_wrapper_.clean_up();
    }

    void Console_Output::operator()(const char* output_string)
    {
        if (use_ncurses_wrapper_)
            ncurses_wrapper_.print_body_line(output_string);
        else
            std::cout << output_string;
    }

    void Console_Output::operator()(const std::string& output_string)
    {
        if (use_ncurses_wrapper_)
            ncurses_wrapper_.print_body_line(output_string.c_str());
        else
            std::cout << output_string;
    }

    void Console_Output::stage_header_line(const char* output_string)
    {
        if (use_ncurses_wrapper_)
            header_text_ += str_convert(output_string);
        else
            std::cout << output_string;
    }

    void Console_Output::stage_header_line(const std::string& output_string)
    {
        if (use_ncurses_wrapper_)
            header_text_ += output_string;
        else
            std::cout << output_string;
    }

    void Console_Output::set_title(const std::string& title_string)
    {
        title_text_ = title_string;
    }

    void Console_Output::begin_staging_header()
    {
        if (use_ncurses_wrapper_)
        {
            staging_header_ = true;
            header_text_ = "";
        }
    }

    void Console_Output::end_staging_header()
    {
        if (use_ncurses_wrapper_)
        {
            staging_header_ = false;
            ncurses_wrapper_.render_header(
                    title_text_.c_str(), header_text_.c_str());
        }
    }

    void Console_Output::use_ncurses()
    {
        if (!use_ncurses_wrapper_)
        {
            use_ncurses_wrapper_ = true;
            ncurses_wrapper_.init();
        }
    }

    void Console_Output::stop_ncurses()
    {
        if (use_ncurses_wrapper_)
        {
            use_ncurses_wrapper_ = false;
            ncurses_wrapper_.clean_up();
        }
    }

    void Console_Output::endl()
    {
        operator()("\n");
    }

    String_Converter str_convert;
    Console_Output console_out;
}   // namespace

