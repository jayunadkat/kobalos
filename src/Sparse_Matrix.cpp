#include <cmath>
#include <complex>

#include <Sparse_Matrix.h>

namespace Kobalos
{
    Sparse_Data_Compare::Sparse_Data_Compare(const std::size_t& num_cols) :
        num_cols_(num_cols)
    {
    }

    const bool Sparse_Data_Compare::operator()(
            const std::pair<std::size_t, std::size_t>& lhs,
            const std::pair<std::size_t, std::size_t>& rhs) const
    {
        return lhs.first*num_cols_ + lhs.second
            < rhs.first*num_cols_ + rhs.second;
    }

    template <typename type_>
    Sparse_Matrix<type_>::Sparse_Matrix(const std::size_t& n) :
        matrix_(Sparse_Data_Compare(n)),
        num_rows_(n),
        num_cols_(n)
    {
    }

    template <typename type_>
    Sparse_Matrix<type_>::Sparse_Matrix(const std::size_t& n,
            const std::size_t& m) :
        matrix_(Sparse_Data_Compare(m)),
        num_rows_(n),
        num_cols_(m)
    {
    }

    template <typename type_>
    Sparse_Matrix<type_>::~Sparse_Matrix()
    {
    }

    template <typename type_>
    type_& Sparse_Matrix<type_>::operator()(const std::size_t& row,
            const std::size_t& column)
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
            throw Exceptions::Range("Sparse_Matrix",
                    num_rows_, row, num_cols_, column);
#endif  // PARANOID

        matrix_iterator m_it(matrix_.begin());
        return matrix_.insert(m_it, element_data(
                    size_t_pair(row, column), 0.))->second;
    }

    template <typename type_>
    const type_ Sparse_Matrix<type_>::operator()(const std::size_t& row,
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
            throw Exceptions::Range("Sparse_Matrix",
                    num_rows_, row, num_cols_, column);
#endif  // PARANOID

        matrix_const_iterator m_cit(matrix_.find(size_t_pair(row, column)));
        if (m_cit != matrix_.end())
            return m_cit->second;
        else
            return 0.;
    }

    template <typename type_>
    type_& Sparse_Matrix<type_>::set(const std::size_t& row,
            const std::size_t& column)
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
            throw Exceptions::Range("Sparse_Matrix",
                    num_rows_, row, num_cols_, column);
#endif  // PARANOID

        matrix_iterator m_it(matrix_.begin());
        return matrix_.insert(m_it, element_data(
                    size_t_pair(row, column), 0.))->second;
    }

    template <typename type_>
    const type_ Sparse_Matrix<type_>::get(const std::size_t& row,
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (row >= num_rows_ || column >= num_cols_)
            throw Exceptions::Range("Sparse_Matrix",
                    num_rows_, row, num_cols_, column);
#endif  // PARANOID

        matrix_const_iterator m_cit(matrix_.find(size_t_pair(row, column)));
        if (m_cit != matrix_.end())
            return m_cit->second;
        else
            return 0.;
    }

    template <typename type_>
    const std::size_t& Sparse_Matrix<type_>::num_rows() const
    {
        return num_rows_;
    }

    template <typename type_>
    const std::size_t& Sparse_Matrix<type_>::num_columns() const
    {
        return num_cols_;
    }

    template <typename type_>
    const std::size_t Sparse_Matrix<type_>::num_elements() const
    {
        return matrix_.size();
    }

    template <typename type_>
    const Vector<type_> Sparse_Matrix<type_>::operator*(
            const Vector<type_>& x) const
    {
        if (num_cols_ != x.size())
        {
            throw Exceptions::Mismatch("Sparse_Matrix::operator*",
                    "Sparse_Matrix", "Vector");
        }

        Vector<type_> temp(num_rows_);
        for (std::size_t i = 0; i < num_rows_; ++i)
            for (std::size_t j = 0; j < num_cols_; ++j)
                temp[i] += get(i, j)*x[j];
        return temp;
    }

    template <typename type_>
    void Sparse_Matrix<type_>::assign(const std::size_t& n)
    {
        num_rows_ = num_cols_ = n;
        matrix_data temp_matrix = matrix_data(Sparse_Data_Compare(num_rows_));
        matrix_ = temp_matrix;
    }

    template <typename type_>
    void Sparse_Matrix<type_>::transpose()
    {
        std::size_t temp_num_rows(num_rows_);
        matrix_data temp_matrix = matrix_data(Sparse_Data_Compare(num_rows_));
        for (matrix_iterator m_it(matrix_.begin()), m_end(matrix_.end());
                m_it != m_end; ++m_it)
            temp_matrix.insert(element_data(size_t_pair(m_it->first.second,
                            m_it->first.first), m_it->second));

        num_rows_ = num_cols_;
        num_cols_ = temp_num_rows;
        matrix_ = temp_matrix;
    }

    template <typename type_>
    void Sparse_Matrix<type_>::swap_rows(const std::size_t& row1,
            const std::size_t& row2)
    {
#ifdef PARANOID
        if (row1 >= num_rows_)
            throw Exceptions::Range("Sparse_Matrix, row access", num_rows_,
                    row1);
        if (row2 >= num_rows_)
            throw Exceptions::Range("Sparse_Matrix, row access", num_rows_,
                    row2);
#endif  // PARANOID

        matrix_iterator m_end(matrix_.end()), m_it;
        for (std::size_t i(0); i < num_cols_; ++i)
        {
            m_it = matrix_.find(size_t_pair(row1, i));
            if (m_it != m_end)
            {
                matrix_.insert(element_data(size_t_pair(num_rows_, i),
                            m_it->second));
                matrix_.erase(m_it);
            }
        }
        for (std::size_t i(0); i < num_cols_; ++i)
        {
            m_it = matrix_.find(size_t_pair(row2, i));
            if (m_it != m_end)
            {
                matrix_.insert(element_data(size_t_pair(row1, i),
                            m_it->second));
                matrix_.erase(m_it);
            }
        }
        for (std::size_t i(0); i < num_cols_; ++i)
        {
            m_it = matrix_.find(size_t_pair(num_rows_, i));
            if (m_it != m_end)
            {
                matrix_.insert(element_data(size_t_pair(row2, i),
                            m_it->second));
                matrix_.erase(m_it);
            }
        }
    }

    template <typename type_>
    const std::size_t Sparse_Matrix<type_>::column_max_index(
            const std::size_t& column) const
    {
#ifdef PARANOID
        if (column >= num_cols_)
            throw Exceptions::Range("Sparse_Matrix, column access",
                    num_cols_, column);
#endif  // PARANOID

        std::size_t max_index(column);
        double max(0.), temp(0.);
        for (std::size_t i(column); i < num_rows_; ++i)
        {
            temp = std::abs(get(i, column));
            if (max < temp)
            {
                max = temp;
                max_index = i;
            }
        }
        return max_index;
    }

    template <typename type_>
    const double Sparse_Matrix<type_>::one_norm() const
    {
        double temp(0.);
        for (matrix_const_iterator m_cit(matrix_.begin()),
                m_end(matrix_.end()); m_cit != m_end; ++m_cit)
            temp += std::abs(m_cit->second);
        return temp;
    }

    template <typename type_>
    const double Sparse_Matrix<type_>::two_norm() const
    {
        double temp(0.);
        for (matrix_const_iterator m_cit(matrix_.begin()),
                m_end(matrix_.end()); m_cit != m_end; ++m_cit)
            temp += std::pow(std::abs(m_cit->second), 2.);
        return std::sqrt(temp);
    }

    template <typename type_>
    const double Sparse_Matrix<type_>::inf_norm() const
    {
        double temp(0.);
        for (matrix_const_iterator m_cit(matrix_.begin()),
                m_end(matrix_.end()); m_cit != m_end; ++m_cit)
            if (temp < std::abs(m_cit->second))
                temp = std::abs(m_cit->second);
        return temp;
    }

    template <typename type_>
    void Sparse_Matrix<type_>::dump(std::ostream& out) const
    {
        for (std::size_t i = 0; i < num_rows_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
                out << get(i, j) << " ";
            out << '\n';
        }

        out << "\n Number of elements: " << num_elements() << '\n';
    }

    template <typename type_>
    void Sparse_Matrix<type_>::dump(const std::size_t& decimal_places,
            std::ostream& out) const
    {
        std::size_t width = decimal_places + 8;
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision((int)width - 8);
        for (std::size_t i = 0; i < num_rows_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
            {
                out << std::setw(width);
                out << get(i, j) << " ";
            }
            out << '\n';
        }

        out << "\n Number of elements: " << num_elements() << '\n';
    }

    template <typename type_>
    void Sparse_Matrix<type_>::dump_sparse(std::ostream& out) const
    {
        for (matrix_const_iterator m_cit(matrix_.begin()),
                m_end(matrix_.end()); m_cit != m_end; ++m_cit)
        {
            out << "(" << m_cit->first.first << ", ";
            out << m_cit->first.second << "): ";
            out << m_cit->second << '\n';
        }

        out << "\n Number of elements: " << num_elements() << '\n';
    }

    template <typename type_>
    void Sparse_Matrix<type_>::get_superlu_data(
            Vector<type_>& a, int* asub, int* xa) const
    {
        std::size_t index(0), row(0);
        matrix_const_iterator m_cit(matrix_.begin()), m_end(matrix_.end());
        xa[0] = 0;
        while (m_cit != m_end)
        {
            a[index] = m_cit->second;
            asub[index] = m_cit->first.second;
            if (m_cit->first.first > row + 1)
                throw Exceptions::Generic("SuperLU",
                        "get_superlu_data generated a singular matrix");
            if (m_cit->first.first == row + 1)
            {
                xa[row + 1] = index;
                ++row;
            }
            ++m_cit;
            ++index;
        }
        xa[row + 1] = index;
    }

    template class Sparse_Matrix<double>;
    template class Sparse_Matrix<std::complex<double> >;
}   // namespace

