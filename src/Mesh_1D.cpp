#include <cmath>
#include <complex>
#include <fstream>

#include <Exceptions.h>
#include <Mesh_1D.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    const double Mesh_1D<type_, xtype_>::epsilon_ = 1e-4;

    template <typename type_, typename xtype_>
    Mesh_1D<type_, xtype_>::Mesh_1D() :
        nodes_(),
        vars_(),
        num_nodes_(0),
        num_vars_(0),
        grid_map_(NULL),
        inv_grid_map_(NULL),
        map_ds_(NULL),
        map_ds2_(NULL)
    {
    }

    template <typename type_, typename xtype_>
    Mesh_1D<type_, xtype_>::Mesh_1D(const Vector<xtype_>& nodes,
            const std::size_t& num_vars) :
        nodes_(nodes),
        vars_(nodes.size()*num_vars, 0.),
        num_nodes_(nodes.size()),
        num_vars_(num_vars),
        grid_map_(NULL),
        inv_grid_map_(NULL),
        map_ds_(NULL),
        map_ds2_(NULL)
    {
    }

    template <typename type_, typename xtype_>
    Mesh_1D<type_, xtype_>::Mesh_1D(const std::size_t& num_nodes,
            const std::size_t& num_vars) :
        nodes_(num_nodes, 0.),
        vars_(num_nodes*num_vars, 0.),
        num_nodes_(num_nodes),
        num_vars_(num_vars),
        grid_map_(NULL),
        inv_grid_map_(NULL),
        map_ds_(NULL),
        map_ds2_(NULL)
    {
    }

    template <typename type_, typename xtype_>
    Mesh_1D<type_, xtype_>::Mesh_1D(const Mesh_1D<type_, xtype_>& source) :
        nodes_(source.nodes_),
        vars_(source.vars_),
        num_nodes_(source.num_nodes_),
        num_vars_(source.num_vars_),
        grid_map_(source.grid_map_),
        inv_grid_map_(source.inv_grid_map_),
        map_ds_(source.map_ds_),
        map_ds2_(source.map_ds2_)
    {
    }

    template <typename type_, typename xtype_>
    Mesh_1D<type_, xtype_>::~Mesh_1D()
    {
        grid_map_ = NULL;
        inv_grid_map_ = NULL;
        map_ds_ = NULL;
        map_ds2_ = NULL;
    }

    template <typename type_, typename xtype_>
    const Vector<xtype_>& Mesh_1D<type_, xtype_>::nodes() const
    {
        return nodes_;
    }
    
    template <typename type_, typename xtype_>
    const xtype_& Mesh_1D<type_, xtype_>::node(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (nodes_)", num_nodes_, node_index);
#endif  // PARANOID

        return nodes_[node_index];
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_1D<type_, xtype_>::unmapped_node(
            const std::size_t& node_index) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::unmapped_node with xtype_ != double");
    }

    template <>
    const double Mesh_1D<double>::unmapped_node(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (nodes_)", num_nodes_, node_index);
#endif  // PARANOID

        if (grid_map_)
            return grid_map_(nodes_[node_index]);
        else
            return nodes_[node_index];
    }

    template <>
    const double Mesh_1D<std::complex<double>, double>::unmapped_node(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (nodes_)", num_nodes_, node_index);
#endif  // PARANOID

        if (grid_map_)
            return grid_map_(nodes_[node_index]);
        else
            return nodes_[node_index];
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_1D<type_, xtype_>::map(const double& s) const
    {
        if (grid_map_)
            return grid_map_(s);
        else
            return s;
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_1D<type_, xtype_>::inv_map(const double& x) const
    {
        if (inv_grid_map_)
            return inv_grid_map_(x);
        else
            return x;
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_1D<type_, xtype_>::map_ds(const xtype_& s) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::map_ds with xtype_ != double");
    }

    template <>
    const double Mesh_1D<double>::map_ds(const double& s) const
    {
        if (map_ds_)
            return map_ds_(s);
        else
        {
            if (grid_map_)
                return (grid_map_(s + epsilon_) - grid_map_(s - epsilon_))
                    /(2.*epsilon_);
            else
                return 1.;
        }
    }

    template <>
    const double Mesh_1D<std::complex<double>, double>::map_ds(
            const double& s) const
    {
        if (map_ds_)
            return map_ds_(s);
        else
        {
            if (grid_map_)
                return (grid_map_(s + epsilon_) - grid_map_(s - epsilon_))
                    /(2.*epsilon_);
            else
                return 1.;
        }
    }

    template <typename type_, typename xtype_>
    const xtype_ Mesh_1D<type_, xtype_>::map_ds2(const xtype_& s) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::map_ds2 with xtype_ != double");
    }

    template <>
    const double Mesh_1D<double>::map_ds2(const double& s) const
    {
        if (map_ds2_)
            return map_ds2_(s);
        else
        {
            if (grid_map_)
                return (grid_map_(s + epsilon_) - 2.*grid_map_(s)
                        + grid_map_(s - epsilon_))/(epsilon_*epsilon_);
            else
                return 0.;
        }
    }

    template <>
    const double Mesh_1D<std::complex<double>, double>::map_ds2(
            const double& s) const
    {
        if (map_ds2_)
            return map_ds2_(s);
        else
        {
            if (grid_map_)
                return (grid_map_(s + epsilon_) - 2.*grid_map_(s)
                        + grid_map_(s - epsilon_))/(epsilon_*epsilon_);
            else
                return 0.;
        }
    }

    template <typename type_, typename xtype_>
    type_& Mesh_1D<type_, xtype_>::operator()(const std::size_t& node_index,
            const std::size_t& var_index)
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (nodes_)", num_nodes_, node_index);
        if (var_index >= num_vars_)
            throw Exceptions::Range("Mesh_1D (vars_)", num_vars_, var_index);
#endif  // PARANOID

        return vars_[num_vars_*node_index + var_index];
    }

    template <typename type_, typename xtype_>
    const type_& Mesh_1D<type_, xtype_>::operator()(
            const std::size_t& node_index, const std::size_t& var_index) const
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (nodes_)", num_nodes_, node_index);
        if (var_index >= num_vars_)
            throw Exceptions::Range("Mesh_1D (vars_)", num_vars_, var_index);
#endif  // PARANOID

        return vars_[num_vars_*node_index + var_index];
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::set_vars_vector(const Vector<type_>& vars_data)
    {
#ifdef PARANOID
        if (vars_data.size() != num_nodes_*num_vars_)
            throw Exceptions::Mismatch("Mesh_1D::set_vars_vector", "Vector");
#endif  // PARANOID

        vars_ = vars_data;
    }

    template <typename type_, typename xtype_>
    const Vector<type_>& Mesh_1D<type_, xtype_>::get_vars_vector() const
    {
        return vars_;
    }

    template <typename type_, typename xtype_>
    const Vector<type_> Mesh_1D<type_, xtype_>::get_vars_at_node(
            const std::size_t& node_index) const
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (vars_)", num_nodes_, node_index);
#endif  // PARANOID

        Vector<type_> temp;
        for (std::size_t i = 0; i < num_vars_; ++i)
            temp.push_back(vars_[num_vars_*node_index + i]);
        return temp;
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::set_vars_at_node(const std::size_t& node_index,
            const Vector<type_>& vars_data)
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (vars_)", num_nodes_, node_index);
        if (vars_data.size() != num_vars_)
            throw Exceptions::Mismatch("Mesh_1D::set_vars_at_node", "Vector");
#endif  // PARANOID

        for (std::size_t i = 0; i < num_vars_; ++i)
            vars_[num_vars_*node_index + i] = vars_data[i];
    }

    template <typename type_, typename xtype_>
    const std::size_t& Mesh_1D<type_, xtype_>::num_nodes() const
    {
        return num_nodes_;
    }

    template <typename type_, typename xtype_>
    const std::size_t& Mesh_1D<type_, xtype_>::num_vars() const
    {
        return num_vars_;
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::dump(std::ostream& out,
            const std::size_t& skip) const
    {
        out << "# Mesh_1D\n";
        out << "# Node ";
        out << "# Data\n";
        for (std::size_t i = 0; i < num_nodes_; i += skip)
        {
            out << nodes_[i];
            for (std::size_t j = 0; j < num_vars_; ++j)
                out << ' ' << vars_[num_vars_*i + j];
            out << '\n';
        }
        out << "\n# Number of nodes: " << num_nodes_ << '\n';
        out << "# Skip size: " << skip << '\n';
        out << "# Number of variables: " << num_vars_ << "\n\n\n";
    }
    
    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::dump(const std::size_t& decimal_places,
            std::ostream& out, const std::size_t& skip) const
    {
        std::size_t width = decimal_places + 8;
        std::size_t old_precision(out.precision());
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision(decimal_places);
        out.setf(std::ios_base::showpos);

        out.setf(std::ios::left);
        out << "# Mesh_1D\n";
        out << std::setw(width) << "# Node" << ' ';
        out << std::setw(width) << "# Data" << '\n';
        out.unsetf(std::ios::left);
        for (std::size_t i = 0; i < num_nodes_; i += skip)
        {
            out << std::setw(width) << nodes_[i];
            for (std::size_t j = 0; j < num_vars_; ++j)
                out << ' ' << std::setw(width) << vars_[num_vars_*i + j];
            out << '\n';
        }
        out << "\n# Number of nodes: " << num_nodes_ << '\n';
        out << "# Skip size: " << skip << '\n';
        out << "# Number of variables: " << num_vars_ << "\n\n\n";
        out.precision(old_precision);
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::load_data(const std::string& path)
    {
        load_data(path.c_str());
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::load_data(const char* path)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::load_data with xtype_ != double "
                "or type_ != double");
    }

    template <>
    void Mesh_1D<double>::load_data(const char* path)
    {
        std::ifstream data;
        std::string line_data;

        data.open(path);
        if (data.is_open())
        {
            /*if (!std::getline(data, line_data))
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Unspecified error during reading of data file");
            }
            if (line_data != "# Mesh_1D")
            {
                data.close();
                throw Exceptions::Generic("Data IO",
                        "Data must be in Mesh_1D output format with "
                        "\"# Mesh_1D\" at head of file");
            }*/

            bool possible_block(false), num_vars_found(false);
            double temp(0.);

            num_nodes_ = 0;
            nodes_.clear();
            num_vars_ = 0;
            vars_.clear();

            while (std::getline(data, line_data))
            {
                if (line_data[0] == '#')
                    continue;
                if (line_data == "")
                {
                    if (possible_block)
                        break;
                    else
                        possible_block = true;
                    continue;
                }
                possible_block = false;
                std::istringstream iss(line_data);
                if (!(iss >> temp))
                    throw Exceptions::Generic("Data IO",
                            "Error during reading of data file, probably "
                            "missing data");
                nodes_.push_back(temp);
                while (iss >> temp)
                {
                    vars_.push_back(temp);
                    if (!num_vars_found)
                        ++num_vars_;
                }
                num_vars_found = true;
            }
            data.close();

            num_nodes_ = nodes_.size();
        }
        else
            throw Exceptions::Generic("Data IO", "Unable to open data file");
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::clear()
    {
        vars_.fill(0.);
    }

    template <typename type_, typename xtype_>
    const type_ Mesh_1D<type_, xtype_>::linear_interpolant(const xtype_& x,
            const std::size_t var) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::linear_interpolant with type_, xtype_ "
                "!= double");
    }

    template <>
    const double Mesh_1D<double>::linear_interpolant(const double& x,
            const std::size_t var) const
    {
        double left(nodes_[0]), right(nodes_[num_nodes_ - 1]);
        bool reversed(right < left);
        bool local(inv_grid_map_ ? inv_grid_map_(x) : x);

#ifdef PARANOID
        if ((!reversed && (x < left || x > right)) ||
                (reversed && (x < right || x > left)))
            throw Exceptions::Invalid_Argument("Mesh_1D::linear_interpolant",
                    "x must be within the domain");
        if (var >= num_vars_)
            throw Exceptions::Invalid_Argument("Mesh_1D::linear_interpolant",
                    "var < num_vars_ is required");
#endif  // PARANOID

        std::size_t node(0);
        double diff(0.), step(0.);
        int mx(reversed*-2 + 1);

        for (std::size_t i(1); i < num_nodes_ - 1; ++i)
            if (mx*local >= mx*nodes_[i])
                node = i;

        diff = local - nodes_[node];
        step = nodes_[node + 1] - nodes_[node];

        if (x == nodes_[num_nodes_ - 1])
            return operator()(num_nodes_ - 1, var);
        else 
            return operator()(node, var) + diff/step*(
                    operator()(node + 1, var) - operator()(node, var));
    }

    template <typename type_, typename xtype_>
    const type_ Mesh_1D<type_, xtype_>::derivative_at_node(
            const std::size_t& node_index, const std::size_t& var_index) const
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::derivative_at_node with type_, xtype_ "
                "!= double");
    }

    template <>
    const double Mesh_1D<double>::derivative_at_node(
            const std::size_t& node_index, const std::size_t& var_index) const
    {
#ifdef PARANOID
        if (node_index >= num_nodes_)
            throw Exceptions::Range("Mesh_1D (nodes_)", num_nodes_, node_index);
        if (var_index >= num_vars_)
            throw Exceptions::Range("Mesh_1D (vars_)", num_vars_, var_index);
        if (!grid_map_)
            throw Exceptions::Generic("Not initialised correctly",
                    "Attempt to use Mesh_1D::derivative_at_node with "
                    "xtype_ != double");
        if (num_nodes_ < 3)
            throw Exceptions::Generic("Not initialised correctly",
                    "num_nodes_ >= 3 required for derivative expressions "
                    "to be well-formed");
#endif  // PARANOID
        
        double inv_2s(0.5/(nodes_[1] - nodes_[0]));
        if (node_index == 0)
            return inv_2s*(
                    - 3*vars_[num_vars_*0 + var_index]
                    + 4*vars_[num_vars_*1 + var_index]
                    - vars_[num_vars_*2 + var_index])/
                map_ds(nodes_[0]);
        else if (node_index == num_nodes_ - 1)
            return inv_2s*(
                    + 3*vars_[num_vars_*(num_nodes_ - 1) + var_index]
                    - 4*vars_[num_vars_*(num_nodes_ - 2) + var_index]
                    + vars_[num_vars_*(num_nodes_ - 3) + var_index])/
                map_ds(nodes_[num_nodes_ - 1]);
        else
        {
            return inv_2s*(
                    + vars_[num_vars_*(node_index + 1) + var_index]
                    - vars_[num_vars_*(node_index - 1) + var_index])/
                map_ds(nodes_[node_index]);
        }
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::uniform_mesh(const xtype_& begin,
            const xtype_& end, const std::size_t& num_nodes,
            const std::size_t& num_vars)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::uniform_mesh with xtype_ != double");
    }

    template <>
    void Mesh_1D<double>::uniform_mesh(const double& begin,
            const double& end, const std::size_t& num_nodes,
            const std::size_t& num_vars)
    {
#ifdef PARANOID
        if (num_nodes <= 1)
            throw Exceptions::Invalid_Argument("Mesh_1D::uniform_mesh",
                    "num_nodes >= 2 is required");
#endif  // PARANOID

        num_nodes_ = num_nodes;
        num_vars_ = num_vars;
        nodes_.assign(num_nodes_, 0.);
        vars_.assign(num_nodes_*num_vars_, 0.);
        
        double inv(1./double(num_nodes_ - 1));
        for (std::size_t i(0); i < num_nodes_; ++i)
            nodes_[i] = begin + i*inv*(end - begin);
    }

    template <>
    void Mesh_1D<std::complex<double>, double>::uniform_mesh(
            const double& begin, const double& end,
            const std::size_t& num_nodes, const std::size_t& num_vars)
    {
#ifdef PARANOID
        if (num_nodes <= 1)
            throw Exceptions::Invalid_Argument("Mesh_1D::uniform_mesh",
                    "num_nodes >= 2 is required");
#endif  // PARANOID

        num_nodes_ = num_nodes;
        num_vars_ = num_vars;
        nodes_.assign(num_nodes_, 0.);
        vars_.assign(num_nodes_*num_vars_, 0.);
        
        double inv(1./double(num_nodes_ - 1));
        for (std::size_t i(0); i < num_nodes_; ++i)
            nodes_[i] = begin + i*inv*(end - begin);
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::power_mesh(const xtype_& begin,
            const xtype_& end, const std::size_t& num_nodes,
            const std::size_t& num_vars, const double& power,
            const std::size_t& bunching_direction)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::power_mesh with xtype_ != double");
    }

    template <>
    void Mesh_1D<double>::power_mesh(const double& begin, const double& end,
            const std::size_t& num_nodes, const std::size_t& num_vars,
            const double& power, const std::size_t& bunching_direction)
    {
#ifdef PARANOID
        if (num_nodes <= 1)
            throw Exceptions::Invalid_Argument("Mesh_1D::power_mesh",
                    "num_nodes >= 2 is required");
        if (bunching_direction != Mesh_Left && bunching_direction != Mesh_Right)
            throw Exceptions::Invalid_Argument("Mesh_1D::power_mesh",
                    "bunching_direction must be Mesh_Left or Mesh_Right");
#endif  // PARANOID

        num_nodes_ = num_nodes;
        num_vars_ = num_vars;
        nodes_.assign(num_nodes_, 0.);
        vars_.assign(num_nodes_*num_vars_, 0.);
        
        double inv(1./double(num_nodes_ - 1));
        switch (bunching_direction)
        {
            case Mesh_Left:
                for (std::size_t i = 0; i < num_nodes_; ++i)
                    nodes_[i] = begin + (end - begin)*std::pow(i*inv, power);
                break;
            case Mesh_Right:
                for (std::size_t i = 0; i < num_nodes_; ++i)
                    nodes_[num_nodes - 1 - i] =
                        end - (end - begin)*std::pow(i*inv, power);
        }
    }

    template <>
    void Mesh_1D<std::complex<double> >::power_mesh(const double& begin,
            const double& end, const std::size_t& num_nodes,
            const std::size_t& num_vars, const double& power,
            const std::size_t& bunching_direction)
    {
#ifdef PARANOID
        if (num_nodes <= 1)
            throw Exceptions::Invalid_Argument("Mesh_1D::power_mesh",
                    "num_nodes >= 2 is required");
        if (bunching_direction != Mesh_Left && bunching_direction != Mesh_Right)
            throw Exceptions::Invalid_Argument("Mesh_1D::power_mesh",
                    "bunching_direction must be Mesh_Left or Mesh_Right");
#endif  // PARANOID

        num_nodes_ = num_nodes;
        num_vars_ = num_vars;
        nodes_.assign(num_nodes_, 0.);
        vars_.assign(num_nodes_*num_vars_, 0.);

        double inv(1./double(num_nodes_ - 1));
        switch (bunching_direction)
        {
            case Mesh_Left:
                for (std::size_t i = 0; i < num_nodes_; ++i)
                    nodes_[i] = begin + (end - begin)*std::pow(i*inv, power);
                break;
            case Mesh_Right:
                for (std::size_t i = 0; i < num_nodes_; ++i)
                    nodes_[num_nodes - 1 - i] =
                        end - (end - begin)*std::pow(i*inv, power);
        }
    }

    template <typename type_, typename xtype_>
    void Mesh_1D<type_, xtype_>::mapped_grid(const xtype_& begin,
            const xtype_& end, const std::size_t& num_nodes,
            const std::size_t& num_vars, external_func grid_map,
            external_func inv_grid_map, external_func map_ds,
            external_func map_ds2)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::mapped_grid with xtype_ != double");
    }

    template <>
    void Mesh_1D<double>::mapped_grid(const double& begin,
            const double& end, const std::size_t& num_nodes,
            const std::size_t& num_vars, external_func grid_map,
            external_func inv_grid_map, external_func map_ds,
            external_func map_ds2)
    {
#ifdef PARANOID
        if (num_nodes <= 1)
            throw Exceptions::Invalid_Argument("Mesh_1D::mapped_grid",
                    "num_nodes >= 2 is required");
#endif  // PARANOID

        num_nodes_ = num_nodes;
        num_vars_ = num_vars;
        nodes_.assign(num_nodes_, 0.);
        vars_.assign(num_nodes_*num_vars_, 0.);

        grid_map_ = grid_map;
        inv_grid_map_ = inv_grid_map;
        map_ds_ = map_ds;
        map_ds2_ = map_ds2;
        
        double scaled_begin(inv_grid_map_(begin));
        double scaled_end(inv_grid_map_(end));
        double inv(1./double(num_nodes_ - 1));
        for (std::size_t i = 0; i < num_nodes_; ++i)
            nodes_[i] = scaled_begin + i*inv*(scaled_end - scaled_begin);
    }

    template <>
    void Mesh_1D<std::complex<double>, double>::mapped_grid(const double& begin,
            const double& end, const std::size_t& num_nodes,
            const std::size_t& num_vars, external_func grid_map,
            external_func inv_grid_map, external_func map_ds,
            external_func map_ds2)
    {
#ifdef PARANOID
        if (num_nodes <= 1)
            throw Exceptions::Invalid_Argument("Mesh_1D::mapped_grid",
                    "num_nodes >= 2 is required");
#endif  // PARANOID

        num_nodes_ = num_nodes;
        num_vars_ = num_vars;
        nodes_.assign(num_nodes_, 0.);
        vars_.assign(num_nodes_*num_vars_, 0.);

        grid_map_ = grid_map;
        inv_grid_map_ = inv_grid_map;
        map_ds_ = map_ds;
        map_ds2_ = map_ds2;

        double scaled_begin(inv_grid_map_(begin));
        double scaled_end(inv_grid_map_(end));
        double inv(1./double(num_nodes_ - 1));
        for (std::size_t i = 0; i < num_nodes_; ++i)
            nodes_[i] = scaled_begin + i*inv*(scaled_end - scaled_begin);
    }

    template <typename type_, typename xtype_>
    const bool Mesh_1D<type_, xtype_>::query(const xtype_& x,
            std::size_t& index)
    {
        throw Exceptions::Generic("Not supported",
                "Attempt to use Mesh_1D::query with xtype_ != double");
    }

    template <>
    const bool Mesh_1D<double>::query(const double& x, std::size_t& index)
    {
#ifdef PARANOID
        if (x < std::min(nodes_.front(), nodes_.back()) - snap_tolerance_
                || x > std::max(nodes_.front(), nodes_.back()) +
                    snap_tolerance_)
            throw Exceptions::Invalid_Argument("Mesh_1D::query",
                    "x must be within the domain");
#endif  // PARANOID
        
        std::size_t i(0);
        for (; i < num_nodes_; ++i)
            if (std::abs(x - nodes_[i]) <= snap_tolerance_)
            {
                index = i;
                return false;
            }

        if (nodes_.front() < nodes_.back())
            while (nodes_[i - 1] > x)
                --i;
        else
            while (nodes_[i - 1] < x)
                --i;

        nodes_.insert(i, x);
        ++num_nodes_;
        vars_.insert(num_vars_*i, num_vars_, 0.);
        index = i;

        return true;
    }

    template <>
    const bool Mesh_1D<std::complex<double>, double>::query(
            const double& x, std::size_t& index)
    {
#ifdef PARANOID
        if (x < std::min(nodes_.front(), nodes_.back()) - snap_tolerance_
                || x > std::max(nodes_.front(), nodes_.back()) +
                    snap_tolerance_)
            throw Exceptions::Invalid_Argument("Mesh_1D::query",
                    "x must be within the domain");
#endif  // PARANOID
        
        std::size_t i(0);
        for (; i < num_nodes_; ++i)
            if (std::abs(x - nodes_[i]) <= snap_tolerance_)
            {
                index = i;
                return false;
            }

        while (nodes_[i - 1] > x)
            --i;
        nodes_.insert(i, x);
        ++num_nodes_;
        vars_.insert(num_vars_*i, num_vars_, 0.);
        index = i;

        return true;
    }

    template class Mesh_1D<double>;
    template class Mesh_1D<std::complex<double> >;
    template class Mesh_1D<std::complex<double>, std::complex<double> >;
}   // namespace

