#include <complex>
#include <sstream>

#include <Banded_Linear_System.h>
#include <Dense_Matrix.h>
#include <Exceptions.h>
#include <Fortran_LAPACK.h>
#include <Timer.h>

#ifdef HAS_INTELMKL
#include <mkl.h>
#include <mkl_lapacke.h>
#endif  // HAS_INTELMKL

namespace Kobalos
{
    template <typename type_>
    Banded_Linear_System<type_>::Banded_Linear_System(Banded_Matrix<type_>& A,
            Vector<type_>& b) :
        A_(A),
        b_(b)
    {
#ifdef PARANOID
        if (A_.num_columns() != b_.size())
            throw Exceptions::Mismatch(
                    "Banded_Linear_System::Banded_Linear_System()",
                    "Banded_Matrix", "Vector");
#endif  // PARANOID
    }

    template <typename type_>
    Banded_Linear_System<type_>::~Banded_Linear_System()
    {
    }

    template <typename type_>
    void Banded_Linear_System<type_>::solve()
    {
#ifdef HAS_INTELMKL
        solve_intelmkl();
#elif defined LAPACK
        solve_lapack();
#else
        solve_native();
        back_substitute();
#endif
    }

    template <typename type_>
    std::size_t Banded_Linear_System<type_>::order() const
    {
        return b_.size();
    }

    template <typename type_>
    void Banded_Linear_System<type_>::solve_lapack()
    {
        throw Exceptions::Generic("Banded_Linear_System::solve_lapack()",
                "Not implemented for the selected data type");
    }

    template <>
    void Banded_Linear_System<std::complex<double> >::solve_lapack()
    {
#ifndef LAPACK
        throw Exceptions::Generic("Banded_Linear_System::solve_lapack()",
                "LAPACK support must be enabled by compiling with -DLAPACK");
#else
        int N = A_.num_rows();
        int K = A_.num_bands();
        int NRHS = 1;
        int LDAB = 3*K + 1;
        std::vector<int> IPIV(N);
        int INFO = 0;
        double* A = (double*)(A_.data_pointer());
        double* b = (double*)(b_.data_pointer());

#ifdef TIME
        Timer t_solve("Calling LAPACK's zgbsv");
        t_solve.start();
#endif  // TIME

        LAPACK_ZGBSV(N, K, K, NRHS, A, LDAB, &IPIV[0], b, N, INFO);

#ifdef TIME
        t_solve.stop();
        t_solve.print();
#endif  // TIME

        if (INFO != 0)
        {
            std::stringstream problem;
            problem << "LAPACK_ZGBSV returned INFO = " << INFO;
            throw Exceptions::Generic("Banded_Linear_System::solve_lapack()",
                    problem.str());
        }
#endif  // LAPACK
    }

    template <>
    void Banded_Linear_System<double>::solve_lapack()
    {
#ifndef LAPACK
        throw Exceptions::Generic("Banded_Linear_System::solve_lapack()",
                "LAPACK support must be enabled by compiling with -DLAPACK");
#else
        int N = A_.num_rows();
        int K = A_.num_bands();
        int NRHS = 1;
        int LDAB = 3*K + 1;
        std::vector<int> IPIV(N);
        int INFO = 0;

#ifdef TIME
        Timer t_solve("Calling LAPACK's dgbsv");
        t_solve.start();
#endif  // TIME

        LAPACK_DGBSV(N, K, K, NRHS, A_.data_pointer(), LDAB, &IPIV[0],
                b_.data_pointer(), N, INFO);

#ifdef TIME
        t_solve.stop();
        t_solve.print();
#endif  // TIME

        if (INFO != 0)
        {
            std::stringstream problem;
            problem << "LAPACK_DGBSV returned INFO = " << INFO;
            throw Exceptions::Generic("Banded_Linear_System::solve_lapack()",
                    problem.str());
        }
#endif  // LAPACK
    }

#ifdef HAS_INTELMKL
    template <>
    void Banded_Linear_System<double>::solve_intelmkl()
    {
        Dense_Matrix<double> A(A_.num_rows(), 0.);
        for (std::size_t i(0); i < A.num_rows(); ++i)
            for (std::size_t j(0); j < A.num_columns(); ++j)
                A(i, j) = A_.get(i, j);

        lapack_int n(A_.num_rows());
        lapack_int info(0);
        Vector<lapack_int> ipiv(n);

        info = LAPACKE_dgesv(LAPACK_COL_MAJOR, n, 1,
                A.data_pointer(), n, ipiv.data_pointer(),
                b_.data_pointer(), n);
    }

    template <>
    void Banded_Linear_System<std::complex<double>>::solve_intelmkl()
    {
        throw Exceptions::Generic("Banded_Linear_System::solve_intelmkl()",
                "std::complex<double> type not supported yet");
    }
#endif  // HAS_INTELMKL

    template <typename type_>
    void Banded_Linear_System<type_>::solve_native()
    {
        std::size_t n = A_.num_rows();
        std::size_t bands = A_.num_bands();
        std::size_t i_max;
        std::vector<type_> pivots(bands, 0.);

        for (std::size_t k = 0; k < n; i_max = ++k)
        {
            i_max = A_.column_max_index(k);

#ifdef PARANOID
            if (std::abs(A_.get(i_max, k)) == 0)
                throw Exceptions::Generic(
                        "Banded_Linear_System::solve_native()",
                        "Matrix given to linear system solver is singular.");
#endif  // PARANOID

            if (i_max != k)
            {
                A_.swap_rows(k, i_max);
                b_.swap(k, i_max);
            }
            for (std::size_t i = 1; i <= bands && k + i < n; ++i)
            {
                pivots[i - 1] = A_.get(k + i, k)/A_.get(k, k);
                b_.operator[](k + i) -= b_[k]*pivots[i - 1];
            }
            for (std::size_t j = k + 1; j < n && j <= k + 2*bands; ++j)
                for (std::size_t i = 1; i <= bands && k + i < n; ++i)
                    A_.set(k + i, j) -= A_.get(k, j)*pivots[i - 1];
        }
    }

    template <typename type_>
    void Banded_Linear_System<type_>::back_substitute()
    {
        std::size_t n = A_.num_rows();
        std::size_t bands = A_.num_bands();
        type_* current = NULL;
        for (std::size_t i = n - 1; i < n; --i)
        {
            current = &(b_[i]);
            for (std::size_t j = std::min(n - 1, i + 2*bands); j > i; --j)
                 *current -= A_.get(i, j)*b_[j];
            *current /= A_.get(i, i);
        }
    }

    template class Banded_Linear_System<double>;
    template class Banded_Linear_System<std::complex<double> >;
}   // namespace

