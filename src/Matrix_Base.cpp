#include <complex>

#include <Exceptions.h>
#include <Matrix_Base.h>

namespace Kobalos
{
    template <typename type_>
    Matrix_Base<type_>::Matrix_Base()
    {
    }
    
    template <typename type_>
    Matrix_Base<type_>::~Matrix_Base()
    {
    }

    template class Matrix_Base<double>;
    template class Matrix_Base<std::complex<double> >;
}   // namespace

