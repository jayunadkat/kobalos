#include <complex>

#include <Console_Output.h>
#include <Exceptions.h>
#include <ODE_BVP.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    void ODE_BVP<type_, xtype_>::assemble_matrix()
    {
        std::size_t row(0), condition_index(0),
            num_conditions(conditions_.num_residuals());
        std::size_t eqn_order(equation_.order());
        std::size_t num_nodes(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        xtype_ inv(0.), mid_x(0.);
        Vector<type_> mid_state(num_vars, 0.);

        A_.assign(num_nodes*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_nodes*num_vars, 0.);
        
        for (std::size_t i(0); i < num_nodes - 1; ++i)
        {
            while (condition_index < num_conditions &&
                    conditions_.node(condition_index) == i)
            {
                conditions_.condition(condition_index).update(
                        mesh_.get_vars_at_node(i));
                for (std::size_t j(0);
                        j < conditions_.condition(condition_index).order(); ++j)
                {
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) =
                            conditions_.condition(
                                    condition_index).jacobian()(j, k);
                    }
                    b_[row] = -conditions_.condition(
                            condition_index).residual()[j];
                    ++row;
                }
                ++condition_index;
            }

            inv = 1./(mesh_.node(i + 1) - mesh_.node(i));
            for (std::size_t j(0); j < num_vars; ++j)
                mid_state[j] = (mesh_(i, j) + mesh_(i + 1, j))/2.;
            mid_x = (mesh_.node(i) + mesh_.node(i + 1))/2.;
            equation_.x1() = mid_x;
            equation_.update(mid_state);
            for (std::size_t j(0); j < eqn_order; ++j)
            {
                A_(row, num_vars*i + j) = -inv;
                A_(row, num_vars*(i + 1) + j) = inv;
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    A_(row, num_vars*i + k) -=
                        equation_.jacobian()(j, k)/2.;
                    A_(row, num_vars*(i + 1) + k) -=
                        equation_.jacobian()(j, k)/2.;
                }
                b_[row] = equation_.residual()[j] - 
                    (mesh_(i + 1, j) - mesh_(i, j))*inv;
                ++row;
            }
        }

        while (condition_index < num_conditions &&
                conditions_.node(condition_index) == num_nodes - 1)
        {
            conditions_.condition(condition_index).update(
                    mesh_.get_vars_at_node(num_nodes - 1));
            for (std::size_t j(0);
                    j < conditions_.condition(condition_index).order(); ++j)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    A_(row, num_vars*(num_nodes - 1) + k) =
                        conditions_.condition(
                                condition_index).jacobian()(j, k);
                }
                b_[row] = -conditions_.condition(
                        condition_index).residual()[j];
                ++row;
            }
            ++condition_index;
        }

#ifdef PARANOID
        if (row != num_nodes*num_vars)
            throw Exceptions::Generic("ODE_BVP::assemble_matrix",
                    "row != num_nodes*num_vars");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    ODE_BVP<type_, xtype_>::ODE_BVP(Residual<type_, xtype_>& equation,
            Conditions_1D<type_, xtype_>& conditions,
            Mesh_1D<type_, xtype_>& mesh) :
        equation_(equation),
        conditions_(conditions),
        A_(mesh.num_nodes()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        mesh_(mesh)
    {
#ifdef PARANOID
        if (equation.order() != conditions.num_conditions())
            throw Exceptions::Invalid_Argument("ODE_BVP::ODE_BVP",
                    "Residual objects form an over/underconstrained system");
        if (equation.num_vars() != conditions.condition(0).num_vars())
            throw Exceptions::Invalid_Argument("ODE_BVP::ODE_BVP",
                    "Equation and conditions objects have differing degrees" 
                    " of freedom");
        if (equation.num_vars() != mesh.num_vars())
            throw Exceptions::Invalid_Argument("ODE_BVP::ODE_BVP",
                    "Mesh object has too few degrees of freedom for equation");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    ODE_BVP<type_, xtype_>::~ODE_BVP()
    {
    }

    template <typename type_, typename xtype_>
    void ODE_BVP<type_, xtype_>::solve()
    {
        std::size_t counter(0);
        std::size_t num_nodes(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);

        do
        {
            assemble_matrix();
            max_residual = b_.inf_norm();
            linear_system_.solve();

            for (std::size_t i(0); i < num_nodes; ++i)
                for (std::size_t j(0); j < num_vars; ++j)
                    mesh_(i, j) += b_[num_vars*i + j];
            
#ifndef QUIET
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual = "
                    + str_convert(max_residual) + "\n");
#endif  // QUIET

            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    template class ODE_BVP<double>;
    template class ODE_BVP<std::complex<double> >;
    template class ODE_BVP<std::complex<double>, std::complex<double> >;
}   // namespace

