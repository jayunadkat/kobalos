#include <algorithm>
#include <complex>
#include <sstream>

#include <Console_Output.h>
#include <Dense_Matrix.h>
#include <Exceptions.h>

namespace Kobalos
{
    template <typename type_>
    Dense_Matrix<type_>::Dense_Matrix() :
        matrix_(),
        num_rows_(0),
        num_cols_(0)
    {
    }

    template <typename type_>
    Dense_Matrix<type_>::Dense_Matrix(const std::size_t& n,
            const type_& value) :
        matrix_(n*n, value),
        num_rows_(n),
        num_cols_(n)
    {
    }

    template <typename type_>
    Dense_Matrix<type_>::Dense_Matrix(const std::size_t&n,
            const std::size_t& m, const type_& value) :
        matrix_(n*m, value),
        num_rows_(n),
        num_cols_(m)
    {
    }

    template <typename type_>
    Dense_Matrix<type_>::Dense_Matrix(const Dense_Matrix<type_>& source) :
        matrix_(),
        num_rows_(0),
        num_cols_(0)
    {
        matrix_ = source.matrix_;
        num_rows_ = source.num_rows_;
        num_cols_ = source.num_cols_;
    }

    template <typename type_>
    Dense_Matrix<type_>::Dense_Matrix(type_* const source_pointer,
            const std::size_t& n, const std::size_t& m) :
        matrix_(),
        num_rows_(0),
        num_cols_(0)
    {
#ifdef PARANOID
        if (n == 0 || m == 0)
        {
            throw Exceptions::Invalid_Argument("Dense_Matrix::Dense_Matrix",
                    "n and m must be non-zero");
        }
        if (source_pointer == NULL)
        {
            throw Exceptions::Invalid_Argument("Dense_Matrix::Dense_Matrix",
                    "source_pointer is NULL");
        }
#endif  // PARANOID

        num_rows_ = n;
        num_cols_ = m;
        matrix_.assign(source_pointer, n*m);
    }

    template <typename type_>
    Dense_Matrix<type_>::~Dense_Matrix()
    {
    }

    template <typename type_>
    const Dense_Matrix<type_>& Dense_Matrix<type_>::operator=(
            const Dense_Matrix<type_>& source)
    {
        if (this != &source)
        {
            num_rows_ = source.num_rows_;
            num_cols_ = source.num_cols_;
            matrix_ = source.matrix_;
        }
        return *this;
    }

    template <typename type_>
    const Vector<type_> Dense_Matrix<type_>::operator*(
            const Vector<type_>& x) const
    {
#ifdef PARANOID
        if (num_cols_ != x.size())
        {
            throw Exceptions::Mismatch("Dense_Matrix::operator*",
                    "Dense_Matrix", "Vector");
        }
#endif  // PARANOID

        Vector<type_> temp(num_rows_);
        for (std::size_t i = 0; i < num_rows_; ++i)
            for (std::size_t j = 0; j < num_cols_; ++j)
                temp[i] += operator()(i, j)*x[j];
        return temp;
    }

    template <typename type_>
    void Dense_Matrix<type_>::operator*=(const type_& k)
    {
        matrix_ *= k;
    }

    template <typename type_>
    const Dense_Matrix<type_> Dense_Matrix<type_>::operator-(
            const Dense_Matrix<type_>& M) const
    {
#ifdef PARANOID
        if (matrix_.size() != M.matrix_.size())
        {
            throw Exceptions::Mismatch("Dense_Matrix::operator+",
                    "Dense_Matrix");
        }
#endif  // PARANOID

        Dense_Matrix<type_> temp(*this);
        temp.matrix_ -= M.matrix_;
        return temp;
    }

    template <typename type_>
    const Dense_Matrix<type_> Dense_Matrix<type_>::operator/(
            const type_& k) const
    {
#ifdef PARANOID
        if (std::abs(k) == 0)
        {
            throw Exceptions::Invalid_Argument("Dense_Matrix::operator/=",
                    "std::abs(k) > 0 is required");
        }
#endif  // PARANOID

        Dense_Matrix<type_> temp(*this);
        temp.matrix_ /= k;
        return temp;
    }

    template <typename type_>
    void Dense_Matrix<type_>::assign(type_* const source_pointer,
            const std::size_t& n, const std::size_t& m)
    {
#ifdef PARANOID
        if (n == 0 || m == 0)
        {
            throw Exceptions::Invalid_Argument("Dense_Matrix::assign",
                    "n and m must be non-zero");
        }
        if (source_pointer == NULL)
        {
            throw Exceptions::Invalid_Argument("Dense_Matrix::assign",
                    "source_pointer is NULL");
        }
#endif  // PARANOID

        num_rows_ = n;
        num_cols_ = m;
        matrix_.assign(source_pointer, n*m);
    }

    template <typename type_>
    void Dense_Matrix<type_>::assign(const std::size_t& n, const std::size_t& m,
            const type_& value)
    {
        matrix_.assign(n*m, value);
        num_rows_ = n;
        num_cols_ = m;
    }

    template <typename type_>
    void Dense_Matrix<type_>::resize(const std::size_t& n, const std::size_t& m)
    {
        matrix_.resize(n*m);
        num_rows_ = n;
        num_cols_ = m;
    }

    template <typename type_>
    void Dense_Matrix<type_>::fill(
            const type_& value)
    {
        matrix_.fill(value);
    }

    template <typename type_>
    void Dense_Matrix<type_>::clear()
    {
        matrix_.clear();
        num_rows_ = num_cols_ = 0;
    }

    template <typename type_>
    void Dense_Matrix<type_>::transpose()
    {
        Vector<type_> temp(num_rows_*num_cols_);
        std::size_t i(0), j(0);
        for (; i < num_rows_; ++i)
            for (j = 0; j < num_cols_; ++j)
                temp[i*num_cols_ + j] = operator()(i, j);
        i = num_rows_;
        num_rows_ = num_cols_;
        num_cols_ = i;
        matrix_ = temp;
    }

    template <typename type_>
    void Dense_Matrix<type_>::swap_rows(const std::size_t& row1,
            const std::size_t& row2)
    {
#ifdef PARANOID
        if (row1 >= num_rows_)
            throw Exceptions::Range("Dense_Matrix, row access", num_rows_,
                    row1);
        if (row2 >= num_rows_)
            throw Exceptions::Range("Dense_Matrix, row access", num_rows_,
                    row2);
#endif  // PARANOID

        for (std::size_t j = 0; j < num_cols_; ++j)
            std::swap(operator()(row1, j), operator()(row2, j));
    }

    template <typename type_>
    const std::size_t& Dense_Matrix<type_>::num_rows() const
    {
        return num_rows_;
    }

    template <typename type_>
    const std::size_t& Dense_Matrix<type_>::num_columns() const
    {
        return num_cols_;
    }

    template <typename type_>
    const std::size_t Dense_Matrix<type_>::num_elements() const
    {
        return num_rows_*num_cols_;
    }

    template <typename type_>
    const double Dense_Matrix<type_>::one_norm() const
    {
        return matrix_.one_norm();
    }

    template <typename type_>
    const double Dense_Matrix<type_>::two_norm() const
    {
        return matrix_.two_norm();
    }

    template <typename type_>
    const double Dense_Matrix<type_>::inf_norm() const
    {
        return matrix_.inf_norm();
    }

    template <typename type_>
    type_* Dense_Matrix<type_>::data_pointer()
    {
#ifdef PARANOID
        if (matrix_.size() == 0)
            throw Exceptions::Generic("Illegal operation",
                    "Attempt to pass pointer to empty vector");
#endif  // PARANOID

        return matrix_.data_pointer();
    }

    template <typename type_>
    void Dense_Matrix<type_>::dump() const
    {
        for (std::size_t i = 0; i < num_rows_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
                console_out(str_convert(matrix_[j*num_rows_ + i]) + " ");
            console_out.endl();
        }
    }

    template <typename type_>
    void Dense_Matrix<type_>::dump(const std::size_t& decimal_places) const
    {
        std::stringstream out;
        std::size_t width = decimal_places + 8;
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision((int)width - 8);
        for (std::size_t i = 0; i < num_rows_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
            {
                out << std::setw(width);
                out << matrix_[j*num_rows_ + i] << " ";
            }
            out << '\n';
        }
        console_out(out.str());
    }

    template class Dense_Matrix<double>;
    template class Dense_Matrix<std::complex<double> >;
}   // namespace

