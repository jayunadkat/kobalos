#include <cmath>

#include <boundary_layer/Rotating_Edge_Streamfunction.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Rotating_Edge_Streamfunction::tolerance_ = 1e-8;
    std::size_t Rotating_Edge_Streamfunction::max_iterations_ = 20;

    Rotating_Edge_Streamfunction::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars) :
        nv_(num_vars)
    {
    }

    Rotating_Edge_Streamfunction::Matrix_Column::~Matrix_Column()
    {
    }

    double Rotating_Edge_Streamfunction::sphere_spin(const double& t)
    {
        if (sphere_spin_)
            return (*sphere_spin_)(t);
        else
            return 1. - std::exp(-t*t);
    }

    double Rotating_Edge_Streamfunction::sphere_wobble(const double& t)
    {
        if (sphere_wobble_)
            return (*sphere_wobble_)(t);
        else
            return 0.;
    }


    void Rotating_Edge_Streamfunction::assemble_matrix(const double& dt)
    {
        if (c2_ < 0)
            throw Exceptions::Generic("Solution",
                    "Call to set_edge expected before solving");

        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2a(0.), inv_r2b(0.), inv_r3(0.), inv_t(0.);
        double f2x_o(0.), f2xx_o(0.), v1x_o(0.), v1xx_o(0.);
        Vector<double> mid_state(num_vars, 0.);
        Matrix_Column col(num_vars);

        A_.assign(num_r*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_r*num_vars, 0.);

        // Sphere surface entries at i = 0
        A_(row, col(0, F)) = 1.;
        b_[row] = -mesh_(0, F);
        ++row;
        A_(row, col(0, FR)) = 1.;
        b_[row] = -mesh_(0, FR);
        ++row;
        A_(row, col(0, V)) = 1.;
        if (unsteady)
            b_[row] =
                + sphere_spin(t_)*(sgn_ + sphere_wobble(t_))
                - mesh_(0, V);
        else
            b_[row] = sgn_ - mesh_(0, V);
        ++row;

        // Interior of boundary layer
        for (std::size_t i(0); i < num_r - 1; ++i)
        {
            inv_r = 1./((mesh_.node(i + 1) - mesh_.node(i))*
                    mesh_.map_ds(
                        0.5*(mesh_.node(i) + mesh_.node(i + 1))));
            if (i > 0)
            {
                inv_r3 = 1./((mesh_.node(i + 1) - mesh_.node(i - 1))*
                        mesh_.map_ds(mesh_.node(i)));
                inv_r2a = -mesh_.map_ds2(mesh_.node(i))/(
                        (mesh_.node(i + 1) - mesh_.node(i - 1))*
                        std::pow(mesh_.map_ds(mesh_.node(i)), 3.));
                inv_r2b = std::pow(1./(
                            (mesh_.node(i + 1) - mesh_.node(i))*
                            mesh_.map_ds(mesh_.node(i))), 2.);
            }
            if (unsteady && i > 0)
            {
                inv_t = 1./dt;
                f2x_o = inv_r3*(
                        + prev_(i + 1, FR)
                        - prev_(i - 1, FR));
                f2xx_o = inv_r2a*(prev_(i + 1, FR) - prev_(i - 1, FR))
                        + inv_r2b*(
                                + prev_(i - 1, FR)
                                - 2.*prev_(i, FR)
                                + prev_(i + 1, FR));
                v1x_o = inv_r3*(
                        + prev_(i + 1, V)
                        - prev_(i - 1, V));
                v1xx_o = inv_r2a*(prev_(i + 1, V) - prev_(i - 1, V))
                        + inv_r2b*(
                                + prev_(i - 1, V)
                                - 2.*prev_(i, V)
                                + prev_(i + 1, V));
            }

            // One equation mid-node in r, two at nodal locations
            if (i > 0)
            {
                A_(row, col(i, F)) =
                    + Ro_*(
                            -(3.*c2_ - 1.)*inv_r3*(
                                + mesh_(i + 1, FR)
                                - mesh_(i - 1, FR)
                                )
                          );
                A_(row, col(i - 1, FR)) =
                    - inv_r2a
                    + inv_r2b
                    + Ro_*(3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                A_(row, col(i, FR)) =
                    - 2.*inv_r2b
                    + 2.*Ro_*(2.*c2_ - 1.)*mesh_(i, FR);
                if (unsteady)
                    A_(row, col(i, FR)) += -2.*Ro_*inv_t;
                A_(row, col(i + 1, FR)) = 
                    + inv_r2a
                    + inv_r2b
                    - Ro_*(3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                A_(row, col(i, V)) =
                    - 2.*Ro_*mesh_(i, V)
                    - 2.;
                b_[row] =
                    - inv_r2a*(mesh_(i + 1, FR) - mesh_(i - 1, FR))
                    - inv_r2b*(
                            + mesh_(i - 1, FR)
                            - 2.*mesh_(i, FR)
                            + mesh_(i + 1, FR))
                    + Ro_*(
                            + (3.*c2_ - 1.)*inv_r3*mesh_(i, F)*(
                                + mesh_(i + 1, FR)
                                - mesh_(i - 1, FR))
                            - (2.*c2_ - 1.)*mesh_(i, FR)*mesh_(i, FR)
                            + mesh_(i, V)*mesh_(i, V)
                          )
                    + 2.*mesh_(i, V);
                if (unsteady)
                    b_[row] +=
                        - f2xx_o
                        + Ro_*(
                                + 2.*inv_t*(
                                    + mesh_(i, FR)
                                    - prev_(i, FR))
                                + (3.*c2_ - 1.)*prev_(i, F)*f2x_o
                                - (2.*c2_ - 1.)*prev_(i, FR)*prev_(i, FR)
                                + prev_(i, V)*prev_(i, V)
                              )
                        + 2.*prev_(i, V);
                ++row;

                A_(row, col(i, F)) =
                    + Ro_*(
                            -(3.*c2_ - 1.)*inv_r3*(
                                + mesh_(i + 1, V)
                                - mesh_(i - 1, V)
                                )
                          );
                A_(row, col(i, FR)) =
                    + 2.*Ro_*c2_*mesh_(i, V)
                    + 2.*c2_;
                A_(row, col(i - 1, V)) =
                    - inv_r2a
                    + inv_r2b
                    + Ro_*(3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                A_(row, col(i, V)) =
                    - 2.*inv_r2b
                    + 2.*Ro_*c2_*mesh_(i, FR);
                if (unsteady)
                    A_(row, col(i, V)) += -2.*Ro_*inv_t;
                A_(row, col(i + 1, V)) =
                    + inv_r2a
                    + inv_r2b
                    - Ro_*(3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                b_[row] =
                    - inv_r2a*(mesh_(i + 1, V) - mesh_(i - 1, V))
                    - inv_r2b*(
                            + mesh_(i - 1, V)
                            - 2.*mesh_(i, V)
                            + mesh_(i + 1, V))
                    + Ro_*(
                            + (3.*c2_ - 1.)*inv_r3*mesh_(i, F)*(
                                + mesh_(i + 1, V)
                                - mesh_(i - 1, V))
                            - 2.*c2_*mesh_(i, FR)*mesh_(i, V)
                          )
                    - 2.*c2_*mesh_(i, FR);
                if (unsteady)
                    b_[row] +=
                        - v1xx_o
                        + Ro_*(
                                + 2.*inv_t*(
                                    + mesh_(i, V)
                                    - prev_(i, V))
                                + (3.*c2_ - 1.)*prev_(i, F)*v1x_o
                                - 2.*c2_*prev_(i, FR)*prev_(i, V)
                              )
                        - 2.*c2_*prev_(i, FR);
                ++row;
            }

            A_(row, col(i, F)) = -inv_r;
            A_(row, col(i + 1, F)) = inv_r;
            A_(row, col(i, FR)) = -0.5;
            A_(row, col(i + 1, FR)) = -0.5;
            b_[row] = inv_r*(mesh_(i, F) - mesh_(i + 1, F))
                + 0.5*(mesh_(i, FR) + mesh_(i + 1, FR));
            ++row;
        }
        // Far-field entries at i = num_r - 1
        A_(row, col(num_r - 1, FR)) = 1.;
        b_[row] = -mesh_(num_r - 1, FR);
        ++row;
        A_(row, col(num_r - 1, V)) = 1.;
        b_[row] = -mesh_(num_r - 1, V);
        ++row;

        if (row != num_r*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Rotating_Edge_Streamfunction::Rotating_Edge_Streamfunction(
            Mesh_1D<double>& mesh):
        col(mesh.num_vars()),
        c2_(-1.),
        Ro_(0.5),
        sgn_(-1),
        t_(0.),
        sphere_spin_(NULL),
        sphere_wobble_(NULL),
        A_(mesh.num_nodes()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        prev_(mesh),
        mesh_(mesh)
    {
        if (mesh_.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Rotating_Edge_Streamfunction::Rotating_Edge_Streamfunction",
                    "Mesh_1D object with num_vars_ = 3 required");
    }

    Rotating_Edge_Streamfunction::~Rotating_Edge_Streamfunction()
    {
    }

    void Rotating_Edge_Streamfunction::solve_steady()
    {
        step(0.);
    }

    void Rotating_Edge_Streamfunction::step(const double& dt)
    {
        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);
        if (dt > tolerance_)
            t_ += dt;

#ifndef QUIET
        if (dt > tolerance_)
            console_out(" 1D solver, t = " + str_convert(t_) + "\n");
        else
            console_out(" 1D solver\n");
#ifdef DEBUG
        if (dt > tolerance_)
            console_out(" -----------------------\n");
        else
            console_out(" ---------\n");
#endif  // DEBUG
#endif  // !QUIET

        prev_.set_vars_vector(mesh_.get_vars_vector());

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix(dt);

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t k(0); k < num_vars; ++k)
                    mesh_(i, k) += b_[num_vars*i + k];
            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_1D<double>& Rotating_Edge_Streamfunction::solution() const
    {
        return mesh_;
    }

    void Rotating_Edge_Streamfunction::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        std::size_t old_precision(out.precision());
        out.precision(decimal_places);
        out << "# Mesh_1D\n";
        out << "# Solution at t = " << t_ << '\n';
        out << "# r    u    v    w\n";
        for (std::size_t i(0), num_r(mesh_.num_nodes()); i < num_r;
                i += output_skip + 1)
        {
            out << mesh_.unmapped_node(i) << ' ';
            out << -mesh_(i, FR) << ' ';
            out << mesh_(i, V) << ' ';
            out << (3.*c2_ - 1.)*mesh_(i, F) << '\n';
        }
        out << "\n\n";
        out.precision(old_precision);
    }
}   // namespace

