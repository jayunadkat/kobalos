#include <cmath>

#include <boundary_layer/Rotating_Edge_EVP.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Rotating_Edge_EVP::tolerance_ = 1e-8;
    std::size_t Rotating_Edge_EVP::max_iterations_ = 20;

    Rotating_Edge_EVP::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars) :
        nv_(num_vars)
    {
    }

    Rotating_Edge_EVP::Matrix_Column::~Matrix_Column()
    {
    }

    void Rotating_Edge_EVP::assemble_matrix()
    {
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());

        // ===================================================================
        // ===================================================================
        //
        //                          MATRIX ASSEMBLY                           

        std::size_t row(0);
        double inv_r(0.), inv_r2a(0.), inv_r2b(0.), inv_r3(0.);
        double fds(0.), fds2(0.), vds(0.);
        Matrix_Column col(num_vars);

        A_.assign(num_r*num_vars, num_r*num_vars, 0.);
        B_.assign(num_r*num_vars, num_r*num_vars, 0.);

        // Sphere surface entries at i = 0
        A_(row, col(0, F)) = 1.;
        ++row;
        A_(row, col(0, FR)) = 1.;
        ++row;
        A_(row, col(0, V)) = 1.;
        ++row;

        // Interior of boundary layer
        for (std::size_t i(0); i < num_r - 1; ++i)
        {
            inv_r = 1./((mesh_.node(i + 1) - mesh_.node(i))*
                    mesh_.map_ds(
                        0.5*(mesh_.node(i) + mesh_.node(i + 1))));
            if (i > 0)
            {
                inv_r3 = 1./((mesh_.node(i + 1) - mesh_.node(i - 1))*
                        mesh_.map_ds(mesh_.node(i)));
                inv_r2a = -mesh_.map_ds2(mesh_.node(i))/(
                        (mesh_.node(i + 1) - mesh_.node(i - 1))*
                        std::pow(mesh_.map_ds(mesh_.node(i)), 3.));
                inv_r2b = std::pow(1./(
                            (mesh_.node(i + 1) - mesh_.node(i))*
                            mesh_.map_ds(mesh_.node(i))), 2.);

                fds = mesh_(i + 1, F) - mesh_(i - 1, F);
                fds2 = mesh_(i + 1, F) - 2.*mesh_(i, F) + mesh_(i - 1, F);
                vds = mesh_(i + 1, V) - mesh_(i - 1, V);
            }

            // One equation mid-node in r, two at nodal locations
            if (i > 0)
            {
                A_(row, col(i - 1, FR)) =
                    + Ro_*2.*mesh_(i, F)*inv_r3
                    - inv_r2a
                    + inv_r2b;
                A_(row, col(i, FR)) =
                    - 2.*inv_r2b;
                A_(row, col(i + 1, FR)) =
                    - Ro_*2.*mesh_(i, F)*inv_r3
                    + inv_r2a
                    + inv_r2b;
                A_(row, col(i, V)) =
                    - Ro_*2.*mesh_(i, V)
                    - 2.;
                if (type_ == Temporal_EVP)
                {
                    A_(row, col(i, F)) =
                        - Ro_*(2. + fixed_param_)*(
                                + inv_r2a*fds
                                + inv_r2b*fds2
                                );
                    A_(row, col(i, FR)) +=
                        + Ro_*(2. + fixed_param_)*inv_r3*fds;
                    B_(row, col(i, FR)) =
                        + Ro_;
                }
                else
                {
                    B_(row, col(i, F)) =
                        + Ro_*(
                                + inv_r2a*fds
                                + inv_r2b*fds2
                                );
                    B_(row, col(i, FR)) =
                        - Ro_*inv_r3*fds;
                    A_(row, col(i, FR)) +=
                        - Ro_*fixed_param_;
                }
                ++row;

                A_(row, col(i, FR)) =
                    - Ro_*2.*mesh_(i, V)
                    - 2.;
                A_(row, col(i - 1, V)) =
                    - Ro_*2.*mesh_(i, F)*inv_r3
                    + inv_r2a
                    - inv_r2b;
                A_(row, col(i, V)) =
                    + 2.*inv_r2b;
                A_(row, col(i + 1, V)) =
                    + Ro_*2.*mesh_(i, F)*inv_r3
                    - inv_r2a
                    - inv_r2b;
                if (type_ == Temporal_EVP)
                {
                    A_(row, col(i, F)) =
                        + Ro_*(2. + fixed_param_)*inv_r3*vds;
                    A_(row, col(i, V)) +=
                        - Ro_*(2. + fixed_param_)*inv_r3*fds;
                    B_(row, col(i, V)) =
                        - Ro_;
                }
                else
                {
                    B_(row, col(i, F)) =
                        - Ro_*inv_r3*vds;
                    B_(row, col(i, V)) =
                        + Ro_*inv_r3*fds;
                    A_(row, col(i, V)) +=
                        + Ro_*fixed_param_;
                }
                ++row;
            }

            A_(row, col(i, F)) = -inv_r;
            A_(row, col(i + 1, F)) = inv_r;
            A_(row, col(i, FR)) = -0.5;
            A_(row, col(i + 1, FR)) = -0.5;
            ++row;
        }
        // Far-field entries at i = num_r - 1
        A_(row, col(num_r - 1, FR)) = 1.;
        ++row;
        A_(row, col(num_r - 1, V)) = 1.;
        ++row;

        if (row != num_r*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        // ===================================================================
        // ===================================================================
    }

    Rotating_Edge_EVP::Rotating_Edge_EVP(
            Mesh_1D<double>& mesh):
        col(mesh.num_vars()),
        Ro_(0.5),
        fixed_param_(0.),
        type_(Temporal_EVP),
        sgn_(-1),
        A_(mesh.num_nodes()*mesh.num_vars(), 0.),
        B_(mesh.num_nodes()*mesh.num_vars(), 0.),
        mesh_(mesh),
        steady_problem_(mesh_),
        evp_problem_(A_, B_)
    {
        if (mesh_.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Rotating_Edge_EVP::Rotating_Edge_EVP",
                    "Mesh_1D object with num_vars_ = 3 required");

        steady_problem_.set_edge("pole");
    }

    Rotating_Edge_EVP::~Rotating_Edge_EVP()
    {
    }

    void Rotating_Edge_EVP::solve()
    {
#ifndef QUIET
        console_out("Solving steady problem: Ro = "
                + str_convert(sgn_*Ro_) + "\n");
        console_out("======================================\n");
#endif  // !QUIET

        steady_problem_.solve_steady();
        
#ifndef QUIET
        console_out("Solving eigenvalue problem\n==========================\n");
#endif  // !QUIET

        assemble_matrix();
        evp_problem_.solve();
    }

    const Generalised_Eigenvalue_System<double>&
        Rotating_Edge_EVP::evp_system() const
    {
        return evp_problem_;
    }

    void Rotating_Edge_EVP::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        std::size_t old_precision(out.precision());
        out.precision(decimal_places);
        out << "# Mesh_1D\n";
        out << "# r    u    v    w\n";
        for (std::size_t i(0), num_r(mesh_.num_nodes()); i < num_r;
                i += output_skip + 1)
        {
            out << mesh_.unmapped_node(i) << ' ';
            out << -mesh_(i, FR) << ' ';
            out << mesh_(i, V) << ' ';
            out << 2.*mesh_(i, F) << '\n';
        }
        out << "\n\n";
        out.precision(old_precision);
    }
}   // namespace

