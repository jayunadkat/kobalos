#include <cmath>

#include <boundary_layer/Fourier_Decomposed_Edge.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Fourier_Decomposed_Edge::tolerance_ = 1e-8;
    std::size_t Fourier_Decomposed_Edge::max_iterations_ = 50;

    Fourier_Decomposed_Edge::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars) :
        nv_(num_vars)
    {
    }

    Fourier_Decomposed_Edge::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Fourier_Decomposed_Edge::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& k)
    {
        return nv_*i + k;
    }

    void Fourier_Decomposed_Edge::assemble_matrix(const double& dt)
    {
        if (c2_ < 0)
            throw Exceptions::Generic("Solution",
                    "Call to set_edge expected before solving");

        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2(0.), inv_r3(0.);
        double sf2dx(0.), sv1dx(0.);
        std::complex<double> f2dx(0.), v1dx(0.);
        Matrix_Column col(num_vars);

        A_.assign(num_r*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_r*num_vars, 0.);

        // Sphere surface entries at i = 0
        A_(row, col(0, f)) = 1.;
        b_[row] = -mesh_(0, f);
        ++row;
        A_(row, col(0, fr)) = 1.;
        b_[row] = -mesh_(0, fr);
        ++row;
        A_(row, col(0, v)) = 1.;
        b_[row] = 1. - mesh_(0, v);
        ++row;

        // Interior of boundary layer
        for (std::size_t i(0); i < num_r - 1; ++i)
        {
            inv_r = 1./(mesh_.node(i + 1) - mesh_.node(i));
            if (i > 0)
            {
                inv_r2 = 1./(mesh_.node(i) - mesh_.node(i - 1));
                inv_r3 = 1./(mesh_.node(i + 1) - mesh_.node(i - 1));
            }
            if (i > 0)
            {
                sf2dx = steady_(i + 1, fr) - steady_(i - 1, fr);
                sv1dx = steady_(i + 1, v) - steady_(i - 1, v);

                f2dx = mesh_(i + 1, fr) - mesh_(i - 1, fr);
                v1dx = mesh_(i + 1, v) - mesh_(i - 1, v);
            }

            // One equation mid-node in r, two at nodal locations
            if (i > 0)
            {
                A_(row, col(i, f)) = -(3.*c2_ - 1.)*inv_r3*sf2dx;
                A_(row, col(i - 1, fr)) = 2.*inv_r3*inv_r2
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, fr)) = -2.*inv_r3*(inv_r + inv_r2)
                    + 2.*(2.*c2_ - 1.)*steady_(i, fr);
                A_(row, col(i, fr)) += -std::complex<double>(0., 1.)*omega_;
                A_(row, col(i + 1, fr)) = 2.*inv_r3*inv_r
                    - (3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, v)) = -2.*steady_(i, v);

                b_[row] = 
                    + std::complex<double>(0., 1.)*omega_*mesh_(i, fr)
                    + 2.*inv_r3*(
                            - inv_r2*mesh_(i - 1, fr)
                            + (inv_r + inv_r2)*mesh_(i, fr)
                            - inv_r*mesh_(i + 1, fr))
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f)*f2dx
                    + (3.*c2_ - 1.)*inv_r3*sf2dx*mesh_(i, f)
                    - 2.*(2.*c2_ - 1.)*steady_(i, fr)*mesh_(i, fr)
                    + 2.*steady_(i, v)*mesh_(i, v);
                ++row;

                A_(row, col(i, f)) = -(3.*c2_ - 1.)*inv_r3*sv1dx;
                A_(row, col(i, fr)) = 2.*c2_*steady_(i, v);
                A_(row, col(i - 1, v)) = 2.*inv_r3*inv_r2
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, v)) = -2.*inv_r3*(inv_r + inv_r2)
                    + 2.*c2_*steady_(i, fr);
                A_(row, col(i, v)) += -std::complex<double>(0., 1.)*omega_;
                A_(row, col(i + 1, v)) = 2.*inv_r3*inv_r
                    - (3.*c2_ - 1.)*inv_r3*steady_(i, f);

                b_[row] =
                    + std::complex<double>(0., 1.)*omega_*mesh_(i, v)
                    + 2.*inv_r3*(
                            - inv_r2*mesh_(i - 1, v)
                            + (inv_r + inv_r2)*mesh_(i, v)
                            - inv_r*mesh_(i + 1, v))
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f)*v1dx
                    + (3.*c2_ - 1.)*inv_r3*sv1dx*mesh_(i, f)
                    - 2.*c2_*steady_(i, fr)*mesh_(i, v)
                    - 2.*c2_*steady_(i, v)*mesh_(i, fr);
                ++row;
            }

            A_(row, col(i, f)) = -inv_r;
            A_(row, col(i + 1, f)) = inv_r;
            A_(row, col(i, fr)) = -0.5;
            A_(row, col(i + 1, fr)) = -0.5;
            b_[row] = inv_r*(mesh_(i, f) - mesh_(i + 1, f))
                + 0.5*(mesh_(i, fr) + mesh_(i + 1, fr));
            ++row;
        }
        // Far-field entries at i = num_r - 1
        /*A_(row, col(num_r - 1, fr)) = 1.;
        b_[row] = -mesh_(num_r - 1, fr);
        ++row;
        A_(row, col(num_r - 1, v)) = 1.;
        b_[row] = -mesh_(num_r - 1, v);
        ++row;*/
        /*A_(row, col(num_r - 3, fr)) = 1.;
        A_(row, col(num_r - 2, fr)) = -4.;
        A_(row, col(num_r - 1, fr)) = 3.;
        b_[row] = -mesh_(num_r - 3, fr) + 4.*mesh_(num_r - 2, fr)
                - 3.*mesh_(num_r - 1, fr);
        ++row;
        A_(row, col(num_r - 3, v)) = 1.;
        A_(row, col(num_r - 2, v)) = -4.;
        A_(row, col(num_r - 1, v)) = 3.;
        b_[row] = -mesh_(num_r - 3, v) + 4.*mesh_(num_r - 2, v)
                - 3.*mesh_(num_r - 1, v);
        ++row;*/
        A_(row, col(num_r - 2, fr)) = -1.;
        A_(row, col(num_r - 1, fr)) = 1.;
        b_[row] = mesh_(num_r - 2, fr) - mesh_(num_r - 1, fr);
        ++row;
        A_(row, col(num_r - 2, v)) = -1.;
        A_(row, col(num_r - 1, v)) = 1.;
        b_[row] = mesh_(num_r - 2, v) - mesh_(num_r - 1, v);
        ++row;

        if (row != num_r*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Fourier_Decomposed_Edge::Fourier_Decomposed_Edge(
            Mesh_1D<double>& mesh_steady, Mesh_1D<std::complex<double> >& mesh):
        c2_(-1.),
        omega_(0.),
        A_(mesh.num_nodes()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        steady_(mesh_steady),
        mesh_(mesh)
    {
#ifdef PARANOID
        if (mesh_.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Fourier_Decomposed_Edge::Fourier_Decomposed_Edge",
                    "Mesh_1D object with num_vars_ = 3 required");
        if (mesh.num_nodes() != mesh_steady.num_nodes() ||
                mesh.num_vars() != mesh_steady.num_vars())
            throw Exceptions::Invalid_Argument(
                    "Fourier_Decomposed_Edge::Fourier_Decomposed_Edge",
                    "Mismatching mesh objects supplied to solver");
#endif  // PARANOID
    }

    Fourier_Decomposed_Edge::~Fourier_Decomposed_Edge()
    {
    }

    void Fourier_Decomposed_Edge::solve()
    {
        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);

#ifndef QUIET
        console_out(" 1D solver\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix();

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t k(0); k < num_vars; ++k)
                    mesh_(i, k) += b_[num_vars*i + k];

            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_1D<std::complex<double> >&
        Fourier_Decomposed_Edge::solution() const
    {
        return mesh_;
    }

    void Fourier_Decomposed_Edge::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        std::size_t old_precision(out.precision());
        out.precision(decimal_places);
        out << "# Mesh_1D\n";
        out << "# r    u    v    w\n";
        std::complex<double> temp;
        for (std::size_t i(0), num_r(mesh_.num_nodes()); i < num_r;
                i += output_skip + 1)
        {
            out << mesh_.node(i) << ' ';
            temp = -mesh_(i, fr);
            out << temp.real() << ' ' << temp.imag() << ' ';
            out << mesh_(i, v).real() << ' ' << mesh_(i, v).imag() << ' ';
            temp = (3.*c2_ - 1.)*mesh_(i, f);
            out << temp.real() << ' ' << temp.imag() << '\n';
        }
        out << "\n\n";
        out.flush();
        out.precision(old_precision);
    }
}   // namespace

