#include <cmath>

#include <boundary_layer/Edge_Streamfunction.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Edge_Streamfunction::tolerance_ = 1e-8;
    std::size_t Edge_Streamfunction::max_iterations_ = 50;

    Edge_Streamfunction::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars) :
        nv_(num_vars)
    {
    }

    Edge_Streamfunction::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Edge_Streamfunction::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& k)
    {
        return nv_*i + k;
    }

    double Edge_Streamfunction::sphere_rot(const double& t)
    {
        if (ext_rot_)
            return (*ext_rot_)(t_);
        else
            return 1. + (S_ - 1.)*std::exp(-t_*t_);
    }

    void Edge_Streamfunction::assemble_matrix(const double& dt)
    {
        if (c2_ < 0)
            throw Exceptions::Generic("Solution",
                    "Call to set_edge expected before solving");

        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2(0.), inv_r3(0.), inv_t(0.);
        double f2x_o(0.), f2xx_o(0.), v1x_o(0.), v1xx_o(0.);
        Vector<double> mid_state(num_vars, 0.);
        Matrix_Column col(num_vars);

        A_.assign(num_r*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_r*num_vars, 0.);

        // Sphere surface entries at i = 0
        A_(row, col(0, F)) = 1.;
        b_[row] = -mesh_(0, F);
        ++row;
        A_(row, col(0, FR)) = 1.;
        b_[row] = -mesh_(0, FR);
        ++row;
        A_(row, col(0, V)) = 1.;
        if (unsteady)
            b_[row] = sphere_rot(t_) - mesh_(0, V);
        else
            b_[row] = 1. - mesh_(0, V);
        ++row;

        // Interior of boundary layer
        for (std::size_t i(0); i < num_r - 1; ++i)
        {
            inv_r = 1./(mesh_.node(i + 1) - mesh_.node(i));
            if (i > 0)
            {
                inv_r2 = 1./(mesh_.node(i) - mesh_.node(i - 1));
                inv_r3 = 1./(mesh_.node(i + 1) - mesh_.node(i - 1));
            }
            if (unsteady && i > 0)
            {
                inv_t = 1./dt;
                f2x_o = inv_r3*(
                        + prev_(i + 1, FR)
                        - prev_(i - 1, FR));
                f2xx_o = 2.*inv_r3*(
                        + inv_r2*prev_(i - 1, FR)
                        - (inv_r + inv_r2)*prev_(i, FR)
                        + inv_r*prev_(i + 1, FR));
                v1x_o = inv_r3*(
                        + prev_(i + 1, V)
                        - prev_(i - 1, V));
                v1xx_o = 2.*inv_r3*(
                        + inv_r2*prev_(i - 1, V)
                        - (inv_r + inv_r2)*prev_(i, V)
                        + inv_r*prev_(i + 1, V));
            }

            // One equation mid-node in r, two at nodal locations
            if (i > 0)
            {
                A_(row, col(i, F)) = -(3.*c2_ - 1.)*inv_r3*(
                        + mesh_(i + 1, FR)
                        - mesh_(i - 1, FR));
                A_(row, col(i - 1, FR)) = 2.*inv_r3*inv_r2
                    + (3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                A_(row, col(i, FR)) = -2.*inv_r3*(inv_r + inv_r2)
                    + 2.*(2.*c2_ - 1.)*mesh_(i, FR);
                if (unsteady)
                    A_(row, col(i, FR)) += -2.*inv_t;
                A_(row, col(i + 1, FR)) = 2.*inv_r3*inv_r
                    - (3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                A_(row, col(i, V)) = -2.*mesh_(i, V);
                b_[row] = 
                    + 2.*inv_r3*(
                            - inv_r2*mesh_(i - 1, FR)
                            + (inv_r + inv_r2)*mesh_(i, FR)
                            - inv_r*mesh_(i + 1, FR))
                    + (3.*c2_ - 1.)*inv_r3*mesh_(i, F)*(
                            + mesh_(i + 1, FR)
                            - mesh_(i - 1, FR))
                    - (2.*c2_ - 1.)*mesh_(i, FR)*mesh_(i, FR)
                    + mesh_(i, V)*mesh_(i, V)
                    - S_*S_;
                if (unsteady)
                    b_[row] +=
                        + 2.*inv_t*(
                                + mesh_(i, FR)
                                - prev_(i, FR))
                        - f2xx_o
                        + (3.*c2_ - 1.)*prev_(i, F)*f2x_o
                        - (2.*c2_ - 1.)*prev_(i, FR)*prev_(i, FR)
                        + prev_(i, V)*prev_(i, V)
                        - S_*S_;
                ++row;

                A_(row, col(i, F)) = -(3.*c2_ - 1.)*inv_r3*(
                        + mesh_(i + 1, V)
                        - mesh_(i - 1, V));
                A_(row, col(i, FR)) = 2.*c2_*mesh_(i, V);
                A_(row, col(i - 1, V)) = 2.*inv_r3*inv_r2
                    + (3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                A_(row, col(i, V)) = -2.*inv_r3*(inv_r + inv_r2)
                    + 2.*c2_*mesh_(i, FR);
                if (unsteady)
                    A_(row, col(i, V)) += -2.*inv_t;
                A_(row, col(i + 1, V)) = 2.*inv_r3*inv_r
                    - (3.*c2_ - 1.)*inv_r3*mesh_(i, F);
                b_[row] = 
                    + 2.*inv_r3*(
                            - inv_r2*mesh_(i - 1, V)
                            + (inv_r + inv_r2)*mesh_(i, V)
                            - inv_r*mesh_(i + 1, V))
                    + (3.*c2_ - 1.)*inv_r3*mesh_(i, F)*(
                            + mesh_(i + 1, V)
                            - mesh_(i - 1, V))
                    - 2.*c2_*mesh_(i, FR)*mesh_(i, V);
                if (unsteady)
                    b_[row] +=
                        + 2.*inv_t*(
                                + mesh_(i, V)
                                - prev_(i, V))
                        - v1xx_o
                        + (3.*c2_ - 1.)*prev_(i, F)*v1x_o
                        - 2.*c2_*prev_(i, FR)*prev_(i, V);
                ++row;
            }

            A_(row, col(i, F)) = -inv_r;
            A_(row, col(i + 1, F)) = inv_r;
            A_(row, col(i, FR)) = -0.5;
            A_(row, col(i + 1, FR)) = -0.5;
            b_[row] = inv_r*(mesh_(i, F) - mesh_(i + 1, F))
                + 0.5*(mesh_(i, FR) + mesh_(i + 1, FR));
            ++row;
        }
        // Far-field entries at i = num_r - 1
        A_(row, col(num_r - 1, FR)) = 1.;
        b_[row] = -mesh_(num_r - 1, FR);
        ++row;
        A_(row, col(num_r - 1, V)) = 1.;
        b_[row] = S_ - mesh_(num_r - 1, V);
        ++row;

        if (row != num_r*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Edge_Streamfunction::Edge_Streamfunction(Mesh_1D<double>& mesh):
        c2_(-1.),
        S_(0.),
        t_(0.),
        ext_rot_(NULL),
        A_(mesh.num_nodes()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        prev_(mesh),
        mesh_(mesh)
    {
        if (mesh_.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Edge_Streamfunction::Edge_Streamfunction",
                    "Mesh_1D object with num_vars_ = 3 required");
    }

    Edge_Streamfunction::~Edge_Streamfunction()
    {
    }

    void Edge_Streamfunction::solve_steady()
    {
        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);

#if not defined QUIET && defined DEBUG
        console_out(" 1D solver\n ---------\n");
#endif  // !QUIET && DEBUG

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix();

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t k(0); k < num_vars; ++k)
                    mesh_(i, k) += b_[num_vars*i + k];
            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    void Edge_Streamfunction::step(const double& dt)
    {
#ifdef PARANOID
        if (dt <= 0)
            throw Exceptions::Invalid_Argument("PDE_IBVP::step",
                    "dt > 0 is required");
#endif  // PARANOID

        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);
        t_ += dt;

#ifndef QUIET
        console_out(" 1D solver, t = " + str_convert(t_) + "\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

        prev_.set_vars_vector(mesh_.get_vars_vector());

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix(dt);

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t k(0); k < num_vars; ++k)
                    mesh_(i, k) += b_[num_vars*i + k];

            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_1D<double>& Edge_Streamfunction::last_solution() const
    {
        return prev_;
    }

    void Edge_Streamfunction::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        std::size_t old_precision(out.precision());
        out.precision(decimal_places);
        out << "# Mesh_1D\n";
        out << "# Solution at t = " << t_ << '\n';
        out << "# r    u    v    w\n";
        for (std::size_t i(0), num_r(mesh_.num_nodes()); i < num_r;
                i += output_skip + 1)
        {
            out << mesh_.node(i) << ' ';
            out << -mesh_(i, FR) << ' ';
            out << mesh_(i, V) << ' ';
            out << (3.*c2_ - 1.)*mesh_(i, F) << '\n';
        }
        out << "\n\n";
        out.precision(old_precision);
    }
}   // namespace

