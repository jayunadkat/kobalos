#include <cmath>

#include <boundary_layer/Rotating_Fourier_Decomposed_Edge.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Rotating_Fourier_Decomposed_Edge::tolerance_ = 1e-8;
    std::size_t Rotating_Fourier_Decomposed_Edge::max_iterations_ = 50;

    Rotating_Fourier_Decomposed_Edge::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars) :
        nv_(num_vars)
    {
    }

    Rotating_Fourier_Decomposed_Edge::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Rotating_Fourier_Decomposed_Edge::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& k)
    {
        return nv_*i + k;
    }

    void Rotating_Fourier_Decomposed_Edge::assemble_matrix(const double& dt)
    {
        if (c2_ < 0)
            throw Exceptions::Generic("Solution",
                    "Call to set_edge expected before solving");

        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2a(0.), inv_r2b(0.), inv_r3(0.);
        double sf2dx(0.), sv1dx(0.);
        Matrix_Column col(num_vars);

        A_.assign(num_r*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_r*num_vars, 0.);

        // Sphere surface entries at i = 0
        A_(row, col(0, f)) = 1.;
        b_[row] = -mesh_(0, f);
        ++row;
        A_(row, col(0, fr)) = 1.;
        b_[row] = -mesh_(0, fr);
        ++row;
        A_(row, col(0, v)) = 1.;
        b_[row] = 1. - mesh_(0, v);
        ++row;

        // Interior of boundary layer
        for (std::size_t i(0); i < num_r - 1; ++i)
        {
            inv_r = 1./((mesh_.node(i + 1) - mesh_.node(i))*
                    mesh_.map_ds(
                        0.5*(mesh_.node(i) + mesh_.node(i + 1))));
            if (i > 0)
            {
                inv_r3 = 1./((mesh_.node(i + 1) - mesh_.node(i - 1))*
                        mesh_.map_ds(mesh_.node(i)));
                inv_r2a = -mesh_.map_ds2(mesh_.node(i))/(
                        (mesh_.node(i + 1) - mesh_.node(i - 1))*
                        std::pow(mesh_.map_ds(mesh_.node(i)), 3.));
                inv_r2b = std::pow(1./(
                            (mesh_.node(i + 1) - mesh_.node(i))*
                            mesh_.map_ds(mesh_.node(i))), 2.);
            }
            if (i > 0)
            {
                sf2dx = steady_(i + 1, fr) - steady_(i - 1, fr);
                sv1dx = steady_(i + 1, v) - steady_(i - 1, v);
            }

            // One equation mid-node in r, two at nodal locations
            if (i > 0)
            {
                A_(row, col(i, f)) = -Ro_*(3.*c2_ - 1.)*inv_r3*sf2dx;
                A_(row, col(i - 1, fr)) =
                    - inv_r2a
                    + inv_r2b
                    + Ro_*(3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, fr)) =
                    + Ro_*(
                            - std::complex<double>(0., 1.)*omega_
                            + 2.*(2.*c2_ - 1.)*steady_(i, fr)
                          )
                    - 2.*inv_r2b;
                A_(row, col(i + 1, fr)) =
                    + inv_r2a
                    + inv_r2b
                    - Ro_*(3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, v)) = -2.*(Ro_*steady_(i, v) + 1.);

                b_[row] = 0.;
                ++row;

                A_(row, col(i, f)) = -Ro_*(3.*c2_ - 1.)*inv_r3*sv1dx;
                A_(row, col(i, fr)) =
                    + 2.*Ro_*c2_*steady_(i, v)
                    + 2*c2_;
                A_(row, col(i - 1, v)) =
                    - inv_r2a
                    + inv_r2b
                    + Ro_*(3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, v)) =
                    + Ro_*(
                            - std::complex<double>(0., 1.)*omega_
                            + 2.*c2_*steady_(i, fr)
                          )
                    - 2.*inv_r2b;
                A_(row, col(i + 1, v)) =
                    + inv_r2a
                    + inv_r2b
                    - Ro_*(3.*c2_ - 1.)*inv_r3*steady_(i, f);

                b_[row] = 0.;
                ++row;
            }

            A_(row, col(i, f)) = -inv_r;
            A_(row, col(i + 1, f)) = inv_r;
            A_(row, col(i, fr)) = -0.5;
            A_(row, col(i + 1, fr)) = -0.5;
            b_[row] = 0.;
            ++row;
        }
        // Far-field entries at i = num_r - 1
        /*A_(row, col(num_r - 1, fr)) = 1.;
        b_[row] = -mesh_(num_r - 1, fr);
        ++row;
        A_(row, col(num_r - 1, v)) = 1.;
        b_[row] = -mesh_(num_r - 1, v);
        ++row;*/
        /*A_(row, col(num_r - 3, fr)) = 1.;
        A_(row, col(num_r - 2, fr)) = -4.;
        A_(row, col(num_r - 1, fr)) = 3.;
        b_[row] = -mesh_(num_r - 3, fr) + 4.*mesh_(num_r - 2, fr)
                - 3.*mesh_(num_r - 1, fr);
        ++row;
        A_(row, col(num_r - 3, v)) = 1.;
        A_(row, col(num_r - 2, v)) = -4.;
        A_(row, col(num_r - 1, v)) = 3.;
        b_[row] = -mesh_(num_r - 3, v) + 4.*mesh_(num_r - 2, v)
                - 3.*mesh_(num_r - 1, v);
        ++row;*/
        A_(row, col(num_r - 2, fr)) = -1.;
        A_(row, col(num_r - 1, fr)) = 1.;
        b_[row] = mesh_(num_r - 2, fr) - mesh_(num_r - 1, fr);
        ++row;
        A_(row, col(num_r - 2, v)) = -1.;
        A_(row, col(num_r - 1, v)) = 1.;
        b_[row] = mesh_(num_r - 2, v) - mesh_(num_r - 1, v);
        ++row;

        if (row != num_r*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Rotating_Fourier_Decomposed_Edge::Rotating_Fourier_Decomposed_Edge(
            Mesh_1D<double>& mesh_steady, Mesh_1D<std::complex<double> >& mesh):
        c2_(-1.),
        omega_(0.),
        A_(mesh.num_nodes()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        steady_(mesh_steady),
        mesh_(mesh)
    {
#ifdef PARANOID
        if (mesh_.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Rotating_Fourier_Decomposed_Edge::"
                    "Rotating_Fourier_Decomposed_Edge",
                    "Mesh_1D object with num_vars_ = 3 required");
        if (mesh.num_nodes() != mesh_steady.num_nodes() ||
                mesh.num_vars() != mesh_steady.num_vars())
            throw Exceptions::Invalid_Argument(
                    "Rotating_Fourier_Decomposed_Edge::"
                    "Rotating_Fourier_Decomposed_Edge",
                    "Mismatching mesh objects supplied to solver");
#endif  // PARANOID
    }

    Rotating_Fourier_Decomposed_Edge::~Rotating_Fourier_Decomposed_Edge()
    {
    }

    void Rotating_Fourier_Decomposed_Edge::solve()
    {
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());

#ifndef QUIET
        console_out(" 1D solver\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

#if not defined QUIET && defined TIME
        Timer t_matrix("Matrix assembly");
        t_matrix.start();
#endif  // !QUIET && TIME

        assemble_matrix();

#if not defined QUIET && defined TIME
        t_matrix.stop();
        t_matrix.print();
#endif  // !QUIET && TIME

        linear_system_.solve();

#if not defined QUIET && defined TIME
        console_out.endl();
#endif  // !QUIET && TIME

        for (std::size_t i(0); i < num_r; ++i)
            for (std::size_t k(0); k < num_vars; ++k)
                mesh_(i, k) = b_[num_vars*i + k];
    }

    const Mesh_1D<std::complex<double> >&
        Rotating_Fourier_Decomposed_Edge::solution() const
    {
        return mesh_;
    }

    void Rotating_Fourier_Decomposed_Edge::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        std::size_t old_precision(out.precision());
        out.precision(decimal_places);
        out << "# Mesh_1D\n";
        out << "# r    u    v    w\n";
        std::complex<double> temp;
        for (std::size_t i(0), num_r(mesh_.num_nodes()); i < num_r;
                i += output_skip + 1)
        {
            out << mesh_.node(i) << ' ';
            temp = -mesh_(i, fr);
            out << temp.real() << ' ' << temp.imag() << ' ';
            out << mesh_(i, v).real() << ' ' << mesh_(i, v).imag() << ' ';
            temp = (3.*c2_ - 1.)*mesh_(i, f);
            out << temp.real() << ' ' << temp.imag() << '\n';
        }
        out << "\n\n";
        out.flush();
        out.precision(old_precision);
    }
}   // namespace

