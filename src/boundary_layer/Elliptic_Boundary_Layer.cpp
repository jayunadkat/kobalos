#include <cmath>

#include <boundary_layer/Elliptic_Boundary_Layer.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Elliptic_Boundary_Layer::tolerance_ = 1e-8;
    std::size_t Elliptic_Boundary_Layer::max_iterations_ = 50;

    Elliptic_Boundary_Layer::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars, const std::size_t& num_r) :
        nv_(num_vars),
        nr_(num_r)
    {
    }

    Elliptic_Boundary_Layer::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Elliptic_Boundary_Layer::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& j, const std::size_t& k)
    {
        return nv_*(nr_*j + i) + k;
    }

    double Elliptic_Boundary_Layer::bump(const double& theta, const double& t)
    {
        if (ext_bump2_ == NULL)
        {
            if (ext_bump_ == NULL)
                return 1.;
            else
                return (*ext_bump_)(t_);
        }
        else
            return (*ext_bump2_)(theta, t_);
    }

    void Elliptic_Boundary_Layer::assemble_matrix_3(const double& dt)
    {
        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2(0.), inv_r3(0.), inv_theta(0.), inv_t(0.);
        double s(0.), c(0.), c2(0.), C1(0.);
        double f1dy(0.), f2dx(0.), f2dy(0.), v1dx(0.), v1dy(0.);
        double f1y_o(0.), f2x_o(0.), f2y_o(0.), f2xx_o(0.);
        double v1x_o(0.), v1y_o(0.), v1xx_o(0.);
        Vector<double> mid_state(num_vars, 0.);
        Matrix_Column col(num_vars, num_r);

        A_.assign(num_r*num_theta*num_vars);
        b_.assign(num_r*num_theta*num_vars, 0.);

        // Equator entries at j = 0, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, num_vars*i + k) = 1.;
                b_[row] = mesh_pole_(i, k) - mesh_(i, 0, k);
                ++row;
            }
        }
        // Interior theta values
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            // Sphere surface entries at i = 0, j.
            A_(row, col(0, j, F)) = 1.;
            b_[row] = -mesh_(0, j, F);
            ++row;
            A_(row, col(0, j, FR)) = 1.;
            b_[row] = -mesh_(0, j, FR);
            ++row;
            A_(row, col(0, j, V)) = 1.;
            if (unsteady)
                b_[row] = bump(mesh_.node_y(j), t_) - mesh_(0, j, V);
            else
                b_[row] = 1. - mesh_(0, j, V);
            ++row;

            // Interior of boundary layer
            for (std::size_t i(0); i < num_r - 1; ++i)
            {
                inv_r = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
                if (i > 0)
                {
                    inv_r2 = 1./(mesh_.node_x(i) - mesh_.node_x(i - 1));
                    inv_r3 = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i - 1));
                }
                inv_theta = 1./(mesh_.node_y(j + 1) - mesh_.node_y(j - 1));
                s = std::sin(mesh_.node_y(j));
                c = std::cos(mesh_.node_y(j));
                c2 = c*c;
                C1 = s*c*inv_theta;
                if (i > 0)
                {
                    f1dy = mesh_(i, j + 1, F) - mesh_(i, j - 1, F);
                    f2dx = mesh_(i + 1, j, FR) - mesh_(i - 1, j, FR);
                    f2dy = mesh_(i, j + 1, FR) - mesh_(i, j - 1, FR);
                    v1dx = mesh_(i + 1, j, V) - mesh_(i - 1, j, V);
                    v1dy = mesh_(i, j + 1, V) - mesh_(i, j - 1, V);
                }
                if (unsteady && i > 0)
                {
                    inv_t = 1./dt;
                    f1y_o = inv_theta*(
                            + prev_(i, j + 1, F)
                            - prev_(i, j - 1, F));
                    f2x_o = inv_r3*(
                            + prev_(i + 1, j, FR)
                            - prev_(i - 1, j, FR));
                    f2y_o = inv_theta*(
                            + prev_(i, j + 1, FR)
                            - prev_(i, j - 1, FR));
                    f2xx_o = 2.*inv_r3*(
                            + inv_r2*prev_(i - 1, j, FR)
                            - (inv_r + inv_r2)*prev_(i, j, FR)
                            + inv_r*prev_(i + 1, j, FR));
                    v1x_o = inv_r3*(
                            + prev_(i + 1, j, V)
                            - prev_(i - 1, j, V));
                    v1y_o = inv_theta*(
                            + prev_(i, j + 1, V)
                            - prev_(i, j - 1, V));
                    v1xx_o = 2.*inv_r3*(
                            + inv_r2*prev_(i - 1, j, V)
                            - (inv_r + inv_r2)*prev_(i, j, V)
                            + inv_r*prev_(i + 1, j, V));
                }

                // One equation mid-node in r, two at nodal locations
                if (i > 0)
                {
                    A_(row, col(i, j, F)) = -(3.*c2 - 1.)*inv_r3*f2dx;
                    A_(row, col(i - 1, j, FR)) = 2.*inv_r3*inv_r2
                        + C1*inv_r3*f1dy + (3.*c2 - 1.)*inv_r3*mesh_(i, j, F);
                    A_(row, col(i, j, FR)) = -2.*inv_r3*(inv_r + inv_r2)
                        + C1*f2dy + 2.*(2.*c2 - 1.)*mesh_(i, j, FR);
                    if (unsteady)
                        A_(row, col(i, j, FR)) += -2.*inv_t;
                    A_(row, col(i + 1, j, FR)) = 2.*inv_r3*inv_r
                        - C1*inv_r3*f1dy - (3.*c2 - 1.)*inv_r3*mesh_(i, j, F);
                    A_(row, col(i, j, V)) = -2.*mesh_(i, j, V);

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, F)) = C1*inv_r3*f2dx;
                    A_(row, col(i, j + 1, F)) = -C1*inv_r3*f2dx;
                    A_(row, col(i, j - 1, FR)) = -C1*mesh_(i, j, FR);
                    A_(row, col(i, j + 1, FR)) = C1*mesh_(i, j, FR);

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        + 2.*inv_r3*(
                                - inv_r2*mesh_(i - 1, j, FR)
                                + (inv_r + inv_r2)*mesh_(i, j, FR)
                                - inv_r*mesh_(i + 1, j, FR))
                        + C1*inv_r3*f2dx*f1dy
                        - C1*f2dy*mesh_(i, j, FR)
                        + (3.*c2 - 1.)*inv_r3*f2dx*mesh_(i, j, F)
                        - (2.*c2 - 1.)*mesh_(i, j, FR)*mesh_(i, j, FR)
                        + mesh_(i, j, V)*mesh_(i, j, V)
                        - S_*S_;
                    if (unsteady)
                        b_[row] +=
                            + 2.*inv_t*(
                                    + mesh_(i, j, FR)
                                    - prev_(i, j, FR))
                            - f2xx_o
                            + s*c*f2x_o*f1y_o
                            - s*c*prev_(i, j, FR)*f2y_o
                            + (3.*c2 - 1.)*prev_(i, j, F)*f2x_o
                            - (2.*c2 - 1.)*prev_(i, j, FR)*prev_(i, j, FR)
                            + prev_(i, j, V)*prev_(i, j, V)
                            - S_*S_;
                    ++row;

                    A_(row, col(i, j, F)) = -(3.*c2 - 1.)*inv_r3*v1dx;
                    A_(row, col(i, j, FR)) = C1*v1dy + 2.*c2*mesh_(i, j, V);
                    A_(row, col(i - 1, j, V)) = 2.*inv_r3*inv_r2
                        + C1*inv_r3*f1dy + (3.*c2 - 1.)*inv_r3*mesh_(i, j, F);
                    A_(row, col(i, j, V)) = -2.*inv_r3*(inv_r + inv_r2)
                        + 2.*c2*mesh_(i, j, FR);
                    if (unsteady)
                        A_(row, col(i, j, V)) += -2.*inv_t;
                    A_(row, col(i + 1, j, V)) = 2.*inv_r3*inv_r
                        - C1*inv_r3*f1dy - (3.*c2 - 1.)*inv_r3*mesh_(i, j, F);

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, F)) = C1*inv_r3*v1dx;
                    A_(row, col(i, j + 1, F)) = -C1*inv_r3*v1dx;
                    A_(row, col(i, j - 1, V)) = -C1*mesh_(i, j, FR);
                    A_(row, col(i, j + 1, V)) = C1*mesh_(i, j, FR);

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        + 2.*inv_r3*(
                                - inv_r2*mesh_(i - 1, j, V)
                                + (inv_r + inv_r2)*mesh_(i, j, V)
                                - inv_r*mesh_(i + 1, j, V))
                        - C1*mesh_(i, j, FR)*v1dy
                        + C1*inv_r3*v1dx*f1dy
                        + (3.*c2 - 1.)*inv_r3*v1dx*mesh_(i, j, F)
                        - 2.*c2*mesh_(i, j, FR)*mesh_(i, j, V);
                    if (unsteady)
                        b_[row] +=
                            + 2.*inv_t*(
                                    + mesh_(i, j, V)
                                    - prev_(i, j, V))
                            - v1xx_o
                            - s*c*prev_(i, j, FR)*v1y_o
                            + s*c*v1x_o*f1y_o
                            + (3.*c2 - 1.)*prev_(i, j, F)*v1x_o
                            - 2.*c2*prev_(i, j, FR)*prev_(i, j, V);
                    ++row;
                }

                A_(row, col(i, j, F)) = -inv_r;
                A_(row, col(i + 1, j, F)) = inv_r;
                A_(row, col(i, j, FR)) = -0.5;
                A_(row, col(i + 1, j, FR)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, F) - mesh_(i + 1, j, F))
                    + 0.5*(mesh_(i, j, FR) + mesh_(i + 1, j, FR));
                ++row;
            }
            // Far-field entries at i = num_r - 1, j
            A_(row, col(num_r - 1, j, FR)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, FR);
            ++row;
            A_(row, col(num_r - 1, j, V)) = 1.;
            b_[row] = S_ - mesh_(num_r - 1, j, V);
            ++row;
        }
        // Pole entries at j = num_theta - 1, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, col(i, num_theta - 1, k)) = 1.;
                b_[row] = mesh_equator_(i, k) - mesh_(i, num_theta - 1, k);
                ++row;
            }
        }

        if (row != num_r*num_theta*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    void Elliptic_Boundary_Layer::assemble_matrix_5(const double& dt)
    {
        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_theta(0.), inv_t(0.);
        double s(0.), c(0.), s2(0.), c2(0.);
        double f1y(0.), f2y(0.), v1y(0.), f2f(0.), f3f(0.), v2f(0.);
        double f3x_o(0.), v2x_o(0.), f1y_o(0.), f2y_o(0.), v1y_o(0.);
        Vector<double> mid_state(num_vars, 0.);
        Matrix_Column col(num_vars, num_r);

        A_.assign(num_r*num_theta*num_vars);
        b_.assign(num_r*num_theta*num_vars, 0.);

        // Equator entries at j = 0, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, num_vars*i + k) = 1.;
                b_[row] = mesh_pole_(i, k) - mesh_(i, 0, k);
                ++row;
            }
        }
        // Interior theta values
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            // Sphere surface entries at i = 0, j.
            A_(row, col(0, j, f)) = 1.;
            b_[row] = -mesh_(0, j, f);
            ++row;
            A_(row, col(0, j, fr)) = 1.;
            b_[row] = -mesh_(0, j, fr);
            ++row;
            A_(row, col(0, j, v)) = 1.;
            b_[row] = 1. - mesh_(0, j, v);
            ++row;
            // Interior of boundary layer
            for (std::size_t i(0); i < num_r - 1; ++i)
            {
                // Note mesh_ is uniform in theta so 2*dtheta is
                // allowable. mesh_ is non-uniform in r but we are
                // mid-node in r so this is fine
                inv_r = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
                inv_theta = 1./(mesh_.node_y(j + 1)
                        - mesh_.node_y(j - 1));
                s = std::sin(mesh_.node_y(j));
                s2 = s*s;
                c = std::cos(mesh_.node_y(j));
                c2 = c*c;
                f1y = 0.25*s*c*inv_theta*(
                        + mesh_(i, j + 1, f)
                        + mesh_(i + 1, j + 1, f)
                        - mesh_(i, j - 1, f)
                        - mesh_(i + 1, j - 1, f));
                f2y = 0.25*s*c*inv_theta*(
                        + mesh_(i, j + 1, fr)
                        + mesh_(i + 1, j + 1, fr)
                        - mesh_(i, j - 1, fr)
                        - mesh_(i + 1, j - 1, fr));
                v1y = 0.25*s*c*inv_theta*(
                        + mesh_(i, j + 1, v)
                        + mesh_(i + 1, j + 1, v)
                        - mesh_(i, j - 1, v)
                        - mesh_(i + 1, j - 1, v));
                f2f = 0.25*s*c*inv_theta*(
                        + mesh_(i, j, fr)
                        + mesh_(i + 1, j, fr));
                f3f = 0.25*s*c*inv_theta*(
                        + mesh_(i, j, frr)
                        + mesh_(i + 1, j, frr));
                v2f = 0.25*s*c*inv_theta*(
                        + mesh_(i, j, vr)
                        + mesh_(i + 1, j, vr));
                if (unsteady)
                {
                    inv_t = 1./dt;
                    f3x_o = inv_r*(
                            + prev_(i + 1, j, frr)
                            - prev_(i, j, frr));
                    v2x_o = inv_r*(
                            + prev_(i + 1, j, vr)
                            - prev_(i, j, vr));
                    f1y_o = 0.5*inv_theta*(
                            + prev_(i, j + 1, f)
                            + prev_(i + 1, j + 1, f)
                            - prev_(i, j - 1, f)
                            - prev_(i + 1, j - 1, f));
                    f2y_o = 0.5*inv_theta*(
                            + prev_(i, j + 1, fr)
                            + prev_(i + 1, j + 1, fr)
                            - prev_(i, j - 1, fr)
                            - prev_(i + 1, j - 1, fr));
                    v1y_o = 0.5*inv_theta*(
                            + prev_(i, j + 1, v)
                            + prev_(i + 1, j + 1, v)
                            - prev_(i, j - 1, v)
                            - prev_(i + 1, j - 1, v));
                }

                // num_vars equations at each point
                A_(row, col(i, j, f)) = -inv_r;
                A_(row, col(i + 1, j, f)) = inv_r;
                A_(row, col(i, j, fr)) = -0.5;
                A_(row, col(i + 1, j, fr)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, f) - mesh_(i + 1, j, f))
                    + 0.5*(mesh_(i, j, fr) + mesh_(i + 1, j, fr));
                ++row;
                A_(row, col(i, j, fr)) = -inv_r;
                A_(row, col(i + 1, j, fr)) = inv_r;
                A_(row, col(i, j, frr)) = -0.5;
                A_(row, col(i + 1, j, frr)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, fr) - mesh_(i + 1, j, fr))
                    + 0.5*(mesh_(i, j, frr) + mesh_(i + 1, j, frr));
                ++row;
                A_(row, col(i, j, f)) = (0.5*s2 - c2)*mesh_(i, j, frr);
                A_(row, col(i + 1, j, f)) =
                    (0.5*s2 - c2)*mesh_(i + 1, j, frr);
                A_(row, col(i, j, fr)) = f2y + (c2 - s2)*mesh_(i, j, fr);
                if (unsteady)
                    A_(row, col(i, j, fr)) += -inv_t;
                A_(row, col(i + 1, j, fr)) = f2y
                    + (c2 - s2)*mesh_(i + 1, j, fr);
                if (unsteady)
                    A_(row, col(i + 1, j, fr)) += -inv_t;
                A_(row, col(i, j, frr)) = -inv_r - f1y
                    + (0.5*s2 - c2)*mesh_(i, j, f);
                A_(row, col(i + 1, j, frr)) = inv_r - f1y
                    + (0.5*s2 - c2)*mesh_(i + 1, j, f);
                A_(row, col(i, j, v)) = -mesh_(i, j, v);
                A_(row, col(i + 1, j, v)) = -mesh_(i + 1, j, v);

                /*============================================================
                 * ==========================================================
                 *
                 *                 FULL CENTRAL DIFFERENCING                  */

                A_(row, col(i, j - 1, f)) = f3f;
                A_(row, col(i + 1, j - 1, f)) = f3f;
                A_(row, col(i, j + 1, f)) = -f3f;
                A_(row, col(i + 1, j + 1, f)) = -f3f;
                A_(row, col(i, j - 1, fr)) = -f2f;
                A_(row, col(i + 1, j - 1, fr)) = -f2f;
                A_(row, col(i, j + 1, fr)) = f2f;
                A_(row, col(i + 1, j + 1, fr)) = f2f;

                /* ==========================================================
                 *============================================================*/

                b_[row] = 
                    + inv_r*(mesh_(i, j, frr) - mesh_(i + 1, j, frr))
                    + f1y*(mesh_(i, j, frr) + mesh_(i + 1, j, frr))
                    - f2y*(mesh_(i, j, fr) + mesh_(i + 1, j, fr))
                    + (c2 - 0.5*s2)*(
                            + mesh_(i, j, f)*mesh_(i, j, frr)
                            + mesh_(i + 1, j, f)*mesh_(i + 1, j, frr))
                    + 0.5*(s2 - c2)*(
                            + mesh_(i, j, fr)*mesh_(i, j, fr)
                            + mesh_(i + 1, j, fr)*mesh_(i + 1, j, fr))
                    + 0.5*(
                            + mesh_(i, j, v)*mesh_(i, j, v)
                            + mesh_(i + 1, j, v)*mesh_(i + 1, j, v))
                    - S_*S_;
                if (unsteady)
                    b_[row] +=
                        + inv_t*(
                                + mesh_(i, j, fr)
                                + mesh_(i + 1, j, fr)
                                - prev_(i, j, fr)
                                - prev_(i + 1, j, fr))
                        - f3x_o
                        + s*c*0.5*(
                                + prev_(i, j, frr)
                                + prev_(i + 1, j, frr))*f1y_o
                        - s*c*0.5*(
                                + prev_(i, j, fr)
                                + prev_(i + 1, j, fr))*f2y_o
                        + (2*c2 - s2)*0.5*(
                                + prev_(i, j, f)*prev_(i, j, frr)
                                + prev_(i + 1, j, f)*prev_(i + 1, j, frr))
                        + (s2 - c2)*0.5*(
                                + prev_(i, j, fr)*prev_(i, j, fr)
                                + prev_(i + 1, j, fr)*prev_(i + 1, j, fr))
                        + 0.5*(
                                + prev_(i, j, v)*prev_(i, j, v)
                                + prev_(i + 1, j, v)*prev_(i + 1, j, v))
                        - S_*S_;
                ++row;
                A_(row, col(i, j, v)) = -inv_r;
                A_(row, col(i + 1, j, v)) = inv_r;
                A_(row, col(i, j, vr)) = -0.5;
                A_(row, col(i + 1, j, vr)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, v) - mesh_(i + 1, j, v))
                    + 0.5*(mesh_(i, j, vr) + mesh_(i + 1, j, vr));
                ++row;
                A_(row, col(i, j, f)) = (0.5*s2 - c2)*mesh_(i, j, vr);
                A_(row, col(i + 1, j, f)) =
                    (0.5*s2 - c2)*mesh_(i + 1, j, vr);
                A_(row, col(i, j, fr)) = v1y + c2*mesh_(i, j, v);
                A_(row, col(i + 1, j, fr)) = v1y + c2*mesh_(i + 1, j, v);
                A_(row, col(i, j, v)) = c2*mesh_(i, j, fr);
                if (unsteady)
                    A_(row, col(i, j, v)) += -inv_t;
                A_(row, col(i + 1, j, v)) = c2*mesh_(i + 1, j, fr);
                if (unsteady)
                    A_(row, col(i + 1, j, v)) += -inv_t;
                A_(row, col(i, j, vr)) = -inv_r - f1y
                    + (0.5*s2 - c2)*mesh_(i, j, f);
                A_(row, col(i + 1, j, vr)) = inv_r - f1y
                    + (0.5*s2 - c2)*mesh_(i + 1, j, f);

                /*============================================================
                 * ==========================================================
                 *
                 *                 FULL CENTRAL DIFFERENCING                  */

                A_(row, col(i, j - 1, f)) = v2f;
                A_(row, col(i + 1, j - 1, f)) = v2f;
                A_(row, col(i, j + 1, f)) = -v2f;
                A_(row, col(i + 1, j + 1, f)) = -v2f;
                A_(row, col(i, j - 1, v)) = -f2f;
                A_(row, col(i + 1, j - 1, v)) = -f2f;
                A_(row, col(i, j + 1, v)) = f2f;
                A_(row, col(i + 1, j + 1, v)) = f2f;

                /* ==========================================================
                 *============================================================*/

                b_[row] = 
                    + inv_r*(mesh_(i, j, vr) - mesh_(i + 1, j, vr))
                    - v1y*(mesh_(i, j, fr) + mesh_(i + 1, j, fr))
                    + f1y*(mesh_(i, j, vr) + mesh_(i + 1, j, vr))
                    + (c2 - 0.5*s2)*(
                            + mesh_(i, j, f)*mesh_(i, j, vr)
                            + mesh_(i + 1, j, f)*mesh_(i + 1, j, vr))
                    - c2*(
                            + mesh_(i, j, fr)*mesh_(i, j, v)
                            + mesh_(i + 1, j, fr)*mesh_(i + 1, j, v));
                if (unsteady)
                    b_[row] +=
                        + inv_t*(
                                + mesh_(i, j, v)
                                + mesh_(i + 1, j, v)
                                - prev_(i, j, v)
                                - prev_(i + 1, j, v))
                        - v2x_o
                        - s*c*0.5*(
                                + prev_(i, j, fr)
                                + prev_(i + 1, j, fr))*v1y_o
                        + s*c*0.5*(
                                + prev_(i, j, vr)
                                + prev_(i + 1, j, vr))*f1y_o
                        + (2*c2 - s2)*0.5*(
                                + prev_(i, j, f)*prev_(i, j, vr)
                                + prev_(i + 1, j, f)*prev_(i + 1, j, vr))
                        - c2*(
                                + prev_(i, j, fr)*prev_(i, j, v)
                                + prev_(i + 1, j, fr)*prev_(i + 1, j, v));
                ++row;
            }
            // Far-field entries at i = num_r - 1, j
            A_(row, col(num_r - 1, j, fr)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 1, j, v)) = 1.;
            b_[row] = S_ - mesh_(num_r - 1, j, v);
            ++row;
        }
        // Pole entries at j = num_theta - 1, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, col(i, num_theta - 1, k)) = 1.;
                b_[row] = mesh_equator_(i, k) - mesh_(i, num_theta - 1, k);
                ++row;
            }
        }

        if (row != num_r*num_theta*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Elliptic_Boundary_Layer::Elliptic_Boundary_Layer(
            Mesh_1D<double>& mesh_pole, Mesh_1D<double>& mesh_equator,
            Mesh_2D<double>& mesh_bl):
        S_(0.),
        t_(0.),
        ext_bump_(NULL),
        ext_bump2_(NULL),
        A_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars()),
        b_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars(), 0.),
        linear_system_(A_, b_),
        mesh_equator_(mesh_equator),
        mesh_pole_(mesh_pole),
        prev_(mesh_bl),
        mesh_(mesh_bl)
    {
    }

    Elliptic_Boundary_Layer::~Elliptic_Boundary_Layer()
    {
    }

    void Elliptic_Boundary_Layer::solve_steady()
    {
        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);

#if not defined QUIET && defined DEBUG
        console_out(" 2D solver\n ---------\n");
#endif  // !QUIET && DEBUG

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            switch (num_vars)
            {
                case 3:
                    assemble_matrix_3();
                    break;
                case 5:
                    assemble_matrix_5();
                    break;
                default:
                    throw Exceptions::Generic("Solution",
                            "Mesh_2D object provided has incorrect num_vars_");
            }

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t j(0); j < num_theta; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) += b_[num_vars*(num_r*j + i) + k];
            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    void Elliptic_Boundary_Layer::step(const double& dt)
    {
#ifdef PARANOID
        if (dt <= 0)
            throw Exceptions::Invalid_Argument("PDE_IBVP::step",
                    "dt > 0 is required");
#endif  // PARANOID

        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);
        t_ += dt;

#ifndef QUIET
        console_out(" 2D solver, t = " + str_convert(t_) + "\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

        prev_.set_vars_vector(mesh_.get_vars_vector());

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            switch (num_vars)
            {
                case 3:
                    assemble_matrix_3(dt);
                    break;
                case 5:
                    assemble_matrix_5(dt);
                    break;
                default:
                    throw Exceptions::Generic("Solution",
                            "Mesh_2D object provided has incorrect num_vars_");
            }

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t j(0); j < num_theta; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) += b_[num_vars*(num_r*j + i) + k];

            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_2D<double>& Elliptic_Boundary_Layer::last_solution() const
    {
        return prev_;
    }

    void Elliptic_Boundary_Layer::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        double mid_theta(0.), c(0.), s(0.);
        std::size_t num_vars(mesh_.num_vars());
        std::size_t old_precision(out.precision());

        out.precision(decimal_places);
        out << "# Mesh_2D\n";
        out << "# Solution at t = " << t_ << '\n';
        out << "# theta    r    u    v    w\n";

        switch (num_vars)
        {
            case 3:
                for (std::size_t j(0), num_theta(mesh_.num_nodes_y());
                        j < num_theta - 1; j += output_skip + 1)
                {
                    mid_theta = 0.5*(mesh_.node_y(j) + mesh_.node_y(j + 1));
                    for (std::size_t i(0), num_r(mesh_.num_nodes_x());
                            i < num_r; i += output_skip + 1)
                    {
                        c = std::cos(mid_theta);
                        s = std::sin(mid_theta);
                        out << mid_theta << ' ';
                        out << mesh_.node_x(i) << ' ';
                        out << -0.5*(mesh_(i, j, FR)
                                + mesh_(i, j + 1, FR)) << ' ';
                        out << 0.5*(mesh_(i, j, V) + mesh_(i, j + 1, V)) << ' ';
                        out << (3.*c*c - 1.)*0.5*(mesh_(i, j, F) +
                                mesh_(i, j + 1, F))
                            + s*c*(mesh_(i, j + 1, F) - mesh_(i, j, F))/(
                                    mesh_.node_y(j + 1)
                                    - mesh_.node_y(j)) << '\n';
                    }
                    out << '\n';
                }
                out << '\n';
                break;
            case 5:
                for (std::size_t j(0), num_theta(mesh_.num_nodes_y());
                        j < num_theta - 1; j += output_skip + 1)
                {
                    mid_theta = 0.5*(mesh_.node_y(j) + mesh_.node_y(j + 1));
                    for (std::size_t i(0), num_r(mesh_.num_nodes_x());
                            i < num_r; i += output_skip + 1)
                    {
                        c = std::cos(mid_theta);
                        s = std::sin(mid_theta);
                        out << mid_theta << ' ';
                        out << mesh_.node_x(i) << ' ';
                        out << -0.5*(mesh_(i, j, fr)
                                + mesh_(i, j + 1, fr)) << ' ';
                        out << 0.5*(mesh_(i, j, v) + mesh_(i, j + 1, v)) << ' ';
                        out << (3.*c*c - 1.)*0.5*(mesh_(i, j, f) +
                                mesh_(i, j + 1, f))
                            + s*c*(mesh_(i, j + 1, f) - mesh_(i, j, f))/(
                                    mesh_.node_y(j + 1)
                                    - mesh_.node_y(j)) << '\n';
                    }
                    out << '\n';
                }
                out << '\n';
                break;
            default:
                throw Exceptions::Generic("Dumping data",
                        "Mesh_2D object provided has incorrect num_vars_");
        }
        out.precision(old_precision);
    }
}   // namespace

