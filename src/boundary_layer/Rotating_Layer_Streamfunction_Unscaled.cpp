#include <cmath>

#include <boundary_layer/Rotating_Layer_Streamfunction_Unscaled.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Rotating_Layer_Streamfunction_Unscaled::tolerance_ = 1e-8;
    std::size_t Rotating_Layer_Streamfunction_Unscaled::max_iterations_ = 20;

    Rotating_Layer_Streamfunction_Unscaled::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars, const std::size_t& num_r) :
        nv_(num_vars),
        nr_(num_r)
    {
    }

    Rotating_Layer_Streamfunction_Unscaled::Matrix_Column::~Matrix_Column()
    {
    }

    double Rotating_Layer_Streamfunction_Unscaled::sphere_spin(const double& t)
    {
        if (sphere_spin_)
            return (*sphere_spin_)(t);
        else
            return 1. - std::exp(-t*t);
    }

    double Rotating_Layer_Streamfunction_Unscaled::sphere_wobble(
            const double& t)
    {
        if (sphere_wobble_)
            return (*sphere_wobble_)(t);
        else
            return 0.;
    }

    void Rotating_Layer_Streamfunction_Unscaled::assemble_matrix(
            const double& dt)
    {
        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2a(0.), inv_r2b(0.), inv_r3(0.), inv_theta(0.),
               inv_t(0.);
        double s(0.), c(0.);
        double f1dy(0.), f2dx(0.), f2dy(0.), v1dx(0.), v1dy(0.),
               f2dx2(0.), v1dx2(0.);
        double f1y_o(0.), f2x_o(0.), f2y_o(0.), f2xx_o(0.);
        double v1x_o(0.), v1y_o(0.), v1xx_o(0.);
        Vector<double> mid_state(num_vars, 0.);

        A_.assign(num_r*num_theta*num_vars);
        b_.assign(num_r*num_theta*num_vars, 0.);

        // Polar entries at j = 0, i
        if (mesh_pole_)
        {
            s = std::sin(mesh_.unmapped_node_y(0));
            c = std::cos(mesh_.unmapped_node_y(0));
            
            for (std::size_t i(0); i < num_r; ++i)
            {
                A_(row, col(i, 0, F)) = 1.;
                b_[row] = (*mesh_pole_)(i, F)*s*s*c - mesh_(i, 0, F);
                ++row;
                A_(row, col(i, 0, FR)) = 1.;
                b_[row] = (*mesh_pole_)(i, FR)*s*s*c - mesh_(i, 0, FR);
                ++row;
                A_(row, col(i, 0, V)) = 1.;
                b_[row] = (*mesh_pole_)(i, V)*s - mesh_(i, 0, V);
                ++row;
            }
        }
        else
        {
            for (std::size_t i(0); i < num_r; ++i)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    A_(row, col(i, 0, k)) = 1.;
                    b_[row] = -mesh_(i, 0, k);
                    ++row;
                }
            }
        }

        // Interior theta values
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            inv_theta = 1./((mesh_.node_y(j + 1) - mesh_.node_y(j - 1))*
                    mesh_.map_ds_y(mesh_.node_y(j)));
            s = 1./std::sin(mesh_.unmapped_node_y(j));
            c = std::cos(mesh_.unmapped_node_y(j));

            // Sphere surface entries at i = 0, j.
            A_(row, col(0, j, F)) = 1.;
            b_[row] = -mesh_(0, j, F);
            ++row;
            A_(row, col(0, j, FR)) = 1.;
            b_[row] = -mesh_(0, j, FR);
            ++row;
            A_(row, col(0, j, V)) = 1.;
            if (unsteady)
                b_[row] =
                    + sphere_spin(t_)*(sgn_ + sphere_wobble(t_))*std::sin(
                            mesh_.unmapped_node_y(j))
                    - mesh_(0, j, V);
            else
                b_[row] =
                    + std::sin(mesh_.unmapped_node_y(j))*sgn_
                    - mesh_(0, j, V);
            ++row;

            // Interior of boundary layer
            for (std::size_t i(0); i < num_r - 1; ++i)
            {
                inv_r = 1./((mesh_.node_x(i + 1) - mesh_.node_x(i))*
                        mesh_.map_ds_x(
                            0.5*(mesh_.node_x(i) + mesh_.node_x(i + 1))));
                if (i > 0)
                {
                    inv_r3 = 1./((mesh_.node_x(i + 1) - mesh_.node_x(i - 1))*
                            mesh_.map_ds_x(mesh_.node_x(i)));
                    inv_r2a = -mesh_.map_ds2_x(mesh_.node_x(i))/(
                            (mesh_.node_x(i + 1) - mesh_.node_x(i - 1))*
                            std::pow(mesh_.map_ds_x(mesh_.node_x(i)), 3.));
                    inv_r2b = std::pow(1./(
                                (mesh_.node_x(i + 1) - mesh_.node_x(i))*
                                mesh_.map_ds_x(mesh_.node_x(i))), 2.);
                    f1dy = mesh_(i, j + 1, F) - mesh_(i, j - 1, F);
                    f2dx = mesh_(i + 1, j, FR) - mesh_(i - 1, j, FR);
                    f2dy = mesh_(i, j + 1, FR) - mesh_(i, j - 1, FR);
                    v1dx = mesh_(i + 1, j, V) - mesh_(i - 1, j, V);
                    v1dy = mesh_(i, j + 1, V) - mesh_(i, j - 1, V);
                    f2dx2 = 
                        + mesh_(i + 1, j, FR)
                        - 2.*mesh_(i, j, FR)
                        + mesh_(i - 1, j, FR);
                    v1dx2 =
                        + mesh_(i + 1, j, V)
                        - 2.*mesh_(i, j, V)
                        + mesh_(i - 1, j, V);
                    if (unsteady)
                    {
                        inv_t = 1./dt;
                        f1y_o = inv_theta*(
                                + prev_(i, j + 1, F)
                                - prev_(i, j - 1, F));
                        f2x_o = inv_r3*(
                                + prev_(i + 1, j, FR)
                                - prev_(i - 1, j, FR));
                        f2y_o = inv_theta*(
                                + prev_(i, j + 1, FR)
                                - prev_(i, j - 1, FR));
                        f2xx_o = inv_r2a*(
                                    + prev_(i + 1, j, FR)
                                    - prev_(i - 1, j, FR))
                            + inv_r2b*(
                                    + prev_(i - 1, j, FR)
                                    - 2.*prev_(i, j, FR)
                                    + prev_(i + 1, j, FR));
                        v1x_o = inv_r3*(
                                + prev_(i + 1, j, V)
                                - prev_(i - 1, j, V));
                        v1y_o = inv_theta*(
                                + prev_(i, j + 1, V)
                                - prev_(i, j - 1, V));
                        v1xx_o = inv_r2a*(
                                + prev_(i + 1, j, V)
                                - prev_(i - 1, j, V))
                            + inv_r2b*(
                                    + prev_(i - 1, j, V)
                                    - 2.*prev_(i, j, V)
                                    + prev_(i + 1, j, V));
                    }
                }

                // One equation mid-node in r, two at nodal locations
                if (i > 0)
                {
                    A_(row, col(i - 1, j, FR)) =
                        + Ro_*s*s*inv_theta*f1dy*inv_r3
                        - s*inv_r2a
                        + s*inv_r2b;
                    A_(row, col(i, j, FR)) =
                        - 2.*s*inv_r2b
                        + Ro_*(
                                - 2.*c*s*s*s*mesh_(i, j, FR)
                                + s*s*inv_theta*f2dy
                              );
                    if (unsteady)
                        A_(row, col(i, j, FR)) += -2.*Ro_*s*inv_t;
                    A_(row, col(i + 1, j, FR)) =
                        - Ro_*s*s*inv_theta*f1dy*inv_r3
                        + s*inv_r2a
                        + s*inv_r2b;
                    A_(row, col(i, j, V)) =
                        - 2.*Ro_*s*c*mesh_(i, j, V)
                        - 2.*c;

                    /*========================================================
                     * ======================================================
                     *
                     *                THETA-DERIVATIVE TERMS                  */

                    A_(row, col(i, j - 1, F)) = Ro_*s*s*inv_r3*f2dx*inv_theta;
                    A_(row, col(i, j + 1, F)) = -Ro_*s*s*inv_r3*f2dx*inv_theta;
                    A_(row, col(i, j - 1, FR)) =
                        -Ro_*s*s*mesh_(i, j, FR)*inv_theta;
                    A_(row, col(i, j + 1, FR)) =
                        Ro_*s*s*mesh_(i, j, FR)*inv_theta;

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        - s*inv_r2a*f2dx
                        - s*inv_r2b*f2dx2
                        - Ro_*(
                                - s*s*inv_theta*f1dy*inv_r3*f2dx
                                - c*s*s*s*mesh_(i, j, FR)*mesh_(i, j, FR)
                                + s*s*mesh_(i, j, FR)*inv_theta*f2dy
                                - c*s*mesh_(i, j, V)*mesh_(i, j, V)
                              )
                        + 2.*c*mesh_(i, j, V);
                    if (unsteady)
                        b_[row] +=
                            - s*f2xx_o
                            + 2.*Ro_*s*inv_t*(
                                    + mesh_(i, j, FR)
                                    - prev_(i, j, FR)
                                    )
                            - Ro_*(
                                    - s*s*f1y_o*f2x_o
                                    - c*s*s*s*prev_(i, j, FR)*prev_(i, j, FR)
                                    + s*s*mesh_(i, j, FR)*f2y_o
                                    - c*s*prev_(i, j, V)*prev_(i, j, V)
                                  )
                            + 2.*c*prev_(i, j, V);
                    ++row;

                    A_(row, col(i, j, FR)) = 
                        + Ro_*(
                                + s*inv_theta*v1dy
                                + c*s*s*mesh_(i, j, V)
                              )
                        + 2.*c*s;
                    A_(row, col(i - 1, j, V)) =
                        + Ro_*s*inv_theta*f1dy*inv_r3
                        - inv_r2a
                        + inv_r2b;
                    A_(row, col(i, j, V)) =
                        - 2.*inv_r2b
                        + Ro_*c*s*s*mesh_(i, j, FR);
                    if (unsteady)
                        A_(row, col(i, j, V)) += -2.*Ro_*inv_t;
                    A_(row, col(i + 1, j, V)) =
                        - Ro_*s*inv_theta*f1dy*inv_r3
                        + inv_r2a
                        + inv_r2b;

                    /*========================================================
                     * ======================================================
                     *
                     *                THETA-DERIVATIVE TERMS                  */

                    A_(row, col(i, j - 1, F)) = Ro_*s*inv_r3*v1dx*inv_theta;
                    A_(row, col(i, j + 1, F)) = -Ro_*s*inv_r3*v1dx*inv_theta;
                    A_(row, col(i, j - 1, V)) = 
                        -Ro_*s*mesh_(i, j, FR)*inv_theta;
                    A_(row, col(i, j + 1, V)) =
                        Ro_*s*mesh_(i, j, FR)*inv_theta;

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        - inv_r2a*v1dx
                        - inv_r2b*v1dx2
                        - Ro_*(
                                - s*inv_theta*f1dy*inv_r3*v1dx
                                + s*mesh_(i, j, FR)*inv_theta*v1dy
                                + c*s*s*mesh_(i, j, FR)*mesh_(i, j, V)
                              )
                        - 2.*c*s*mesh_(i, j, FR);
                    if (unsteady)
                        b_[row] +=
                            - v1xx_o
                            + 2.*Ro_*inv_t*(
                                    + mesh_(i, j, V)
                                    - prev_(i, j, V)
                                    )
                            - Ro_*(
                                    - s*f1y_o*v1x_o
                                    + s*prev_(i, j, FR)*v1y_o
                                    + c*s*s*prev_(i, j, FR)*prev_(i, j, V)
                                  )
                            - 2.*c*s*prev_(i, j, FR);
                    ++row;
                }

                A_(row, col(i, j, F)) = -inv_r;
                A_(row, col(i + 1, j, F)) = inv_r;
                A_(row, col(i, j, FR)) = -0.5;
                A_(row, col(i + 1, j, FR)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, F) - mesh_(i + 1, j, F))
                    + 0.5*(mesh_(i, j, FR) + mesh_(i + 1, j, FR));
                ++row;
            }
            // Far-field entries at i = num_r - 1, j
            A_(row, col(num_r - 1, j, FR)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, FR);
            ++row;
            A_(row, col(num_r - 1, j, V)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, V);
            ++row;
        }

        // Equator entries at j = num_theta - 1, i
        if (mesh_equator_)
        {
            s = std::sin(mesh_.unmapped_node_y(num_theta - 1));
            c = std::cos(mesh_.unmapped_node_y(num_theta - 1));
            
            for (std::size_t i(0); i < num_r; ++i)
            {
                A_(row, col(i, num_theta - 1, F)) = 1.;
                b_[row] = (*mesh_equator_)(i, F)*s*s*c
                    - mesh_(i, num_theta - 1, F);
                ++row;
                A_(row, col(i, num_theta - 1, FR)) = 1.;
                b_[row] = (*mesh_equator_)(i, FR)*s*s*c
                    - mesh_(i, num_theta - 1, FR);
                ++row;
                A_(row, col(i, num_theta - 1, V)) = 1.;
                b_[row] = (*mesh_equator_)(i, V)*s - mesh_(i, num_theta - 1, V);
                ++row;
            }
        }
        else
        {
            A_(row, col(0, num_theta - 1, F)) = 1.;
            b_[row] = -mesh_(0, num_theta - 1, F);
            ++row;
            A_(row, col(0, num_theta - 1, FR)) = 1.;
            b_[row] = -mesh_(0, num_theta - 1, FR);
            ++row;
            A_(row, col(0, num_theta - 1, V)) = 1.;
            if (unsteady)
                b_[row] =
                    + sphere_spin(t_)*(sgn_ + sphere_wobble(t_))
                    - mesh_(0, num_theta - 1, V);
            else
                b_[row] =
                    + sgn_
                    - mesh_(0, num_theta - 1, V);
            ++row;

            inv_theta = 1./((mesh_.node_y(num_theta - 1)
                        - mesh_.node_y(num_theta - 3))*
                    mesh_.map_ds_y(mesh_.node_y(num_theta - 1)));
            for (std::size_t i(1); i < num_r; ++i)
            {
                /*A_(row, col(i - 1, num_theta - 1, F)) = -inv_r;
                A_(row, col(i, num_theta - 1, F)) = inv_r;
                A_(row, col(i - 1, num_theta - 1, FR)) = -0.5;
                A_(row, col(i, num_theta - 1, FR)) = -0.5;
                b_[row] = inv_r*(
                mesh_(i - 1, num_theta - 1, F) - mesh_(i, num_theta - 1, F))
                + 0.5*(mesh_(i - 1, num_theta - 1, FR)
                + mesh_(i, num_theta - 1, FR));
                ++row;*/

                A_(row, col(i, num_theta - 1, F)) = 1.;
                b_[row] = -mesh_(i, num_theta - 1, F);
                ++row;
                A_(row, col(i, num_theta - 1, FR)) = 1.;
                b_[row] = -mesh_(i, num_theta - 1, FR);
                ++row;
                A_(row, col(i, num_theta - 3, V)) = inv_theta;
                A_(row, col(i, num_theta - 2, V)) = -4.*inv_theta;
                A_(row, col(i, num_theta - 1, V)) = 3.*inv_theta;
                b_[row] = -inv_theta*(
                        + mesh_(i, num_theta - 3, V)
                        - 4.*mesh_(i, num_theta - 2, V)
                        + 3.*mesh_(i, num_theta - 1, V)
                        );
                ++row;
            }
        }

        if (row != num_r*num_theta*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Rotating_Layer_Streamfunction_Unscaled::
        Rotating_Layer_Streamfunction_Unscaled(
            Mesh_2D<double>& mesh_bl) :
        col(mesh_bl.num_vars(), mesh_bl.num_nodes_x()),
        Ro_(0.5),
        sgn_(-1),
        t_(0.),
        sphere_spin_(NULL),
        sphere_wobble_(NULL),
        A_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars()),
        b_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars(), 0.),
        linear_system_(A_, b_),
        prev_(mesh_bl),
        mesh_(mesh_bl),
        mesh_pole_(NULL),
        mesh_equator_(NULL)
    {
        if (mesh_bl.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Rotating_Layer_Streamfunction_Unscaled::"
                    "Rotating_Layer_Streamfunction_Unscaled",
                    "Meshes with num_vars_ = 3 required");
    }

    Rotating_Layer_Streamfunction_Unscaled::
        ~Rotating_Layer_Streamfunction_Unscaled()
    {
    }

    void Rotating_Layer_Streamfunction_Unscaled::set_initial_guess()
    {
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        double r(0.), r2(0.), theta(0.), s(0.), c(0.);

        for (std::size_t j(0); j < num_theta; ++j)
        {
            theta = mesh_.unmapped_node_y(j);
            s = std::sin(theta);
            c = std::cos(theta);
            for (std::size_t i(0); i < num_r; ++i)
            {
                r = mesh_.unmapped_node_x(i);
                r2 = sgn_ < 0 ? r - 1 : r;
                
                mesh_(i, j, F) = 0.5*(1. - std::exp(-r*r))*s*s*c;
                mesh_(i, j, FR) = r*std::exp(-r*r)*s*c;
                mesh_(i, j, V) = r2*std::exp(-r*r)*s;
            }
        }
    }

    void Rotating_Layer_Streamfunction_Unscaled::solve_steady()
    {
        step(0.);
    }

    void Rotating_Layer_Streamfunction_Unscaled::step(const double& dt)
    {
        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);
        if (dt > tolerance_)
            t_ += dt;

#ifndef QUIET
        if (dt > tolerance_)
            console_out(" 2D solver, t = " + str_convert(t_) + "\n");
        else
            console_out(" 2D solver\n");
#ifdef DEBUG
        if (dt > tolerance_)
            console_out(" -----------------------\n");
        else
            console_out(" ---------\n");
#endif  // DEBUG
#endif  // !QUIET

        prev_.set_vars_vector(mesh_.get_vars_vector());

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix(dt);

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t j(0); j < num_theta; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) += b_[num_vars*(num_r*j + i) + k];
            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_2D<double>&
        Rotating_Layer_Streamfunction_Unscaled::solution() const
    {
        return mesh_;
    }

    void Rotating_Layer_Streamfunction_Unscaled::dump_flow_variables(
            std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        double mid_theta(0.), inv_theta(0.), s(0.);
        std::size_t old_precision(out.precision());

        out.precision(decimal_places);
        out << "# Mesh_2D\n";
        out << "# Solution at t = " << t_ << '\n';
        out << "# theta    r    u    v    w\n";

        for (std::size_t j(0), num_theta(mesh_.num_nodes_y());
                j < num_theta - 1; j += output_skip + 1)
        {
            mid_theta = 0.5*(mesh_.unmapped_node_y(j)
                    + mesh_.unmapped_node_y(j + 1));
            inv_theta = 1./(mesh_.unmapped_node_y(j + 1)
                    - mesh_.unmapped_node_y(j));
            for (std::size_t i(0), num_r(mesh_.num_nodes_x());
                    i < num_r; i += output_skip + 1)
            {
                s = 1./std::sin(mid_theta);
                out << mid_theta << ' ';
                out << mesh_.unmapped_node_x(i) << ' ';
                out << -0.5*s*(mesh_(i, j, FR)
                        + mesh_(i, j + 1, FR)) << ' ';
                out << 0.5*(mesh_(i, j, V) + mesh_(i, j + 1, V)) << ' ';
                out << s*inv_theta*(mesh_(i, j + 1, F) - mesh_(i, j, F))
                    << std::endl;
            }
            out << std::endl;
        }
        out << std::endl;
        out.precision(old_precision);
    }
}   // namespace

