#include <cmath>

#include <boundary_layer/Quasi_1D_Cowley.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Quasi_1D_Cowley::tolerance_ = 1e-8;
    std::size_t Quasi_1D_Cowley::max_iterations_ = 20;

    Quasi_1D_Cowley::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars) : nv_(num_vars)
    {
    }

    Quasi_1D_Cowley::Matrix_Column::~Matrix_Column()
    {
    }

    void Quasi_1D_Cowley::assemble_matrix()
    {
        if (theta_index_ == 0 || theta_index_ + 2 > mesh_.num_nodes_y())
            throw Exceptions::Generic("Invalid state of Quasi_1D_Cowley object",
                    "0 < theta_index_ < num_theta - 1 is required");
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_vars(mesh_.num_vars());

        // ===================================================================
        // ===================================================================
        //
        //                          MATRIX ASSEMBLY                           

        std::size_t row(0);
        std::size_t& j(theta_index_);
        double inv_r(0.), inv_r2(0.), inv_r3(0.), inv_theta(0.);
        double f2dr(0.), fdtheta(0.), f2dtheta(0.),
               vdr(0.), vdtheta(0.);
        double S(1./std::sin(mesh_.node_y(j))), C(std::cos(mesh_.node_y(j)));
        Matrix_Column col(num_vars);

        A_.fill_zero();
        B_.fill_zero();

        // Sphere surface entries at i = 0
        A_(row, col(0, F), 0) = 1.;
        ++row;
        A_(row, col(0, FR), 0) = 1.;
        ++row;
        A_(row, col(0, V), 0) = 1.;
        ++row;

        // Interior of boundary layer
        for (std::size_t i(0); i < num_r - 1; ++i)
        {
            inv_r = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
            if (i > 0)
            {
                inv_r3 = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i - 1));
                inv_r2 = inv_r*inv_r;
                inv_theta = 1./(mesh_.node_y(j + 1) - mesh_.node_y(j - 1));

                f2dr = mesh_(i + 1, j, FR) - mesh_(i - 1, j, FR);
                fdtheta = mesh_(i, j + 1, F) - mesh_(i, j - 1, F);
                f2dtheta = mesh_(i, j + 1, FR) - mesh_(i, j - 1, FR);
                vdr = mesh_(i + 1, j, V) - mesh_(i - 1, j, V);
                vdtheta = mesh_(i, j + 1, V) - mesh_(i, j - 1, V);
            }

            // One equation mid-node in r, two at nodal locations
            if (i > 0)
            {
                A_(row, col(i - 1, FR), 0) =
                    + S*inv_r2
                    + Ro_*S*S*fdtheta*inv_theta*inv_r3;
                A_(row, col(i, FR), 0) =
                    - 2.*S*inv_r2
                    + Ro_*(
                            - 2.*C*S*S*S*mesh_(i, j, FR)
                            + S*S*inv_theta*f2dtheta
                          )
                    - Ro_*S*S*mesh_(i, j, FR)*wavenumbers_(j, 1);
                A_(row, col(i, FR), 1) =
                    + Ro_*S*S*mesh_(i, j, FR)*wavenumbers_(j, 0);
                A_(row, col(i + 1, FR), 0) =
                    + S*inv_r2
                    - Ro_*S*S*fdtheta*inv_theta*inv_r3;
                A_(row, col(i, F), 0) =
                    + Ro_*S*S*inv_r3*f2dr*wavenumbers_(j, 1);
                A_(row, col(i, F), 1) =
                    - Ro_*S*S*inv_r3*f2dr*wavenumbers_(j, 0);
                A_(row, col(i, V), 0) =
                    - 2.*Ro_*S*C*mesh_(i, j, V)
                    - 2.*C;
                B_(row, col(i, FR), 0) =
                    + Ro_*S*wavenumbers_(j, 1);
                B_(row, col(i, FR), 0) =
                    - Ro_*S*wavenumbers_(j, 0);
                ++row;

                A_(row, col(i - 1, V), 0) =
                    + inv_r2
                    + Ro_*S*inv_theta*fdtheta*inv_r3;
                A_(row, col(i, V), 0) =
                    - 2.*inv_r2
                    + Ro_*S*mesh_(i, j, FR)*(
                            + C
                            - wavenumbers_(j, 1)
                            );
                A_(row, col(i, V), 1) =
                    + Ro_*S*mesh_(i, j, FR)*wavenumbers_(j, 0);
                A_(row, col(i, F), 0) =
                    + S*inv_r3*vdr*wavenumbers_(j, 1);
                A_(row, col(i, F), 1) =
                    - S*inv_r3*vdr*wavenumbers_(j, 0);
                A_(row, col(i, FR), 0) =
                    + Ro_*S*inv_theta*vdtheta
                    + Ro_*S*S*C*mesh_(i, j, V)
                    + 2.*S*C;
                B_(row, col(i, V), 0) =
                    + Ro_*wavenumbers_(j, 1);
                B_(row, col(i, V), 1) =
                    - Ro_*wavenumbers_(j, 0);
                ++row;
            }

            A_(row, col(i, F), 0) = -inv_r;
            A_(row, col(i + 1, F), 0) = inv_r;
            A_(row, col(i, FR), 0) = -0.5;
            A_(row, col(i + 1, FR), 0) = -0.5;
            ++row;
        }
        // Far-field entries at i = num_r - 1
        A_(row, col(num_r - 1, FR), 0) = 1.;
        ++row;
        A_(row, col(num_r - 1, V), 0) = 1.;
        ++row;

        if (row != static_cast<std::size_t>(A_.num_rows()))
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        // ===================================================================
        // ===================================================================
    }

    Quasi_1D_Cowley::Quasi_1D_Cowley(Mesh_2D<double>& mesh,
            Mesh_1D<double>& wavenumbers) :
        col(mesh.num_vars()),
        Ro_(0.3),
        sgn_(-1),
        theta_index_(1),
        mesh_(mesh),
        wavenumbers_(wavenumbers),
        A_(mesh.num_nodes_x()*mesh.num_vars()),
        B_(mesh.num_nodes_x()*mesh.num_vars()),
        evp_problem_(A_, B_)
    {
        if (mesh_.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Quasi_1D_Cowley::Quasi_1D_Cowley",
                    "Mesh_2D object with num_vars_ = 3 required");
    }

    Quasi_1D_Cowley::~Quasi_1D_Cowley()
    {
    }

    void Quasi_1D_Cowley::solve()
    {
#ifndef QUIET
        console_out("Solving eigenvalue problem\n==========================\n");
#endif  // !QUIET

        assemble_matrix();
        evp_problem_.solve();
    }

    const Generalised_Eigenvalue_System_Complex&
        Quasi_1D_Cowley::evp_system() const
    {
        return evp_problem_;
    }
}   // namespace

