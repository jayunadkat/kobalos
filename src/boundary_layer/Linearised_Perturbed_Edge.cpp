#include <cmath>

#include <boundary_layer/Linearised_Perturbed_Edge.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Linearised_Perturbed_Edge::tolerance_ = 1e-8;
    std::size_t Linearised_Perturbed_Edge::max_iterations_ = 50;

    Linearised_Perturbed_Edge::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars) :
        nv_(num_vars)
    {
    }

    Linearised_Perturbed_Edge::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Linearised_Perturbed_Edge::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& k)
    {
        return nv_*i + k;
    }

    double Linearised_Perturbed_Edge::sphere_rotation(const double& t)
    {
        if (ext_rot_ == NULL)
            return 0.;
        else
            return (*ext_rot_)(t_);
    }

    void Linearised_Perturbed_Edge::assemble_matrix(const double& dt)
    {
        if (c2_ < 0)
            throw Exceptions::Generic("Solution",
                    "Call to set_edge expected before solving");

        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2(0.), inv_r3(0.), inv_t(0.);
        double sf2dx(0.), sv1dx(0.);
        double f2dx(0.), v1dx(0.);
        double f2x_o(0.), f2xx_o(0.), v1x_o(0.), v1xx_o(0.);
        Matrix_Column col(num_vars);

        A_.assign(num_r*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_r*num_vars, 0.);

        // Sphere surface entries at i = 0
        A_(row, col(0, f)) = 1.;
        b_[row] = -mesh_(0, f);
        ++row;
        A_(row, col(0, fr)) = 1.;
        b_[row] = -mesh_(0, fr);
        ++row;
        A_(row, col(0, v)) = 1.;
        if (unsteady)
            b_[row] = sphere_rotation(t_) - mesh_(0, v);
        else
            b_[row] = 1. - mesh_(0, v);
        ++row;

        // Interior of boundary layer
        for (std::size_t i(0); i < num_r - 1; ++i)
        {
            inv_r = 1./(mesh_.node(i + 1) - mesh_.node(i));
            if (i > 0)
            {
                inv_r2 = 1./(mesh_.node(i) - mesh_.node(i - 1));
                inv_r3 = 1./(mesh_.node(i + 1) - mesh_.node(i - 1));
            }
            if (i > 0)
            {
                sf2dx = steady_(i + 1, fr) - steady_(i - 1, fr);
                sv1dx = steady_(i + 1, v) - steady_(i - 1, v);

                f2dx = mesh_(i + 1, fr) - mesh_(i - 1, fr);
                v1dx = mesh_(i + 1, v) - mesh_(i - 1, v);
            }
            if (unsteady && i > 0)
            {
                inv_t = 1./dt;
                f2x_o = inv_r3*(
                        + prev_(i + 1, fr)
                        - prev_(i - 1, fr));
                f2xx_o = 2.*inv_r3*(
                        + inv_r2*prev_(i - 1, fr)
                        - (inv_r + inv_r2)*prev_(i, fr)
                        + inv_r*prev_(i + 1, fr));
                v1x_o = inv_r3*(
                        + prev_(i + 1, v)
                        - prev_(i - 1, v));
                v1xx_o = 2.*inv_r3*(
                        + inv_r2*prev_(i - 1, v)
                        - (inv_r + inv_r2)*prev_(i, v)
                        + inv_r*prev_(i + 1, v));
            }

            // One equation mid-node in r, two at nodal locations
            if (i > 0)
            {
                A_(row, col(i, f)) = -(3.*c2_ - 1.)*inv_r3*sf2dx;
                A_(row, col(i - 1, fr)) = 2.*inv_r3*inv_r2
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, fr)) = -2.*inv_r3*(inv_r + inv_r2)
                    + 2.*(2.*c2_ - 1.)*steady_(i, fr);
                if (unsteady)
                    A_(row, col(i, fr)) += -2.*inv_t;
                A_(row, col(i + 1, fr)) = 2.*inv_r3*inv_r
                    - (3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, v)) = -2.*steady_(i, v);

                b_[row] = 
                    + 2.*inv_r3*(
                            - inv_r2*mesh_(i - 1, fr)
                            + (inv_r + inv_r2)*mesh_(i, fr)
                            - inv_r*mesh_(i + 1, fr))
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f)*f2dx
                    + (3.*c2_ - 1.)*inv_r3*sf2dx*mesh_(i, f)
                    - 2.*(2.*c2_ - 1.)*steady_(i, fr)*mesh_(i, fr)
                    + 2.*steady_(i, v)*mesh_(i, v);
                if (unsteady)
                    b_[row] +=
                        + 2.*inv_t*(
                                + mesh_(i, fr)
                                - prev_(i, fr))
                        - f2xx_o
                        + (3.*c2_ - 1.)*steady_(i, f)*f2x_o
                        + (3.*c2_ - 1.)*inv_r3*sf2dx*prev_(i, f)
                        - 2.*(2.*c2_ - 1.)*steady_(i, fr)*prev_(i, fr)
                        + 2.*steady_(i, v)*prev_(i, v);
                ++row;

                A_(row, col(i, f)) = -(3.*c2_ - 1.)*inv_r3*sv1dx;
                A_(row, col(i, fr)) = 2.*c2_*steady_(i, v);
                A_(row, col(i - 1, v)) = 2.*inv_r3*inv_r2
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f);
                A_(row, col(i, v)) = -2.*inv_r3*(inv_r + inv_r2)
                    + 2.*c2_*steady_(i, fr);
                if (unsteady)
                    A_(row, col(i, v)) += -2.*inv_t;
                A_(row, col(i + 1, v)) = 2.*inv_r3*inv_r
                    - (3.*c2_ - 1.)*inv_r3*steady_(i, f);

                b_[row] = 
                    + 2.*inv_r3*(
                            - inv_r2*mesh_(i - 1, v)
                            + (inv_r + inv_r2)*mesh_(i, v)
                            - inv_r*mesh_(i + 1, v))
                    + (3.*c2_ - 1.)*inv_r3*steady_(i, f)*v1dx
                    + (3.*c2_ - 1.)*inv_r3*sv1dx*mesh_(i, f)
                    - 2.*c2_*steady_(i, fr)*mesh_(i, v)
                    - 2.*c2_*steady_(i, v)*mesh_(i, fr);
                if (unsteady)
                    b_[row] +=
                        + 2.*inv_t*(
                                + mesh_(i, v)
                                - prev_(i, v))
                        - v1xx_o
                        + (3.*c2_ - 1.)*steady_(i, f)*v1x_o
                        + (3.*c2_ - 1.)*inv_r3*sv1dx*prev_(i, f)
                        - 2.*c2_*steady_(i, fr)*prev_(i, v)
                        - 2.*c2_*steady_(i, v)*prev_(i, fr);
                ++row;
            }

            A_(row, col(i, f)) = -inv_r;
            A_(row, col(i + 1, f)) = inv_r;
            A_(row, col(i, fr)) = -0.5;
            A_(row, col(i + 1, fr)) = -0.5;
            b_[row] = inv_r*(mesh_(i, f) - mesh_(i + 1, f))
                + 0.5*(mesh_(i, fr) + mesh_(i + 1, fr));
            ++row;
        }
        // Far-field entries at i = num_r - 1
        A_(row, col(num_r - 2, fr)) = -1.;
        A_(row, col(num_r - 1, fr)) = 1.;
        b_[row] = mesh_(num_r - 2, fr) - mesh_(num_r - 1, fr);
        ++row;
        A_(row, col(num_r - 2, v)) = -1.;
        A_(row, col(num_r - 1, v)) = 1.;
        b_[row] = mesh_(num_r - 2, v) - mesh_(num_r - 1, v);
        ++row;

        if (row != num_r*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Linearised_Perturbed_Edge::Linearised_Perturbed_Edge(
            Mesh_1D<double>& mesh_steady, Mesh_1D<double>& mesh):
        c2_(-1.),
        S_(0.),
        t_(0.),
        ext_rot_(NULL),
        A_(mesh.num_nodes()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        steady_(mesh_steady),
        prev_(mesh),
        mesh_(mesh)
    {
#ifdef PARANOID
        if (mesh_.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Linearised_Perturbed_Edge::Linearised_Perturbed_Edge",
                    "Mesh_1D object with num_vars_ = 3 required");
        if (mesh.num_nodes() != mesh_steady.num_nodes() ||
                mesh.num_vars() != mesh_steady.num_vars())
            throw Exceptions::Invalid_Argument(
                    "Linearised_Perturbed_Edge::Linearised_Perturbed_Edge",
                    "Mismatching mesh objects supplied to solver");
#endif  // PARANOID
    }

    Linearised_Perturbed_Edge::~Linearised_Perturbed_Edge()
    {
    }

    void Linearised_Perturbed_Edge::step(const double& dt)
    {
        if (dt <= 0)
            throw Exceptions::Invalid_Argument("PDE_IBVP::step",
                    "dt > 0 is required");

        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);
        t_ += dt;

#ifndef QUIET
        console_out(" 1D solver, t = " + str_convert(t_) + "\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

        prev_.set_vars_vector(mesh_.get_vars_vector());

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix(dt);

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t k(0); k < num_vars; ++k)
                    mesh_(i, k) += b_[num_vars*i + k];

            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_1D<double>& Linearised_Perturbed_Edge::last_solution() const
    {
        return prev_;
    }

    void Linearised_Perturbed_Edge::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        std::size_t old_precision(out.precision());
        out.precision(decimal_places);
        out << "# Solution at t = " << t_ << '\n';
        out << "# r    u    v    w\n";
        for (std::size_t i(0), num_r(mesh_.num_nodes()); i < num_r;
                i += output_skip + 1)
        {
            out << mesh_.node(i) << ' ';
            out << -mesh_(i, fr) << ' ';
            out << mesh_(i, v) << ' ';
            out << (3.*c2_ - 1.)*mesh_(i, f) << '\n';
        }
        out << "\n\n";
        out.precision(old_precision);
    }
}   // namespace

