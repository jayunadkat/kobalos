#include <cmath>

#include <boundary_layer/Rotating_Fourier_Decomposed_Layer_Unscaled.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Rotating_Fourier_Decomposed_Layer_Unscaled::tolerance_ = 1e-8;
    std::size_t Rotating_Fourier_Decomposed_Layer_Unscaled::max_iterations_ = 50;

    Rotating_Fourier_Decomposed_Layer_Unscaled::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars, const std::size_t& num_r) :
        nv_(num_vars),
        nr_(num_r)
    {
    }

    Rotating_Fourier_Decomposed_Layer_Unscaled::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Rotating_Fourier_Decomposed_Layer_Unscaled::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& j, const std::size_t& k)
    {
        return nv_*(nr_*j + i) + k;
    }

    void Rotating_Fourier_Decomposed_Layer_Unscaled::assemble_matrix_3(const double& dt)
    {
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2a(0.), inv_r2b(0.), inv_r3(0.), inv_theta(0.);
        double s(0.), c(0.);
        double sf1dy(0.), sf2dx(0.), sf2dy(0.), sv1dx(0.), sv1dy(0.);
        Matrix_Column col(num_vars, num_r);

        A_.assign(num_r*num_theta*num_vars);
        b_.assign(num_r*num_theta*num_vars, 0.);

        // Polar entries at j = 0, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, num_vars*i + k) = 1.;
                b_[row] = -mesh_(i, 0, k);
                ++row;
            }
        }
        // Interior theta values
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            // Sphere surface entries at i = 0, j.
            A_(row, col(0, j, f)) = 1.;
            b_[row] = -mesh_(0, j, f);
            //double theta(mesh_.unmapped_node_y(j));
            //b_[row] = 1.*std::exp(-8000.*(theta - 0.1)*(theta - 0.1))*(
            //        theta - 0.1) - mesh_(0, j, f);
            ++row;
            A_(row, col(0, j, fr)) = 1.;
            b_[row] = -mesh_(0, j, fr);
            ++row;
            A_(row, col(0, j, v)) = 1.;
            b_[row] = std::sin(mesh_.unmapped_node_y(j)) - mesh_(0, j, v);
            //b_[row] = -mesh_(0, j, v);
            ++row;

            // Interior of boundary layer
            for (std::size_t i(0); i < num_r - 1; ++i)
            {
                inv_r = 1./((mesh_.node_x(i + 1) - mesh_.node_x(i))*
                        mesh_.map_ds_x(
                            0.5*(mesh_.node_x(i) + mesh_.node_x(i + 1))));
                if (i > 0)
                {
                    inv_r3 = 1./((mesh_.node_x(i + 1) - mesh_.node_x(i - 1))*
                            mesh_.map_ds_x(mesh_.node_x(i)));
                    inv_r2a = -mesh_.map_ds2_x(mesh_.node_x(i))/(
                            (mesh_.node_x(i + 1) - mesh_.node_x(i - 1))*
                            std::pow(mesh_.map_ds_x(mesh_.node_x(i)), 3.));
                    inv_r2b = std::pow(1./(
                                (mesh_.node_x(i + 1) - mesh_.node_x(i))*
                                mesh_.map_ds_x(mesh_.node_x(i))), 2.);
                }
                inv_theta = 1./((mesh_.node_y(j + 1) - mesh_.node_y(j - 1))*
                        mesh_.map_ds_y(mesh_.node_y(j)));
                s = 1./std::sin(mesh_.unmapped_node_y(j));
                c = std::cos(mesh_.unmapped_node_y(j));
                if (i > 0)
                {
                    sf1dy = steady_(i, j + 1, f) - steady_(i, j - 1, f);
                    sf2dx = steady_(i + 1, j, fr) - steady_(i - 1, j, fr);
                    sf2dy = steady_(i, j + 1, fr) - steady_(i, j - 1, fr);
                    sv1dx = steady_(i + 1, j, v) - steady_(i - 1, j, v);
                    sv1dy = steady_(i, j + 1, v) - steady_(i, j - 1, v);
                }

                // One equation mid-node in r, two at nodal locations
                if (i > 0)
                {
                    A_(row, col(i - 1, j, fr)) =
                        - s*inv_r2a
                        + s*inv_r2b
                        + Ro_*s*s*inv_theta*sf1dy*inv_r3;
                    A_(row, col(i, j, fr)) =
                        + Ro_*(
                                - std::complex<double>(0., 1.)*omega_*s
                                - 2.*c*s*s*s*steady_(i, j, fr)
                                + s*s*inv_theta*sf2dy
                              )
                        - 2.*s*inv_r2b;
                    A_(row, col(i + 1, j, fr)) =
                        + s*inv_r2a
                        + s*inv_r2b
                        - Ro_*s*s*inv_theta*sf1dy*inv_r3;
                    A_(row, col(i, j, v)) = -2.*c*(Ro_*s*steady_(i, j, v) + 1.);

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, f)) = Ro_*s*s*inv_r3*sf2dx*inv_theta;
                    A_(row, col(i, j + 1, f)) = -Ro_*s*s*inv_r3*sf2dx*inv_theta;
                    A_(row, col(i, j - 1, fr)) =
                        -Ro_*s*s*steady_(i, j, fr)*inv_theta;
                    A_(row, col(i, j + 1, fr)) =
                        Ro_*s*s*steady_(i, j, fr)*inv_theta;

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 0.;
                    ++row;

                    A_(row, col(i, j, fr)) =
                        + Ro_*(
                                + s*inv_theta*sv1dy
                                + c*s*s*steady_(i, j, v)
                              )
                        + 2.*c*s;
                    A_(row, col(i - 1, j, v)) =
                        - inv_r2a
                        + inv_r2b
                        + Ro_*s*inv_theta*sf1dy*inv_r3;
                    A_(row, col(i, j, v)) =
                        + Ro_*(
                                - std::complex<double>(0., 1.)*omega_
                                + c*s*s*steady_(i, j, fr)
                              )
                        - 2.*inv_r2b;
                    A_(row, col(i + 1, j, v)) =
                        + inv_r2a
                        + inv_r2b
                        - Ro_*s*inv_theta*sf1dy*inv_r3;

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, f)) = Ro_*s*inv_r3*sv1dx*inv_theta;
                    A_(row, col(i, j + 1, f)) = -Ro_*s*inv_r3*sv1dx*inv_theta;
                    A_(row, col(i, j - 1, v)) =
                        -Ro_*s*steady_(i, j, fr)*inv_theta;
                    A_(row, col(i, j + 1, v)) =
                        Ro_*s*steady_(i, j, fr)*inv_theta;

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 0.;
                    ++row;
                }

                A_(row, col(i, j, f)) = -inv_r;
                A_(row, col(i + 1, j, f)) = inv_r;
                A_(row, col(i, j, fr)) = -0.5;
                A_(row, col(i + 1, j, fr)) = -0.5;
                b_[row] = 0.;
                ++row;
            }
            // Far-field entries at i = num_r - 1, j
            // SECOND-ORDER-ACCURATE GRADIENT CONDITIONS (FAIL)
            /*A_(row, col(num_r - 3, j, fr)) = 1.;
            A_(row, col(num_r - 2, j, fr)) = -4.;
            A_(row, col(num_r - 1, j, fr)) = 3.;
            b_[row] = -mesh_(num_r - 3, j, fr) + 4.*mesh_(num_r - 2, j, fr)
                - 3.*mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 3, j, v)) = 1.;
            A_(row, col(num_r - 2, j, v)) = -4.;
            A_(row, col(num_r - 1, j, v)) = 3.;
            b_[row] = -mesh_(num_r - 3, j, v) + 4.*mesh_(num_r - 2, j, v)
                - 3.*mesh_(num_r - 1, j, v);
            ++row;*/
            // FIRST-ORDER-ACCURATE GRADIENT CONDITIONS (WORK)
            A_(row, col(num_r - 2, j, fr)) = -1.;
            A_(row, col(num_r - 1, j, fr)) = 1.;
            b_[row] = mesh_(num_r - 2, j, fr) - mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 2, j, v)) = -1.;
            A_(row, col(num_r - 1, j, v)) = 1.;
            b_[row] = mesh_(num_r - 2, j, v) - mesh_(num_r - 1, j, v);
            ++row;
            // ORIGINAL VANISHING FAR-FIELD CONDITIONS (SHOW BC INCONSISTENCY)
            /*A_(row, col(num_r - 1, j, fr)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 1, j, v)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, v);
            ++row;*/
        }
        // Equator entries at j = num_theta - 1, i
        A_(row, col(0, num_theta - 1, f)) = 1.;
        b_[row] = -mesh_(0, num_theta - 1, f);
        ++row;
        A_(row, col(0, num_theta - 1, fr)) = 1.;
        b_[row] = -mesh_(0, num_theta - 1, fr);
        ++row;
        A_(row, col(0, num_theta - 1, v)) = 1.;
        b_[row] = 1. - mesh_(0, num_theta - 1, v);
        ++row;

        inv_theta = 1./((mesh_.node_y(num_theta - 1)
                    - mesh_.node_y(num_theta - 3))*
                mesh_.map_ds_y(mesh_.node_y(num_theta - 1)));
        for (std::size_t i(1); i < num_r; ++i)
        {
            /*A_(row, col(i - 1, num_theta - 1, F)) = -inv_r;
              A_(row, col(i, num_theta - 1, F)) = inv_r;
              A_(row, col(i - 1, num_theta - 1, FR)) = -0.5;
              A_(row, col(i, num_theta - 1, FR)) = -0.5;
              b_[row] = inv_r*(
              mesh_(i - 1, num_theta - 1, F) - mesh_(i, num_theta - 1, F))
              + 0.5*(mesh_(i - 1, num_theta - 1, FR)
              + mesh_(i, num_theta - 1, FR));
              ++row;*/

            A_(row, col(i, num_theta - 1, f)) = 1.;
            b_[row] = -mesh_(i, num_theta - 1, f);
            ++row;
            A_(row, col(i, num_theta - 1, fr)) = 1.;
            b_[row] = -mesh_(i, num_theta - 1, fr);
            ++row;
            A_(row, col(i, num_theta - 3, v)) = inv_theta;
            A_(row, col(i, num_theta - 2, v)) = -4.*inv_theta;
            A_(row, col(i, num_theta - 1, v)) = 3.*inv_theta;
            b_[row] = -inv_theta*(
                    + mesh_(i, num_theta - 3, v)
                    - 4.*mesh_(i, num_theta - 2, v)
                    + 3.*mesh_(i, num_theta - 1, v)
                    );
            ++row;
        }

        if (row != num_r*num_theta*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Rotating_Fourier_Decomposed_Layer_Unscaled::Rotating_Fourier_Decomposed_Layer_Unscaled(
            Mesh_2D<double>& mesh_steady,
            Mesh_2D<std::complex<double> >& mesh_bl):
        omega_(0.),
        A_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars()),
        b_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars(), 0.),
        linear_system_(A_, b_),
        steady_(mesh_steady),
        mesh_(mesh_bl)
    {
#ifdef PARANOID
        if (mesh_bl.num_nodes_x() != mesh_steady.num_nodes_x() ||
                mesh_bl.num_nodes_y() != mesh_steady.num_nodes_y() ||
                mesh_bl.num_vars() != mesh_steady.num_vars())
            throw Exceptions::Invalid_Argument(
                    "Rotating_Fourier_Decomposed_Layer_Unscaled::"
                    "Rotating_Fourier_Decomposed_Layer_Unscaled",
                    "Mismatching mesh objects supplied to solver");
#endif  // PARANOID
    }

    Rotating_Fourier_Decomposed_Layer_Unscaled::~Rotating_Fourier_Decomposed_Layer_Unscaled()
    {
    }

    void Rotating_Fourier_Decomposed_Layer_Unscaled::solve()
    {
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

#ifndef QUIET
        console_out(" 2D solver\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix_3();

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t j(0); j < num_theta; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) = b_[num_vars*(num_r*j + i) + k];
    }

    const Mesh_2D<std::complex<double> >&
        Rotating_Fourier_Decomposed_Layer_Unscaled::solution() const
    {
        return mesh_;
    }

    void Rotating_Fourier_Decomposed_Layer_Unscaled::dump_flow_variables(
            std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        double mid_theta(0.), inv_theta(0.), s(0.);
        std::size_t old_precision(out.precision());
        std::complex<double> temp;

        out.precision(decimal_places);
        out << "# Mesh_2D\n";
        out << "# theta    r    u    v    w\n";

        for (std::size_t j(0), num_theta(mesh_.num_nodes_y());
                j < num_theta - 1; j += output_skip + 1)
        {
            mid_theta = 0.5*(mesh_.unmapped_node_y(j)
                    + mesh_.unmapped_node_y(j + 1));
            inv_theta = 1./(mesh_.unmapped_node_y(j + 1)
                    - mesh_.unmapped_node_y(j));
            for (std::size_t i(0), num_r(mesh_.num_nodes_x());
                    i < num_r; i += output_skip + 1)
            {
                s = 1./std::sin(mid_theta);
                out << mid_theta << ' ';
                out << mesh_.unmapped_node_x(i) << ' ';
                temp = -0.5*s*(mesh_(i, j, fr) + mesh_(i, j + 1, fr));
                out << temp.real() << ' ' << temp.imag() << ' ';
                temp = 0.5*(mesh_(i, j, v) + mesh_(i, j + 1, v));
                out << temp.real() << ' ' << temp.imag() << ' ';
                temp = s*inv_theta*(mesh_(i, j + 1, f) - mesh_(i, j, f));
                out << temp.real() << ' ' << temp.imag() << std::endl;
            }
            out << std::endl;
        }
        out.precision(old_precision);
    }
}   // namespace

