#include <cmath>

#include <boundary_layer/Fourier_Decomposed_Layer.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Fourier_Decomposed_Layer::tolerance_ = 1e-8;
    std::size_t Fourier_Decomposed_Layer::max_iterations_ = 50;

    Fourier_Decomposed_Layer::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars, const std::size_t& num_r) :
        nv_(num_vars),
        nr_(num_r)
    {
    }

    Fourier_Decomposed_Layer::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Fourier_Decomposed_Layer::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& j, const std::size_t& k)
    {
        return nv_*(nr_*j + i) + k;
    }

    void Fourier_Decomposed_Layer::assemble_matrix_3(const double& dt)
    {
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2(0.), inv_r3(0.), inv_theta(0.);
        double s(0.), c(0.), c2(0.), C1(0.);
        double sf1dy(0.), sf2dx(0.), sf2dy(0.), sv1dx(0.), sv1dy(0.);
        std::complex<double> f1dy(0.), f2dx(0.), f2dy(0.), v1dx(0.), v1dy(0.);
        Matrix_Column col(num_vars, num_r);

        A_.assign(num_r*num_theta*num_vars);
        b_.assign(num_r*num_theta*num_vars, 0.);

        // Polar entries at j = 0, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, num_vars*i + k) = 1.;
                b_[row] = mesh_pole_(i, k) - mesh_(i, 0, k);
                ++row;
            }
        }
        // Interior theta values
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            // Sphere surface entries at i = 0, j.
            A_(row, col(0, j, f)) = 1.;
            b_[row] = -mesh_(0, j, f);
            ++row;
            A_(row, col(0, j, fr)) = 1.;
            b_[row] = -mesh_(0, j, fr);
            ++row;
            A_(row, col(0, j, v)) = 1.;
            b_[row] = 1. - mesh_(0, j, v);
            ++row;

            // Interior of boundary layer
            for (std::size_t i(0); i < num_r - 1; ++i)
            {
                inv_r = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
                if (i > 0)
                {
                    inv_r2 = 1./(mesh_.node_x(i) - mesh_.node_x(i - 1));
                    inv_r3 = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i - 1));
                }
                inv_theta = 1./(mesh_.node_y(j + 1) - mesh_.node_y(j - 1));
                s = std::sin(mesh_.node_y(j));
                c = std::cos(mesh_.node_y(j));
                c2 = c*c;
                C1 = s*c*inv_theta;
                if (i > 0)
                {
                    sf1dy = steady_(i, j + 1, f) - steady_(i, j - 1, f);
                    sf2dx = steady_(i + 1, j, fr) - steady_(i - 1, j, fr);
                    sf2dy = steady_(i, j + 1, fr) - steady_(i, j - 1, fr);
                    sv1dx = steady_(i + 1, j, v) - steady_(i - 1, j, v);
                    sv1dy = steady_(i, j + 1, v) - steady_(i, j - 1, v);

                    f1dy = mesh_(i, j + 1, f) - mesh_(i, j - 1, f);
                    f2dx = mesh_(i + 1, j, fr) - mesh_(i - 1, j, fr);
                    f2dy = mesh_(i, j + 1, fr) - mesh_(i, j - 1, fr);
                    v1dx = mesh_(i + 1, j, v) - mesh_(i - 1, j, v);
                    v1dy = mesh_(i, j + 1, v) - mesh_(i, j - 1, v);
                }

                // One equation mid-node in r, two at nodal locations
                if (i > 0)
                {
                    A_(row, col(i, j, f)) = -(3.*c2 - 1.)*inv_r3*sf2dx;
                    A_(row, col(i - 1, j, fr)) = 2.*inv_r3*inv_r2
                        + C1*inv_r3*sf1dy
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f);
                    A_(row, col(i, j, fr)) = -2.*inv_r3*(inv_r + inv_r2)
                        + C1*sf2dy + 2.*(2.*c2 - 1.)*steady_(i, j, fr);
                    A_(row, col(i, j, fr)) += -std::complex<double>(0., 1.)*
                        omega_;
                    A_(row, col(i + 1, j, fr)) = 2.*inv_r3*inv_r
                        - C1*inv_r3*sf1dy
                        - (3.*c2 - 1.)*inv_r3*steady_(i, j, f);
                    A_(row, col(i, j, v)) = -2.*steady_(i, j, v);

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, f)) = C1*inv_r3*sf2dx;
                    A_(row, col(i, j + 1, f)) = -C1*inv_r3*sf2dx;
                    A_(row, col(i, j - 1, fr)) = -C1*steady_(i, j, fr);
                    A_(row, col(i, j + 1, fr)) = C1*steady_(i, j, fr);

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        + std::complex<double>(0., 1.)*omega_*mesh_(i, j, fr)
                        + 2.*inv_r3*(
                                - inv_r2*mesh_(i - 1, j, fr)
                                + (inv_r + inv_r2)*mesh_(i, j, fr)
                                - inv_r*mesh_(i + 1, j, fr))
                        + C1*inv_r3*sf2dx*f1dy
                        + C1*inv_r3*sf1dy*f2dx
                        - C1*steady_(i, j, fr)*f2dy
                        - C1*sf2dy*mesh_(i, j, fr)
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f)*f2dx
                        + (3.*c2 - 1.)*inv_r3*sf2dx*mesh_(i, j, f)
                        - 2.*(2.*c2 - 1.)*steady_(i, j, fr)*mesh_(i, j, fr)
                        + 2.*steady_(i, j, v)*mesh_(i, j, v);
                    ++row;

                    A_(row, col(i, j, f)) = -(3.*c2 - 1.)*inv_r3*sv1dx;
                    A_(row, col(i, j, fr)) = C1*sv1dy + 2.*c2*steady_(i, j, v);
                    A_(row, col(i - 1, j, v)) = 2.*inv_r3*inv_r2
                        + C1*inv_r3*sf1dy
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f);
                    A_(row, col(i, j, v)) = -2.*inv_r3*(inv_r + inv_r2)
                        + 2.*c2*steady_(i, j, fr);
                    A_(row, col(i, j, v)) += -std::complex<double>(0., 1.)*
                        omega_;
                    A_(row, col(i + 1, j, v)) = 2.*inv_r3*inv_r
                        - C1*inv_r3*sf1dy
                        - (3.*c2 - 1.)*inv_r3*steady_(i, j, f);

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, f)) = C1*inv_r3*sv1dx;
                    A_(row, col(i, j + 1, f)) = -C1*inv_r3*sv1dx;
                    A_(row, col(i, j - 1, v)) = -C1*steady_(i, j, fr);
                    A_(row, col(i, j + 1, v)) = C1*steady_(i, j, fr);

                    /* ======================================================
                     *========================================================*/

                    b_[row] =
                        + std::complex<double>(0., 1.)*omega_*mesh_(i, j, v)
                        + 2.*inv_r3*(
                                - inv_r2*mesh_(i - 1, j, v)
                                + (inv_r + inv_r2)*mesh_(i, j, v)
                                - inv_r*mesh_(i + 1, j, v))
                        - C1*steady_(i, j, fr)*v1dy
                        - C1*sv1dy*mesh_(i, j, fr)
                        + C1*inv_r3*sv1dx*f1dy
                        + C1*inv_r3*sf1dy*v1dx
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f)*v1dx
                        + (3.*c2 - 1.)*inv_r3*sv1dx*mesh_(i, j, f)
                        - 2.*c2*steady_(i, j, fr)*mesh_(i, j, v)
                        - 2.*c2*steady_(i, j, v)*mesh_(i, j, fr);
                    ++row;
                }

                A_(row, col(i, j, f)) = -inv_r;
                A_(row, col(i + 1, j, f)) = inv_r;
                A_(row, col(i, j, fr)) = -0.5;
                A_(row, col(i + 1, j, fr)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, f) - mesh_(i + 1, j, f))
                    + 0.5*(mesh_(i, j, fr) + mesh_(i + 1, j, fr));
                ++row;
            }
            // Far-field entries at i = num_r - 1, j
            /*A_(row, col(num_r - 1, j, fr)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 1, j, v)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, v);
            ++row;*/
            /*A_(row, col(num_r - 3, j, fr)) = 1.;
            A_(row, col(num_r - 2, j, fr)) = -4.;
            A_(row, col(num_r - 1, j, fr)) = 3.;
            b_[row] = -mesh_(num_r - 3, j, fr) + 4.*mesh_(num_r - 2, j, fr)
                - 3.*mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 3, j, v)) = 1.;
            A_(row, col(num_r - 2, j, v)) = -4.;
            A_(row, col(num_r - 1, j, v)) = 3.;
            b_[row] = -mesh_(num_r - 3, j, v) + 4.*mesh_(num_r - 2, j, v)
                - 3.*mesh_(num_r - 1, j, v);
            ++row;*/
            A_(row, col(num_r - 2, j, fr)) = -1.;
            A_(row, col(num_r - 1, j, fr)) = 1.;
            b_[row] = mesh_(num_r - 2, j, fr) - mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 2, j, v)) = -1.;
            A_(row, col(num_r - 1, j, v)) = 1.;
            b_[row] = mesh_(num_r - 2, j, v) - mesh_(num_r - 1, j, v);
            ++row;
        }
        // Equator entries at j = num_theta - 1, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, col(i, num_theta - 1, k)) = 1.;
                b_[row] = mesh_equator_(i, k) - mesh_(i, num_theta - 1, k);
                ++row;
            }
        }

        if (row != num_r*num_theta*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Fourier_Decomposed_Layer::Fourier_Decomposed_Layer(
            Mesh_1D<std::complex<double> >& mesh_pole,
            Mesh_1D<std::complex<double> >& mesh_equator,
            Mesh_2D<double>& mesh_steady,
            Mesh_2D<std::complex<double> >& mesh_bl):
        omega_(0.),
        A_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars()),
        b_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars(), 0.),
        linear_system_(A_, b_),
        mesh_equator_(mesh_equator),
        mesh_pole_(mesh_pole),
        steady_(mesh_steady),
        mesh_(mesh_bl)
    {
#ifdef PARANOID
        if (mesh_bl.num_nodes_x() != mesh_pole.num_nodes() ||
                mesh_bl.num_nodes_x() != mesh_equator.num_nodes() ||
                mesh_bl.num_vars() != mesh_pole.num_vars() ||
                mesh_bl.num_vars() != mesh_equator.num_vars() ||
                mesh_bl.num_nodes_x() != mesh_steady.num_nodes_x() ||
                mesh_bl.num_nodes_y() != mesh_steady.num_nodes_y() ||
                mesh_bl.num_vars() != mesh_steady.num_vars())
            throw Exceptions::Invalid_Argument(
                    "Fourier_Decomposed_Layer::Fourier_Decomposed_Layer",
                    "Mismatching mesh objects supplied to solver");
#endif  // PARANOID
    }

    Fourier_Decomposed_Layer::~Fourier_Decomposed_Layer()
    {
    }

    void Fourier_Decomposed_Layer::solve()
    {
        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);

#ifndef QUIET
        console_out(" 2D solver\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix_3();

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t j(0); j < num_theta; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) += b_[num_vars*(num_r*j + i) + k];

            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_2D<std::complex<double> >&
        Fourier_Decomposed_Layer::solution() const
    {
        return mesh_;
    }

    void Fourier_Decomposed_Layer::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        double mid_theta(0.), c(0.), s(0.);
        std::size_t old_precision(out.precision());

        out.precision(decimal_places);
        out << "# Mesh_2D\n";
        out << "# theta    r    u    v    w\n";

        for (std::size_t j(0), num_theta(mesh_.num_nodes_y());
                j < num_theta - 1; j += output_skip + 1)
        {
            mid_theta = 0.5*(mesh_.node_y(j) + mesh_.node_y(j + 1));
            std::complex<double> temp;
            for (std::size_t i(0), num_r(mesh_.num_nodes_x());
                    i < num_r; i += output_skip + 1)
            {
                c = std::cos(mid_theta);
                s = std::sin(mid_theta);
                out << mid_theta << ' ';
                out << mesh_.node_x(i) << ' ';
                temp = -0.5*(mesh_(i, j, fr) 
                        + mesh_(i, j + 1, fr));
                out << temp.real() << ' ' << temp.imag() << ' ';
                temp = 0.5*(mesh_(i, j, v) + mesh_(i, j + 1, v));
                out << temp.real() << ' ' << temp.imag() << ' ';
                temp = (3.*c*c - 1.)*0.5*(mesh_(i, j, f) + mesh_(i, j + 1, f))
                    + s*c*(mesh_(i, j + 1, f) - mesh_(i, j, f))/(
                            mesh_.node_y(j + 1)- mesh_.node_y(j));
                out << temp.real() << ' ' << temp.imag() << '\n';
            }
            out << '\n';
        }
        out << '\n';
        out.flush();
        out.precision(old_precision);
    }
}   // namespace

