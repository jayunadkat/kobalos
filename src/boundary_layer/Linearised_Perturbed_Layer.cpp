#include <cmath>

#include <boundary_layer/Linearised_Perturbed_Layer.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Linearised_Perturbed_Layer::tolerance_ = 1e-8;
    std::size_t Linearised_Perturbed_Layer::max_iterations_ = 50;

    Linearised_Perturbed_Layer::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars, const std::size_t& num_r) :
        nv_(num_vars),
        nr_(num_r)
    {
    }

    Linearised_Perturbed_Layer::Matrix_Column::~Matrix_Column()
    {
    }

    std::size_t Linearised_Perturbed_Layer::Matrix_Column::operator()(
            const std::size_t& i, const std::size_t& j, const std::size_t& k)
    {
        return nv_*(nr_*j + i) + k;
    }

    double Linearised_Perturbed_Layer::sphere_rotation(const double& t)
    {
        if (ext_rot_ == NULL)
            return 0.;
        else
            return (*ext_rot_)(t_);
    }

    void Linearised_Perturbed_Layer::assemble_matrix_3(const double& dt)
    {
        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2(0.), inv_r3(0.), inv_theta(0.), inv_t(0.);
        double s(0.), c(0.), c2(0.), C1(0.);
        double sf1dy(0.), sf2dx(0.), sf2dy(0.), sv1dx(0.), sv1dy(0.);
        double f1dy(0.), f2dx(0.), f2dy(0.), v1dx(0.), v1dy(0.);
        double f1y_o(0.), f2x_o(0.), f2y_o(0.), f2xx_o(0.);
        double v1x_o(0.), v1y_o(0.), v1xx_o(0.);
        Matrix_Column col(num_vars, num_r);

        A_.assign(num_r*num_theta*num_vars);
        b_.assign(num_r*num_theta*num_vars, 0.);

        // Polar entries at j = 0, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, num_vars*i + k) = 1.;
                b_[row] = mesh_pole_(i, k) - mesh_(i, 0, k);
                ++row;
            }
        }
        // Interior theta values
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            // Sphere surface entries at i = 0, j.
            A_(row, col(0, j, f)) = 1.;
            b_[row] = -mesh_(0, j, f);
            ++row;
            A_(row, col(0, j, fr)) = 1.;
            b_[row] = -mesh_(0, j, fr);
            ++row;
            A_(row, col(0, j, v)) = 1.;
            if (unsteady)
                b_[row] = sphere_rotation(t_) - mesh_(0, j, v);
            else
                b_[row] = 1. - mesh_(0, j, v);
            ++row;

            // Interior of boundary layer
            for (std::size_t i(0); i < num_r - 1; ++i)
            {
                inv_r = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i));
                if (i > 0)
                {
                    inv_r2 = 1./(mesh_.node_x(i) - mesh_.node_x(i - 1));
                    inv_r3 = 1./(mesh_.node_x(i + 1) - mesh_.node_x(i - 1));
                }
                inv_theta = 1./(mesh_.node_y(j + 1) - mesh_.node_y(j - 1));
                s = std::sin(mesh_.node_y(j));
                c = std::cos(mesh_.node_y(j));
                c2 = c*c;
                C1 = s*c*inv_theta;
                if (i > 0)
                {
                    sf1dy = steady_(i, j + 1, f) - steady_(i, j - 1, f);
                    sf2dx = steady_(i + 1, j, fr) - steady_(i - 1, j, fr);
                    sf2dy = steady_(i, j + 1, fr) - steady_(i, j - 1, fr);
                    sv1dx = steady_(i + 1, j, v) - steady_(i - 1, j, v);
                    sv1dy = steady_(i, j + 1, v) - steady_(i, j - 1, v);

                    f1dy = mesh_(i, j + 1, f) - mesh_(i, j - 1, f);
                    f2dx = mesh_(i + 1, j, fr) - mesh_(i - 1, j, fr);
                    f2dy = mesh_(i, j + 1, fr) - mesh_(i, j - 1, fr);
                    v1dx = mesh_(i + 1, j, v) - mesh_(i - 1, j, v);
                    v1dy = mesh_(i, j + 1, v) - mesh_(i, j - 1, v);
                }
                if (unsteady && i > 0)
                {
                    inv_t = 1./dt;
                    f1y_o = inv_theta*(
                            + prev_(i, j + 1, f)
                            - prev_(i, j - 1, f));
                    f2x_o = inv_r3*(
                            + prev_(i + 1, j, fr)
                            - prev_(i - 1, j, fr));
                    f2y_o = inv_theta*(
                            + prev_(i, j + 1, fr)
                            - prev_(i, j - 1, fr));
                    f2xx_o = 2.*inv_r3*(
                            + inv_r2*prev_(i - 1, j, fr)
                            - (inv_r + inv_r2)*prev_(i, j, fr)
                            + inv_r*prev_(i + 1, j, fr));
                    v1x_o = inv_r3*(
                            + prev_(i + 1, j, v)
                            - prev_(i - 1, j, v));
                    v1y_o = inv_theta*(
                            + prev_(i, j + 1, v)
                            - prev_(i, j - 1, v));
                    v1xx_o = 2.*inv_r3*(
                            + inv_r2*prev_(i - 1, j, v)
                            - (inv_r + inv_r2)*prev_(i, j, v)
                            + inv_r*prev_(i + 1, j, v));
                }

                // One equation mid-node in r, two at nodal locations
                if (i > 0)
                {
                    A_(row, col(i, j, f)) = -(3.*c2 - 1.)*inv_r3*sf2dx;
                    A_(row, col(i - 1, j, fr)) = 2.*inv_r3*inv_r2
                        + C1*inv_r3*sf1dy
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f);
                    A_(row, col(i, j, fr)) = -2.*inv_r3*(inv_r + inv_r2)
                        + C1*sf2dy + 2.*(2.*c2 - 1.)*steady_(i, j, fr);
                    if (unsteady)
                        A_(row, col(i, j, fr)) += -2.*inv_t;
                    A_(row, col(i + 1, j, fr)) = 2.*inv_r3*inv_r
                        - C1*inv_r3*sf1dy
                        - (3.*c2 - 1.)*inv_r3*steady_(i, j, f);
                    A_(row, col(i, j, v)) = -2.*steady_(i, j, v);

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, f)) = C1*inv_r3*sf2dx;
                    A_(row, col(i, j + 1, f)) = -C1*inv_r3*sf2dx;
                    A_(row, col(i, j - 1, fr)) = -C1*steady_(i, j, fr);
                    A_(row, col(i, j + 1, fr)) = C1*steady_(i, j, fr);

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        + 2.*inv_r3*(
                                - inv_r2*mesh_(i - 1, j, fr)
                                + (inv_r + inv_r2)*mesh_(i, j, fr)
                                - inv_r*mesh_(i + 1, j, fr))
                        + C1*inv_r3*sf2dx*f1dy
                        + C1*inv_r3*sf1dy*f2dx
                        - C1*steady_(i, j, fr)*f2dy
                        - C1*sf2dy*mesh_(i, j, fr)
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f)*f2dx
                        + (3.*c2 - 1.)*inv_r3*sf2dx*mesh_(i, j, f)
                        - 2.*(2.*c2 - 1.)*steady_(i, j, fr)*mesh_(i, j, fr)
                        + 2.*steady_(i, j, v)*mesh_(i, j, v);
                    if (unsteady)
                        b_[row] +=
                            + 2.*inv_t*(
                                    + mesh_(i, j, fr)
                                    - prev_(i, j, fr))
                            - f2xx_o
                            + s*c*inv_r3*sf2dx*f1y_o
                            + s*c*inv_theta*sf1dy*f2x_o
                            - s*c*steady_(i, j, fr)*f2y_o
                            - s*c*inv_theta*sf2dy*prev_(i, j, fr)
                            + (3.*c2 - 1.)*steady_(i, j, f)*f2x_o
                            + (3.*c2 - 1.)*inv_r3*sf2dx*prev_(i, j, f)
                            - 2.*(2.*c2 - 1.)*steady_(i, j, fr)*prev_(i, j, fr)
                            + 2.*steady_(i, j, v)*prev_(i, j, v);
                    ++row;

                    A_(row, col(i, j, f)) = -(3.*c2 - 1.)*inv_r3*sv1dx;
                    A_(row, col(i, j, fr)) = C1*sv1dy + 2.*c2*steady_(i, j, v);
                    A_(row, col(i - 1, j, v)) = 2.*inv_r3*inv_r2
                        + C1*inv_r3*sf1dy
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f);
                    A_(row, col(i, j, v)) = -2.*inv_r3*(inv_r + inv_r2)
                        + 2.*c2*steady_(i, j, fr);
                    if (unsteady)
                        A_(row, col(i, j, v)) += -2.*inv_t;
                    A_(row, col(i + 1, j, v)) = 2.*inv_r3*inv_r
                        - C1*inv_r3*sf1dy
                        - (3.*c2 - 1.)*inv_r3*steady_(i, j, f);

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, f)) = C1*inv_r3*sv1dx;
                    A_(row, col(i, j + 1, f)) = -C1*inv_r3*sv1dx;
                    A_(row, col(i, j - 1, v)) = -C1*steady_(i, j, fr);
                    A_(row, col(i, j + 1, v)) = C1*steady_(i, j, fr);

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        + 2.*inv_r3*(
                                - inv_r2*mesh_(i - 1, j, v)
                                + (inv_r + inv_r2)*mesh_(i, j, v)
                                - inv_r*mesh_(i + 1, j, v))
                        - C1*steady_(i, j, fr)*v1dy
                        - C1*sv1dy*mesh_(i, j, fr)
                        + C1*inv_r3*sv1dx*f1dy
                        + C1*inv_r3*sf1dy*v1dx
                        + (3.*c2 - 1.)*inv_r3*steady_(i, j, f)*v1dx
                        + (3.*c2 - 1.)*inv_r3*sv1dx*mesh_(i, j, f)
                        - 2.*c2*steady_(i, j, fr)*mesh_(i, j, v)
                        - 2.*c2*steady_(i, j, v)*mesh_(i, j, fr);
                    if (unsteady)
                        b_[row] +=
                            + 2.*inv_t*(
                                    + mesh_(i, j, v)
                                    - prev_(i, j, v))
                            - v1xx_o
                            - s*c*steady_(i, j, fr)*v1y_o
                            - s*c*inv_theta*sv1dy*prev_(i, j, fr)
                            + s*c*inv_r3*sv1dx*f1y_o
                            + s*c*inv_theta*sf1dy*v1x_o
                            + (3.*c2 - 1.)*steady_(i, j, f)*v1x_o
                            + (3.*c2 - 1.)*inv_r3*sv1dx*prev_(i, j, f)
                            - 2.*c2*steady_(i, j, fr)*prev_(i, j, v)
                            - 2.*c2*steady_(i, j, v)*prev_(i, j, fr);
                    ++row;
                }

                A_(row, col(i, j, f)) = -inv_r;
                A_(row, col(i + 1, j, f)) = inv_r;
                A_(row, col(i, j, fr)) = -0.5;
                A_(row, col(i + 1, j, fr)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, f) - mesh_(i + 1, j, f))
                    + 0.5*(mesh_(i, j, fr) + mesh_(i + 1, j, fr));
                ++row;
            }
            // Far-field entries at i = num_r - 1, j
            A_(row, col(num_r - 1, j, fr)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, fr);
            ++row;
            A_(row, col(num_r - 1, j, v)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, v);
            ++row;
        }
        // Equator entries at j = num_theta - 1, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, col(i, num_theta - 1, k)) = 1.;
                b_[row] = mesh_equator_(i, k) - mesh_(i, num_theta - 1, k);
                ++row;
            }
        }

        if (row != num_r*num_theta*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Linearised_Perturbed_Layer::Linearised_Perturbed_Layer(
            Mesh_1D<double>& mesh_pole, Mesh_1D<double>& mesh_equator,
            Mesh_2D<double>& mesh_steady, Mesh_2D<double>& mesh_bl):
        S_(0.),
        t_(0.),
        ext_rot_(NULL),
        A_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars()),
        b_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars(), 0.),
        linear_system_(A_, b_),
        mesh_equator_(mesh_equator),
        mesh_pole_(mesh_pole),
        steady_(mesh_steady),
        prev_(mesh_bl),
        mesh_(mesh_bl)
    {
#ifdef PARANOID
        if (mesh_bl.num_nodes_x() != mesh_pole.num_nodes() ||
                mesh_bl.num_nodes_x() != mesh_equator.num_nodes() ||
                mesh_bl.num_vars() != mesh_pole.num_vars() ||
                mesh_bl.num_vars() != mesh_equator.num_vars() ||
                mesh_bl.num_nodes_x() != mesh_steady.num_nodes_x() ||
                mesh_bl.num_nodes_y() != mesh_steady.num_nodes_y() ||
                mesh_bl.num_vars() != mesh_steady.num_vars())
            throw Exceptions::Invalid_Argument(
                    "Linearised_Perturbed_Layer::Linearised_Perturbed_Layer",
                    "Mismatching mesh objects supplied to solver");
#endif  // PARANOID
    }

    Linearised_Perturbed_Layer::~Linearised_Perturbed_Layer()
    {
    }

    void Linearised_Perturbed_Layer::step(const double& dt)
    {
        if (dt <= 0)
            throw Exceptions::Invalid_Argument("PDE_IBVP::step",
                    "dt > 0 is required");

        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);
        t_ += dt;

#ifndef QUIET
        console_out(" 2D solver, t = " + str_convert(t_) + "\n");
#ifdef DEBUG
        console_out(" -----------------------\n");
#endif  // DEBUG
#endif  // QUIET

        prev_.set_vars_vector(mesh_.get_vars_vector());

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix_3(dt);

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t j(0); j < num_theta; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) += b_[num_vars*(num_r*j + i) + k];

            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_2D<double>& Linearised_Perturbed_Layer::last_solution() const
    {
        return prev_;
    }

    void Linearised_Perturbed_Layer::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        double mid_theta(0.), c(0.), s(0.);
        std::size_t old_precision(out.precision());

        out.precision(decimal_places);
        out << "# Solution at t = " << t_ << '\n';
        out << "# theta    r    u    v    w\n";

        for (std::size_t j(0), num_theta(mesh_.num_nodes_y());
                j < num_theta - 1; j += output_skip + 1)
        {
            mid_theta = 0.5*(mesh_.node_y(j) + mesh_.node_y(j + 1));
            for (std::size_t i(0), num_r(mesh_.num_nodes_x());
                    i < num_r; i += output_skip + 1)
            {
                c = std::cos(mid_theta);
                s = std::sin(mid_theta);
                out << mid_theta << ' ';
                out << mesh_.node_x(i) << ' ';
                out << -0.5*(mesh_(i, j, fr)
                        + mesh_(i, j + 1, fr)) << ' ';
                out << 0.5*(mesh_(i, j, v) + mesh_(i, j + 1, v)) << ' ';
                out << (3.*c*c - 1.)*0.5*(mesh_(i, j, f) +
                        mesh_(i, j + 1, f))
                    + s*c*(mesh_(i, j + 1, f) - mesh_(i, j, f))/(
                            mesh_.node_y(j + 1)
                            - mesh_.node_y(j)) << '\n';
            }
            out << '\n';
        }
        out.precision(old_precision);
    }
}   // namespace

