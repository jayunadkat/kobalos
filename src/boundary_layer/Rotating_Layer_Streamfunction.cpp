#include <cmath>

#include <boundary_layer/Rotating_Layer_Streamfunction.h>
#include <Console_Output.h>
#include <Timer.h>

namespace Kobalos
{
    double Rotating_Layer_Streamfunction::tolerance_ = 1e-8;
    std::size_t Rotating_Layer_Streamfunction::max_iterations_ = 20;

    Rotating_Layer_Streamfunction::Matrix_Column::Matrix_Column(
            const std::size_t& num_vars, const std::size_t& num_r) :
        nv_(num_vars),
        nr_(num_r)
    {
    }

    Rotating_Layer_Streamfunction::Matrix_Column::~Matrix_Column()
    {
    }

    double Rotating_Layer_Streamfunction::sphere_spin(const double& t)
    {
        if (sphere_spin_)
            return (*sphere_spin_)(t);
        else
            return 1. - std::exp(-t*t);
    }

    double Rotating_Layer_Streamfunction::sphere_wobble(const double& t)
    {
        if (sphere_wobble_)
            return (*sphere_wobble_)(t);
        else
            return 0.;
    }

    void Rotating_Layer_Streamfunction::assemble_matrix(const double& dt)
    {
        bool unsteady(dt > tolerance_);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());

        /*====================================================================
         * ==================================================================
         *
         *                          MATRIX ASSEMBLY                           */

        std::size_t row(0);
        double inv_r(0.), inv_r2a(0.), inv_r2b(0.), inv_r3(0.), inv_theta(0.),
               inv_t(0.);
        double s(0.), c(0.), c2(0.), C1(0.);
        double f1dy(0.), f2dx(0.), f2dy(0.), v1dx(0.), v1dy(0.),
               f2dx2(0.), v1dx2(0.);
        double f1y_o(0.), f2x_o(0.), f2y_o(0.), f2xx_o(0.);
        double v1x_o(0.), v1y_o(0.), v1xx_o(0.);
        Vector<double> mid_state(num_vars, 0.);

        A_.assign(num_r*num_theta*num_vars);
        b_.assign(num_r*num_theta*num_vars, 0.);

        // Polar entries at j = 0, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, num_vars*i + k) = 1.;
                b_[row] = mesh_pole_(i, k) - mesh_(i, 0, k);
                ++row;
            }
        }
        // Interior theta values
        for (std::size_t j(1); j < num_theta - 1; ++j)
        {
            // Sphere surface entries at i = 0, j.
            A_(row, col(0, j, F)) = 1.;
            b_[row] = -mesh_(0, j, F);
            ++row;
            A_(row, col(0, j, FR)) = 1.;
            b_[row] = -mesh_(0, j, FR);
            ++row;
            A_(row, col(0, j, V)) = 1.;
            if (unsteady)
                b_[row] =
                    + sphere_spin(t_)*(sgn_ + sphere_wobble(t_))
                    - mesh_(0, j, V);
            else
                b_[row] = sgn_ - mesh_(0, j, V);
            ++row;

            // Interior of boundary layer
            for (std::size_t i(0); i < num_r - 1; ++i)
            {
                inv_r = 1./((mesh_.node_x(i + 1) - mesh_.node_x(i))*
                        mesh_.map_ds_x(
                            0.5*(mesh_.node_x(i) + mesh_.node_x(i + 1))));
                if (i > 0)
                {
                    inv_r3 = 1./((mesh_.node_x(i + 1) - mesh_.node_x(i - 1))*
                            mesh_.map_ds_x(mesh_.node_x(i)));
                    inv_r2a = -mesh_.map_ds2_x(mesh_.node_x(i))/(
                            (mesh_.node_x(i + 1) - mesh_.node_x(i - 1))*
                            std::pow(mesh_.map_ds_x(mesh_.node_x(i)), 3.));
                    inv_r2b = std::pow(1./(
                                (mesh_.node_x(i + 1) - mesh_.node_x(i))*
                                mesh_.map_ds_x(mesh_.node_x(i))), 2.);
                }
                inv_theta = 1./((mesh_.node_y(j + 1) - mesh_.node_y(j - 1))*
                        mesh_.map_ds_y(mesh_.node_y(j)));
                s = std::sin(mesh_.unmapped_node_y(j));
                c = std::cos(mesh_.unmapped_node_y(j));
                c2 = c*c;
                C1 = s*c*inv_theta;
                if (i > 0)
                {
                    f1dy = mesh_(i, j + 1, F) - mesh_(i, j - 1, F);
                    f2dx = mesh_(i + 1, j, FR) - mesh_(i - 1, j, FR);
                    f2dy = mesh_(i, j + 1, FR) - mesh_(i, j - 1, FR);
                    v1dx = mesh_(i + 1, j, V) - mesh_(i - 1, j, V);
                    v1dy = mesh_(i, j + 1, V) - mesh_(i, j - 1, V);
                    f2dx2 = 
                        + mesh_(i + 1, j, FR)
                        - 2.*mesh_(i, j, FR)
                        + mesh_(i - 1, j, FR);
                    v1dx2 =
                        + mesh_(i + 1, j, V)
                        - 2.*mesh_(i, j, V)
                        + mesh_(i - 1, j, V);
                }
                if (unsteady && i > 0)
                {
                    inv_t = 1./dt;
                    f1y_o = inv_theta*(
                            + prev_(i, j + 1, F)
                            - prev_(i, j - 1, F));
                    f2x_o = inv_r3*(
                            + prev_(i + 1, j, FR)
                            - prev_(i - 1, j, FR));
                    f2y_o = inv_theta*(
                            + prev_(i, j + 1, FR)
                            - prev_(i, j - 1, FR));
                    f2xx_o = inv_r2a*(prev_(i + 1, j, FR) - prev_(i - 1, j, FR))
                        + inv_r2b*(
                                + prev_(i - 1, j, FR)
                                - 2.*prev_(i, j, FR)
                                + prev_(i + 1, j, FR));
                    v1x_o = inv_r3*(
                            + prev_(i + 1, j, V)
                            - prev_(i - 1, j, V));
                    v1y_o = inv_theta*(
                            + prev_(i, j + 1, V)
                            - prev_(i, j - 1, V));
                    v1xx_o = inv_r2a*(prev_(i + 1, j, V) - prev_(i - 1, j, V))
                        + inv_r2b*(
                                + prev_(i - 1, j, V)
                                - 2.*prev_(i, j, V)
                                + prev_(i + 1, j, V));
                }

                // One equation mid-node in r, two at nodal locations
                if (i > 0)
                {
                    A_(row, col(i, j, F)) = -Ro_*(3.*c2 - 1.)*inv_r3*f2dx;
                    A_(row, col(i - 1, j, FR)) =
                        - inv_r2a
                        + inv_r2b
                        + Ro_*(
                                + C1*inv_r3*f1dy
                                + (3.*c2 - 1.)*inv_r3*mesh_(i, j, F)
                              );
                    A_(row, col(i, j, FR)) =
                        - 2.*inv_r2b
                        + Ro_*(
                                + C1*f2dy
                                + 2.*(2.*c2 - 1.)*mesh_(i, j, FR)
                              );
                    if (unsteady)
                        A_(row, col(i, j, FR)) += -2.*Ro_*inv_t;
                    A_(row, col(i + 1, j, FR)) =
                        + inv_r2a
                        + inv_r2b
                        + Ro_*(
                                - C1*inv_r3*f1dy
                                - (3.*c2 - 1.)*inv_r3*mesh_(i, j, F)
                              );
                    A_(row, col(i, j, V)) =
                        - 2.*Ro_*mesh_(i, j, V)
                        - 2.;

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, F)) = Ro_*C1*inv_r3*f2dx;
                    A_(row, col(i, j + 1, F)) = -Ro_*C1*inv_r3*f2dx;
                    A_(row, col(i, j - 1, FR)) = -Ro_*C1*mesh_(i, j, FR);
                    A_(row, col(i, j + 1, FR)) = Ro_*C1*mesh_(i, j, FR);

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        - inv_r2a*f2dx
                        - inv_r2b*f2dx2
                        + Ro_*(
                                + C1*inv_r3*f2dx*f1dy
                                - C1*f2dy*mesh_(i, j, FR)
                                + (3.*c2 - 1.)*inv_r3*f2dx*mesh_(i, j, F)
                                - (2.*c2 - 1.)*mesh_(i, j, FR)*mesh_(i, j, FR)
                                + mesh_(i, j, V)*mesh_(i, j, V)
                              )
                        + 2.*mesh_(i, j, V);
                    if (unsteady)
                        b_[row] +=
                            - f2xx_o
                            + Ro_*(
                                    + 2.*inv_t*(
                                        + mesh_(i, j, FR)
                                        - prev_(i, j, FR))
                                    + s*c*f2x_o*f1y_o
                                    - s*c*prev_(i, j, FR)*f2y_o
                                    + (3.*c2 - 1.)*prev_(i, j, F)*f2x_o
                                    - (2.*c2 - 1.)*prev_(i, j, FR)
                                        *prev_(i, j, FR)
                                    + prev_(i, j, V)*prev_(i, j, V)
                                  )
                            + 2.*prev_(i, j, V);
                    ++row;

                    A_(row, col(i, j, F)) = -Ro_*(3.*c2 - 1.)*inv_r3*v1dx;
                    A_(row, col(i, j, FR)) = 
                        + Ro_*(
                                + C1*v1dy
                                + 2.*c2*mesh_(i, j, V)
                              )
                        + 2.*c2;
                    A_(row, col(i - 1, j, V)) =
                        - inv_r2a
                        + inv_r2b
                        + Ro_*(
                                + C1*inv_r3*f1dy
                                + (3.*c2 - 1.)*inv_r3*mesh_(i, j, F)
                              );
                    A_(row, col(i, j, V)) =
                        - 2.*inv_r2b
                        + 2.*Ro_*c2*mesh_(i, j, FR);
                    if (unsteady)
                        A_(row, col(i, j, V)) += -2.*Ro_*inv_t;
                    A_(row, col(i + 1, j, V)) =
                        + inv_r2a
                        + inv_r2b
                        + Ro_*(
                                - C1*inv_r3*f1dy
                                - (3.*c2 - 1.)*inv_r3*mesh_(i, j, F)
                              );

                    /*========================================================
                     * ======================================================
                     *
                     *               FULL CENTRAL DIFFERENCING                */

                    A_(row, col(i, j - 1, F)) = Ro_*C1*inv_r3*v1dx;
                    A_(row, col(i, j + 1, F)) = -Ro_*C1*inv_r3*v1dx;
                    A_(row, col(i, j - 1, V)) = -Ro_*C1*mesh_(i, j, FR);
                    A_(row, col(i, j + 1, V)) = Ro_*C1*mesh_(i, j, FR);

                    /* ======================================================
                     *========================================================*/

                    b_[row] = 
                        - inv_r2a*v1dx
                        - inv_r2b*v1dx2
                        + Ro_*(
                                - C1*mesh_(i, j, FR)*v1dy
                                + C1*inv_r3*v1dx*f1dy
                                + (3.*c2 - 1.)*inv_r3*v1dx*mesh_(i, j, F)
                                - 2.*c2*mesh_(i, j, FR)*mesh_(i, j, V)
                              )
                        - 2.*c2*mesh_(i, j, FR);
                    if (unsteady)
                        b_[row] +=
                            - v1xx_o
                            + Ro_*(
                                    + 2.*inv_t*(
                                        + mesh_(i, j, V)
                                        - prev_(i, j, V))
                                    - s*c*prev_(i, j, FR)*v1y_o
                                    + s*c*v1x_o*f1y_o
                                    + (3.*c2 - 1.)*prev_(i, j, F)*v1x_o
                                    - 2.*c2*prev_(i, j, FR)*prev_(i, j, V)
                                  )
                            - 2.*c2*prev_(i, j, FR);
                    ++row;
                }

                A_(row, col(i, j, F)) = -inv_r;
                A_(row, col(i + 1, j, F)) = inv_r;
                A_(row, col(i, j, FR)) = -0.5;
                A_(row, col(i + 1, j, FR)) = -0.5;
                b_[row] = inv_r*(mesh_(i, j, F) - mesh_(i + 1, j, F))
                    + 0.5*(mesh_(i, j, FR) + mesh_(i + 1, j, FR));
                ++row;
            }
            // Far-field entries at i = num_r - 1, j
            A_(row, col(num_r - 1, j, FR)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, FR);
            ++row;
            A_(row, col(num_r - 1, j, V)) = 1.;
            b_[row] = -mesh_(num_r - 1, j, V);
            ++row;
        }
        // Equator entries at j = num_theta - 1, i
        for (std::size_t i(0); i < num_r; ++i)
        {
            for (std::size_t k(0); k < num_vars; ++k)
            {
                A_(row, col(i, num_theta - 1, k)) = 1.;
                b_[row] = mesh_equator_(i, k) - mesh_(i, num_theta - 1, k);
                ++row;
            }
        }

        if (row != num_r*num_theta*num_vars)
            throw Exceptions::Generic("Solution",
                    "Matrix assembly failed to fill matrix correctly");

        /* ==================================================================
         *====================================================================*/
    }

    Rotating_Layer_Streamfunction::Rotating_Layer_Streamfunction(
            Mesh_1D<double>& mesh_pole, Mesh_1D<double>& mesh_equator,
            Mesh_2D<double>& mesh_bl):
        col(mesh_bl.num_vars(), mesh_bl.num_nodes_x()),
        Ro_(0.5),
        sgn_(-1),
        t_(0.),
        sphere_spin_(NULL),
        sphere_wobble_(NULL),
        A_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars()),
        b_(mesh_bl.num_nodes_x()*mesh_bl.num_nodes_y()*mesh_bl.num_vars(), 0.),
        linear_system_(A_, b_),
        mesh_equator_(mesh_equator),
        mesh_pole_(mesh_pole),
        prev_(mesh_bl),
        mesh_(mesh_bl)
    {
        if (mesh_bl.num_vars() != 3)
            throw Exceptions::Invalid_Argument(
                    "Rotating_Layer_Streamfunction::"
                    "Rotating_Layer_Streamfunction",
                    "Meshes with num_vars_ = 3 required");
    }

    Rotating_Layer_Streamfunction::~Rotating_Layer_Streamfunction()
    {
    }

    void Rotating_Layer_Streamfunction::solve_steady()
    {
        step(0.);
    }

    void Rotating_Layer_Streamfunction::step(const double& dt)
    {
        std::size_t counter(0);
        std::size_t num_r(mesh_.num_nodes_x());
        std::size_t num_theta(mesh_.num_nodes_y());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);
        if (dt > tolerance_)
            t_ += dt;

#ifndef QUIET
        if (dt > tolerance_)
            console_out(" 2D solver, t = " + str_convert(t_) + "\n");
        else
            console_out(" 2D solver\n");
#ifdef DEBUG
        if (dt > tolerance_)
            console_out(" -----------------------\n");
        else
            console_out(" ---------\n");
#endif  // DEBUG
#endif  // !QUIET

        prev_.set_vars_vector(mesh_.get_vars_vector());

        do
        {
#if not defined QUIET && defined TIME
            Timer t_matrix("Matrix assembly");
            t_matrix.start();
#endif  // !QUIET && TIME

            assemble_matrix(dt);

#if not defined QUIET && defined TIME
            t_matrix.stop();
            t_matrix.print();
#endif  // !QUIET && TIME

            max_residual = b_.inf_norm();

#if not defined QUIET && defined DEBUG
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: " + 
                    str_convert(max_residual) + "\n");
#endif  // !QUIET && DEBUG

            if (max_residual < tolerance_)
                break;

            linear_system_.solve();

#if not defined QUIET && defined TIME
            console_out.endl();
#endif  // !QUIET && TIME

            for (std::size_t i(0); i < num_r; ++i)
                for (std::size_t j(0); j < num_theta; ++j)
                    for (std::size_t k(0); k < num_vars; ++k)
                        mesh_(i, j, k) += b_[num_vars*(num_r*j + i) + k];
            ++counter;
        }
        while (max_residual > tolerance_ && counter < max_iterations_);

        if (counter >= max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    const Mesh_2D<double>& Rotating_Layer_Streamfunction::solution() const
    {
        return mesh_;
    }

    void Rotating_Layer_Streamfunction::dump_flow_variables(std::ostream& out,
            const std::size_t& decimal_places,
            const std::size_t& output_skip) const
    {
        double mid_theta(0.), c(0.), s(0.);
        std::size_t old_precision(out.precision());

        out.precision(decimal_places);
        out << "# Mesh_2D\n";
        out << "# Solution at t = " << t_ << '\n';
        out << "# theta    r    u    v    w\n";

        for (std::size_t j(0), num_theta(mesh_.num_nodes_y());
                j < num_theta - 1; j += output_skip + 1)
        {
            mid_theta = 0.5*(mesh_.unmapped_node_y(j)
                    + mesh_.unmapped_node_y(j + 1));
            for (std::size_t i(0), num_r(mesh_.num_nodes_x());
                    i < num_r; i += output_skip + 1)
            {
                c = std::cos(mid_theta);
                s = std::sin(mid_theta);
                out << mid_theta << ' ';
                out << mesh_.unmapped_node_x(i) << ' ';
                out << -0.5*(mesh_(i, j, FR)
                        + mesh_(i, j + 1, FR)) << ' ';
                out << 0.5*(mesh_(i, j, V) + mesh_(i, j + 1, V)) << ' ';
                out << (3.*c*c - 1.)*0.5*(mesh_(i, j, F) +
                        mesh_(i, j + 1, F))
                    + s*c*(mesh_(i, j + 1, F) - mesh_(i, j, F))/(
                            mesh_.unmapped_node_y(j + 1)
                            - mesh_.unmapped_node_y(j))
                    << '\n';
            }
            out << '\n';
        }
        out << '\n';
        out.precision(old_precision);
    }
}   // namespace

