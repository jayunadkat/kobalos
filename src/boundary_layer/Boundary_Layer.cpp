#include <boundary_layer/Boundary_Layer.h>

namespace BoundaryLayer
{
    using namespace Kobalos;

    bool unsteady = false;
    double S = 0.;
    external_func ext_bump = NULL;

    double Omega(const double& t)
    {
        if (unsteady)
        {
            if (ext_bump)
                return (*ext_bump)(t);
            else
                return 1. + (S - 1)*std::exp(-10.*t*t);
        }
        else
            return 1.;
    }

    void set_external_bump(external_func bump)
    {
        ext_bump = bump;
    }

    void Pole_Problem::update_jacobian_t()
    {
    }

    void Pole_Problem::update_jacobian_x()
    {
    }

    Pole_Problem::Pole_Problem() :
        Equation_1D<double>(5)
    {
    }

    Pole_Problem::~Pole_Problem()
    {
    }

    void Pole_Problem::matrix_t(const Vector<double>& state)
    {
        matrix_t_(frr, fr) = -1.;
        matrix_t_(vr, v) = -1.;
    }

    void Pole_Problem::jacobian_t_vec(const Vector<double>& state)
    {
    }

    void Pole_Problem::jacobian_x_vec(const Vector<double>& state)
    {
    }

    void Equator_Problem::update_jacobian_t()
    {
    }

    void Equator_Problem::update_jacobian_x()
    {
    }

    Equator_Problem::Equator_Problem() :
        Equation_1D<double>(5)
    {
    }

    Equator_Problem::~Equator_Problem()
    {
    }

    void Equator_Problem::matrix_t(const Vector<double>& state)
    {
        matrix_t_(frr, fr) = -1.;
        matrix_t_(vr, v) = -1.;
    }

    void Equator_Problem::jacobian_t_vec(const Vector<double>& state)
    {
    }

    void Equator_Problem::jacobian_x_vec(const Vector<double>& state)
    {
    }

    BC_Left::BC_Left() :
        Residual<double>(3, 5)
    {
    }

    BC_Left::~BC_Left()
    {
    }

    BC_Right::BC_Right() :
        Residual<double>(2, 5)
    {
    }

    BC_Right::~BC_Right()
    {
    }

    void Full_Problem::update_jacobian_t()
    {
    }

    void Full_Problem::update_jacobian_x()
    {
    }

    void Full_Problem::update_jacobian_y()
    {
    }

    Full_Problem::Full_Problem() :
        Equation_2D<double>(5)
    {
    }

    Full_Problem::~Full_Problem()
    {
    }

    void Full_Problem::matrix_t(const Vector<double>& state)
    {
        matrix_t_(frr, fr) = -1.;
        matrix_t_(vr, v) = -1.;
    }

    void Full_Problem::matrix_y(const Vector<double>& state)
    {
        double k(std::sin(x2())*std::cos(x2()));

        matrix_y_(frr, f) = -k*state[frr];
        matrix_y_(frr, fr) = k*state[fr];
        matrix_y_(vr, f) = -k*state[vr];
        matrix_y_(vr, v) = k*state[fr];
    }
}  // namespace

