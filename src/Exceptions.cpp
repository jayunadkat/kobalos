#include <sstream>

#include <Console_Output.h>
#include <Exceptions.h>

namespace Kobalos
{
    namespace Exceptions
    {
        Generic::Generic(const std::string& type) throw()
        {
            message_ = "--------------------------------------------------\n";
            message_ += " Kobalos Exception: " + type + '\n';
            message_ += "--------------------------------------------------\n";
        }

        Generic::Generic(const std::string& type,
                const std::string& info) throw()
        {
            message_ = "--------------------------------------------------\n";
            message_ += " Kobalos Exception: " + type + '\n';
            message_ += "--------------------------------------------------\n";
            message_ += info + "\n\n";

#ifndef QUIET
            console_out(message_);
#endif  // QUIET
        }

        Generic::~Generic() throw()
        {
        }

        const char* Generic::what() const throw()
        {
            std::string what_message = "An unhandled exception occurred.";

#ifndef DEBUG
            what_message += "For further details, remove -DQUIET from your "
                "compiler flags";
#endif  // DEBUG

            return what_message.c_str();
        }

        Range::Range(const std::string& container_name,
                const std::size_t container_size,
                const std::size_t index) :
            Generic("Range")
        {
            std::stringstream temp;

            temp << "Container type: " << container_name << '\n';
            temp << "Container size: " << container_size << '\n';
            temp << "Attempted to access index: " << index << "\n\n";
            message_ += temp.str();

#ifndef QUIET
            console_out(message_);
#endif  // QUIET
        }

        Range::Range(const std::string& container_name,
                const std::size_t container_size1, const std::size_t index1,
                const std::size_t container_size2, const std::size_t index2) :
            Generic("Range")
        {
            std::stringstream temp;

            temp << "Container type: " << container_name << '\n';
            temp << "Container size: " << container_size1;
            temp << " x " << container_size2 << '\n';
            temp << "Attempted to access index: (" << index1;
            temp << ", " << index2 << ")\n\n";
            message_ += temp.str();

#ifndef QUIET
            console_out(message_);
#endif  // QUIET
        }

        Invalid_Argument::Invalid_Argument(const std::string& function_name,
                const std::string& info) :
            Generic("Invalid_Argument")
        {
            message_ += "In function " + function_name + '\n';
            message_ += info + "\n\n";

#ifndef QUIET
            console_out(message_);
#endif  // QUIET
        }

        Mismatch::Mismatch(const std::string& function_name,
                const std::string& container_name) :
            Generic("Mismatch")
        {
            message_ += "In function " + function_name + '\n';
            message_ += "Objects of type " + container_name +
                " have mismatching sizes" + "\n\n";

#ifndef QUIET
            console_out(message_);
#endif  // QUIET
        }

        Mismatch::Mismatch(const std::string& function_name,
                const std::string& container_name1,
                const std::string& container_name2) :
            Generic("Mismatch")
        {
            message_ += "In function " + function_name + '\n';
            message_ += "Objects of type " + container_name1 + " and ";
            message_ += container_name2 + " have mismatching sizes" + "\n\n";

#ifndef QUIET
            console_out(message_);
#endif  // QUIET
        }
    }   // namespace Exceptions
}   // namespace

