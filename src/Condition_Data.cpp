#include <complex>

#include <Condition_Data.h>

namespace Kobalos
{
    template <template <typename, typename> class condition_type_,
             typename type_, typename xtype_>
    Condition_Data<condition_type_, type_, xtype_>::Condition_Data(
            const xtype_& location, condition_type_<type_, xtype_>& condition) :
        coord_(location),
        condition_(&condition)
    {
    }

    template <template <typename, typename> class condition_type_,
             typename type_, typename xtype_>
    Condition_Data<condition_type_, type_, xtype_>::~Condition_Data()
    {
    }

    template <template <typename, typename> class condition_type_,
             typename type_, typename xtype_>
    const bool Condition_Data<condition_type_, type_, xtype_>::sort_predicate(
            const Condition_Data& lhs, const Condition_Data& rhs)
    {
        return lhs.node_ < rhs.node_;
    }

    template class Condition_Data<Residual, double>;
    template class Condition_Data<Residual, std::complex<double> >;
    template class Condition_Data<Residual, std::complex<double>,
             std::complex<double> >;
    template class Condition_Data<Mesh_1D, double>;
    template class Condition_Data<Mesh_1D, std::complex<double> >;
    template class Condition_Data<Mesh_1D, std::complex<double>,
             std::complex<double> >;
}   // namespace
