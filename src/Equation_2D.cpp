#include <complex>

#include <Equation_2D.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    void Equation_2D<type_, xtype_>::update_jacobian_y()
    {
        Vector<type_> temp(this->state_);
        Dense_Matrix<type_> matrix_y_old(matrix_y_);
        
        jacobian_y_.clear();
        for (std::size_t i(0); i < this->num_vars_; ++i)
        {
            temp[i] += this->delta_;
            matrix_y(temp);
            temp[i] -= this->delta_;
            jacobian_y_.push_back((matrix_y_ - matrix_y_old)/this->delta_);
        }
        matrix_y_ = matrix_y_old;
    }

    template <typename type_, typename xtype_>
    Equation_2D<type_, xtype_>::Equation_2D(const std::size_t& order) :
        Equation_1D<type_, xtype_>(order),
        matrix_y_(order, 0.),
        jacobian_y_(order, Dense_Matrix<type_>(order, 0.)),
        jacobian_y_vec_(order, 0.)
    {
    }

    template <typename type_, typename xtype_>
    Equation_2D<type_, xtype_>::Equation_2D(const std::size_t& order,
            const std::size_t& num_vars) :
        Equation_1D<type_, xtype_>(order, num_vars),
        matrix_y_(order, 0.),
        jacobian_y_(order, Dense_Matrix<type_>(order, 0.)),
        jacobian_y_vec_(order, 0.)
    {
    }

    template <typename type_, typename xtype_>
    Equation_2D<type_, xtype_>::~Equation_2D()
    {
    }

    template class Equation_2D<double>;
    template class Equation_2D<std::complex<double> >;
    template class Equation_2D<std::complex<double>, std::complex<double> >;
}   // namespace
