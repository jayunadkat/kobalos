#include <algorithm>
#include <complex>

#include <Exceptions.h>
#include <Conditions_1D.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    Conditions_1D<type_, xtype_>::Conditions_1D(Mesh_1D<type_, xtype_>& mesh) :
        conditions_(),
        mesh_(mesh)
    {
    }

    template <typename type_, typename xtype_>
    Conditions_1D<type_, xtype_>::~Conditions_1D()
    {
    }

    template <typename type_, typename xtype_>
    void Conditions_1D<type_, xtype_>::add(const xtype_& location,
            Residual<type_, xtype_>& condition)
    {
#ifdef PARANOID
            if (condition.num_vars() != mesh_.num_vars())
                throw Exceptions::Invalid_Argument("Conditions_1D::add",
                        "Residual object incompatible with the number"
                        " of variables stored in the mesh object");
#endif  // PARANOID

        conditions_.push_back(Condition_Data<Residual, type_, xtype_>(location,
                    condition));

        if (mesh_.query(conditions_.back().coord_, conditions_.back().node_))
            for (std::size_t i(0), num_nodes(conditions_.size() - 1);
                    i < num_nodes; ++i)
                if (conditions_[i].node_ >= conditions_.back().node_)
                    ++(conditions_[i].node_);

        std::sort(conditions_.begin(), conditions_.end(),
                Condition_Data<Residual, type_, xtype_>::sort_predicate);
    }

    template <typename type_, typename xtype_>
    const std::size_t Conditions_1D<type_, xtype_>::num_conditions() const
    {
        std::size_t total_order(0);
        for (std::size_t i(0), size(conditions_.size()); i < size; ++i)
            total_order += (*(conditions_[i].condition_)).order();
        return total_order;
    }

    template <typename type_, typename xtype_>
    const std::size_t Conditions_1D<type_, xtype_>::num_residuals() const
    {
        return conditions_.size();
    }

    template <typename type_, typename xtype_>
    const xtype_& Conditions_1D<type_, xtype_>::location(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= conditions_.size())
            throw Exceptions::Range("Conditions_1D",
                    conditions_.size(), index);
#endif  // PARANOID

        return conditions_[index].coord_;
    }

    template <typename type_, typename xtype_>
    Residual<type_, xtype_>& Conditions_1D<type_, xtype_>::condition(
            const std::size_t& index)
    {
#ifdef PARANOID
        if (index >= conditions_.size())
            throw Exceptions::Range("Conditions_1D",
                    conditions_.size(), index);
#endif  // PARANOID

        return *(conditions_[index].condition_);
    }

    template <typename type_, typename xtype_>
    const std::size_t& Conditions_1D<type_, xtype_>::node(
            const std::size_t& index) const
    {
#ifdef PARANOID
        if (index >= conditions_.size())
            throw Exceptions::Range("Conditions_1D",
                    conditions_.size(), index);
#endif  // PARANOID

        return conditions_[index].node_;
    }

    template class Conditions_1D<double>;
    template class Conditions_1D<std::complex<double>, double>;
    template class Conditions_1D<std::complex<double>, std::complex<double> >;
}   // namespace

