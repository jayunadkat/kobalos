#include <ctime>
#include <sstream>
#include <string>

#include <Console_Output.h>
#include <Exceptions.h>
#include <Timer.h>

namespace Kobalos
{
    Timer::Timer() :
        stopped_(true),
        counter_(0),
        start_(0),
        delta_t_(0),
        task_("")
    {
    }

    Timer::Timer(const std::string& task) :
        stopped_(true),
        counter_(0),
        start_(0),
        delta_t_(0),
        task_(task)
    {
    }

    Timer::~Timer()
    {
    }

    void Timer::start()
    {
        if (stopped_)
        {
            start_ = clock();
            counter_++;
            stopped_ = false;
        }
    }

    void Timer::stop()
    {
        if (!stopped_)
        {
            delta_t_ += clock() - start_;
            stopped_ = true;
        }
    }

    void Timer::reset()
    {
        stop();
        counter_ = 0;
        delta_t_ = 0;
    }

    const int Timer::count() const
    {
        return counter_;
    }

    const double Timer::running_time() const
    {
        if (counter_ == 0)
            return 0.;
        if (stopped_)
            return 1.e3*double(delta_t_)/CLOCKS_PER_SEC;
        else
            return 1.e3*double(delta_t_ + clock() - start_)/CLOCKS_PER_SEC;
    }

    const double Timer::average_time() const
    {
        if (counter_ == 0)
            return 0.;
        if (stopped_)
            return 1.e3*double(delta_t_)/(CLOCKS_PER_SEC*counter_);
        else
        {
            std::string problem = "Function average_time() ";
            problem += "requires Timer object to be stopped";
            throw Exceptions::Generic("Timer", problem);
            return 0.;
        }
    }

    std::size_t Timer::seconds() const
    {
        if (counter_ == 0)
            return 0;
        if (stopped_)
            return double(delta_t_)/CLOCKS_PER_SEC + 0.5;
        else
            return double(delta_t_ + clock() - start_)/CLOCKS_PER_SEC + 0.5;
    }

    void Timer::print() const
    {
        const double elapsed_time = running_time();
        
        console_out(task_ + "\n-Total elapsed CPU time: ");

        if (elapsed_time > 1000)
        {
            console_out(str_convert(elapsed_time/1000.) + " s\n");
            if (stopped_)
            {
                console_out("-Average time: ");
                console_out(str_convert(average_time()/1000.) + " s\n");
            }
        }
        else
        {
            console_out(str_convert(elapsed_time) + " ms\n");
            if (stopped_ && counter_ > 1)
            {
                console_out("-Average time: ");
                console_out(str_convert(average_time()) + " ms\n");
            }
        }
    }
}   // namespace

