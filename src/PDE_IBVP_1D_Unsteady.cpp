#include <complex>

#include <Console_Output.h>
#include <Exceptions.h>
#include <PDE_IBVP.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    void PDE_IBVP<PDE_1D_Unsteady, type_, xtype_>::assemble_matrix(
            const double& dt)
    {
        std::size_t row(0), condition_index(0),
            num_conditions(conditions_.num_residuals());
        std::size_t eqn_order(equation_.order());
        std::size_t num_nodes(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        xtype_ inv_x(0.);
        double inv_t(1./dt);
        Vector<type_> mid_state(num_vars, 0.), diff_state_t(num_vars, 0.),
            diff_state_x(num_vars, 0.);

        A_.assign(num_nodes*num_vars, 2*num_vars - 1, 0.);
        b_.assign(num_nodes*num_vars, 0.);

        for (std::size_t i(0); i < num_nodes - 1; ++i)
        {
            while (condition_index < num_conditions &&
                    conditions_.node(condition_index) == i)
            {
                conditions_.condition(condition_index).t() = this->t_ + dt;
                conditions_.condition(condition_index).x1() = mesh_.node(i);
                conditions_.condition(condition_index).update(
                        mesh_.get_vars_at_node(i));
                for (std::size_t j(0);
                        j < conditions_.condition(condition_index).order(); ++j)
                {
                    for (std::size_t k(0); k < num_vars; ++k)
                    {
                        A_(row, num_vars*i + k) =
                            conditions_.condition(
                                    condition_index).jacobian()(j, k);
                    }
                    b_[row] = -conditions_.condition(
                            condition_index).residual()[j];
                    ++row;
                }
                ++condition_index;
            }

            inv_x = 1./(mesh_.node(i + 1) - mesh_.node(i));
            for (std::size_t j(0); j < num_vars; ++j)
            {
                mid_state[j] = (
                        + mesh_(i, j)
                        + mesh_(i + 1, j)
                        + prev_sol_(i, j) 
                        + prev_sol_(i + 1, j))/4.;
                diff_state_t[j] = (
                        + mesh_(i, j) 
                        + mesh_(i + 1, j) 
                        - prev_sol_(i, j) 
                        - prev_sol_(i + 1, j)); 
                diff_state_x[j] = (
                        + mesh_(i + 1, j) 
                        + prev_sol_(i + 1, j) 
                        - mesh_(i, j) 
                        - prev_sol_(i, j)); 
            }

            equation_.t() = this->t_ + dt/2.;
            equation_.x1() = (mesh_.node(i) + mesh_.node(i + 1))/2.;
            equation_.update(mid_state);
            equation_.update_jacobian_t_vec(diff_state_t*inv_t);
            equation_.update_jacobian_x_vec(diff_state_x*inv_x);
            for (std::size_t j(0); j < eqn_order; ++j)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    A_(row, num_vars*i + k) +=
                        + equation_.matrix_t()(j, k)*inv_t
                        + equation_.jacobian_t_vec()(j, k)/4.
                        - equation_.matrix_x()(j, k)*inv_x
                        + equation_.jacobian_x_vec()(j, k)/4.
                        - equation_.jacobian()(j, k)/2.;
                    A_(row, num_vars*(i + 1) + k) +=
                        + equation_.matrix_t()(j, k)*inv_t
                        + equation_.jacobian_t_vec()(j, k)/4.
                        + equation_.matrix_x()(j, k)*inv_x
                        + equation_.jacobian_x_vec()(j, k)/4.
                        - equation_.jacobian()(j, k)/2.;
                }
                b_[row] = 2.*equation_.residual()[j];
                for (std::size_t k(0); k < eqn_order; ++k)
                    b_[row] -=
                        + equation_.matrix_t()(j, k)*diff_state_t[k]*inv_t
                        + equation_.matrix_x()(j, k)*diff_state_x[k]*inv_x;
                ++row;
            }
        }

        while (condition_index < num_conditions &&
                conditions_.node(condition_index) == num_nodes - 1)
        {
            conditions_.condition(condition_index).t() = this->t_ + dt;
            conditions_.condition(condition_index).x1() =
                mesh_.node(num_nodes - 1);
            conditions_.condition(condition_index).update(
                    mesh_.get_vars_at_node(num_nodes - 1));
            for (std::size_t j(0);
                    j < conditions_.condition(condition_index).order(); ++j)
            {
                for (std::size_t k(0); k < num_vars; ++k)
                {
                    A_(row, num_vars*(num_nodes - 1) + k) =
                        conditions_.condition(
                                condition_index).jacobian()(j, k);
                }
                b_[row] = -conditions_.condition(
                        condition_index).residual()[j];
                ++row;
            }
            ++condition_index;
        }

#ifdef PARANOID
        if (row != num_nodes*num_vars)
            throw Exceptions::Generic("PDE_IBVP::assemble_matrix",
                    "row != num_nodes*num_vars");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    PDE_IBVP<PDE_1D_Unsteady, type_, xtype_>::PDE_IBVP(
            Equation_1D<type_, xtype_>& equation,
            Conditions_1D<type_, xtype_>& conditions,
            Mesh_1D<type_, xtype_>& mesh) :
        PDE_IBVP<PDE_Base, type_, xtype_>(),
        equation_(equation),
        conditions_(conditions),
        A_(mesh.num_nodes()*mesh.num_vars(), 2*mesh.num_vars() - 1, 0.),
        b_(mesh.num_nodes()*mesh.num_vars(), 0.),
        linear_system_(A_, b_),
        prev_sol_(mesh),
        mesh_(mesh)
    {
#ifdef PARANOID
        if (equation.order() != conditions.num_conditions())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Residual objects form an over/underconstrained system");
        if (equation.num_vars() != conditions.condition(0).num_vars())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Equation and conditions objects have differing degrees"
                    " of freedom");
        if (equation.num_vars() != mesh.num_vars())
            throw Exceptions::Invalid_Argument("PDE_IBVP::PDE_IBVP",
                    "Mesh object has too few degrees of freedom for equation");
#endif  // PARANOID
    }

    template <typename type_, typename xtype_>
    PDE_IBVP<PDE_1D_Unsteady, type_, xtype_>::~PDE_IBVP()
    {
    }

    template <typename type_, typename xtype_>
    void PDE_IBVP<PDE_1D_Unsteady, type_, xtype_>::step(const double& dt)
    {
#ifdef PARANOID
        if (dt == 0)
            throw Exceptions::Invalid_Argument("PDE_IBVP::step",
                    "dt != 0 is required");
#endif  // PARANOID

        std::size_t counter(0);
        std::size_t num_nodes(mesh_.num_nodes());
        std::size_t num_vars(mesh_.num_vars());
        double max_residual(0.);

        prev_sol_.set_vars_vector(mesh_.get_vars_vector());

#ifndef QUIET
        console_out("Solving 1D problem\n");
        console_out(" Time " + str_convert(this->t_ + dt) + "\n");
#endif  // QUIET

        do
        {
            assemble_matrix(dt);
            max_residual = b_.inf_norm();

#if defined DEBUG && not defined QUIET
            console_out("Iteration " + str_convert(counter + 1) + ", ");
            console_out("maximum residual at start: "
                    + str_convert(max_residual) + "\n");
#endif  // DEBUG && !QUIET
            
            if (max_residual < this->tolerance_)
                break;
            linear_system_.solve();

            for (std::size_t i(0); i < num_nodes; ++i)
                for (std::size_t j(0); j < num_vars; ++j)
                    mesh_(i, j) += b_[num_vars*i + j];
            ++counter;
        }
        while (max_residual > this->tolerance_ &&
                counter < this->max_iterations_);
        this->t_ += dt;

        if (counter >= this->max_iterations_)
            throw Exceptions::Generic("Solution",
                    "Solution algorithm failed to converge");
    }

    template <typename type_, typename xtype_>
    const Vector<double>
        PDE_IBVP<PDE_1D_Unsteady, type_, xtype_>::change_two_norm() const
    {
        Vector<double> norms;
        double temp(0.);
        for (std::size_t i(0), num_vars(mesh_.num_vars()),
                num_nodes(mesh_.num_nodes()); i < num_vars; ++i)
        {
            temp = 0.;
            temp += std::pow(std::abs(mesh_(0, i) - prev_sol_(0, i)), 2.)/
                std::abs(mesh_.node(1) - mesh_.node(0));
            temp += std::pow(std::abs(mesh_(num_nodes - 1, i) -
                        prev_sol_(num_nodes - 1, i)), 2.)/std::abs(
                        mesh_.node(num_nodes - 1) - mesh_.node(num_nodes - 2));
            for (std::size_t j(1); j < num_nodes - 1; ++j)
            {
                temp += std::pow(std::abs(mesh_(j, i) - prev_sol_(j, i)), 2.)/
                    std::abs(mesh_.node(j) - mesh_.node(j - 1));
                temp += std::pow(std::abs(mesh_(j, i) - prev_sol_(j, i)), 2.)/
                    std::abs(mesh_.node(j + 1) - mesh_.node(j));
            }
            
            norms.push_back(std::sqrt(temp));
        }

        return norms;
    }

    template class PDE_IBVP<PDE_1D_Unsteady, double, double>;
    template class PDE_IBVP<PDE_1D_Unsteady, std::complex<double>, double>;
    template class PDE_IBVP<PDE_1D_Unsteady, std::complex<double>,
             std::complex<double> >;
}   // namespace

