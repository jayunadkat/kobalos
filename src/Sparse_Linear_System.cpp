#include <complex>
#include <sstream>

#include <Exceptions.h>
#include <Sparse_Linear_System.h>

#ifdef SUPERLU
#include <SLU.h>
#endif

#include <Timer.h>

namespace Kobalos
{
    template <typename type_>
    Sparse_Linear_System<type_>::Sparse_Linear_System(Sparse_Matrix<type_>& A,
            Vector<type_>& b) :
        A_(A),
        b_(b)
    {
#ifdef PARANOID
        if (A_.num_columns() != b_.size())
            throw Exceptions::Mismatch(
                    "Sparse_Linear_System::Sparse_Linear_System()",
                    "Sparse_Matrix", "Vector");
#endif  // PARANOID
    }

    template <typename type_>
    Sparse_Linear_System<type_>::~Sparse_Linear_System()
    {
    }

    template <typename type_>
    void Sparse_Linear_System<type_>::solve()
    {
#ifdef SUPERLU
        solve_superlu();
#else
        solve_native();
        back_substitute();
#endif  // SUPERLU
    }

    template <typename type_>
    std::size_t Sparse_Linear_System<type_>::order() const
    {
        return b_.size();
    }

    template <typename type_>
    void Sparse_Linear_System<type_>::solve_superlu()
    {
        throw Exceptions::Generic("Sparse_Linear_System::solve_superlu()",
                "Not implemented for the selected data type");
    }

    template <>
    void Sparse_Linear_System<std::complex<double> >::solve_superlu()
    {
#ifndef SUPERLU
        throw Exceptions::Generic("Sparse_Linear_System::solve_superlu()",
                "SuperLU support must be enabled by compiling with -DSUPERLU");
#else

        SuperMatrix A, L, U, B;
        Vector<std::complex<double> > a;
        doublecomplex *a2, *b2;
        int *asub, *xa;

        int *perm_r, *perm_c;
        int nrhs, info, m, n, nnz;
        superlu_options_t options;
        SuperLUStat_t stat;

        // Initialise a
        n = A_.num_rows();
        m = A_.num_columns();
        nnz = A_.num_elements();
        nrhs = 1;

        a.assign(nnz, 0.);
        if (!(a2 = doublecomplexMalloc(nnz)))
            throw Exceptions::Generic("SuperLU", "intMalloc fails for a2[]");
        if (!(asub = intMalloc(nnz)))
            throw Exceptions::Generic("SuperLU", "intMalloc fails for asub[]");
        if (!(xa = intMalloc(n + 1)))
            throw Exceptions::Generic("SuperLU", "intmalloc fails for xa[]");
        if (!(b2 = doublecomplexMalloc(n)))
            throw Exceptions::Generic("SuperLU", "intmalloc fails for b2[]");

#ifdef TIME
        Timer t_data("Creating SuperLU-compatible data");
        t_data.start();
#endif  // TIME

        A_.get_superlu_data(a, asub, xa);

        for (int i(0); i < nnz; ++i)
        {
            a2[i].r = a[i].real();
            a2[i].i = a[i].imag();
        }
        for (int i(0); i < n; ++i)
        {
            b2[i].r = b_[i].real();
            b2[i].i = b_[i].imag();
        }

        zCreate_CompCol_Matrix(
                &A, n, m, nnz, a2, asub, xa, SLU_NR,
                SLU_Z, SLU_GE);
        zCreate_Dense_Matrix(
                &B, n, nrhs, b2, n, SLU_DN, SLU_Z, SLU_GE);

#ifdef TIME
        t_data.stop();
        t_data.print();
#endif  // TIME

        if (!(perm_r = intMalloc(n)))
            throw Exceptions::Generic("SuperLU",
                    "intMalloc fails for perm_r[]");
        if (!(perm_c = intMalloc(m)))
            throw Exceptions::Generic("SuperLU",
                    "intMalloc fails for perm_c[]");

        /* Set the default input options. */
        set_default_options(&options);

        /* Initialize the statistics variables. */
        StatInit(&stat);

#ifdef TIME
        Timer t_solve("Calling SuperLU's zgssv");
        t_solve.start();
#endif  // TIME

        /* Solve the linear system. */
        zgssv(&options, &A, perm_c, perm_r, &L, &U, &B, &stat, &info);

#ifdef TIME
        t_solve.stop();
        t_solve.print();
#endif  // TIME

        doublecomplex *sol = (doublecomplex*)((DNformat*)B.Store)->nzval;
        for (int i(0); i < n; ++i)
        {
            b_[i].real(sol[i].r);
            b_[i].imag(sol[i].i);
        }

#ifdef DEBUG
        /*
        SCformat *Lstore;
        NCformat *Ustore;
        mem_usage_t mem_usage;
        Lstore = (SCformat*) L.Store;
        Ustore = (NCformat*) U.Store;
        printf("[DEBUG] No of nonzeros in factor L = %d\n", Lstore->nnz);
        printf("[DEBUG] No of nonzeros in factor U = %d\n", Ustore->nnz);
        printf("[DEBUG] No of nonzeros in L+U = %d\n",
                Lstore->nnz + Ustore->nnz - n);
        printf("[DEBUG] FILL ratio = %.1f\n",
                (float)(Lstore->nnz + Ustore->nnz - n)/nnz);
        zQuerySpace(&L, &U, &mem_usage);
        printf("[DEBUG] L\\U MB %.3f\ttotal MB needed %.3f\n",
                mem_usage.for_lu/1e6, mem_usage.total_needed/1e6);
        */
#endif

        /* De-allocate storage */
        SUPERLU_FREE (perm_r);
        SUPERLU_FREE (perm_c);
        Destroy_CompCol_Matrix(&A);
        Destroy_SuperMatrix_Store(&B);
        Destroy_SuperNode_Matrix(&L);
        Destroy_CompCol_Matrix(&U);
        StatFree(&stat);

        if (info != 0)
        {
            std::stringstream problem;
            problem << "SUPERLU returned info = " << info;
            throw Exceptions::Generic("Sparse_Linear_System::solve_superlu()",
                    problem.str());
        }
#endif  // SUPERLU
    }

    template <>
    void Sparse_Linear_System<double>::solve_superlu()
    {
#ifndef SUPERLU
        throw Exceptions::Generic("Sparse_Linear_System::solve_superlu()",
                "SuperLU support must be enabled by compiling with -DSUPERLU");
#else

        SuperMatrix A, L, U, B;
        Vector<double> a;
        int *asub, *xa;

        int *perm_r, *perm_c;
        int nrhs, info, m, n, nnz;
        superlu_options_t options;
        SuperLUStat_t stat;

        // Initialise a
        n = A_.num_rows();
        m = A_.num_columns();
        nnz = A_.num_elements();
        nrhs = 1;

        a.assign(nnz, 0.);
        if (!(asub = intMalloc(nnz)))
            throw Exceptions::Generic("SuperLU", "intMalloc fails for asub[]");
        if (!(xa = intMalloc(n + 1)))
            throw Exceptions::Generic("SuperLU", "intmalloc fails for xa[]");

#ifdef TIME
        Timer t_data("Creating SuperLU-compatible data");
        t_data.start();
#endif  // TIME

        A_.get_superlu_data(a, asub, xa);

        dCreate_CompCol_Matrix(
                &A, n, m, nnz, a.data_pointer(), asub, xa, SLU_NR, SLU_D,
                SLU_GE);
        dCreate_Dense_Matrix(
                &B, n, nrhs, b_.data_pointer(), n, SLU_DN, SLU_D, SLU_GE);

#ifdef TIME
        t_data.stop();
        t_data.print();
#endif  // TIME

        if (!(perm_r = intMalloc(n)))
            throw Exceptions::Generic("SuperLU",
                    "intMalloc fails for perm_r[]");
        if (!(perm_c = intMalloc(m)))
            throw Exceptions::Generic("SuperLU",
                    "intMalloc fails for perm_c[]");

        /* Set the default input options. */
        set_default_options(&options);

        /* Initialize the statistics variables. */
        StatInit(&stat);

#ifdef TIME
        Timer t_solve("Calling SuperLU's dgssv");
        t_solve.start();
#endif  // TIME

        /* Solve the linear system. */
        dgssv(&options, &A, perm_c, perm_r, &L, &U, &B, &stat, &info);

#ifdef TIME
        t_solve.stop();
        t_solve.print();
#endif  // TIME

        double *sol = (double*)((DNformat*)B.Store)->nzval;
        for (int i(0); i < n; ++i)
            b_[i] = sol[i];

#ifdef DEBUG
        /*
        SCformat *Lstore;
        NCformat *Ustore;
        mem_usage_t mem_usage;
        Lstore = (SCformat*) L.Store;
        Ustore = (NCformat*) U.Store;
        printf("[DEBUG] No of nonzeros in factor L = %d\n", Lstore->nnz);
        printf("[DEBUG] No of nonzeros in factor U = %d\n", Ustore->nnz);
        printf("[DEBUG] No of nonzeros in L+U = %d\n",
                Lstore->nnz + Ustore->nnz - n);
        printf("[DEBUG] FILL ratio = %.1f\n",
                (float)(Lstore->nnz + Ustore->nnz - n)/nnz);
        dQuerySpace(&L, &U, &mem_usage);
        printf("[DEBUG] L\\U MB %.3f\ttotal MB needed %.3f\n",
                mem_usage.for_lu/1e6, mem_usage.total_needed/1e6);
        */
#endif

        /* De-allocate storage */
        SUPERLU_FREE (asub);
        SUPERLU_FREE (xa);
        SUPERLU_FREE (perm_r);
        SUPERLU_FREE (perm_c);
        Destroy_SuperMatrix_Store(&A);
        Destroy_SuperMatrix_Store(&B);
        Destroy_SuperNode_Matrix(&L);
        Destroy_CompCol_Matrix(&U);
        StatFree(&stat);

        if (info != 0)
        {
            std::stringstream problem;
            problem << "SUPERLU returned info = " << info;
            throw Exceptions::Generic("Sparse_Linear_System::solve_superlu()",
                    problem.str());
        }
#endif  // SUPERLU
    }

    template <typename type_>
    void Sparse_Linear_System<type_>::solve_native()
    {
        std::size_t n = A_.num_rows();
        std::size_t i_max;
        std::vector<type_> pivots(n, 0.);

        for (std::size_t k = 0; k < n; i_max = ++k)
        {
            i_max = A_.column_max_index(k);

#ifdef PARANOID
            if (std::abs(A_.get(i_max, k)) == 0)
                throw Exceptions::Generic(
                        "Sparse_Linear_System::solve_native()",
                        "Matrix given to linear system solver is singular.");
#endif  // PARANOID

            if (i_max != k)
            {
                A_.swap_rows(k, i_max);
                b_.swap(k, i_max);
            }
            for (std::size_t i = 1; k + i < n; ++i)
            {
                pivots[i - 1] = A_.get(k + i, k)/A_.get(k, k);
                b_.operator[](k + i) -= b_[k]*pivots[i - 1];
            }
            for (std::size_t j = k + 1; j < n; ++j)
                for (std::size_t i = 1; k + i < n; ++i)
                    A_.set(k + i, j) -= A_.get(k, j)*pivots[i - 1];
        }
    }

    template <typename type_>
    void Sparse_Linear_System<type_>::back_substitute()
    {
        std::size_t n = A_.num_rows();
        type_* current = NULL;
        for (std::size_t i = n - 1; i < n; --i)
        {
            current = &(b_[i]);
            for (std::size_t j = n - 1; j > i; --j)
                 *current -= A_.get(i, j)*b_[j];
            *current /= A_.get(i, i);
        }
    }

    template class Sparse_Linear_System<double>;
    template class Sparse_Linear_System<std::complex<double> >;
}   // namespace

