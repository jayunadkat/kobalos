#include <complex>
#include <sstream>

#include <Banded_Matrix.h>
#include <Console_Output.h>
#include <Exceptions.h>

namespace Kobalos
{
    template <typename type_> 
    Banded_Matrix<type_>::Banded_Matrix() :
        matrix_(),
        num_rows_(0),
        num_cols_(0),
        num_bands_(0)
    {
    }

    template <typename type_> 
    Banded_Matrix<type_>::Banded_Matrix(const std::size_t& n,
            const std::size_t& bands, const type_& value) :
        matrix_(n*(3*bands + 1), value),
        num_rows_(3*bands + 1),
        num_cols_(n),
        num_bands_(bands)
    {
#ifdef PARANOID
        if (2*bands + 1 > n)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "2*bands + 1 <= n required");
        }
#endif  // PARANOID
    }

    template <typename type_> 
    Banded_Matrix<type_>::Banded_Matrix(const Banded_Matrix<type_>& source) :
        matrix_(),
        num_rows_(0),
        num_cols_(0)
    {
        matrix_ = source.matrix_;
        num_rows_ = source.num_rows_;
        num_cols_ = source.num_cols_;
        num_bands_ = source.num_bands_;
    }

    template <typename type_> 
    Banded_Matrix<type_>::Banded_Matrix(type_* const source_pointer,
            const std::size_t& n, const std::size_t& bands) :
        matrix_(),
        num_rows_(0),
        num_cols_(0)
    {
#ifdef PARANOID
        if (n == 0 || bands == 0)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "n and bands must be non-zero");
        }
        if (2*bands + 1 > n)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "2*bands + 1 <= n required");
        }
        if (source_pointer == NULL)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "source_pointer is NULL");
        }
#endif  // PARANOID

        num_rows_ = 3*bands + 1;
        num_cols_ = n;
        num_bands_ = bands;
        matrix_.assign(source_pointer, n*num_rows_);
    }

    template <typename type_> 
    Banded_Matrix<type_>::~Banded_Matrix()
    {
    }

    template <typename type_> 
    const Banded_Matrix<type_>& Banded_Matrix<type_>::operator=(
            const Banded_Matrix<type_>& source)
    {
        if (this != &source)
        {
            num_rows_ = source.num_rows_;
            num_cols_ = source.num_cols_;
            num_bands_ = source.num_bands_;
            matrix_ = source.matrix_;
        }
        return *this;
    }

    template <typename type_> 
    const Vector<type_> Banded_Matrix<type_>::operator*(
            const Vector<type_>& x) const
    {
#ifdef PARANOID
        if (num_cols_ != x.size())
        {
            throw Exceptions::Mismatch("Banded_Matrix::operator*",
                    "Banded_Matrix", "Vector");
        }
#endif  // PARANOID

        std::size_t numrows = num_cols_;
        Vector<type_> temp(numrows);
        for (std::size_t i = 0; i < numrows; ++i)
            for (std::size_t j = 0; j < num_cols_; ++j)
                if (j + num_bands_ >= i && 
                            j <= i + 2*num_bands_)
                    temp[i] += operator()(i, j)*x[j];
        return temp;
    }

    template <typename type_> 
    void Banded_Matrix<type_>::operator*=(const type_& k)
    {
        matrix_ *= k;
    }

    template <typename type_> 
    void Banded_Matrix<type_>::assign(type_* const source_pointer,
            const std::size_t& n, const std::size_t& bands)
    {
#ifdef PARANOID
        if (n == 0 || bands == 0)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "n and bands must be non-zero");
        }
        if (2*bands + 1 > n)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "2*bands + 1 <= n required");
        }
        if (source_pointer == NULL)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "source_pointer is NULL");
        }
#endif  // PARANOID

        num_rows_ = 3*bands + 1;
        num_cols_ = n;
        num_bands_ = bands;
        matrix_.assign(source_pointer, n*num_rows_);
    }

    template <typename type_> 
    void Banded_Matrix<type_>::assign(const std::size_t& n,
            const std::size_t& bands, const type_& value)
    {
#ifdef PARANOID
        if (2*bands + 1 > n)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "2*bands + 1 <= n required");
        }
#endif  // PARANOID

        num_rows_ = 3*bands + 1;
        num_cols_ = n;
        num_bands_ = bands;
        matrix_.assign(n*num_rows_, value);
    }

    template <typename type_> 
    void Banded_Matrix<type_>::resize(const std::size_t& n,
            const std::size_t& bands)
    {
#ifdef PARANOID
        if (2*bands + 1 > n)
        {
            throw Exceptions::Invalid_Argument("Banded_Matrix::Banded_Matrix",
                    "2*bands + 1 <= n required");
        }
#endif  // PARANOID

        num_rows_ = 3*bands + 1;
        num_cols_ = n;
        num_bands_ = bands;
        matrix_.resize(n*num_rows_);
    }

    template <typename type_> 
    void Banded_Matrix<type_>::fill(const type_& value)
    {
        matrix_.fill(value);
    }

    template <typename type_> 
    void Banded_Matrix<type_>::clear()
    {
        matrix_.clear();
        num_rows_ = num_cols_ = num_bands_ = 0;
    }

    template <typename type_> 
    void Banded_Matrix<type_>::transpose()
    {
        for (std::size_t i = 1; i <= num_bands_; ++i)
            for (std::size_t j = 0; j < num_cols_; ++j)
                std::swap(matrix_[j*num_rows_ + 2*num_bands_ - i],
                        matrix_[j*num_rows_ + 2*num_bands_ + i]);
    }

    template <typename type_> 
    const std::size_t& Banded_Matrix<type_>::num_rows() const
    {
        return num_cols_;
    }

    template <typename type_> 
    const std::size_t& Banded_Matrix<type_>::num_columns() const
    {
        return num_cols_;
    }

    template <typename type_> 
    const std::size_t Banded_Matrix<type_>::num_elements() const
    {
        return num_rows_*num_cols_;
    }

    template <typename type_> 
    const std::size_t& Banded_Matrix<type_>::num_bands() const
    {
        return num_bands_;
    }

    template <typename type_> 
    const double Banded_Matrix<type_>::one_norm() const
    {
        return matrix_.one_norm();
    }

    template <typename type_> 
    const double Banded_Matrix<type_>::two_norm() const
    {
        return matrix_.two_norm();
    }

    template <typename type_> 
    const double Banded_Matrix<type_>::inf_norm() const
    {
        return matrix_.inf_norm();
    }

    template <typename type_>
    type_* Banded_Matrix<type_>::data_pointer()
    {
#ifdef PARANOID
        if (matrix_.size() == 0)
            throw Exceptions::Generic("Illegal operation",
                    "Attempt to pass pointer to empty vector");
#endif  // PARANOID

        return matrix_.data_pointer();
    }

    template <typename type_> 
    void Banded_Matrix<type_>::dump() const
    {
        for (std::size_t i = 0; i < num_rows_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
                console_out(str_convert(matrix_[j*num_rows_ + i]) + " ");
            console_out.endl();
        }
    }

    template <typename type_> 
    void Banded_Matrix<type_>::dump(const std::size_t& decimal_places) const
    {
        std::stringstream out;
        std::size_t width = decimal_places + 8;
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision((int)width - 8);
        for (std::size_t i = 0; i < num_rows_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
            {
                out << std::setw(width);
                out << matrix_[j*num_rows_ + i] << " ";
            }
            out << '\n';
        }
        console_out(out.str());
    }

    template <typename type_> 
    void Banded_Matrix<type_>::dump_dense() const
    {
        for (std::size_t i = 0; i < num_cols_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
            {
                if (!(j + num_bands_ >= i && j <= i + 2*num_bands_))
                    console_out("0, ");
                else if (i == j)
                    console_out("*" + str_convert(operator()(i, j)) + ", ");
                else
                    console_out(str_convert(operator()(i, j)) + ", ");
            }
            console_out.endl();
        }
    }

    template <typename type_> 
    void Banded_Matrix<type_>::dump_dense(
            const std::size_t& decimal_places) const
    {
        std::stringstream out;
        std::size_t width = decimal_places + 8;
        if (decimal_places == 0)
            out.unsetf(std::ios_base::scientific);
        else
            out << std::scientific << std::setprecision((int)width - 8);
        for (std::size_t i = 0; i < num_cols_; ++i)
        {
            for (std::size_t j = 0; j < num_cols_; ++j)
            {
                out << std::setw(width);
                if (!(j + num_bands_ >= i && j <= i + 2*num_bands_))
                    out << "0 ";
                else
                    out << operator()(i, j) << " ";
            }
            out << '\n';
        }
        console_out(out.str());
    }

    template class Banded_Matrix<double>;
    template class Banded_Matrix<std::complex<double> >;
}   // namespace

