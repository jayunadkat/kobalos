#include <cstring>

#include <Exceptions.h>
#include <File_Output.h>

namespace Kobalos
{
    File_Output::File_Compare::File_Compare()
    {
    }

    File_Output::File_Compare::~File_Compare()
    {
    }

    const bool File_Output::File_Compare::operator()(const std::string& lhs,
            const std::string& rhs) const
    {
        return lhs < rhs;
    }

    File_Output::File_Output(const char* path)
    {
        std::sprintf(path_, path);
        if (path_[std::strlen(path_) - 1] != '/')
            std::strcat(path_, "/");
    }

    File_Output::~File_Output()
    {
        close_all();
    }

    void File_Output::open(const std::string& tag, const char* filename)
    {
        std::pair<map_data::const_iterator, bool> ret =
            files_.insert(file_data(tag, new std::ofstream()));
        if (ret.second)
        {
            char full_path[150];
            std::sprintf(full_path, path_);
            ((*(ret.first)).second)->open(std::strcat(full_path, filename));
        }
        else
            throw Exceptions::Generic("File output", 
                    "File \"" + std::string(filename) + "\" already exists");
    }

    std::ofstream& File_Output::write(const std::string& tag)
    {
        map_data::const_iterator cit = files_.find(tag);
        if (cit != files_.end())
            return *((*cit).second);
        else
            throw Exceptions::Generic("File output",
                    "Tag \"" + tag + "\" not found");
    }

    std::ofstream& File_Output::operator()(const std::string& tag)
    {
        return write(tag);
    }

    void File_Output::close(const std::string& tag)
    {
        map_data::iterator it = files_.find(tag);
        if (it != files_.end())
        {
            ((*it).second)->flush();
            ((*it).second)->close();
            delete (*it).second;
            files_.erase(it);
        }
    }

    void File_Output::close_all()
    {
        for (map_data::iterator it(files_.begin()), end(files_.end());
                it != end; ++it)
        {
            ((*it).second)->flush();
            ((*it).second)->close();
            delete (*it).second;
            files_.erase(it);
        }
    }
}   // namespace

