#include <complex>

#include <Equation_1D.h>

namespace Kobalos
{
    template <typename type_, typename xtype_>
    void Equation_1D<type_, xtype_>::update_jacobian_t()
    {
        Vector<type_> temp(this->state_);
        Dense_Matrix<type_> matrix_t_old(matrix_t_);
        
        jacobian_t_.clear();
        for (std::size_t i(0); i < this->num_vars_; ++i)
        {
            temp[i] += this->delta_;
            matrix_t(temp);
            temp[i] -= this->delta_;
            jacobian_t_.push_back((matrix_t_ - matrix_t_old)/this->delta_);
        }
        matrix_t_ = matrix_t_old;
    }

    template <typename type_, typename xtype_>
    void Equation_1D<type_, xtype_>::update_jacobian_x()
    {
        Vector<type_> temp(this->state_);
        Dense_Matrix<type_> matrix_x_old(matrix_x_);
        
        jacobian_x_.clear();
        for (std::size_t i(0); i < this->num_vars_; ++i)
        {
            temp[i] += this->delta_;
            matrix_x(temp);
            temp[i] -= this->delta_;
            jacobian_x_.push_back((matrix_x_ - matrix_x_old)/this->delta_);
        }
        matrix_x_ = matrix_x_old;
    }

    template <typename type_, typename xtype_>
    Equation_1D<type_, xtype_>::Equation_1D(const std::size_t& order) :
        Residual<type_, xtype_>(order),
        matrix_t_(order, 0.),
        matrix_x_(order, 0.),
        jacobian_t_(order, Dense_Matrix<type_>(order, 0.)),
        jacobian_x_(order, Dense_Matrix<type_>(order, 0.)),
        jacobian_t_vec_(order, 0.),
        jacobian_x_vec_(order, 0.)
    {
        for (std::size_t i(0); i < this->order_; ++i)
            matrix_x_(i, i) = 1.;
    }

    template <typename type_, typename xtype_>
    Equation_1D<type_, xtype_>::Equation_1D(const std::size_t& order,
            const std::size_t& num_vars) :
        Residual<type_, xtype_>(order, num_vars),
        matrix_t_(order, 0.),
        matrix_x_(order, 0.),
        jacobian_t_(num_vars, Dense_Matrix<type_>(order, 0.)),
        jacobian_x_(num_vars, Dense_Matrix<type_>(order, 0.)),
        jacobian_t_vec_(order, 0.),
        jacobian_x_vec_(order, 0.)
    {
        for (std::size_t i(0); i < order; ++i)
            matrix_x_(i, i) = 1.;
    }

    template <typename type_, typename xtype_>
    Equation_1D<type_, xtype_>::~Equation_1D()
    {
    }

    template class Equation_1D<double>;
    template class Equation_1D<std::complex<double> >;
    template class Equation_1D<std::complex<double>, std::complex<double> >;
}   // namespace
