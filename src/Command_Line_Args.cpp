#include <algorithm>

#include <Command_Line_Args.h>
#include <Console_Output.h>

namespace Kobalos
{
    Command_Line_Args::Command_Line_Args(int argc, char** argv) :
        flags_(),
        options_(),
        option_values_(),
        flags_recognised_(),
        options_recognised_()
    {
        std::vector<std::string> arguments(argv + 1, argv + argc);

        for (int i(0), num_args(argc - 1); i < num_args; ++i)
        {
            std::size_t length(arguments[i].size());
            if (length > 1)
            {
                if (arguments[i][0] == '-')
                {
                    if (arguments[i][1] == '-')
                    {
                        // Found option
                        if (i != num_args - 1)
                        {
                            if (arguments[i + 1].substr(0, 2) == "--")
                            {
                                options_.push_back(arguments[i]);
                                option_values_.push_back("");
                                options_recognised_.push_back(false);
                            }
                            else
                            {
                                options_.push_back(arguments[i]);
                                option_values_.push_back(arguments[i + 1]);
                                options_recognised_.push_back(false);
                                ++i;
                            }
                        }
                        else
                        {
                            options_.push_back(arguments[i]);
                            option_values_.push_back("");
                            options_recognised_.push_back(false);
                        }
                    }
                    else
                    {
                        // Found flag collection
                        for (std::size_t j(1); j < length; ++j)
                            if (std::find(flags_.begin(), flags_.end(),
                                        arguments[i].substr(j, 1)) ==
                                    flags_.end())
                            {
                                flags_.push_back("-" +
                                        arguments[i].substr(j, 1));
                                flags_recognised_.push_back(false);
                            }
                    }
                }
            }
        }
    }

    Command_Line_Args::~Command_Line_Args()
    {
    }

    void Command_Line_Args::flag(const std::string& flag, bool& storage)
    {
        std::vector<std::string>::iterator it(
                std::find(flags_.begin(), flags_.end(), flag));
        if (it != flags_.end())
        {
            storage = true;
            flags_recognised_[it - flags_.begin()] = true;
        }
    }

    void Command_Line_Args::option(const std::string& option, bool& storage)
    {
        std::vector<std::string>::iterator it(
                std::find(options_.begin(), options_.end(), option));
        if (it != options_.end())
        {
            storage = true;
            options_recognised_[it - options_.begin()] = true;
        }
    }

    void Command_Line_Args::option(const std::string& option, char* storage)
    {
        std::vector<std::string>::iterator it(
                std::find(options_.begin(), options_.end(), option));
        if (it != options_.end())
        {
            std::sprintf(
                    storage, option_values_[it - options_.begin()].c_str());
            options_recognised_[it - options_.begin()] = true;
        }
    }

    void Command_Line_Args::option(const std::string& option, double& storage)
    {
        std::vector<std::string>::iterator it(
                std::find(options_.begin(), options_.end(), option));
        if (it != options_.end())
        {
            storage = std::atof(
                    option_values_[it - options_.begin()].c_str());
            options_recognised_[it - options_.begin()] = true;
        }
    }

    void Command_Line_Args::option(const std::string& option, int& storage)
    {
        std::vector<std::string>::iterator it(
                std::find(options_.begin(), options_.end(), option));
        if (it != options_.end())
        {
            storage = std::atoi(
                    option_values_[it - options_.begin()].c_str());
            options_recognised_[it - options_.begin()] = true;
        }
    }

    void Command_Line_Args::option(const std::string& option, long& storage)
    {
        std::vector<std::string>::iterator it(
                std::find(options_.begin(), options_.end(), option));
        if (it != options_.end())
        {
            storage = std::atoi(
                    option_values_[it - options_.begin()].c_str());
            options_recognised_[it - options_.begin()] = true;
        }
    }

    void Command_Line_Args::option(const std::string& option,
            std::size_t& storage)
    {
        std::vector<std::string>::iterator it(
                std::find(options_.begin(), options_.end(), option));
        if (it != options_.end())
        {
            storage = std::atoi(
                    option_values_[it - options_.begin()].c_str());
            options_recognised_[it - options_.begin()] = true;
        }
    }

    void Command_Line_Args::option(const std::string& option,
            std::string& storage)
    {
        std::vector<std::string>::iterator it(
                std::find(options_.begin(), options_.end(), option));
        if (it != options_.end())
        {
            storage = option_values_[it - options_.begin()];
            options_recognised_[it - options_.begin()] = true;
        }
    }

    void Command_Line_Args::print_recognised_arguments() const
    {
        std::string flags_text(""), options_text("");
        for (std::size_t i(0), size(flags_.size()); i < size; ++i)
            if (flags_recognised_[i])
                flags_text += flags_[i] + "\n";
        for (std::size_t i(0), size(options_.size()); i < size; ++i)
            if (options_recognised_[i])
                options_text += options_[i] + "(" + option_values_[i] + ")\n";

        if (flags_text.size() > 0)
        {
            console_out.stage_header_line("Recognised flags:\n");
            console_out.stage_header_line(flags_text + "\n");
        }
        if (options_text.size() > 0)
        {
            console_out.stage_header_line("Recognised options(value):\n");
            console_out.stage_header_line(options_text + "\n");
        }
    }
}   // namespace

