#! /bin/bash

if [ $# == 0 ]
then
    scons -j4
else
    if [ $1 == "kobalos" ]
    then
        shift
        scons -j4 library=1 $@
    elif [ $1 == "clean" ]
    then
        scons -j4 library=1 -c
    else
        scons -j4 target=$@
    fi
fi
